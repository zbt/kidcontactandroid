package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.JoinClassInput;

/**
 * Created by hzheng3 on 14-6-9.
 */
public final class RequestJoinClass extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestJoinClass(JoinClassInput input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_JOIN_CLASS),
                input, Succeed.class, listener, errorListener);
    }
}
