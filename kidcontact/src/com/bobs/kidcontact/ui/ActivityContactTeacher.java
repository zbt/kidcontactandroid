package com.bobs.kidcontact.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.ui.FragmentTabsChat;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.model.Teacher;
import com.bobs.kidcontact.model.TeacherList;
import com.bobs.kidcontact.model.User;
import com.bobs.kidcontact.requests.RequestGetTeacher;
import com.bobs.kidcontact.utils.RequestUtil;
import java.util.ArrayList;

/**
 * Created by junyongl on 14-1-23.
 */
public class ActivityContactTeacher extends ActionBarActivityBase {
    private Context mContext;
    private FragmentTabsChat mFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_contact_teacher);
        final FragmentManager fragmentManager = getSupportFragmentManager();
        RequestUtil<TeacherList> requestUtil = new RequestUtil<TeacherList>() {
            @Override
            public Request<TeacherList> onPrepareRequest(Response.Listener<TeacherList> listener, Response.ErrorListener errListener) {
                return new RequestGetTeacher(listener, errListener);
            }

            @Override
            public void onSuccess(TeacherList response) {
                Toast.makeText(mContext, R.string.teacher_get_list, Toast.LENGTH_SHORT).show();

                ArrayList<User> toUsers = new ArrayList<User>();
                for (Teacher teacher : response.mTeachers) {
                    toUsers.add(teacher);
                }
                FragmentTabsChat fragment = FragmentTabsChat.newInstance(toUsers);
                fragmentManager.beginTransaction().replace(R.id.frame_message_details, fragment).commit();
                mFragment = fragment;
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        requestUtil.add2queue();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    protected void refreshViews(Conversation conversation) {
        if (null == conversation) {
            return;
        }
        int count = conversation.getNewMsgCount();
        count = count < 0 ? 0 : count;
        if (!conversation.getGroupChat()) {
            // Notify fragment tabs chat
            Message msg = new Message();
            msg.what = Events.EVENT_CONVERSATION_CHANGED;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CONVERSATION_CHANGED, conversation);
            msg.setData(data);
            if (null != mFragment) {
                mFragment.getEventHandlerForView().sendMessage(msg);
            }
        }
    }
}
