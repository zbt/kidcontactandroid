package com.bobs.kidcontact.chat.engine;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.baidu.android.pushservice.PushManager;
import com.bobs.kidcontact.chat.service.BaiduInfoHolder;
import com.bobs.kidcontact.model.MessageProxy;
import com.bobs.kidcontact.model.SingleCloudMsg;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.CookieGsonRequest.Succeed;
import com.bobs.kidcontact.requests.RequestSendClassCloudMsg;
import com.bobs.kidcontact.requests.RequestSendSingleCloudMsg;
import com.bobs.kidcontact.utils.RequestUtil;

/**
 * Created by hzheng3 on 14-2-18.
 */
public class BaiduPushEngine implements IPushEngine {
    private Context mContext;
    private String mApiKey;
    private String mSecretKey;
    private static final String TAG = "BaiduPushEngine";

    public BaiduPushEngine(Context context, String apiKey, String secretKey) {
        mContext = context;
        mApiKey = apiKey;
        mSecretKey = secretKey;
    }

    @Override
    public boolean sendMsgToUser(long userId, int deviceType, String msg) {
        String cloudId = BaiduInfoHolder.getInstance().userIdToCloudId(userId);
        if (cloudId == null || cloudId.isEmpty()) {
            return false;
        }
        final SingleCloudMsg message = new SingleCloudMsg();
        message.mId = userId;
        message.mCloudId = cloudId;
        message.mDeviceType = deviceType;
        message.mInfo = msg;

        RequestUtil<CookieGsonRequest.Succeed> requestUtil = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                return new RequestSendSingleCloudMsg(message, listener, errListener);
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {
                // nothing for now
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        requestUtil.add2queue();
        return true;
    }

    @Override
    public boolean sendMsgToGroup(String groupId, String msg) {
        final String msgFinal = msg;
        RequestUtil<CookieGsonRequest.Succeed> request = new RequestUtil<CookieGsonRequest.Succeed>() {

            @Override
            public Request<Succeed> onPrepareRequest(
                    Listener<Succeed> listener, ErrorListener errListener) {
                MessageProxy msgProxy = new MessageProxy();
                msgProxy.mMessage = msgFinal;
                return new RequestSendClassCloudMsg(msgProxy, listener, errListener);
            }

            @Override
            public void onSuccess(Succeed response) {
                Log.d(TAG, "send group message successfully.");
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        request.add2queue();
        return true;
    }

    @Override
    public void joinGroups(List<String> groups) {
        PushManager.setTags(mContext, groups);
    }

    @Override
    public void leaveGroups(List<String> groups) {
        PushManager.delTags(mContext, groups);
    }
}
