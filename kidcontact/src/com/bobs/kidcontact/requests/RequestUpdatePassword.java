package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-2.
 */
public class RequestUpdatePassword  extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestUpdatePassword(UpdatePassword input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_UPDATE_PASSWD),
                input, Succeed.class, listener, errorListener);
    }

    public static class UpdatePassword {
        @SerializedName("password")
        public String mPasswd;
    }
}