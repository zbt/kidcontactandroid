package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-2-12.
 */
public class AssignmentReply extends Reply {
    @SerializedName("assignmentReplyFiles")
    public ArrayList<FileProxy> mFiles;
}
