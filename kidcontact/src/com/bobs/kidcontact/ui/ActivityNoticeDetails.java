package com.bobs.kidcontact.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.AckNotice;
import com.bobs.kidcontact.model.Assignment;
import com.bobs.kidcontact.model.AssignmentReply;
import com.bobs.kidcontact.model.AssignmentZip;
import com.bobs.kidcontact.model.IdProxy;
import com.bobs.kidcontact.model.Notice;
import com.bobs.kidcontact.model.NoticeBase;
import com.bobs.kidcontact.model.Reply;
import com.bobs.kidcontact.model.Vote;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest;
import com.bobs.kidcontact.requests.RequestAddAckNoticeReply;
import com.bobs.kidcontact.requests.RequestAddAssignmentReply;
import com.bobs.kidcontact.requests.RequestAddInfoId;
import com.bobs.kidcontact.requests.RequestAddVoteReply;
import com.bobs.kidcontact.requests.RequestDeleteAckNotice;
import com.bobs.kidcontact.requests.RequestDeleteAssignment;
import com.bobs.kidcontact.requests.RequestDeletePublicNotice;
import com.bobs.kidcontact.requests.RequestDeleteVote;
import com.bobs.kidcontact.requests.RequestZipAssignment;
import com.bobs.kidcontact.utils.DebugLog;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.Gson;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import de.greenrobot.event.EventBus;

/**
 * Created by junyongl on 14-1-13.
 */
public class ActivityNoticeDetails extends ActionBarActivityBase {
    private static final String TAG = "ActivityNoticeDetails";
    private static final int REQUEST_FILE_CHOOSER = 11;
    private Notice mNotice;
    private TextView mTitle;
    private TextView mAuthor;
    private TextView mCreatedTime;
    private TextView mContent;
    private NetworkRoundImageView mAvatar;
    private Context mContext;
    private Button mBtAction;
    private Button mBtShare;
    private RequestUtil<?> mReq;
    private LinearLayout mStatisticContainer;
    private boolean isAckNotice = false;
    private boolean isPublicNotice = false;
    private boolean isVote = false;
    private boolean isQuestionary = false;
    private boolean isAssignment = false;
    private RadioGroup mVoteRadio;
    private LayoutInflater mInflater;
    private NoticeBase mBaseNotice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtil.setThemeForUser(this);
        setContentView(R.layout.activity_notice_details);
        mContext = this;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        Gson gson = new Gson();
        mNotice = gson.fromJson(bundle.getString("data-notice"), Notice.class);

        mInflater = (LayoutInflater)mContext.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        mTitle = (TextView)findViewById(R.id.tv_notice_title);
        mAuthor = (TextView) findViewById(R.id.notice_author);
        mCreatedTime = (TextView) findViewById(R.id.notice_created_time);
        mContent = (TextView) findViewById(R.id.tv_notice_content);
        mAvatar = (NetworkRoundImageView) findViewById(R.id.author_avatar);
        mBtAction = (Button) findViewById(R.id.bt_action);
        mBtShare = (Button) findViewById(R.id.bt_share);
        mStatisticContainer = (LinearLayout) findViewById(R.id.statistic_container);
        mVoteRadio = (RadioGroup) findViewById(R.id.radio_vote_options);

        if (mNotice.mAckNotice != null) {
            isAckNotice = true;
            fillData(mNotice.mAckNotice);
            if (KidContactApp.getInstance().getRole() == Constants.TYPE_PARENT) {
                if (isAcknowledged(mNotice.mAckNotice)){
                    mBtAction.setEnabled(false);
                    mBtAction.setText(R.string.send_notice_ack);
                }
                mBtAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ackNotice();
                    }
                });
            } else {
                mBtAction.setVisibility(View.GONE);

                if (mNotice.mAckNotice.mReplies != null) {
                    View statistic = mInflater.inflate(R.layout.view_statistic, null);
                    TextView number = (TextView) statistic.findViewById(R.id.statistic_number);
                    number.setText(String.valueOf(mNotice.mAckNotice.mReplies.size()));
                    mStatisticContainer.addView(statistic);
                }
            }

        } else if (mNotice.mPublicNotice != null) {
            isPublicNotice = true;
            fillData(mNotice.mPublicNotice);
            mBtAction.setVisibility(View.GONE);
        } else if (mNotice.mVote != null) {
            isVote = true;

            fillData(mNotice.mVote);
        } else if (mNotice.mQuestionary != null) {
            isQuestionary = true;
            fillData(mNotice.mQuestionary);
        } else if (mNotice.mAssignment != null) {
            isAssignment = true;
            fillData(mNotice.mAssignment);

        }

    }

    private boolean isAcknowledged(AckNotice mAckNotice) {
        if (mAckNotice.mReplies != null) {
            for (Reply reply : mAckNotice.mReplies) {
                if (reply.mCreatorId == KidContactApp.getInstance().getId()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isVoted(Vote vote) {
        if (vote.mReplies != null) {
            for (Reply reply : vote.mReplies) {
                if (reply.mCreatorId == KidContactApp.getInstance().getId()) {
                    ((RadioButton)mVoteRadio.getChildAt(
                            Integer.valueOf(reply.mInfo))).setChecked(true);
                    return true;
                }
            }
        }
        return false;
    }

    private void ackNotice() {
        mReq = new RequestUtil<Reply>() {
            @Override
            public Request<Reply> onPrepareRequest(Response.Listener<Reply> listener, Response.ErrorListener errListener) {
                RequestAddInfoId.InfoId infoId = new RequestAddInfoId.InfoId();
                infoId.mId = mNotice.getId();
                infoId.mInfo = "reply";
                return new RequestAddAckNoticeReply(infoId, listener, errListener);
            }

            @Override
            public void onSuccess(Reply response) {
                Log.d(TAG, "ack Notice success");
                Toast.makeText(mContext, R.string.okay_send_notice_ack, Toast.LENGTH_SHORT).show();
                mBtAction.setEnabled(false);
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            }
        };
        mReq.add2queue();
    }

    private void voteNotice() {
        mReq = new RequestUtil<Reply>() {
            @Override
            public Request<Reply> onPrepareRequest(Response.Listener<Reply> listener, Response.ErrorListener errListener) {
                RequestAddInfoId.InfoId infoId = new RequestAddInfoId.InfoId();
                infoId.mId = mNotice.getId();
                infoId.mInfo = String.valueOf(mVoteRadio.indexOfChild(
                        mVoteRadio.findViewById(mVoteRadio.getCheckedRadioButtonId())));
                return new RequestAddVoteReply(infoId, listener, errListener);
            }

            @Override
            public void onSuccess(Reply response) {
                Log.d(TAG, "vote Notice success");
                Toast.makeText(mContext, R.string.okay_send_vote_ack, Toast.LENGTH_SHORT).show();
                mBtAction.setEnabled(false);

            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            }
        };
        mReq.add2queue();
    }


    private void fillData(NoticeBase notice) {
        mBaseNotice = notice;
        if (notice.mTitle != null) {
            mTitle.setText(notice.mTitle);
        }

        if (notice.mInfo != null) {
            if (isVote) {
                Vote.VoteContent content = new Gson().fromJson(notice.mInfo, Vote.VoteContent.class);
                mContent.setText(content.mContent);
                if (KidContactApp.getInstance().getRole() == Constants.TYPE_PARENT) {
                    initOptionsVote(content.mOptions);
                    mBtAction.setText(R.string.send_vote_ack);
                    mBtAction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            voteNotice();
                        }
                    });
                    if (isVoted((Vote)notice)) {
                        mBtAction.setEnabled(false);
                        mBtAction.setText(R.string.already_sent_vote_ack);
                    }
                } else {
                    initOptionStatistic(content.mOptions, (Vote) notice);
                    mBtAction.setVisibility(View.GONE);
                }
            } else {
                mContent.setText(notice.mInfo);
            }

            if (isAssignment) {
                final Assignment.AssignmentContent content = new Gson().fromJson(notice.mInfo, Assignment.AssignmentContent.class);
                View statistic = mInflater.inflate(R.layout.layout_due_date, null);
                TextView dueDate = (TextView) statistic.findViewById(R.id.tv_due_date);
                dueDate.setText(content.mDueDate + mContext.getString(R.string.before_date));
                mStatisticContainer.addView(statistic);
                mContent.setText(content.mContent);
                if (content.isAttachmentNeed == true && KidContactApp.getInstance().getRole() == Constants.TYPE_PARENT) {
                    mBtAction.setText(getString(R.string.upload_attachment));
                    mBtAction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(isOverDue(content.mDueDate)) {
                                Toast.makeText(mContext, R.string.assign_out_of_date, Toast.LENGTH_LONG).show();
                            } else {
                                uploadAttachment();
                            }
                        }
                    });
                } else if (content.isAttachmentNeed == true && KidContactApp.getInstance().getRole() == Constants.TYPE_TEACHER) { //Teacher
                    mBtAction.setText(getString(R.string.download_attachment));
                    mBtAction.setEnabled(false);
                    getAttachmentUrl(notice.mId);
                } else {
                    mBtAction.setVisibility(View.GONE);
                }
            }
        }

        if (notice.mCreatorName != null) {
            mAuthor.setText(notice.mCreatorName);
        }

        if (notice.mDateCreated != null) {
            mCreatedTime.setText(UIUtil.dateToString(UIUtil.stringToDate(notice.mDateCreated,
                    Constants.DATA_DATE_FORMAT), mContext.getResources().
                    getString(R.string.moment_created_time_string_fromat)));
        }
        mAvatar.setImageUrl(Constants.getAvatarThumbURLOfCreator(notice.mCreatorId),
                KidContactApp.getInstance().getImageLoader());

    }

    private boolean isOverDue(String dueDate) {
        Date date = UIUtil.stringToDate(dueDate, mContext.getString(R.string.assignment_due_data_format));
        Calendar calendar = Calendar.getInstance();
        if (calendar.getTime().after(date)) {
            return true;
        }
        return false;
    }

    private void getAttachmentUrl(final int mId) {
        final RequestUtil<AssignmentZip> requestUtil =  new RequestUtil<AssignmentZip>() {
            @Override
            public Request<AssignmentZip> onPrepareRequest(Response.Listener<AssignmentZip> listener, Response.ErrorListener errListener) {
                IdProxy idProxy = new IdProxy();
                idProxy.mId = mId;
                return new RequestZipAssignment(idProxy, listener, errListener);
            }

            @Override
            public void onSuccess(AssignmentZip response) {
                final String url = response.mAssignmentPath;
                mBtAction.setEnabled(true);
                mBtAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (url != null) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(url));
                            startActivity(intent);
                        }
                    }
                });
                mBtShare.setVisibility(View.VISIBLE);
                mBtShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                    }
                });
            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);

            }
        };
        requestUtil.add2queue();
    }

    private void uploadAttachment() {
        Intent target = FileUtils.createGetContentIntent();
        Intent intent = Intent.createChooser(
                target, "选择文件");
        try {
            startActivityForResult(intent, REQUEST_FILE_CHOOSER);
        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
        }
//        Toast.makeText(mContext, "This feature is under constructing", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_FILE_CHOOSER) {
            if (data != null) {
                // Get the URI of the selected file
                final Uri uri = data.getData();
                DebugLog.d("Uri = " + uri.toString());
                //Toast.makeText(mContext, "Uri = " + uri.toString(), Toast.LENGTH_SHORT).show();
                final String path = FileUtils.getPath(this, uri);
                if (!isFileFormatAllowed(path)) {
                    DebugLog.d("File type " + FileUtils.getExtension(path) + " is not allowed");
                    Toast.makeText(mContext, R.string.notice_donot_support_format, Toast.LENGTH_SHORT).show();
                    return;
                }
                File file = FileUtils.getFile(mContext, uri);
                addAssignmentReply(file);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addAssignmentReply(final File file) {
        RequestUtil<AssignmentReply> addAssignmentReplyRequest = new RequestUtil<AssignmentReply>() {
            @Override
            public Request<AssignmentReply> onPrepareRequest(Response.Listener<AssignmentReply> listener, Response.ErrorListener errListener) {
                ArrayList<CookieMuiltiPicUploadRequest.PicPair> filesArray = new ArrayList<CookieMuiltiPicUploadRequest.PicPair>();
                CookieMuiltiPicUploadRequest.PicPair picPair = new CookieMuiltiPicUploadRequest.PicPair();
                picPair.mPic = file;
                filesArray.add(picPair);
                return new RequestAddAssignmentReply("", mNotice.getId(), filesArray, listener, errListener);
            }

            @Override
            public void onSuccess(AssignmentReply response) {
                DebugLog.d("Add AssignmentReply succeed " + response.toString());
                Toast.makeText(mContext, R.string.notice_upload_success, Toast.LENGTH_SHORT).show();
            }
        };
        addAssignmentReplyRequest.add2queue();
    }

    private boolean isFileFormatAllowed(String uri) {
        DebugLog.d("Got the file path: " + uri);
        String extension = FileUtils.getExtension(uri);
        final String [] allowedExtensions = { ".jpg", ".png", ".jpeg", ".gif", ".ppt", ".mp3", ".mp4", ".7z", ".rar", ".zip", ".txt"};
        for (String ext : allowedExtensions) {
            if (extension.equals(ext)) {
                return true;
            }
        }
        return false;
     }

    private void initOptionsVote(ArrayList<String> options) {
        for (int i = 0; i < options.size(); i++) {
            RadioButton radioButton = new RadioButton(mContext);
            radioButton.setText(options.get(i));
            mVoteRadio.addView(radioButton);
            if (i == 0) {
                radioButton.setChecked(true);
            }
        }
    }

    private void initOptionStatistic(ArrayList<String> options, Vote notice) {
        ArrayList<Reply> replies = notice.mReplies;
        int [] statisticResult = new int[options.size()];
        if ( replies != null && replies.size() != 0) {
            for ( Reply re : replies) {
                int i = Integer.valueOf(re.mInfo);
                if (i >= 0) {
                    //TODO fix the bug mInfo = "-1"
                    statisticResult[i]++;
                }
            }
        }
        for (int i = 0; i < statisticResult.length; i++) {
            View statistic = mInflater.inflate(R.layout.view_statistic, null);
            TextView number = (TextView) statistic.findViewById(R.id.statistic_number);
            number.setText(String.valueOf(statisticResult[i]));
            TextView content = (TextView) statistic.findViewById(R.id.statistic_content);
            content.setText(options.get(i));
            mStatisticContainer.addView(statistic);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_delete:
                showDialog();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteNotice() {
        RequestUtil<CookieGsonRequest.Succeed> deleteNoticeRequest = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                IdProxy idProxy = new IdProxy();
                if (isAckNotice) {
                    idProxy.mId = Long.valueOf(mNotice.mAckNotice.mId);
                    return new RequestDeleteAckNotice(idProxy, listener, errListener);
                } else if (isPublicNotice) {
                    idProxy.mId = Long.valueOf(mNotice.mPublicNotice.mId);
                    return new RequestDeletePublicNotice(idProxy, listener, errListener);
                } else if (isVote) {
                    idProxy.mId = Long.valueOf(mNotice.mVote.mId);
                    return new RequestDeleteVote(idProxy, listener, errListener);
                } else if (isQuestionary) {
                    idProxy.mId = Long.valueOf(mNotice.mQuestionary.mId);
                    return null;
                } else if (isAssignment) {
                    idProxy.mId = Long.valueOf(mNotice.mAssignment.mId);
                    return new RequestDeleteAssignment(idProxy, listener, errListener);
                } else {
                    return null;
                }
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {
                DebugLog.d("Delete Notice Succeed!");
                onBackPressed();
                EventBus.getDefault().post(new UpdateStatusUIEvent());
            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
                DebugLog.e("Delete Notice Error " + e.toString());
            }
        };
        deleteNoticeRequest.add2queue();

    }

    public void showDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        AlertDialogFragment fragment = new AlertDialogFragment();
        fragment.show(fragmentManager, "alert_dialog");
        }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_notice_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mBaseNotice.mCreatorId == KidContactApp.getInstance().getId()) {
            menu.findItem(R.id.menu_delete).setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public class AlertDialogFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("确认删除这条消息，此操作不可恢复！")
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteNotice();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            onBackPressed();
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}
