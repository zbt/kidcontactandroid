package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.model.IdProxy;

/**
 * Created by hzheng3 on 14-2-2.
 */
public abstract class RequestDeleteEntityOfId extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestDeleteEntityOfId(String url, IdProxy input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(url, input, Succeed.class, listener, errorListener);
    }
}