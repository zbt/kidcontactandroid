package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 2014/5/14.
 */
public class SingleCloudMsg {
    @SerializedName("id")
    public long mId;
    @SerializedName("cloudId")
    public String mCloudId;
    @SerializedName("deviceType")
    public int mDeviceType;
    @SerializedName("info")
    public String mInfo;
}
