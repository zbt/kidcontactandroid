package com.bobs.kidcontact.requests;

import java.util.ArrayList;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Moment;
import com.google.gson.annotations.SerializedName;

/**
 * Created by yhu6 on 14-01-05.
 * request to server to get a page of moments
 */
public final class RequestGetMoments extends CookieGsonRequest<RequestGetMoments.MomentPage> {
    private static final String IDOFFSET = "idOffset";
    public RequestGetMoments(long idOffset, String lastUpdatedTime, Response.Listener<MomentPage> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getMomentsURL(idOffset, lastUpdatedTime),
                RequestGetMoments.MomentPage.class, listener, errorListener);
	}

    public static class MomentPage {
        @SerializedName("moments")
        public ArrayList<Moment> mMomentsInPage;
    }

    public static String getMomentsURL(long idOffset, String lastUpdatedTime) {
        if (lastUpdatedTime != null && !lastUpdatedTime.isEmpty()) {
            return Constants.getFunctionURL(Constants.FUNC_GETMOMENT)
                    + Constants.QUESTION
                    + IDOFFSET + Constants.EQUAL + idOffset
                    + Constants.AND
                    + "lastUpdated" + Constants.EQUAL + lastUpdatedTime;
        } else {
            return Constants.getFunctionURL(Constants.FUNC_GETMOMENT)
                    + Constants.QUESTION
                    + IDOFFSET + Constants.EQUAL + idOffset;
        }
    }
}
