package com.bobs.kidcontact.chat.service;

import java.util.List;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

/**
 * Maintained by zhanglong
 */
public class NotificationConstructor {
    public static final int NOTIFICATION_ID = 1;
    private Context mContext;
    private NotificationInfoHolder mLastNotificationInfo = null;
    private int mNewMsgCount = 0;
    private Bitmap mLargeIcon = null;
    public NotificationConstructor(Context context) {
        mContext = context;
        mLargeIcon = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher);
    }

    /** If an previous notification exists, we'll update it; or we'll create a new one */
    @SuppressLint("NewApi")
    public Notification newNotification(CloudMessage cloudMsg) {
        // Create content for notification
        String contentTitle = "家校联系册";
        // String contentText = ++mNewMsgCount + "条未读信息";
        String contentText = parseNotifyContent(cloudMsg);

        // Update last notification info
        if (null == mLastNotificationInfo) {
            mLastNotificationInfo = new NotificationInfoHolder();
        }
        mLastNotificationInfo.setId(NOTIFICATION_ID);
        mLastNotificationInfo.setContentTitle(contentTitle);
        mLastNotificationInfo.setContentText(contentText);

        // Build notification
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                              .setLargeIcon(mLargeIcon)
                              .setSmallIcon(R.drawable.ic_launcher)
                              .setContentTitle(contentTitle)
                              .setContentText(contentText);
        Intent resultIntent = mContext.getPackageManager().getLaunchIntentForPackage("com.bobs.kidcontact");
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(100);
        int size = tasks.size();
        for (int i = 0; i < size; i++) {
            String packageName = tasks.get(i).topActivity.getPackageName();
            if (packageName.equals("com.bobs.kidcontact")) {
                try {
                    resultIntent = new Intent(mContext, Class.forName(tasks.get(i).topActivity.getClassName()));
                    resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    // It's not that serious that we move on
                }
                break;
            }
        }
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);

        return builder.build();
    }
//
//    public Notification newNotification(long userId) {
//        // TODO temporary
//        return newNotification("");
//    }
//    
//    public Notification newNotification(int ctrlType) {
//        return newNotification("");
//    }
    
    /** Clear some cached info */
    public void clear() {
        mLastNotificationInfo = null;
        mNewMsgCount = 0;
    }
    
    private String parseNotifyContent(CloudMessage cloudMsg) {
        String content = "";
        if (cloudMsg.isChatMessage()) {
            // If it's a common chat message(single or group)
            content = "您有新的消息";
        } else if (cloudMsg.isCtrlMessage()) {
            switch (cloudMsg.toCtrlMsg().getType()) {
            case CtrlMessage.TYPE_NOTICE_NEW:
                content = "您有新的通知";
                break;
            case CtrlMessage.TYPE_NOTICE_UPDATE:
                content = "您的通知有更新";
                break;
            case CtrlMessage.TYPE_LEAVE_NEW:
                content = "您有新的请假请求";
                break;
            case CtrlMessage.TYPE_LEAVE_UPDATE:
                content = "您的请假请求已经被处理";
                break;
            case CtrlMessage.TYPE_REMOTE_LOGIN:
                content = mContext.getString(R.string.remote_login_notice_content);
                break;
            default:
                content = "您有新的更新";
                break;
            }
        }
        return content;
    }

}
