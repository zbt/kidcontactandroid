package com.bobs.kidcontact.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.contants.ErrCode;
import com.bobs.kidcontact.requests.KidContactError;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.ui.KidContactApp.AfterBackGroundLogin;

public abstract class RequestUtil<T> {
    private static final String TAG = "RequestUtil";
    public static final long DEFAULT_TIMEOUT = -1;
    private final Handler mMainHandler;
    private final Response.Listener<T> mRespListener = new Response.Listener<T>() {
        @Override
        public void onResponse(T response) {
            onSuccess(response);
            mMainHandler.removeCallbacksAndMessages(null);
        }
    };
    private final Response.ErrorListener mErrListenner = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            onError(error);
            mMainHandler.removeCallbacksAndMessages(null);
        }
    };
    private long mTimeout;
    private Request<T> mRequest;

    public RequestUtil() {
        this(DEFAULT_TIMEOUT);
    }

    public RequestUtil(long timeout) {
        mMainHandler = new Handler(Looper.getMainLooper());
        mTimeout = timeout;

    }

    public final synchronized Request<T> getRequest() {
        if (mRequest == null) {
            mRequest = onPrepareRequest(mRespListener, mErrListenner);
            if (Constants.DEBUG && (mRequest == null)) {
                throw new RuntimeException("null request!!!");
            }
        }
        return mRequest;
    }

    public final long getTimeout() {
        return mTimeout;
    }

    public final void setTimeout(long timeout) {
        mTimeout = timeout;
    }

    public void add2queue() {
        getReqQueue().add(getRequest());
        if (mTimeout > 0) {
            mMainHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    cancel();
                    mErrListenner.onErrorResponse(new TimeoutError());
                }
            }, mTimeout);
        }
    }

    /**
     * WARNING: MUST call this from main thread!!!
     * @throws Throwable
     */
    public void cancel() {
        if (Constants.DEBUG) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new RuntimeException("cancel() should not be called from outside of main thread");
            }
        }
        getReqQueue().cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return getRequest().equals(request);
            }
        });
        mMainHandler.removeCallbacksAndMessages(null);
    }

    /**
     * Cancel all the requests with the given tag.
     * WARNING: MUST call this from main thread!!!
     * @param tag
     */
    public void cancel(Object tag) {
        if (Constants.DEBUG) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new RuntimeException("cancel() should not be called from outside of main thread");
            }
        }
        getReqQueue().cancelAll(tag);
        mMainHandler.removeCallbacksAndMessages(null);
    }

    public final RequestQueue getReqQueue() {
        return KidContactApp.getInstance().getRequestQueue();
    }

    public static final int interpretErr2ResForLogin(VolleyError e) {
        if (e instanceof TimeoutError) {
            return R.string.error_request_timeout;
        } else if (e instanceof KidContactError) {
            return R.string.LOGIN_FAILED;
        } else if (e instanceof NetworkError) {
            return R.string.error_network;
        } else {
            return R.string.error_internal;
        }
    }

    public static final int interpretErr2Res(VolleyError e) {
        if (e instanceof TimeoutError) {
            return R.string.error_request_timeout;
        } else if (e instanceof KidContactError) {
            return ErrCode.getErrRes(((KidContactError) e).mErrorCode);
        } else if (e instanceof NetworkError) {
            return R.string.error_network;
        } else {
            return R.string.error_internal;
        }
    }

    public static final String interpretErr2String(VolleyError e) {
        int id = interpretErr2Res(e);
        if (id == 0) return null;
        Context ctx = KidContactApp.getInstance();
        if (ctx == null) return null;
        return ctx.getString(id);
    }

    public abstract Request<T> onPrepareRequest(Response.Listener<T> listener, Response.ErrorListener errListener);

    public abstract void onSuccess(T response);

    public void onError(VolleyError e) {
        Log.i(TAG, "onError " + e.toString());
        if (e instanceof KidContactError) {
            KidContactError kError = (KidContactError) e;
            if (kError.mErrorCode == 302) {
                KidContactApp.getInstance().doBackGroundLogin(null);
            }
        }
    }

    public void onError(VolleyError e, final AfterBackGroundLogin afterBackGroundLogin) {
        Log.i(TAG, "onError " + e.toString());
        if ((e instanceof TimeoutError) || (e instanceof NetworkError)) {
            Toast.makeText(KidContactApp.getInstance().getApplicationContext(), R.string.connect_failed, Toast.LENGTH_SHORT).show();
        } else if (e instanceof KidContactError) {
            KidContactError kError = (KidContactError) e;
            if (kError.mErrorCode == 302) {
                KidContactApp.getInstance().doBackGroundLogin(afterBackGroundLogin);
            }
        }
    }
}
