package com.bobs.kidcontact.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by hzheng3 on 14-1-14.
 */
public class Record implements Parcelable {
    @SerializedName("id")
    public int mId;
    @SerializedName("creatorId")
    public int mCreatorId;
    @SerializedName("creatorName")
    public String mCreatorName;
    @SerializedName("info")
    public String mInfo;
    @SerializedName("dateCreated")
    public String mDateCreated;
    @SerializedName("lastUpdated")
    public String mLastUpdated;
    @SerializedName("recordReplies")
    public ArrayList<Reply> mReplies;
    @SerializedName("recordLikes")
    public ArrayList<Like> mLikes;
    @SerializedName("recordTags")
    public ArrayList<TagProxy> mTags;
    @SerializedName("recordPics")
    public ArrayList<PicProxy> mPics;
    @SerializedName("likes")
    public int mLikeCount;

    private transient PicProxy.PicProxyComparator mPicProxyComparator = new PicProxy.PicProxyComparator();

    public void sortPics() {
        if(mPics != null) {
            if (mPics.size() > 1) {
                Collections.sort(mPics, mPicProxyComparator);
            }
        }
    }

    public String getAuthor() {
        return mCreatorName;
    }
    public String getContent() {
        return mInfo;
    }

    // below are temporary
    transient long mCreatedTime;

    public void setAuthor(String author) {
        mCreatorName = author;
    }
    public void setContent(String content) {
        mInfo = content;
    }
    public void setPictures(ArrayList<PicProxy> pics) {
        mPics = pics;
    }
    public void setReplies(ArrayList<Reply> replies) {
        mReplies = replies;
    }
    public ArrayList<Reply> getReplies() {
        return mReplies;
    }
    public ArrayList<PicProxy> getPictures() {
        return mPics;
    }

    public Date getCreatedDate() {
        return UIUtil.stringToDate(mDateCreated, Constants.DATA_DATE_FORMAT);
    }

    public void setCreatedDate(String date) {
        mDateCreated = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeInt(this.mCreatorId);
        dest.writeString(this.mCreatorName);
        dest.writeString(this.mInfo);
        dest.writeString(this.mDateCreated);
        dest.writeString(this.mLastUpdated);
        dest.writeSerializable(this.mReplies);
        dest.writeSerializable(this.mLikes);
        dest.writeSerializable(this.mTags);
        dest.writeSerializable(this.mPics);
        dest.writeInt(this.mLikeCount);
    }

    public Record() {
    }

    @SuppressWarnings("unchecked")
    private Record(Parcel in) {
        this.mId = in.readInt();
        this.mCreatorId = in.readInt();
        this.mCreatorName = in.readString();
        this.mInfo = in.readString();
        this.mDateCreated = in.readString();
        this.mLastUpdated = in.readString();
        this.mReplies = (ArrayList<Reply>) in.readSerializable();
        this.mLikes = (ArrayList<Like>) in.readSerializable();
        this.mTags = (ArrayList<TagProxy>) in.readSerializable();
        this.mPics = (ArrayList<PicProxy>) in.readSerializable();
        this.mLikeCount = in.readInt();
    }

    public static Parcelable.Creator<Record> CREATOR = new Parcelable.Creator<Record>() {
        public Record createFromParcel(Parcel source) {
            return new Record(source);
        }

        public Record[] newArray(int size) {
            return new Record[size];
        }
    };
}
