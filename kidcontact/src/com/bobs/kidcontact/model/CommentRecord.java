package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-14.
 */
public class CommentRecord {
    @SerializedName("id")
    public int mId;
    @SerializedName("comment")
    public Comment mComment;
    @SerializedName("record")
    public Record mRecord;

    public long getId() {
        return mId;
//        if (mComment != null) {
//            return mComment.mId;
//        } else {
//            return  mRecord.mId;
//        }
    }
}
