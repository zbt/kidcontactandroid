package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.UnreadMsgs;

/**
 * Created by hzheng3 on 14-5-20.
 */
public final class RequestGetUnreadMsg extends CookieGsonRequest<UnreadMsgs> {
    private static final String COUNT = "count";
    public RequestGetUnreadMsg(int count, Response.Listener<UnreadMsgs> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getCRsURL(count),
                UnreadMsgs.class, listener, errorListener);
    }

    public static String getCRsURL(int count) {
            return Constants.getFunctionURL(Constants.FUNC_GET_UNREAD_MSG)
                + Constants.QUESTION
                + COUNT + Constants.EQUAL + count;
       }
}