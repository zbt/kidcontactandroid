package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-11.
 */
public abstract class RequestAddInfoTitle<T> extends CookieGsonJsonRequest<T> {
    public RequestAddInfoTitle(String url, InfoTitle input, Class<T> objectClass,
                               Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(url, input, objectClass, listener, errorListener);
    }

    public static class InfoTitle {
        @SerializedName("info")
        public String mInfo;
        @SerializedName("title")
        public String mTitle;
    }
}