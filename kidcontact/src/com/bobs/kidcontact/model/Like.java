package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by hzheng3 on 14-3-23.
 * Shared for MomentLike, RecordLike
 */
public class Like {
    @SerializedName("id")
    public long mId;
    @SerializedName("creatorId")
    public int mCreatorId;
    @SerializedName("creatorName")
    public String mCreatorName;
    @SerializedName("dateCreated")
    public String mDateCreated;
    @SerializedName("lastUpdated")
    public String mLastUpdated;

    public static class LikeComparator implements Comparator<Like> {

        @Override
        public int compare(Like lhs, Like rhs) {
            if( lhs.mId < rhs.mId)
                return -1;
            else if (lhs.mId > rhs.mId)
                return 1;
            else
                return 0;
        }
    }
}
