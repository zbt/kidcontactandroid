package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Leave;

/**
 * Created by hzheng3 on 14-1-11.
 */
public final class RequestAddLeave extends RequestAddInfoTitle<Leave> {
    public RequestAddLeave(InfoTitle input, Response.Listener<Leave> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ADD_LEAVE),
                input, Leave.class, listener, errorListener);
    }
}
