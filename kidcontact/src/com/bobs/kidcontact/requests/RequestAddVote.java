package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Vote;

/**
 * Created by hzheng3 on 14-1-11.
 */
public final class RequestAddVote extends RequestAddInfoTitle<Vote> {
    public RequestAddVote(InfoTitle input, Response.Listener<Vote> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ADD_VOTE),
                input, Vote.class, listener, errorListener);
    }
}
