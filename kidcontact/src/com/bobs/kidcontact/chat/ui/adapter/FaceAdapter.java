package com.bobs.kidcontact.chat.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.ui.KidContactApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FaceAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private int currentPage = 0;
    private Map<String, Integer> mFaceMap;
    private static List<Map.Entry<String, Integer>> faceList = new ArrayList<Map.Entry<String, Integer>>();

    public FaceAdapter(Context context, int currentPage) {
        this.inflater = LayoutInflater.from(context);
        this.currentPage = currentPage;
        mFaceMap = KidContactApp.getInstance().getFaceMap();
        initData();
    }

    private void initData() {
        if (faceList.size() != 0) {
            return;
        }
        for(Map.Entry<String, Integer> entry:mFaceMap.entrySet()){
            faceList.add(entry);
        }
    }

    @Override
    public int getCount() {
        return KidContactApp.NUM + 1;
    }

    @Override
    public Object getItem(int position) {
        return faceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static List<Map.Entry<String, Integer>> getFaceList() {
        return faceList;
    }

    @SuppressWarnings("deprecation")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.face, null, false);
            viewHolder.faceIV = (ImageView) convertView
                    .findViewById(R.id.face_iv);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position == KidContactApp.NUM) {
            viewHolder.faceIV.setImageResource(R.drawable.emotion_del_selector);
            viewHolder.faceIV.setBackgroundDrawable(null);
        } else {
            int count = KidContactApp.NUM * currentPage + position;
            if (count < 107) {
                viewHolder.faceIV.setImageResource(faceList.get(count).getValue());
            } else {
                viewHolder.faceIV.setImageDrawable(null);
            }
        }
        return convertView;
    }

    public static class ViewHolder {
        ImageView faceIV;
    }
}
