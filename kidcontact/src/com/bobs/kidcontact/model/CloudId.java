package com.bobs.kidcontact.model;

import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-16.
 */
public class CloudId {
    @SerializedName("cloudId")
    public String mCloudId;
    @SerializedName("deviceType")
    public int mDeviceType;

    public CloudId() {
        mDeviceType = Constants.DEVICE_TYPE_ANDROID;
    }
}
