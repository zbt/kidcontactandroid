package com.bobs.kidcontact.contants;

import android.content.Context;
import android.util.SparseIntArray;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.ui.KidContactApp;

public final class ErrCode {
    public static final int ERR_INTERNAL_ERROR = 500;
    public static final int ERR_EXPIRED = 302;
    public static final int ERR_INVALID_PROVICNE = 1;
    public static final int ERR_INVALID_CITY = 2;
    public static final int ERR_INVALID_DISTRICT = 3;
    public static final int ERR_INVALID_SCHOOL = 4;
    public static final int ERR_SCHOOL_ALREADY_EXIST = 5;
    public static final int ERR_CLASS_ALREADY_EXIST = 6;
    public static final int ERR_SAVE_SCHOOL_FAILED = 7;
    public static final int ERR_INVALID_ARGUMENT = 8;
    public static final int ERR_SAVE_CLASS_FAILED = 9;
    public static final int ERR_USER_ALREADY_REGISTERED = 10;
    public static final int ERR_ALREADY_BOND = 11;
    public static final int ERR_INVALID_REG_CODE = 12;
    public static final int ERR_CREATE_USER_FAILED = 13;
    public static final int ERR_INVALID_CLASS = 14;
    public static final int ERR_MISSING_PARAMETER = 15;
    public static final int ERR_BIND_FAILED = 16;
    public static final int ERR_CHILD_BOUND = 17;
    public static final int ERR_DUPLICATE_PARENT = 18;
    public static final int ERR_SAVE_FILE_TO_DISK_FAILED = 19;
    public static final int ERR_SAVE_PIC_FAILED = 20;
    public static final int ERR_NO_CONTENT = 21;
    public static final int ERR_ADD_MOMENT_FAILED = 22;
    public static final int ERR_ADD_RECORD_FAILED = 23;
    public static final int ERR_ASSOCIATE_PIC_FAILED = 24;
    public static final int ERR_ADD_MR_FAILED = 25;
    public static final int ERR_ADD_INFO_FAILED = 26;
    public static final int ERR_ADD_PUBLIC_NOTICE_FAILED = 27;
    public static final int ERR_ADD_ACK_NOTICE_FAILED = 28;
    public static final int ERR_ADD_VOTE_FAILED = 29;
    public static final int ERR_ADD_QUESTIONARY_FAILED = 30;
    public static final int ERR_ADD_ASSIGNMENT_FAILED = 31;
    public static final int ERR_INVALID_CHILDREN_INFO = 32;
    public static final int ERR_INVALID_PERMISSION = 33;
    public static final int ERR_MISSING_CHILDID = 34;
    public static final int ERR_INVALID_CHILDID = 35;
    public static final int ERR_ADD_COMMENT_FAILED = 36;
    public static final int ERR_UPDATE_AVATAR_FAILED = 37;
    public static final int ERR_NO_SUCH_ENTITY = 38;
    public static final int ERR_SAVE_REPLY_FAILED = 39;
    public static final int ERR_ADD_REPLY_FAILED = 40;
    public static final int ERR_UPDATE_PASSWD_FAILED = 41;
    public static final int ERR_DELETE_FAILED = 42;
    public static final int ERR_CREATE_LEAVE_FAILED = 43;
    public static final int ERR_UPDATE_USER_INFO_FAILED = 44;
    public static final int ERR_ADD_ASSIGNMENT_REPLY_FAILED = 45;
    public static final int ERR_UPDATE_CLOUDID_FAILED = 47;

    public static final SparseIntArray errMapping;

    static {
        SparseIntArray mapping = new SparseIntArray();
        mapping.put(ERR_INTERNAL_ERROR, R.string.ERR_INTERNAL_ERROR);
        mapping.put(ERR_EXPIRED, R.string.ERR_EXPIRED);
        mapping.put(ERR_INVALID_PROVICNE, R.string.ERR_INVALID_PROVICNE);
        mapping.put(ERR_INVALID_CITY, R.string.ERR_INVALID_CITY);
        mapping.put(ERR_INVALID_DISTRICT, R.string.ERR_INVALID_DISTRICT);
        mapping.put(ERR_INVALID_SCHOOL, R.string.ERR_INVALID_SCHOOL);
        mapping.put(ERR_SCHOOL_ALREADY_EXIST, R.string.ERR_SCHOOL_ALREADY_EXIST);
        mapping.put(ERR_CLASS_ALREADY_EXIST, R.string.ERR_CLASS_ALREADY_EXIST);
        mapping.put(ERR_SAVE_SCHOOL_FAILED, R.string.ERR_SAVE_SCHOOL_FAILED);
        mapping.put(ERR_INVALID_ARGUMENT, R.string.ERR_INVALID_ARGUMENT);
        mapping.put(ERR_SAVE_CLASS_FAILED, R.string.ERR_SAVE_CLASS_FAILED);
        mapping.put(ERR_USER_ALREADY_REGISTERED, R.string.ERR_USER_ALREADY_REGISTERED);
        mapping.put(ERR_ALREADY_BOND, R.string.ERR_ALREADY_BOND);
        mapping.put(ERR_INVALID_REG_CODE, R.string.ERR_INVALID_REG_CODE);
        mapping.put(ERR_CREATE_USER_FAILED, R.string.ERR_CREATE_USER_FAILED);
        mapping.put(ERR_INVALID_CLASS, R.string.ERR_INVALID_CLASS);
        mapping.put(ERR_MISSING_PARAMETER, R.string.ERR_MISSING_PARAMETER);
        mapping.put(ERR_BIND_FAILED, R.string.ERR_BIND_FAILED);
        mapping.put(ERR_CHILD_BOUND, R.string.ERR_CHILD_BOUND);
        mapping.put(ERR_DUPLICATE_PARENT, R.string.ERR_DUPLICATE_PARENT);
        mapping.put(ERR_SAVE_FILE_TO_DISK_FAILED, R.string.ERR_SAVE_FILE_TO_DISK_FAILED);
        mapping.put(ERR_SAVE_PIC_FAILED, R.string.ERR_SAVE_PIC_FAILED);
        mapping.put(ERR_NO_CONTENT, R.string.ERR_NO_CONTENT);
        mapping.put(ERR_ADD_MOMENT_FAILED, R.string.ERR_ADD_MOMENT_FAILED);
        mapping.put(ERR_ADD_RECORD_FAILED, R.string.ERR_ADD_RECORD_FAILED);
        mapping.put(ERR_ASSOCIATE_PIC_FAILED, R.string.ERR_ASSOCIATE_PIC_FAILED);
        mapping.put(ERR_ADD_MR_FAILED, R.string.ERR_ADD_MR_FAILED);
        mapping.put(ERR_ADD_INFO_FAILED, R.string.ERR_ADD_INFO_FAILED);
        mapping.put(ERR_ADD_PUBLIC_NOTICE_FAILED, R.string.ERR_ADD_PUBLIC_NOTICE_FAILED);
        mapping.put(ERR_ADD_ACK_NOTICE_FAILED, R.string.ERR_ADD_ACK_NOTICE_FAILED);
        mapping.put(ERR_ADD_VOTE_FAILED, R.string.ERR_ADD_VOTE_FAILED);
        mapping.put(ERR_ADD_QUESTIONARY_FAILED, R.string.ERR_ADD_QUESTIONARY_FAILED);
        mapping.put(ERR_ADD_ASSIGNMENT_FAILED, R.string.ERR_ADD_ASSIGNMENT_FAILED);
        mapping.put(ERR_INVALID_CHILDREN_INFO, R.string.ERR_INVALID_CHILDREN_INFO);
        mapping.put(ERR_INVALID_PERMISSION, R.string.ERR_INVALID_PERMISSION);
        mapping.put(ERR_MISSING_CHILDID, R.string.ERR_MISSING_CHILDID);
        mapping.put(ERR_INVALID_CHILDID, R.string.ERR_INVALID_CHILDID);
        //mapping.put(ERR_ADD_COMMENT_FAILED, R.string.ERR_ADD_COMMENT_FAILED);
        mapping.put(ERR_UPDATE_AVATAR_FAILED, R.string.ERR_UPDATE_AVATAR_FAILED);
        mapping.put(ERR_NO_SUCH_ENTITY, R.string.ERR_NO_SUCH_ENTITY);
        mapping.put(ERR_SAVE_REPLY_FAILED, R.string.ERR_SAVE_REPLY_FAILED);
        mapping.put(ERR_ADD_REPLY_FAILED, R.string.ERR_ADD_REPLY_FAILED);
        mapping.put(ERR_UPDATE_PASSWD_FAILED, R.string.ERR_UPDATE_PASSWD_FAILED);
        mapping.put(ERR_DELETE_FAILED, R.string.ERR_DELETE_FAILED);
        mapping.put(ERR_CREATE_LEAVE_FAILED, R.string.ERR_CREATE_LEAVE_FAILED);
        mapping.put(ERR_UPDATE_USER_INFO_FAILED, R.string.ERR_UPDATE_USER_INFO_FAILED);
        mapping.put(ERR_ADD_ASSIGNMENT_REPLY_FAILED, R.string.ERR_ADD_ASSIGNMENT_REPLY_FAILED);
        mapping.put(ERR_UPDATE_CLOUDID_FAILED, R.string.ERR_UPDATE_CLOUDID_FAILED);

        errMapping = mapping;
    }

    public static int getErrRes(int errCode) {
        int id = errMapping.get(errCode);
        if (id == 0) {
            id = R.string.signin_error;
        }
        return id;
    }

    public static String getErrString(int errCode) {
        int id = getErrRes(errCode);
        if (id == 0) return null;
        Context ctx = KidContactApp.getInstance();
        if (ctx == null) return null;
        return ctx.getString(id);
    }
}
