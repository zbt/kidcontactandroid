package com.bobs.kidcontact.requests;


import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Picture;
import com.bobs.kidcontact.ui.KidContactApp;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-6.
 */

/**
 * Request to update avatar
 * Even though input is an array of PicPair, please supply *only one*
 */
public final class RequestUpdateAvatar extends CookieMuiltiPicUploadRequest<Picture> {
    //private static final String TAG = "RequestUpdateAvatar";

    public RequestUpdateAvatar(ArrayList<PicPair> picPairs,
                               Response.Listener<Picture> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_UPDATE_AVATAR),
                picPairs, Picture.class, listener, errorListener);
        KidContactApp.getInstance().cleanCacheOfCurrentUserAvatar();
    }


}
