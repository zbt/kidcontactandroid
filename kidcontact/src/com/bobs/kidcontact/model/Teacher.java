package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-16.
 */
public class Teacher extends User {
    @SerializedName("teacherId")
    public String mTeacherId;
    @SerializedName("credit")
    public Integer mCredit;
}
