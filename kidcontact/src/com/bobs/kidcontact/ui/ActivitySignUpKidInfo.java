package com.bobs.kidcontact.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.requests.CookieGsonRequest.Succeed;
import com.bobs.kidcontact.requests.RequestBindToChild;
import com.bobs.kidcontact.requests.RequestBindToChild.BindToChild;
import com.bobs.kidcontact.requests.RequestBindToChildWithAvatar;
import com.bobs.kidcontact.ui.view.FragmentPictureSelector;
import com.bobs.kidcontact.utils.L;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.UIUtil;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest.PicPair;

public class ActivitySignUpKidInfo extends FragmentActivityBase implements
        DatePickerDialog.OnDateSetListener, DialogInterface.OnClickListener {
	private static final String TAG = "ActivitySignUpKidInfo";
	private EditText mEtKidName;
	private EditText mEtKidBirth;
	private EditText mEtParent;
	private Button mBtLogin;
    private LinearLayout mAvatarArea;
    private NetworkRoundImageView mAvatarImg;
    private File cacheAvatarFile;
	private RequestQueue mReqQueue;
	private final Handler mHandler = new Handler();
    private Bitmap mBitmap;
    public static final String DATEPICKER_TAG = "datepicker";

	private final Response.Listener<Succeed> mBoundListener = new Response.Listener<Succeed>() {
		@Override
		public void onResponse(Succeed response) {
	        if (mBitmap != null) {
	            mBitmap.recycle();
	        }
            saveBoundStatus();
			Intent intent = new Intent(ActivitySignUpKidInfo.this, ActivityParentMain.class);
			intent.putExtra(ActivityStatus.EXTRA_INFO, "Parent Role Logged in");
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			mHandler.removeCallbacksAndMessages(null);

			L.d(TAG, "Success");
		}
	};
	private final Response.ErrorListener mErrListener = new Response.ErrorListener() {
		@Override
		public void onErrorResponse(VolleyError error) {
			enableInput();
			Toast.makeText(ActivitySignUpKidInfo.this, R.string.signin_error, Toast.LENGTH_SHORT).show();
			mHandler.removeCallbacksAndMessages(null);

			L.d(TAG, "" + error.getMessage());
		}
	};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_kid_info);
        final Calendar calendar = Calendar.getInstance();
        // hide the unuse button
        ((Button) findViewById(R.id.bt_right)).setVisibility(View.GONE);

        mEtKidName = (EditText) findViewById(R.id.et_kid_name);
        mEtKidBirth = (EditText) findViewById(R.id.et_kid_birthday);
        mEtParent = (EditText) findViewById(R.id.et_parent_appellation);
        mBtLogin = (Button) findViewById(R.id.bt_left);
        mReqQueue = KidContactApp.getInstance().getRequestQueue();

        mBtLogin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				boundToChild();
			}
        });
        final DatePickerDialog mDatePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        mEtKidBirth.setFocusable(false);
        mEtKidBirth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatePickerDialog.setYearRange(1985, 2028);
                mDatePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
            }
        });
        mAvatarArea = (LinearLayout) findViewById(R.id.avatar_container);
        mAvatarArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentPictureSelector fragment = new FragmentPictureSelector();
                fragment.show(getSupportFragmentManager(), "select_picture");
                fragment.setOnClickListener(ActivitySignUpKidInfo.this);
            }
        });
        mAvatarImg = (NetworkRoundImageView) findViewById(R.id.user_avatar);
        mAvatarImg.setImageResource(R.drawable.ic_avatar);
        mAvatarImg.setImageUrl(null,
                KidContactApp.getInstance().getImageLoader());
        mAvatarImg.setDefaultImageResId(R.drawable.ic_avatar);
    }

    private void disableInput() {
    	mEtKidName.setEnabled(false);
    	mEtKidBirth.setEnabled(false);
    	mEtParent.setEnabled(false);
    	mBtLogin.setEnabled(false);
    }

    private void enableInput() {
    	mEtKidName.setEnabled(true);
    	mEtKidBirth.setEnabled(true);
    	mEtParent.setEnabled(true);
    	mBtLogin.setEnabled(true);
    }

	@Override
	protected void onPause() {
		super.onPause();
		mReqQueue.cancelAll(new RequestQueue.RequestFilter() {
			@Override
			public boolean apply(Request<?> request) {
				return true;
			}
		});
		mHandler.removeCallbacksAndMessages(null);
	}

	@Override
	protected void onResume() {
		super.onResume();
		enableInput();
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	 if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_PICK) {
             Uri photoUri = data.getData();
             if (cacheAvatarFile.length() > 0) {
                 mBitmap = BitmapFactory.decodeFile(cacheAvatarFile.getPath());
                 mAvatarImg.setImageBitmap(mBitmap);
             } else if (photoUri != null) {
                 startActivityForResult(UIUtil.getCropIntent(photoUri), Constants.REQUEST_CROP) ;
             }
         } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CAPTURE) {
             Uri tempUri = Uri.fromFile(cacheAvatarFile);
             startActivityForResult(UIUtil.getCaptureCropIntent(tempUri), Constants.REQUEST_CROP) ;
         }  else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CROP) {
             Bundle extras = data.getExtras();

             if (extras != null) {
                 mBitmap = extras.getParcelable("data");
                 mAvatarImg.setImageBitmap(mBitmap);
                 FileOutputStream fOut = null;
 				try {
 					fOut = new FileOutputStream(cacheAvatarFile);
 				} catch (FileNotFoundException e) {
 					e.printStackTrace();
 				}

                 mBitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                 try {
 					fOut.flush();
 				} catch (IOException e) {
 					e.printStackTrace();
 				}
                 try {
 					fOut.close();
 				} catch (IOException e) {
 					e.printStackTrace();
 				}
             }
         }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void boundToChild() {
		BindToChild input = new BindToChild();
		input.mChildName = getCldName();
		if (input.mChildName == null) {
			Toast.makeText(this, R.string.kidinfo_err_kidname, Toast.LENGTH_SHORT).show();
			return;
		}
		input.mChildBirthDay = getCldBirth();
		if (input.mChildBirthDay == null) {
			Toast.makeText(this, R.string.kidinfo_err_kidbirth, Toast.LENGTH_SHORT).show();
			return;
		}
		input.mName = getPar();
		if (input.mName == null) {
			Toast.makeText(this, R.string.kidinfo_err_relation, Toast.LENGTH_SHORT).show();
			return;
		}
		if (mBitmap != null) {
            mReqQueue.add(new RequestBindToChildWithAvatar(input.mChildName, input.mChildBirthDay,
                    input.mName, getAvatarImagePair(), mBoundListener, mErrListener));
        } else {
            mReqQueue.add(new RequestBindToChild(input, mBoundListener, mErrListener));
        }
		disableInput();

		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				onPause();
				onResume();
				Toast.makeText(ActivitySignUpKidInfo.this, R.string.signin_error, Toast.LENGTH_SHORT).show();
				L.d(TAG, "login timeout!");
				// Return to the launch screen page
				Intent intent = new Intent(ActivitySignUpKidInfo.this, ActivityLaunchScreen.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				ActivitySignUpKidInfo.this.startActivity(intent);
			}
		}, 5000);
	}

	private String getCldName() {
		String usrname = mEtKidName.getText().toString().trim();
		if (usrname.length() > 0) {
			return usrname;
		}
		return null;
	}

	private String getCldBirth() {
		String birth = mEtKidBirth.getText().toString();
		if (birth.length() == 8 && birth.matches("[0-9]+")) {
			return birth;
		}
		return null;
	}

	private String getPar() {
		String name = mEtParent.getText().toString().trim();
		if (name.length() > 0) {
			return name;
		}
		return null;
	}

    private ArrayList<PicPair> getAvatarImagePair() {
        ArrayList<PicPair> pics = new ArrayList<PicPair>();
        File cacheAvatarThumb = null;
        Bitmap t = ThumbnailUtils.extractThumbnail(mBitmap, 64, 64);
        FileOutputStream fos;
        try {
            cacheAvatarThumb = UIUtil.getAvatarImageTempFile("avatar_thumb");
            fos = new FileOutputStream(cacheAvatarThumb);
            t.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            t.recycle();
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if ( cacheAvatarThumb != null && cacheAvatarFile != null){
            PicPair pair = new PicPair();
            pair.mPic = cacheAvatarFile;
            pair.mThumb = cacheAvatarThumb;
            pics.add(pair);
            return pics;
        } else {
            return null;
        }
    }

    private void saveBoundStatus() {
        KidContactApp.getSharePrefUtil().setChildName(getCldName());
        KidContactApp.getSharePrefUtil().setChildBirth(getCldBirth());
        KidContactApp.getSharePrefUtil().setParentName(getPar());
        KidContactApp.getSharePrefUtil().setSaveBoundState(true);
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        mEtKidBirth.setText(dateFormat.format(calendar.getTime()));
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        cacheAvatarFile = UIUtil.getAvatarImageTempFile("cacheAvatar");
        switch (which) {
            //index of "picture_picker_array"
            case 0:
                //Take picture via camera
                Intent intent_capture = UIUtil.getCaptureImageIntent(cacheAvatarFile);
                if (intent_capture != null) {
                    startActivityForResult(intent_capture, Constants.REQUEST_CAPTURE);
                }
                break;
            case 1:
                //Select picture from gallery
                Intent intent = UIUtil.getUserImageIntent(cacheAvatarFile);
                if (intent != null) {
                    startActivityForResult(intent, Constants.REQUEST_PICK);
                }
                break;
            default:
                break;
        }
    }
}
