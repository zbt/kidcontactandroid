package com.bobs.kidcontact.ui.teacher;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.ui.FragmentChat;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Child;
import com.bobs.kidcontact.ui.ActionBarActivityBase;
import com.bobs.kidcontact.ui.ActivityPublish;
import com.google.gson.Gson;

/**
 * Created by junyongl on 14-1-20.
 */
public class ActivityChildDetails extends ActionBarActivityBase implements ActionBar.TabListener{
    private static final String TAG = "ActivityChildDetails";
    private ActionBar mActionBar;
    private ChildDetailsPagerAdapter mPagerAdapter;
    private ViewPager mViewpager;
    private Child mChild;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_main);
        mActionBar = getSupportActionBar();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mChild = new Gson().fromJson(getIntent().getStringExtra("child"), Child.class);
        if (mChild.mName != null) {
            mActionBar.setTitle(mChild.mName);
        }
        Log.d(TAG, "show the details of the child " + mChild.mName);
        mPagerAdapter = new ChildDetailsPagerAdapter(getSupportFragmentManager());
        mViewpager = (ViewPager) findViewById(R.id.pager);
        mViewpager.setAdapter(mPagerAdapter);
        mViewpager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                getSupportActionBar().setSelectedNavigationItem(position);
            }
        });

        mActionBar.addTab(mActionBar.newTab().setText(getString(R.string.comment_record)).setTabListener(this));
        mActionBar.addTab(mActionBar.newTab().setText(getString(R.string.messages)).setTabListener(this));
        //todo 

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_child_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_publish_review:
                publish(Constants.PublishType.REVIEW);
                return true;
            case R.id.menu_publish_record:
                publish(Constants.PublishType.RECORD);
                return true;
            case R.id.menu_call_parent:
            	String phoneNumber = mChild.mParents.get(0).mPhoneNumber;
            	Uri uri = Uri.parse("tel:"+ phoneNumber);
            	Intent it = new Intent(Intent.ACTION_CALL, uri);
            	startActivity(it);
            	return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void publish(Constants.PublishType type) {
        Intent intent = new Intent(this, ActivityPublish.class);
        switch (type) {
            case REVIEW:
                intent.putExtra("publish_type", Constants.PublishType.REVIEW.ordinal());
                intent.putExtra("child", new Gson().toJson(mChild));
                break;

            case RECORD:
                intent.putExtra("publish_type", Constants.PublishType.RECORD.ordinal());
                intent.putExtra("child", new Gson().toJson(mChild));
                break;
        }
        startActivity(intent);
    }


    private class ChildDetailsPagerAdapter extends FragmentPagerAdapter {
        public ChildDetailsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    Bundle args = new Bundle();
                    args.putString("child", new Gson().toJson(mChild));
                    FragmentChildDetails fragment = new FragmentChildDetails();
                    fragment.setArguments(args);
                    return fragment;

                case 1:
                    return FragmentChat.newInstance(mChild.mParents.get(0));
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
