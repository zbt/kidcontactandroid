package com.bobs.kidcontact.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.Like;
import com.bobs.kidcontact.model.Reply;

import java.util.ArrayList;

/**
 * Created by junyongl on 13-12-27.
 */
public class CommentView extends LinearLayout {
    private LayoutInflater mInflater;
    private LinearLayout mViewContainer;
//    private ListView mReplyList;
    private ArrayList<Reply> mComments = new ArrayList<Reply>();
    private LayoutParams mParams;
    private Context mContext;

    public CommentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewContainer = (LinearLayout) mInflater.inflate(R.layout.view_comment, null);
        mParams = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        mReplyList = (ListView) mViewContainer.findViewById(R.id.comment_list);
//        mAuthor = (TextView) mViewContainer.findViewById(R.id.comment_author_time);
//        mAvatar = (NetworkRoundImageView) mViewContainer.findViewById(R.id.user_avatar);
//        mContent = (TextView) mViewContainer.findViewById(R.id.comment_content);
        addView(mViewContainer, mParams);

    }

    public void bind(ArrayList<Reply> replys){
        for (Reply reply : replys) {
            TextView replyText = new TextView(mContext);

            replyText.setPadding(8,2,8,2);
//            View replyView = mInflater.inflate(R.layout.view_reply_item, null);
//            TextView mAuthor = (TextView) replyView.findViewById(R.id.comment_author_time);
//            TextView mContent = (TextView) replyView.findViewById(R.id.comment_content);
            SpannableStringBuilder builder = new SpannableStringBuilder();
            if (reply.getAuthor() != null) {
                builder.append(reply.getAuthor());
                builder.append(":");
            }
            if (reply.getContent() != null) {
                builder.append(" ");
                builder.append(reply.getContent());
            }
            if (reply.getAuthor() != null) {
                builder.setSpan(new StyleSpan(Typeface.BOLD), 0, reply.getAuthor().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                builder.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.blue)), 0, reply.getAuthor().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
            replyText.setText(builder);
            replyText.setTextSize(14);

            mViewContainer.addView(replyText, mParams);
        }

//        mAvatar.setImageUrl(Constants.getAvatarThumbURLOfCreator(reply.mCreatorId),
//                KidContactApp.getInstance().getImageLoader());
    }

    public void bindLikes(ArrayList<Like> likes) {
        TextView likeText = new TextView(mContext);
        likeText.setPadding(8,2,8,2);
        likeText.setBackgroundColor(mContext.getResources().getColor(R.color.banner_bottom));
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append("赞：");
        for(Like like: likes) {
            if (like.mCreatorName != null) {
                builder.append(", ");
                builder.append(like.mCreatorName);
            }
        }
        builder.replace(2, 4, "");
        builder.setSpan(new StyleSpan(Typeface.BOLD), 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        builder.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.list_button_pressed)), 0, 2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        likeText.setText(builder);
        likeText.setTextSize(14);
        mViewContainer.addView(likeText, 0, mParams);
    }
}
