package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CloudId;

/**
 * Created by hzheng3 on 14-2-2.
 */
public class RequestUpdateCloudId extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestUpdateCloudId(CloudId input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_UPDATE_CLOUDID),
                input, Succeed.class, listener, errorListener);
    }
}