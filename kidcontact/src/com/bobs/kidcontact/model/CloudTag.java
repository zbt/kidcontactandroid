package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-3-2.
 */
public class CloudTag {
    @SerializedName("cloudTag")
    public String mCloudTag;
}
