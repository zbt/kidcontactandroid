package com.bobs.kidcontact.chat.database;

import java.util.ArrayList;
import java.util.List;
import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.service.Utils;
import com.bobs.kidcontact.ui.KidContactApp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * This class is not thread safe, it just focus on database operations.
 * We have at least one table, which stores all the conversation tables,
 * in our "chat" database. We'll also maintain a unique table for each conversation.
 *
 * @author longzhang hzheng3
 *
 */
public class ChatDB {
    public static int EMPTY_DB_ID = -1;
    // public static final String CHAT_DBNAME = "chat.db";
    private static final String TAG = "ChatDB";
    private static final String TABLE_NAME_CONVERSATIONS = "conversations";
    private static final String SQL_CREATE_CONVERSATIONS_TABLE = "create table IF NOT EXISTS " +
            TABLE_NAME_CONVERSATIONS + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, display_name TEXT, attendees TEXT," +
            " group_chat INTEGER, group_chat_joined INTEGER, group_id TEXT, user_id INGETER," +
            " new_msg_count INGETER, last_msg_date TEXT, notify_enabled INTEGER, table_name TEXT);";
    private SQLiteDatabase mDB = null;
    private Context mContext;

    public ChatDB(Context context) {
        mContext = context;
        mDB = mContext.openOrCreateDatabase("chat" + KidContactApp.getSharePrefUtil().getUserName() + ".db", Context.MODE_PRIVATE, null);
        Log.d(TAG, "current database name: " + "chat" + KidContactApp.getSharePrefUtil().getUserName() + ".db");
        createConversationsTable();
    }

    /**
     * According to conversation info, to:
     * 1, add one row into conversations table
     * 2, create new table according to conversation property;
     * If group chat, table name is mGroupId; If single chat, table name is mUserId; they should be unique
     * @param conversation
     * @return null if operation failed
     */
    public Conversation createConversation(Conversation conversation) {
        if (!checkStatus() || !conversation.isValid()) {
            return null;
        }

        // Create a new table for this conversation
        boolean result = false;
        if (conversation.getGroupChat()) {
            result = createConversationTable(conversationNameToTableName("" + conversation.getGroupId()));
            conversation.setTableName(conversationNameToTableName(conversation.getGroupId()));
        } else {
            result = createConversationTable(conversationNameToTableName("" + conversation.getUserId()));
            conversation.setTableName(conversationNameToTableName("" + conversation.getUserId()));
        }
        if (!result) {
            return null;
        }

        // Insert a new row into conversations table
        long rowId = insertIntoConversationsTable(conversation);
        if (rowId < 0) {
            return null;
        } else {
            conversation.setId(rowId);
        }

        // Success
        return conversation;
    }

    /**
     * According to conversation info to update table conversations
     * @param conversation
     * @return
     */
    public Conversation updateConversation(Conversation conversation) {
        if (!checkStatus() || !conversation.isValid()) {
            return null;
        }

        // Update
        ContentValues values = createContentValues(conversation);
        if (mDB.update(TABLE_NAME_CONVERSATIONS, values, "_id=" + conversation.getId(), null) > 0) {
            // Notify upper context
            Utils.sendConversationChangedBroadcast(mContext, conversation);
        }

        return conversation;
    }

    /**
     * This should affect two tables, first is to delete one row from conversations, second is to drop corresponding message table
     * @param conversation
     * @return
     */
    public boolean deleteConversation(Conversation conversation) {
        if (!checkStatus()) {
            return false;
        }

        // Delete the row and drop the table
        if (!dropConversationTable("" + conversation.getId()) ||
            !deleteFromConversationsTable(conversation.getId())) {
            return false;
        }

        // Notify upper context
        Utils.sendConversationDeletedBroadcast(mContext, conversation);
        return true;
    }

    /**
     * Retrieve all conversations from table conversations
     * @return empty if nothing is there, null if an error happens
     */
    public List<Conversation> getAllConversations() {
        if (!checkStatus()) {
            return null;
        }
        List<Conversation> result = new ArrayList<Conversation>();

        // Find
        Cursor cursor = mDB.query(TABLE_NAME_CONVERSATIONS, null, null, null, null, null, "_id");
        Conversation[] conversations = new Conversation[cursor.getCount()];
        for (int i = 0; cursor.moveToNext(); i++) {
            conversations[i] = new Conversation();
            conversations[i].setId(cursor.getLong(0));
            conversations[i].setDisplayName(cursor.getString(1));
            conversations[i].setAttendees(cursor.getString(2));
            conversations[i].setGroupChat(cursor.getInt(3) == 1 ? true : false);
            conversations[i].setGroupChatJoined(cursor.getInt(4) == 1 ? true : false);
            conversations[i].setGroupId(cursor.getString(5));
            conversations[i].setUserId(cursor.getLong(6));
            conversations[i].setNewMsgCount(cursor.getInt(7));
            conversations[i].setLastMsgDate(cursor.getString(8));
            conversations[i].setNotifyEnabled(cursor.getInt(9) == 1 ? true : false);
            conversations[i].setTableName(cursor.getString(10));
            result.add(conversations[i]);
        }
        cursor.close();

        return result;
    }

    /**
     * Get the conversation with the given id from table conversations
     * @param conversationId a id of conversation
     * @return a Conversation if we find it in the table, null otherwise
     */
    public Conversation getConversationById(long conversationId) {
        if (!checkStatus()) {
            return null;
        }

        Cursor cursor = mDB.rawQuery("select* from " + TABLE_NAME_CONVERSATIONS + " where _id=" + conversationId + ";", null);
        // Check result
        if (cursor.getCount() <= 0) {
            cursor.close();
            return null;
        }
        if (cursor.getCount() > 1) {
            Log.e(TAG, Common.getLastTrace() + ": id conflict!");
            // We'll use the first row
        }

        cursor.moveToFirst();
        Conversation conv = new Conversation();
        conv.setId(cursor.getLong(0));
        conv.setDisplayName(cursor.getString(1));
        conv.setAttendees(cursor.getString(2));
        conv.setGroupChat(cursor.getInt(3) == 1 ? true : false);
        conv.setGroupChatJoined(cursor.getInt(4) == 1 ? true : false);
        conv.setGroupId(cursor.getString(5));
        conv.setUserId(cursor.getLong(6));
        conv.setNewMsgCount(cursor.getInt(7));
        conv.setLastMsgDate(cursor.getString(8));
        conv.setNotifyEnabled(cursor.getInt(9) == 1 ? true : false);
        conv.setTableName(cursor.getString(10));

        cursor.close();
        return conv;
    }


    /**
     * Add message into conversation, two tables should be affected
     * 1, The table that stores messages should be added with one row
     * 2, Conversations table should be updated with new conversation info, like mLastMsgDate
     * @param conversation
     * @param msg
     * @param updateNewMsgCount
     * @return the newly inserted ChatMessage if succeed, null if failed
     */
    public ChatMessage addMsgToConversation(Conversation conversation, ChatMessage msg, boolean updateNewMsgCount) {
        if (!checkStatus() || !msg.isValid()) {
            return null;
        }
        Conversation oldConv = null;

        // Check if table exists
        oldConv = getConversationByName(msg.getConversationName());
        if (null == oldConv) {
            // This a new conversation and message that we've never stored, so create table for them
            oldConv = createConversation(conversation);
            if (null == oldConv) {
                return null;
            }
        }

        // Update conversation table
        long result = insertIntoConversationTable(conversationNameToTableName(msg.getConversationName()), msg);
        if (result < 0) {
            return null;
        } else {
            msg.setId(result);
        }

        // Update new message count if necessary
        if (updateNewMsgCount) {
            conversation.setNewMsgCount(oldConv.getNewMsgCount() + 1);
        }

        // Update conversations table
        if (null == updateConversation(conversation)) {
            return null;
        }

        // Notify upper context
        Utils.sendConversationChangedBroadcast(mContext, conversation);
        Utils.sendMessageChangedBroadcast(mContext, msg);
        return msg;
    }

    /** Clear the new message count of a conversation with the given user id */
    public boolean clearNewMsgCountByUserId(long userId) {
        // Check status
        if (!checkStatus()) {
            return false;
        }

        // Clear new message count
        try {
            mDB.execSQL("update " + TABLE_NAME_CONVERSATIONS + " set " + Conversation.FIELD_NAME_NEW_MSG_COUNT
                + "=0 where " + Conversation.FIELD_NAME_USER_ID + "=" + userId + ";");
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return false;
        }

        // Notify the upper context
        Conversation conversation = getConversationByUserId(userId);
        if (null != conversation) {
            Utils.sendConversationChangedBroadcast(mContext, conversation);
        }

        // Succeed
        return true;
    }

    /** Clear the new message count of a conversation with the given group id */
    public boolean clearNewMsgCountByGroupId(String groupId) {
        // Check status
        if (!checkStatus()) {
            return false;
        }

        // Clear new message count
        try {
            mDB.execSQL("update " + TABLE_NAME_CONVERSATIONS + " set " + Conversation.FIELD_NAME_NEW_MSG_COUNT
                + "=0 where " + Conversation.FIELD_NAME_GROUP_ID + "='" + groupId + "';");
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return false;
        }

        // Notify the upper context
        Conversation conversation = getConversationByGroupId(groupId);
        if (null != conversation) {
            Utils.sendConversationChangedBroadcast(mContext, conversation);
        }

        // Succeed
        return true;
    }

    /**
     * Update the tables with the the given ChatMessage
     * @return the updated ChatMessage if succeed, null if failed.
     */
    public ChatMessage updateChatMessage(ChatMessage msg) {
        // Check status
        if (null == mDB || !msg.isValid()) {
            return null;
        }

        // Update conversation table
        ContentValues values = createContentValues(msg);
        if (mDB.update(conversationNameToTableName(msg.getConversationName()), values, "_id=" + msg.getId(), null) > 0) {
            // Notify upper context
            Utils.sendMessageChangedBroadcast(mContext, msg);
        }

        return msg;
    }

    /**
     * Get messages of a conversation from database.
     * @param conversationId the id of a conversation
     * @param offset the position from which messages will be searched
     * @param limit the number of messages to get
     * @return a list of messages of the given conversation, null if error
     */
    public List<ChatMessage> getMessages(long conversationId, long offset, long limit) {
        // Check status
        if (!checkStatus() || offset < 0 || limit < 0) {
            return null;
        }

        // Find
        Conversation conversation = getConversationById(conversationId);
        if (null == conversation) {
            return null;
        }
        List<ChatMessage> messages = new ArrayList<ChatMessage>();
        Cursor cursor = mDB.query(conversation.getTableName(), null, "_id<=" + offset, null, null, null, "_id DESC", "" + limit);
        while (cursor.moveToNext()) {
            ChatMessage msg = new ChatMessage();
            msg.setId(cursor.getLong(0));
            msg.setInfo(cursor.getString(1));
            msg.setIsInComing(cursor.getInt(2) == 1);
            msg.setUserId(cursor.getLong(3));
            msg.setType(cursor.getInt(4));
            msg.setMsgDate(cursor.getString(5));
            msg.setStatus(cursor.getInt(6));
            msg.setConversationName(cursor.getString(7));
            messages.add(0, msg);
        }
        cursor.close();

        return messages;
    }

    public long getMaxMessageId(Conversation conversation) {
        // Check status
        if (!checkStatus() || !conversation.isValid()) {
            return -1;
        }
        if (null == conversation.getTableName() || conversation.getTableName().equals("")) {
            return -1;
        }
        Cursor cursor = mDB.rawQuery("select MAX(_id) from " + conversation.getTableName() + ";", null);
        if (null == cursor) {
            return -1;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return -1;
        }
        cursor.moveToFirst();
        long res = cursor.getLong(0);
        cursor.close();
        return res;
    }

    /**
     * Remove message from table that stores it
     * @param conversation
     * @param msg
     * @return true if succeeded
     */
    public boolean delMsgFromConversation(Conversation conversation, ChatMessage msg) {
        if (!checkStatus()) {
            return false;
        }

        // Delete
        if (!deleteFromConversationTable(conversationNameToTableName(msg.getConversationName()), msg.getId())) {
            return false;
        }

        // Notify upper context
        Utils.sendMessageDeletedBroadcast(mContext, msg);
        return true;
    }

    /** Delete a ChatMessage with the given group id and message id */
    public boolean deleteGroupChatMsg(String groupId, long msgId) {
        if (!checkStatus()) {
            return false;
        }
        ChatMessage msgBak = getMessage(conversationNameToTableName(groupId), msgId);
        if (null == msgBak) {
            return false;
        }
        if (!deleteFromConversationTable(conversationNameToTableName(groupId), msgId)) {
            return false;
        }
        Utils.sendMessageDeletedBroadcast(mContext, msgBak);
        return true;
    }
    
    /** Delete a ChatMessage with the given to-user id and message id */
    public boolean deleteSingleChatMsg(long toUserId, long msgId) {
        if (!checkStatus()) {
            return false;
        }
        ChatMessage msgBak = getMessage(conversationNameToTableName("" + toUserId), msgId);
        if (null == msgBak) {
            return false;
        }
        if (!deleteFromConversationTable(conversationNameToTableName("" + toUserId), msgId)) {
            return false;
        }
        Utils.sendMessageDeletedBroadcast(mContext, msgBak);
        return true;
    }

    /**
     * Get the new message count of a conversation with the given group id.
     * @param groupId
     * @return the count of the new message, -1 if error
     */
    public int getNewMsgCount(String groupId) {
        // Check status
        if (!checkStatus()) {
            return -1;
        }

        // Query
        Cursor cursor = mDB.rawQuery("select " + Conversation.FIELD_NAME_NEW_MSG_COUNT +
                " from " + TABLE_NAME_CONVERSATIONS + " where " +
                Conversation.FIELD_NAME_GROUP_ID + "='" + groupId + "';", null);
        if (null == cursor) {
            return -1;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return -1;
        }

        // Succeed
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result;
    }

    /** Do some clearing. */
    public void close() {
        if (null != mDB) {
            mDB.close();
            mDB = null;
        }
    }

    /**
     * Create our conversations table if it doesn't exist.
     * @return true if succeed, false if failed
     */
    private boolean createConversationsTable() {
        if (!checkStatus()) {
            return false;
        }

        // Create
        try {
            mDB.execSQL(SQL_CREATE_CONVERSATIONS_TABLE);
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return false;
        }

        // Success
        return true;
    }

    /**
     * Create a new conversation table with the given name.
     * @return true if successful, false if failed
     */
    private boolean createConversationTable(String tableName) {
        if (!checkStatus()) {
            return false;
        }

        // Check parameter(s)
        if (null == tableName) {
           Log.e(TAG, Common.getLastTrace() + ": wrong parameter: " + tableName);
           return false;
        }

        // Create
        try {
            mDB.execSQL("create table IF NOT EXISTS " + tableName + " (_id INTEGER PRIMARY KEY AUTOINCREMENT, info TEXT," +
                    " is_incoming INTEGER, user_id INTEGER, type INTEGER, msg_date TEXT," +
                    " status INTEGER, conversation_name TEXT);");
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string when creating conversation table: "
                    + e.getMessage());
            return false;
        }

        // Success
        return true;
    }

    /**
     * Insert a new row into conversations table, this doesn't affect any conversation tables
     * @return the row id of the newly inserted conversation, -1 if failed
     */
    private long insertIntoConversationsTable(Conversation conversation) {
        if (!checkStatus()) {
            return -1;
        }

        // Insert
        long result = -1;
        try {
            ContentValues values = createContentValues(conversation);
            result = mDB.insert(TABLE_NAME_CONVERSATIONS, null, values);
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return -1;
        }

        return result;
    }

    /**
     * Delete a row from conversations table, this doesn't affect any conversation tables
     * @param conversationId the id of a conversation to be deleted
     * @return true if successful, false if failed
     */
    private boolean deleteFromConversationsTable(long conversationId) {
        if (!checkStatus()) {
            return false;
        }

        // Delete
        try {
            mDB.execSQL("delete from " + TABLE_NAME_CONVERSATIONS + " where _id=" + conversationId + ";");
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Drop a conversation table, this doesn't affect conversations table
     * @param tableName the name of conversation table
     * @return true if successful, false if failed
     */
    private boolean dropConversationTable(String tableName) {
        if (!checkStatus()) {
            return false;
        }

        // Drop
        try {
            mDB.execSQL("drop table " + tableName + ";");
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Insert a new message row into a conversation table that has the given name.
     * @param tableName the name of an existed conversation table
     * @param msg a ChatMessage to be stored
     * @return the row id of newly inserted message, -1 if failed
     */
    private long insertIntoConversationTable(String tableName, ChatMessage msg) {
        if (!checkStatus()) {
            return -1;
        }

        // Insert
        long result = -1;
        try {
            ContentValues values = createContentValues(msg);
            result = mDB.insert(tableName, null, values);
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return -1;
        }

        return result;
    }

    /**
     * Delete a message row from a conversation table that has the given name.
     * @param tableName the name of an existed conversation table
     * @param msgId the id of the message to be deleted
     * @return true if succeed, false if failed
     */
    private boolean deleteFromConversationTable(String tableName, long msgId) {
        if (!checkStatus()) {
            return false;
        }

        // Delete
        try {
            mDB.execSQL("delete from " + tableName + " where _id=" + msgId + ";");
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return false;
        }

        return true;
    }

    /** Create a ContentValues from a given Conversation */
    private ContentValues createContentValues(Conversation conversation) {
        ContentValues values = new ContentValues();
        values.put(Conversation.FIELD_NAME_DISPLAY_NAME, conversation.getDisplayName());
        values.put(Conversation.FIELD_NAME_ATTENDEES, conversation.getAttendees());
        values.put(Conversation.FIELD_NAME_GROUP_CHAT, conversation.getGroupChat() ? 1 : 0);
        values.put(Conversation.FIELD_NAME_GROUP_CHAT_JOINED, conversation.getGroupChatJoined() ? 1 : 0);
        values.put(Conversation.FIELD_NAME_GROUP_ID, conversation.getGroupId());
        values.put(Conversation.FIELD_NAME_USER_ID, conversation.getUserId());
        values.put(Conversation.FIELD_NAME_NEW_MSG_COUNT, conversation.getNewMsgCount());
        values.put(Conversation.FIELD_NAME_LAST_MSG_DATE, conversation.getLastMsgDate());
        values.put(Conversation.FIELD_NAME_NOTIFY_ENABLED, conversation.getNotifyEnabled());
        values.put(Conversation.FIELD_NAME_TABLE_NAME, conversation.getTableName());
        return values;
    }

    /** Create a ContentValues from a given ChatMessage */
    private ContentValues createContentValues(ChatMessage msg) {
        ContentValues values = new ContentValues();
        values.put(ChatMessage.FIELD_NAME_INFO, msg.getInfo());
        values.put(ChatMessage.FIELD_NAME_IS_INCOMING, msg.getIsInComing() ? 1 : 0);
        values.put(ChatMessage.FIELD_NAME_USER_ID, msg.getUserId());
        values.put(ChatMessage.FIELD_NAME_TYPE, msg.getType());
        values.put(ChatMessage.FIELD_NAME_MSG_DATE, msg.getMsgDate());
        values.put(ChatMessage.FIELD_NAME_STATUS, msg.getStatus());
        values.put(ChatMessage.FIELD_NAME_CONVERSATION_NAME, msg.getConversationName());
        return values;
    }

    /** Convert a conversation name to a table name */
    private String conversationNameToTableName (String conversationName) {
        return "_" + conversationName;
    }

    /**
     * Check the status of the SQLiteDatabase instance
     * @return true if it is instantiated, false if not
     */
    private boolean checkStatus() {
        if (null == mDB) {
            Log.e(TAG, Common.getLastTrace() + ": SQLiteDatabase has not been created.");
            return false;
        }
        return true;
    }

    /**
     * Get the Conversation by its table name.
     * @param conversationName the table name of a Conversation
     * @return a Conversation if succeed, null if failed
     */
    public Conversation getConversationByName(String conversationName) {
        // Check status
        if (!checkStatus()) {
            return null;
        }

        String[] clns = {"_id"};
        Cursor cursor = mDB.query(TABLE_NAME_CONVERSATIONS, clns, Conversation.FIELD_NAME_TABLE_NAME + "='" + conversationNameToTableName(conversationName) + "'", null, null, null, null);
        long id = -1;
        if (cursor == null) {
            return null;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return null;
        }
        if (cursor.moveToNext()) {
            id = cursor.getLong(0);
        }
        cursor.close();
        return getConversationById(id);
    }

    /**
     * Get the conversation by its group id, this works only when you want to
     * get a group chat conversation.
     * @param groupId the group id of a group chat conversation
     * @return a group chat conversation, null if not found or error
     */
    public Conversation getConversationByGroupId(String groupId) {
        // Check status
        if (!checkStatus()) {
            return null;
        }

        long id = -1;
        String[] clns = {"_id"};
        Cursor cursor = mDB.query(TABLE_NAME_CONVERSATIONS, clns, Conversation.FIELD_NAME_GROUP_ID + "='" + groupId + "'", null, null, null, null);
        if (null == cursor) {
            return null;
        }
        if (cursor.moveToNext()) {
            id = cursor.getLong(0);
        } else {
            Log.i(TAG, "can't find the conversation with the group id: " + groupId);
            cursor.close();
            return null;
        }

        // succeed
        cursor.close();
        return getConversationById(id);
    }

    /**
     * Get the conversation by its user id, this works only when you want to
     * get a single chat conversation.
     * @param userId the user id of a single chat conversation
     * @return a single chat conversation, null if not found or error
     */
    public Conversation getConversationByUserId(long userId) {
        // Check status
        if (!checkStatus()) {
            return null;
        }

        long id = -1;
        String[] clns = {"_id"};
        Cursor cursor = mDB.query(TABLE_NAME_CONVERSATIONS, clns, Conversation.FIELD_NAME_USER_ID + "=" + userId + "", null, null, null, null);
        if (null == cursor) {
            return null;
        }
        if (cursor.moveToNext()) {
            id = cursor.getLong(0);
        } else {
            Log.i(TAG, "can't find the conversation with the user id: " + userId);
            cursor.close();
            return null;
        }

        // succeed
        cursor.close();
        return getConversationById(id);
    }

    /** Query a ChatMessage with the given table name and message id */
    private ChatMessage getMessage(String tableName, long msgId) {
        if (!checkStatus()) {
            return null;
        }
        Cursor cursor = mDB.query(tableName, null, ChatMessage.FIELD_NAME_ID + "=" + msgId, null, null, null, null);
        if (null == cursor) {
            return null;
        }
        ChatMessage msg = null;
        if (cursor.moveToNext()) {
            msg = new ChatMessage();
            msg.setId(cursor.getLong(0));
            msg.setInfo(cursor.getString(1));
            msg.setIsInComing(cursor.getInt(2) == 1);
            msg.setUserId(cursor.getLong(3));
            msg.setType(cursor.getInt(4));
            msg.setMsgDate(cursor.getString(5));
            msg.setStatus(cursor.getInt(6));
            msg.setConversationName(cursor.getString(7));
        }
        cursor.close();
        return msg;
    }

    /**
     * A exception class for ChatDB.
     * @author longzhang
     *
     */
    public class ChatDBException extends Exception {
        private static final long serialVersionUID = 1L;

        ChatDBException(String err) {
            super(err);
        }
    }
}
