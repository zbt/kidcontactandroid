package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.EatMenu;


/**
 * Created by hzheng3 on 14-6-9.
 */


public final class RequestAddEatMenu extends RequestAddPic<EatMenu> {

    public RequestAddEatMenu(PicPair input, Response.Listener<EatMenu> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_UPLOAD_EAT_MENU_PIC), input, EatMenu.class, listener, errorListener);
    }
}
