package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by hzheng3 on 14-1-14.
 * Shared for MomentReply, CommentReply, RecordReply
 * AckNoticeReply, QuestionaryReply, VoteReply
 */
public class Reply {
    @SerializedName("id")
    public long mId;
    @SerializedName("creatorId")
    public int mCreatorId;
    @SerializedName("creatorName")
    public String mCreatorName;
    @SerializedName("info")
    public String mInfo;
    @SerializedName("dateCreated")
    public String mDateCreated;
    @SerializedName("lastUpdated")
    public String mLastUpdated;

    public String getCreatedTime() {
        return mDateCreated;
    }

    public void setCreatedTime(String mCreatedTime) {
        this.mDateCreated = mCreatedTime;
    }

    public String getAuthor() {
        return mCreatorName;
    }

    public void setAuthor(String mAuthor) {
        this.mCreatorName = mAuthor;
    }

    public String getContent() {
        return mInfo;
    }

    public void setContent(String mContent) {
        this.mInfo = mContent;
    }

    public static class ReplyComparator implements Comparator<Reply> {

        @Override
        public int compare(Reply lhs, Reply rhs) {
            if( lhs.mId < rhs.mId)
                return -1;
            else if (lhs.mId > rhs.mId)
                return 1;
            else
                return 0;
        }
    }
}
