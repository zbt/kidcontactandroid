package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 2014/6/27.
 */
public class Klass {
    @SerializedName("id")
    public long mId;
    @SerializedName("className")
    public String mClassName;
    @SerializedName("regCodes")
    public ArrayList<RegCode> mRegCodes;
}
