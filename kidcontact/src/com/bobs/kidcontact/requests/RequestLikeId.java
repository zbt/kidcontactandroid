package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.model.IdProxy;

/**
 * Created by hzheng3 on 14-3-23.
 */
public abstract class RequestLikeId<T> extends CookieGsonJsonRequest<T> {
    public RequestLikeId(String url, IdProxy input, Class<T> objectClass,
                         Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(url, input, objectClass, listener, errorListener);
    }
}