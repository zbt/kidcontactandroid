package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Reply;

/**
 * Created by hzheng3 on 14-1-11.
 */
public final class RequestAddVoteReply extends RequestAddReply {
    public RequestAddVoteReply(InfoId input, Response.Listener<Reply> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ADD_VOTE_REPLY),
                input, listener, errorListener);
    }
}
