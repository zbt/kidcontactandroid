package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Klass;

/**
 * Created by hzheng3 on 14-6-27.
 */
public final class RequestGetMyCurrentClass extends CookieGsonRequest<Klass> {

    public RequestGetMyCurrentClass(Response.Listener<Klass> listener, Response.ErrorListener errorListener) {
        super(Method.GET, Constants.getFunctionURL(Constants.FUNC_GET_MY_CURRENT_CLASS),
                Klass.class, listener, errorListener);
    }
}