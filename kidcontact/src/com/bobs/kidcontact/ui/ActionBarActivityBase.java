package com.bobs.kidcontact.ui;


import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.chat.service.ChatService;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.chat.service.Utils;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

/**
 * This is the base class for all the action bar activities in this application.
 * @author zhanglong
 */
public abstract class ActionBarActivityBase extends ActionBarActivity {
    public static final String TAG = "ActivityBase";
    public static final int TAB_POSITION_PARENT_STATUS = 0;
    public static final int TAB_POSITION_PARENT_KID = 1;
    public static final int TAB_POSITION_PARENT_CLASS = 2;
    public static final int TAB_POSITION_PARENT_MORE = 3;
    public static final int TAB_POSITION_TEACHER_STATUS = 0;
    public static final int TAB_POSITION_TEACHER_MEMBER = 1;
    public static final int TAB_POSITION_TEACHER_CLASS = 2;
    public static final int TAB_POSITION_TEACHER_MORE = 3;
    public static final String KEY_NEW_MSG_COUNT = "key_new_msg_count";
    public static final String KEY_TAB_POSITION = "key_tab_position";
    public static final String KEY_TAB0_UPDATE_COUNT = "key_tab0_update_count";
    public static final String KEY_TAB1_UPDATE_COUNT = "key_tab1_update_count";
    public static final String KEY_TAB2_UPDATE_COUNT = "key_tab2_update_count";
    public static final String KEY_TAB3_UPDATE_COUNT = "key_tab3_update_count";
    public static final int EVENT_NEW_MSG_COUNT = 0;
    public static final int EVENT_ALL_UPDATE_COUNT = 1;
    private boolean mShouldStopChatService = false;
    protected IChatService.Stub mChatServiceStub = null;
    protected Object mChatStubWatcher = new Object();
    protected UpdateReceiver mUpdateReceiver = null;
    protected ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, Common.getLastTrace() + ": switch user: bind chat service successfully.");
            mChatServiceStub = (IChatService.Stub) iBinder;
            synchronized (mChatStubWatcher) {
                mChatStubWatcher.notifyAll();
            }
            if (!clearNotification()) {
                Log.w(TAG, "failed to clear notification!");
            }
            onGetChatServiceStub(mChatServiceStub);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, Common.getLastTrace() + ": switch user: unbind chat service successfully.");
            mChatServiceStub = null;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        if(!KidContactApp.couldAutoSignin()) { // that means we're in a bad status
            KidContactApp.returnToSigninPage(this);
            finish();
            return;
        }
        if (null == startService(new Intent(this, ChatService.class))) {
            Log.e(TAG, "failed to start chat service!");
        } else {
            Log.d(TAG, Common.getLastTrace() + ": switch user: binding chat service.");
            if (!bindService(new Intent(this, ChatService.class), mConnection, Context.BIND_AUTO_CREATE)) {
                Log.e(TAG, "failed to bind to chat service!");
            }
        }
        registerUpdateReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshViewsAsync();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mUpdateReceiver) {
            unregisterReceiver(mUpdateReceiver);
            mUpdateReceiver = null;
        }
        if (null != mChatServiceStub && mChatServiceStub.isBinderAlive()) {
            Log.d(TAG, Common.getLastTrace() + ": switch user: unbindinging chat service.");
            unbindService(mConnection);
            mChatServiceStub = null;
        }
        if (mShouldStopChatService) {
            stopService(new Intent(this, ChatService.class));
        }
    }

    /** Cancel the existing notification and clear all notification cache */
    protected boolean clearNotification() {
        try {
            mChatServiceStub.cancelNotification();
            mChatServiceStub.clearNotificationCache();
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Get the chat service stub, might be null.
     */
    public IChatService.Stub getChatServiceStub() {
        return mChatServiceStub;
    }

    /**
     * Get the chat service stub, this will wait until a valid stub has been got
     * or the given timeout expires.
     * @return
     */
    public IChatService.Stub getChatServiceStubWait(long ms) {
        if (null == mChatServiceStub) {
            synchronized (mChatStubWatcher) {
                try {
                    mChatStubWatcher.wait(ms);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return mChatServiceStub;
    }

    /**
     * Set a flag to indicate that the chat service should be stopped, but it
     * is not guaranteed unless this is last the module who is bound to chat service.
     * @param shouldStop
     */
    public void shouldStopChatService(boolean shouldStop) {
        mShouldStopChatService = shouldStop;
    }

    public Handler getViewEventHandler() {
        return null;
    }

    /** A receiver for all the broadcasts that involve data changing */
    protected class UpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Utils.ACTION_CONVERSATION_CHANGED)) {
                Bundle dataConversation = intent.getExtras();
                if (null == dataConversation) {
                    Log.w(TAG, "no data found!");
                    return;
                }
                Conversation conversation = dataConversation.getParcelable(Utils.KEY_CONVERSATION);
                if (null == conversation) {
                    Log.w(TAG, "no conversation found!");
                    return;
                }
                refreshViews(conversation);
            } else if (intent.getAction().equals(Utils.ACTION_CTRL_MSG_CHANGED)) {
                Log.d(TAG, "handling ctrl msg.");
                Bundle dataCtrlMsg = intent.getExtras();
                if (null == dataCtrlMsg) {
                    Log.w(TAG, "no data found!");
                    return;
                }
                CtrlMessage msg = dataCtrlMsg.getParcelable(Utils.KEY_CTRL_MSG);
                if (null == msg) {
                    Log.w(TAG, "no CtrlMessage found!");
                    return;
                }
                refreshViews(msg);
            } else if (intent.getAction().equals(Utils.ACTION_CTRL_MSG_DELETED)) {
                Log.d(TAG, "handling ctrl msg.");
                Bundle dataCtrlMsg = intent.getExtras();
                if (null == dataCtrlMsg) {
                    Log.w(TAG, "no data found!");
                    return;
                }
                CtrlMessage msg = dataCtrlMsg.getParcelable(Utils.KEY_CTRL_MSG);
                if (null == msg) {
                    Log.w(TAG, "no CtrlMessage found!");
                    return;
                }
                refreshViewsDel(msg);
            } else {
                // Should not come here!
                Log.w(TAG, "unrecognized action!");
                return;
            }
        }
    }

    /** Refresh views of current activity */
    protected void refreshViews() {
    }

    /** Refresh views according to a newly changed conversation */
    protected void refreshViews(Conversation conversation) {
    }

    /**
     * Refresh the views according to a newly received CtrlMessage. We only check
     * and handle remote login event in the root version of this method.
     */
    protected void refreshViews(CtrlMessage msg) {
        // Check and handle remote login
        if (CtrlMessage.TYPE_REMOTE_LOGIN == msg.getType()) {
            shouldStopChatService(true);
            showRemoteLoginNotice();
        }
    }

    /**
     * Show a dialog to notice the user that his or her account has been used
     * in another device.
     */
    private void showRemoteLoginNotice() {
        new AlertDialog.Builder(this)
            .setTitle(R.string.remote_login_notice_content)
            .setPositiveButton(R.string.remote_login_notice_ok, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ActionBarActivityBase.this.finish();
                }
            })
            .setCancelable(false)
            .create()
            .show();
    }

    /** Refresh the views according to a newly deleted CtrlMessage */
    protected void refreshViewsDel(CtrlMessage msg) {
        Log.d(TAG, "handle delete CtrlMessage.");
    }

    /** refresh the views in background because this may be slow */
    protected void refreshViewsAsync() {
    }

    /** Register an {@link #ActionBarActivityBase.UpdateReceiver} */
    protected void registerUpdateReceiver() {
        if (null != mUpdateReceiver) {
            Log.w(TAG, Common.getLastTrace() + ": alraedy registered UpdateReceiver!");
            return;
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(Utils.ACTION_CONVERSATION_CHANGED);
        filter.addAction(Utils.ACTION_CTRL_MSG_CHANGED);
        filter.addAction(Utils.ACTION_CTRL_MSG_DELETED);
        mUpdateReceiver = new UpdateReceiver();
        registerReceiver(mUpdateReceiver, filter);
    }

    /** Will be called after the chat service stub is got */
    protected void onGetChatServiceStub(IChatService.Stub stub) {
    }

}
