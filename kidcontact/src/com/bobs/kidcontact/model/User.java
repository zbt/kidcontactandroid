package com.bobs.kidcontact.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-16.
 */
public class User implements Parcelable {
    @SerializedName("id")
    public long mId = -1L;
    @SerializedName("name")
    public String mName;
    @SerializedName("phoneNumber")
    public String mPhoneNumber;
    @SerializedName("dateCreated")
    public String mDateCreated;
    @SerializedName("lastUpdated")
    public String mLastUpdated;
    @SerializedName("cloudId")
    public String mCloudId;
    @SerializedName("deviceType")
    public int mDeviceType;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mPhoneNumber);
        dest.writeString(this.mDateCreated);
        dest.writeString(this.mLastUpdated);
        dest.writeString(this.mCloudId);
        dest.writeInt(mDeviceType);
    }

    public User() {
    }

    private User(Parcel in) {
        this.mId = in.readLong();
        this.mName = in.readString();
        this.mPhoneNumber = in.readString();
        this.mDateCreated = in.readString();
        this.mLastUpdated = in.readString();
        this.mCloudId = in.readString();
        this.mDeviceType = in.readInt();
    }

    public static Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
