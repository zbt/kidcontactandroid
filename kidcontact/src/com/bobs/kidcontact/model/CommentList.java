package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-15.
 */
public class CommentList {
    @SerializedName("comments")
    ArrayList<Comment> mComments;
}
