package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 2014/5/21.
 */
public class MessageProxy {
    @SerializedName("message")
    public String mMessage;
}
