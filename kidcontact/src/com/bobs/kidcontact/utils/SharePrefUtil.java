package com.bobs.kidcontact.utils;

import java.security.InvalidParameterException;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by junyongl on 14-2-28.
 */
public class SharePrefUtil {
    public static final String INVALID_STRING = "null0";
    private static final String FILE_PREFERENCE_KID_CONTACT = "file_share_preference_kid_contact";
    private static final String KEY_HAS_NEW_MOMENT = "key_has_new_moment";
    private static final String KEY_USER_ROLE = "key_user_role";
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private static final String KEY_JSESSION_COOKIE = "key_jsession_cookie";
    private static final String KEY_REMEMBER_ME_COOKIE = "key_remember_me_cookie";
    private static final String KEY_CHILD_NAME = "key_child_name";
    private static final String KEY_CHILD_BIRTH = "key_child_birth";
    private static final String KEY_TEACHER_NAME = "key_teacher_name";
    private static final String KEY_PARENT_NAME = "key_parent_name";
    private static final String KEY_TEACHER_ID = "key_teacher_id";
    private static final String KEY_LAST_USER_NAME = "key_last_userName";
    private static final String KEY_USER_NAME = "key_userName";
    private static final String KEY_PASSWORD = "key_password";
    private static final String KEY_SAVE_STATE = "key_save_state";
    private static final String KEY_SAVE_BOUND_STATE = "key_save_bound_state";
    private static final String KEY_USER_ID = "key_user_id";
    private static final String KEY_AVATAR_URL = "key_avatar_url";
    private static final String KEY_FIRST_USE = "key_first_use";
    private static final String KEY_STATUS_HEADER = "key_status_header";
    private static final String KEY_CLASS_ID = "key_class_id";

    public SharePrefUtil(Context context) {
        sp = context.getSharedPreferences(FILE_PREFERENCE_KID_CONTACT, Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public void setHasNewMoment(Boolean state) {
        editor.putBoolean(KEY_HAS_NEW_MOMENT, state);
        editor.commit();
    }

    public boolean getHasNewMoment() {
        return sp.getBoolean(KEY_HAS_NEW_MOMENT, false);
    }

    public void setJsessionCookies(String jsessionCookie) {
        editor.putString(KEY_JSESSION_COOKIE, jsessionCookie);
        editor.putString(KEY_REMEMBER_ME_COOKIE, jsessionCookie);
        editor.commit();
    }

    public void setRememberMeCookies(String rememberMeCookie) {
        editor.putString(KEY_REMEMBER_ME_COOKIE, rememberMeCookie);
        editor.commit();
    }

    public String getJsessionCookie() {
        return sp.getString(KEY_JSESSION_COOKIE, null);
    }

    public String getRememberMeCookie() {
        return sp.getString(KEY_REMEMBER_ME_COOKIE, null);
    }

    public String getChildName() {
        return sp.getString(KEY_CHILD_NAME, null);
    }

    public void setChildName(String childName) {
        editor.putString(KEY_CHILD_NAME, childName);
        editor.commit();
    }

    public String getChildBirth() {
        return sp.getString(KEY_CHILD_BIRTH, null);
    }

    public void setChildBirth(String childBirth) {
        editor.putString(KEY_CHILD_BIRTH, childBirth);
        editor.commit();
    }

    public String getTeacherName() {
        return sp.getString(KEY_TEACHER_NAME, null);
    }

    public void setTeacherName(String teacherName) {
        editor.putString(KEY_TEACHER_NAME, teacherName);
        editor.commit();
    }

    public String getParentName() {
        return sp.getString(KEY_PARENT_NAME, null);
    }

    public void setParentName(String parentName) {
        editor.putString(KEY_PARENT_NAME, parentName);
        editor.commit();
    }

    public String getTeacherID() {
        return sp.getString(KEY_TEACHER_ID, null);
    }

    public void setTeacherID(String teacherID) {
        editor.putString(KEY_TEACHER_ID, teacherID);
        editor.commit();
    }

    public long getUserID() {
        return sp.getLong(KEY_USER_ID, -1);
    }

    public void setUserID(long userID) {
        editor.putLong(KEY_USER_ID, userID);
        editor.commit();
    }

    public int getRole() {
        return sp.getInt(KEY_USER_ROLE, -1);
    }

    public void setRole(int role) {
        editor.putInt(KEY_USER_ROLE, role);
        editor.commit();
    }
    
    public long getClassId(){
    	return sp.getLong(KEY_CLASS_ID, -1);
    }
    
    public void setClassId(long classId){
    	editor.putLong(KEY_CLASS_ID, classId);
    	editor.commit();
    }

    public String getLastUserName() {
        return sp.getString(KEY_LAST_USER_NAME, "null1");
    }

    public void setLastUserName() {
        editor.putString(KEY_LAST_USER_NAME, getUserName());
        editor.commit();
    }

    public String getUserName() {
        return sp.getString(KEY_USER_NAME, INVALID_STRING);
    }

    public void setUserName(String userName) {
        editor.putString(KEY_USER_NAME, userName);
        editor.commit();
    }

    public String getPassword() {
        return sp.getString(KEY_PASSWORD, INVALID_STRING);
    }

    public void setPassword(String password) {
        editor.putString(KEY_PASSWORD, password);
        editor.commit();
    }

    public String getAvatarUrl() {
        return sp.getString(KEY_AVATAR_URL, null);
    }

    public void setAvatarUrl(String url) {
        editor.putString(KEY_AVATAR_URL, url);
        editor.commit();
    }

    public boolean getSaveState() {
        return sp.getBoolean(KEY_SAVE_STATE, false);
    }

    public void setSaveState(boolean isSaved) {
        editor.putBoolean(KEY_SAVE_STATE, isSaved);
        editor.commit();
    }

    public boolean getSaveBoundState() {
        return sp.getBoolean(KEY_SAVE_BOUND_STATE, false);
    }

    public void setSaveBoundState(boolean isSaved) {
        editor.putBoolean(KEY_SAVE_BOUND_STATE, isSaved);
        editor.commit();
    }

    public boolean isFirstUse() {
        return sp.getBoolean(KEY_FIRST_USE, true);
    }

    public void setFirstUse(boolean isFirstUse) {
        editor.putBoolean(KEY_FIRST_USE, isFirstUse);
        editor.commit();
    }

    public String getStatusHeader() {
        return sp.getString(KEY_STATUS_HEADER, null);
    }

    public void setStatusHeader(String headerJson) {
        editor.putString(KEY_STATUS_HEADER, headerJson);
        editor.commit();
    }

    // Last refresh time
    public void setLastRefreshStatusTime(String date) {
        if (null == date) {
            throw new InvalidParameterException("date can't be null!");
        }
        editor.putString("last_refresh_status_time", date);
        editor.commit();
    }

    public String getLastRefreshStatusTime() {
        return sp.getString("last_refresh_status_time", new Date().toLocaleString());
    }

    public void clearSharedPreferences() {
        editor.clear();
        editor.commit();
    }

    public void setLastRefreshClassTime(String date){
    	if (null == date) {
            throw new InvalidParameterException("date can't be null!");
        }
        editor.putString("last_refresh_class_time", date);
        editor.commit();
    }

    public String getLastRefreshClassTime() {
    	return sp.getString("last_refresh_status_time", new Date().toLocaleString());
    }

}
