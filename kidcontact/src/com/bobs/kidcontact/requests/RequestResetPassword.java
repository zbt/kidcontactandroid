package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-03-17
 */
public class RequestResetPassword extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestResetPassword(String phoneNumber, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_RESET_PASSWORD),
                new ResetPassword(phoneNumber, "noneedForNow"), Succeed.class, listener, errorListener);
    }

    public static class ResetPassword {
        @SerializedName("phoneNumber")
        public String mPhoneNumber;
        @SerializedName("resetToken")
        public String mToken;

        public ResetPassword(String phoneNumber, String token) {
            mPhoneNumber = phoneNumber;
            mToken = token;
        }
    }
}