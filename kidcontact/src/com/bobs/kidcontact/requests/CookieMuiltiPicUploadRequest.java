package com.bobs.kidcontact.requests;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Request to upload multiple pictures
 * <p/>
 * The multipart-form request uploading several pictures should have the following parts, example below
 * "picDescription":"[{"pic":"file1","thumb":"file1thumb"},{"pic":"file2","thumb":"file2thumb"},{"pic":"file3","thumb":""}]"
 * "file1":MultipartFile
 * "file1thumb":MultipartFile
 * "file2":MultipartFile
 * "file2thumb":MultipartFile
 * "file3":MultipartFile
 */

public abstract class CookieMuiltiPicUploadRequest<T> extends CookieGsonRequest<T> {
    private static final String TAG = "CookieMuiltiPicUploadRequest";

    private ArrayList<PicPair> mPicPairs = new ArrayList<PicPair>();
    private ArrayList<PicPairDescription> mPicPairDescriptions;
    private Gson mGson;

    protected MultipartEntity entity = new MultipartEntity();

    public CookieMuiltiPicUploadRequest(String url, ArrayList<PicPair> picPairs, Class<T> objectClass,
                                        Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, objectClass, listener, errorListener);
        mPicPairs = picPairs;
        mGson = new Gson();
        buildMultipartEntity();
    }

    public CookieMuiltiPicUploadRequest(String url, PicPair picPair, Class<T> objectClass,
                                        Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, objectClass, listener, errorListener);
        mPicPairs.add(picPair);
        mGson = new Gson();
        buildMultipartEntity();
    }

    @SuppressWarnings("deprecation")
    private void buildMultipartEntity() {
        removeInvalidPicPairs();
        if (mPicPairs == null || mPicPairs.isEmpty()) {
            Log.e(TAG, "WTF, Pices does not exist");
            return;
        }
        String picDescription = getPicsDescription();
        Log.i(TAG, "picDescription is " + picDescription);

        int i = 0;
        for (PicPair picPair : mPicPairs) {
            if (picPair.mPic != null) {
                addOneFile(mPicPairDescriptions.get(i).mPic, picPair.mPic);
            }
            if (picPair.mThumb != null) {
                addOneFile(mPicPairDescriptions.get(i).mPicThumb, picPair.mThumb);
            }
            i++;
        }
        try {
            entity.addPart("picDescription", new StringBody(picDescription));
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "buildMultipartEntity failed " + e.getMessage());
        }
    }

    @SuppressWarnings("deprecation")
    private void addOneFile(String key, File file) {
        byte[] data = new byte[(int) file.length()];
        try {
            new FileInputStream(file).read(data);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Failed to retrieve data from file ");
        }
        String filePath = file.getAbsolutePath();
        String suffix = filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());
        String mimeType = "image/";
        if ("jpeg".equals(suffix) || "jpg".equals(suffix)) {
            mimeType += "jpeg";
        } else if ("png".equals(suffix)) {
            mimeType += suffix;
        } else { // For uploading normal file, like txt
            mimeType = "application/octet-stream";
        }
        Log.i(TAG, "mimeType is " + mimeType);
        Log.i(TAG, "File part path is " + file.getAbsolutePath());
        entity.addPart(key, new ByteArrayBody(data, mimeType, file.getAbsolutePath()));
    }

    @Override
    public String getBodyContentType() {
        String contentType = entity.getContentType().getValue();
        Log.i(TAG, "ContentType is " + contentType);
        return contentType;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            entity.writeTo(bos);
        } catch (IOException e) {
            Log.e(TAG, "IOException writing to ByteArrayOutputStream");
        }
        byte[] ret = bos.toByteArray();
        if (ret != null) {
            Log.i(TAG, "Body size is " + ret.length);
        } else {
            Log.e(TAG, "No body!");
        }
        return ret;
    }

    public static class PicPair {
        public File mPic;
        public File mThumb;
    }

    private String getPicKey(int i) {
        return "file" + i;
    }

    private String getPicThumbKey(int i) {
        return "file" + i + "thumb";
    }

    private class PicPairDescription {
        @SerializedName("pic")
        public String mPic;
        @SerializedName("thumb")
        public String mPicThumb;
    }

    private void removeInvalidPicPairs() {
        ArrayList<Integer> removeIndexes = new ArrayList<Integer>();
        int i = 0;
        for (PicPair picPair : mPicPairs) {
            if (picPair.mPic == null && picPair.mThumb == null) {
                removeIndexes.add(i);
            }
            i++;
        }
        for (int index : removeIndexes) {
            mPicPairs.remove(index);
        }
    }

    private String getPicsDescription() {
        mPicPairDescriptions = new ArrayList<PicPairDescription>();
        int i = 0;
        for (PicPair picPair : mPicPairs) {
            PicPairDescription picPairDescription = new PicPairDescription();
            if (picPair.mPic != null) {
                picPairDescription.mPic = getPicKey(i);
            }
            if (picPair.mThumb != null) {
                picPairDescription.mPicThumb = getPicThumbKey(i);
            }
            mPicPairDescriptions.add(picPairDescription);
            i++;
        }
        return mGson.toJson(mPicPairDescriptions);
    }
}
