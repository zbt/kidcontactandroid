package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-12.
 */
public class FileProxy {
    @SerializedName("file")
    public Picture mFile;
}
