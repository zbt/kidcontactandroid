package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 2014/6/27.
 */
public class RegCode {
    @SerializedName("code")
    public String mCode;
    @SerializedName("type")
    public int mType;

    public static final int TYPE_TEACHER = 1;
    public static final int TYPE_PARENT = 2;
}
