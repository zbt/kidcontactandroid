#!/usr/bin/perl
open(FILE_PHONE, ">"."phone") or die "can't open the file: ".$!;

while (<>) {
	if ($_ =~ /=REG-\d{11}-REG=/) {
		my $phone = $_;
		$phone =~ s/.*=REG-//;
		$phone =~ s/-REG=.*//;
		print FILE_PHONE $phone;
	}
}

close(FILE_PHONE);
