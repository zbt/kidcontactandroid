package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 2014/6/9.
 */
public class AssociatePicInput {
    @SerializedName("id")
    public long mId;
    @SerializedName("picIds")
    public ArrayList<Long> mPicIds;
}
