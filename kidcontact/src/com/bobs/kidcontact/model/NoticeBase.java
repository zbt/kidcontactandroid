package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-11.
 */
public class NoticeBase {
    @SerializedName("id")
    public int mId;
    @SerializedName("title")
    public String mTitle;
    @SerializedName("creatorId")
    public int mCreatorId;
    @SerializedName("creatorName")
    public String mCreatorName;
    @SerializedName("info")
    public String mInfo;
    @SerializedName("dateCreated")
    public String mDateCreated;
    @SerializedName("lastUpdated")
    public String mLastUpdated;
}
