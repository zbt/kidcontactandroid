package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;

public final class RequestHeartBeat extends CookieGsonRequest<CookieGsonRequest.Succeed> {
    public RequestHeartBeat(Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getURL(),
                Succeed.class, listener, errorListener);
	}

    public static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_HEART_BEAT);
    }
}
