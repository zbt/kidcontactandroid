package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-11.
 */
public class Vote extends NoticeBase {
    @SerializedName("voteReplies")
    public ArrayList<Reply> mReplies;

    public static class VoteContent {
        @SerializedName("noticeContent")
        public String mContent;
        @SerializedName("voteOptions")
        public ArrayList<String> mOptions;

    }
}
