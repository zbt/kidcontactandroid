package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.TimeTable;

/**
 * Created by hzheng3 on 14-6-9.
 */
public final class RequestGetTimeTable extends CookieGsonRequest<TimeTable> {

    public RequestGetTimeTable(Response.Listener<TimeTable> listener, Response.ErrorListener errorListener) {
        super(Method.GET, Constants.getFunctionURL(Constants.FUNC_GET_TIME_TABLE),
                TimeTable.class, listener, errorListener);
    }
}