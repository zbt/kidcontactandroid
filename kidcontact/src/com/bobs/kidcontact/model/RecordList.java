package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-14.
 */
public class RecordList {
    @SerializedName("records")
    public ArrayList<Record> mRecords;
}
