package com.bobs.kidcontact.requests;

import android.util.Log;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.utils.UIUtil;

import org.apache.http.entity.mime.content.StringBody;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-6.
 */

/**
 * The bind request is submitted with multipart-form request which contain following parts
 * Please only supply one PicPair in ArrayList<PicPair> argument
 * MUST: "childName":"xxxxxx"
 * MUST: "childBirthDay": "xxxxxx"
 * MUST: "name" : "xxxxxx"
 * MUST: picture related
  */
public final class RequestBindToChildWithAvatar extends CookieMuiltiPicUploadRequest<CookieGsonRequest.Succeed> {
    private static final String TAG = "RequestBindToChildWithAvatar";

    private String mChildName;
    private String mChildBirthDay;
    private String mName;
    public RequestBindToChildWithAvatar(String childName, String childBirthday, String name,
                                        ArrayList<PicPair> picPairs,
                                        Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(getURL(), picPairs, Succeed.class, listener, errorListener);

        mChildName = UIUtil.encoder(childName);
        mChildBirthDay = childBirthday;
        mName = UIUtil.encoder(name);
        buildChildInfo();
    }

    @SuppressWarnings("deprecation")
    private void buildChildInfo() {
        if (mChildName != null && !mChildName.isEmpty()
                && mChildBirthDay != null && !mChildBirthDay.isEmpty()
                && mName != null && !mName.isEmpty()) {
            try {
                entity.addPart("childName", new StringBody(mChildName));
                entity.addPart("childBirthDay", new StringBody(mChildBirthDay));
                entity.addPart("name", new StringBody(mName));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "add children failed " + e.getMessage());
            }
        }
    }

    private static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_BIND_CHILD_WITH_AVATAR);
    }
}
