package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Notice;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-11.
 */
public final class RequestGetNotices extends CookieGsonRequest<RequestGetNotices.Notices> {
    private static final String IDOFFSET = "idOffset";
    public RequestGetNotices(long idOffset, Response.Listener<Notices> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getNoticesURL(idOffset),
                RequestGetNotices.Notices.class, listener, errorListener);
    }

    public static class Notices {
        @SerializedName("notices")
        public ArrayList<Notice> mNotices;
    }

    public static String getNoticesURL(long idOffset) {
        return Constants.getFunctionURL(Constants.FUNC_GET_NOTICES)
                + Constants.QUESTION
                + IDOFFSET + Constants.EQUAL + idOffset;
    }
}
