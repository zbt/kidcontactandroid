package com.bobs.kidcontact.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Child;
import com.bobs.kidcontact.model.ChildList;
import com.bobs.kidcontact.model.Moment;
import com.bobs.kidcontact.model.RecordList;
import com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest.PicPair;
import com.bobs.kidcontact.requests.RequestAddMoment;
import com.bobs.kidcontact.requests.RequestAddRecord;
import com.bobs.kidcontact.ui.ActivityPublish.IPublish;
import com.bobs.kidcontact.ui.teacher.ActivityChildSelector;
import com.bobs.kidcontact.ui.view.FlowLayout;
import com.bobs.kidcontact.ui.view.FragmentPictureSelector;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.Gson;

import de.greenrobot.event.EventBus;

/**
 * Created by leon on 14-1-4.
 */
public class FragmentPublishMoment extends Fragment implements IPublish, DialogInterface.OnClickListener {
    private static final String TAG = "FragmentPublishMoment";
    private GridView mAddPicturesGrid;
    private EditText mContent;
    private final ArrayList<Uri> mPicturesUri = new ArrayList<Uri>();
    private Context mContext;
    private int gridWidth;
    AddPicturesAdapter mAdapter;
    private AsyncTask<Void, String, RequestUtil<Moment>> mMomentPrepareTask;
    private AsyncTask<Void, String, RequestUtil<RecordList>> mRecordPrepareTask;
    private RequestUtil<?> mPublishTask;
    private ProgressDialog mDialog;
    private File mCacheDir;
    private int mSeqNum = 0;
    private int mPublishType;
    private Child mInitChild;
    private FlowLayout mTargetContainer;
    private ArrayList<Child> mChildren = new ArrayList<Child>();
    private File cacheImageFile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        final float widthpixels = mContext.getResources().getDisplayMetrics().widthPixels;
        final float scale = mContext.getResources().getDisplayMetrics().density;
        gridWidth = (int) ((widthpixels - 32* scale)/3 -8);
        Bundle bundle = getArguments();
        mPublishType = bundle.getInt("publish_type");
        mInitChild = new Gson().fromJson(bundle.getString("child"), Child.class);
        mDialog = new ProgressDialog(mContext) {
            @Override
            protected void onStop() {
                super.onStop();
                FragmentPublishMoment.this.onStop();
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publish_moment, container, false);
        mAddPicturesGrid = (GridView) view.findViewById(R.id.grid_add_picture);
        mContent = (EditText) view.findViewById(R.id.edit_moment_content);
        mAdapter = new AddPicturesAdapter();
        mAddPicturesGrid.setAdapter(mAdapter);
        registerForContextMenu(mAddPicturesGrid);

        ViewStub stub = (ViewStub) view.findViewById(R.id.stub_selector);
        if (KidContactApp.getInstance().getRole() == Constants.TYPE_TEACHER &&
                mPublishType == Constants.PublishType.RECORD.ordinal()) {
            View selectorView = stub.inflate();
            ((TextView)selectorView.findViewById(R.id.tx_target)).setText(
                    mContext.getString(R.string.record));
            mTargetContainer = (FlowLayout) selectorView.findViewById(R.id.target_container);
            if (mInitChild != null) {
                Log.d(TAG, "Get the Init user " + mInitChild.mName);
                mChildren.add(mInitChild);
            }
            initRecipientsView();
        }

        return view;
    }

    private void initRecipientsView() {
        mTargetContainer.removeAllViews();
        final float scale = mContext.getResources().getDisplayMetrics().density;
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams (ViewGroup.LayoutParams.WRAP_CONTENT, (int) (32 * scale));
        params.setMargins(4,4,4,4);
        for (int i = 0; i < mChildren.size(); i++) {
            Child c = mChildren.get(i);
            Button btRecipient = new Button(mContext);
            btRecipient.setBackgroundResource(R.drawable.button_recipient);
            btRecipient.setText(c.mName);
            btRecipient.setGravity(Gravity.CENTER);
            btRecipient.setTextSize((float) 13.0);
            mTargetContainer.addView(btRecipient, params);
        }
        Button btAdd = new Button(mContext);
        btAdd.setBackgroundResource(R.drawable.button_recipient);
        btAdd.setText(mContext.getString(R.string.add_recipient));
        btAdd.setGravity(Gravity.CENTER);
        btAdd.setTextSize((float) 13.0);
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChildList childList = new ChildList();
                childList.mChildren = mChildren;
                Intent intent = new Intent(mContext, ActivityChildSelector.class);
                intent.putExtra("selected_children", new Gson().toJson(childList));
                startActivityForResult(intent, 2);
            }
        });
        mTargetContainer.addView(btAdd, params);
        mTargetContainer.invalidate();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_PICK:
                    Uri uri = data.getData();
                    Log.d(TAG, "Pick picture URI: " + uri.toString());
                    if (uri != null) {
                        mPicturesUri.add(uri);
                        mAdapter.notifyDataSetChanged();
                    }
                    break;
                case Constants.REQUEST_CAPTURE:
                    if (cacheImageFile != null) {
                        Uri uri_file = Uri.fromFile(cacheImageFile);
                        Log.d(TAG, "Pick picture URI: " + uri_file.toString());
                        if (uri_file != null) {
                            mPicturesUri.add(uri_file);
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
                case 2:
                    ChildList childList = new Gson().fromJson(data.getStringExtra("selected_children"), ChildList.class);
                    mChildren = childList.mChildren;
                    initRecipientsView();
                    break;
                default:
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            //index of "picture_picker_array"
            case 0:
                //Take picture via camera
                try {
                    cacheImageFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent intent_capture = UIUtil.getCaptureImageIntent(cacheImageFile);
                if (intent_capture != null) {
                    startActivityForResult(intent_capture, Constants.REQUEST_CAPTURE);
                }
                break;
            case 1:
                //Select picture from gallery
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, Constants.REQUEST_PICK);
                break;
            default:
                break;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private class AddPicturesAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return mPicturesUri.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return mPicturesUri.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (position == getCount() - 1) {
                LayoutInflater inflater =  (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.view_add_picture, parent, false);
                convertView.setLayoutParams(new GridView.LayoutParams(gridWidth, gridWidth));
                TextView addPicture = (TextView) convertView.findViewById(R.id.add_picture);
                addPicture.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentPictureSelector fragment = new FragmentPictureSelector();
                        fragment.show(getChildFragmentManager(), "picture_selector");
                        fragment.setOnClickListener(FragmentPublishMoment.this);
                    }
                });
            } else {
                ImageView imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(gridWidth, gridWidth));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                convertView = imageView;
                Bitmap bitmap = null;
                Uri uri = (Uri)getItem(position);
                if (uri.getScheme().equals("content")) {
                    bitmap = MediaStore.Images.Thumbnails.getThumbnail(
                            mContext.getContentResolver(), ContentUris.parseId(uri),
                            MediaStore.Images.Thumbnails.MINI_KIND,
                            null );
                } else if (uri.getScheme().equals("file")) {
                    bitmap = getThumbnailBitmap(uri);
                }
                imageView.setImageBitmap(bitmap);
            }
            return convertView;
        }

        private final class ViewHolder {
            ImageView imageView;
        }
    }

    private Bitmap getThumbnailBitmap(Uri uri) {
        // Get the dimensions of the View
        int targetW = 160;
        int targetH = 160;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(uri.getPath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(uri.getPath(), bmOptions);
        return bitmap;
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        if (contentUri.getScheme().equals("file")) {
            return contentUri.getPath();
        }
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void publish() {
        if (mPublishType == Constants.PublishType.RECORD.ordinal()) {
            // publish Record
            mRecordPrepareTask = new AsyncTask<Void, String, RequestUtil<RecordList>>() {
                @Override
                protected RequestUtil<RecordList> doInBackground(Void... params) {
                    int count = mPicturesUri.size();
                    if (count == 0) {
                        return null;
                    }
                    prepareCache();
                    final ArrayList<PicPair> pics = new ArrayList<PicPair>();
                    for (int i = 0; i < count; i++) {
                        if (isCancelled()) {
                            return null;
                        }
                        Uri uri = mPicturesUri.get(i);
                        publishProgress("准备 " + uri.toString());
                        String abspath = getRealPathFromURI(mContext, uri);
                        Log.d(TAG, "Adding " + abspath);
                        pics.add(processImage(abspath));
                    }
                    RequestUtil<RecordList> req = new RequestUtil<RecordList>() {
                        @Override
                        public Request<RecordList> onPrepareRequest(
                                Listener<RecordList> listener, ErrorListener errListener) {
                            String txt = mContent.getText().toString();
                            if (txt.length() == 0) {
                                txt = " ";
                            }
                            ArrayList<Long> children = new ArrayList<Long>();
                            for (Child c : mChildren) {
                                children.add(c.mId);
                            }
                            return new RequestAddRecord(txt, null, pics, children, listener, errListener);
                        }

                        @Override
                        public void onSuccess(RecordList response) {
                            mDialog.dismiss();
                            Toast.makeText(mContext, R.string.publish_success, Toast.LENGTH_SHORT).show();
                            getActivity().finish();
                            EventBus.getDefault().post(new UpdateKidDetailUIEvent());
                        }

                        @Override
                        public void onError(VolleyError e) {
                            mDialog.dismiss();
                            Toast.makeText(mContext, R.string.publish_fail, Toast.LENGTH_SHORT).show();
                            super.onError(e);
                        }
                    };
                    req.setTimeout(10000 * count);
                    return req;
                }

                @Override
                protected void onCancelled() {
                    super.onCancelled();
                    mDialog.dismiss();
                }

                @Override
                protected void onPostExecute(RequestUtil<RecordList> result) {
                    super.onPostExecute(result);
                    if (result != null) {
                        mPublishTask = result;
                        mDialog.setMessage("上传中 ...");
                        mPublishTask.add2queue();
                    } else {
                        mDialog.dismiss();
                    }
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mDialog.setMessage("开始上传 ...");
                    mDialog.show();
                }

                @Override
                protected void onProgressUpdate(String... values) {
                    super.onProgressUpdate(values);
                    if (values != null && values.length > 0) {
                        mDialog.setMessage(values[0]);
                    }
                }
            };
            mRecordPrepareTask.execute();

        } else {
            // Teacher can publish Moments.
            mMomentPrepareTask = new AsyncTask<Void, String, RequestUtil<Moment>>() {
                @Override
                protected RequestUtil<Moment> doInBackground(Void... params) {
                    int count = mPicturesUri.size();
                    if (count == 0) {
                        return null;
                    }
                    prepareCache();
                    final ArrayList<PicPair> pics = new ArrayList<PicPair>();
                    for (int i = 0; i < count; i++) {
                        if (isCancelled()) {
                            return null;
                        }
                        Uri uri = mPicturesUri.get(i);
                        publishProgress("准备 " + uri.toString());
                        String abspath = getRealPathFromURI(mContext, uri);
                        Log.d(TAG, "Adding " + abspath);
                        pics.add(processImage(abspath));
                    }
                    RequestUtil<Moment> req = new RequestUtil<Moment>() {
                        @Override
                        public Request<Moment> onPrepareRequest(
                                Listener<Moment> listener, ErrorListener errListener) {
                            String txt = mContent.getText().toString();
                            if (txt.length() == 0) {
                                txt = " ";
                            }
                            return new RequestAddMoment(txt, null, pics, listener, errListener);
                        }

                        @Override
                        public void onSuccess(Moment response) {
                            mDialog.dismiss();
                            Toast.makeText(mContext, R.string.publish_success, Toast.LENGTH_SHORT).show();
//                            KidContactApp.getSharePrefUtil().setHasNewMoment(true);
                            getActivity().finish();
                            EventBus.getDefault().post(new UpdateMomentUIEvent());
                        }

                        @Override
                        public void onError(VolleyError e) {
                            mDialog.dismiss();
                            Toast.makeText(mContext, R.string.publish_fail, Toast.LENGTH_SHORT).show();
                            super.onError(e);
                        }
                    };
                    req.setTimeout(10000 * count);
                    return req;
                }

                @Override
                protected void onCancelled() {
                    super.onCancelled();
                    mDialog.dismiss();
                }

                @Override
                protected void onPostExecute(RequestUtil<Moment> result) {
                    super.onPostExecute(result);
                    if (result != null) {
                        mPublishTask = result;
                        mDialog.setMessage("上传 ...");
                        mPublishTask.add2queue();
                    } else {
                        mDialog.dismiss();
                    }
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mDialog.setMessage("准备上传 ...");
                    mDialog.show();
                }

                @Override
                protected void onProgressUpdate(String... values) {
                    super.onProgressUpdate(values);
                    if (values != null && values.length > 0) {
                        mDialog.setMessage(values[0]);
                    }
                }
            };
            mMomentPrepareTask.execute();
        }



    }

    @Override
    public void onStop() {
        super.onStop();
        if (mMomentPrepareTask != null) {
            mMomentPrepareTask.cancel(true);
        }
        if (mPublishTask != null) {
            mPublishTask.cancel();
        }
        mDialog.dismiss();
    }

    private void prepareCache() {
        if (mCacheDir == null) {
            mCacheDir = new File(mContext.getCacheDir(), TAG);
            if (!mCacheDir.exists()) {
                if (!mCacheDir.mkdirs()) {
                    throw new RuntimeException("Can't create cache for thumbnails");
                }
            }
        }
        File[] files = mCacheDir.listFiles();
        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }
    }

    private PicPair processImage(String path) {
        String path_big = path;
        String path_small = path;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opts);
        int orgHeight = opts.outHeight;
        int orgWidth = opts.outWidth;
        Bitmap thum = null;

        if (orgHeight > Constants.PIC_MAX_HEIGHT || orgWidth > Constants.PIC_MAX_WIDTH) {
            int heightRatio = Math.round(orgHeight / (float) Constants.PIC_MAX_HEIGHT);
            int widthRatio = Math.round(orgWidth / (float) Constants.PIC_MAX_WIDTH);
            int resizeScale = (heightRatio > widthRatio) ? heightRatio : widthRatio;
            opts.inSampleSize = resizeScale;
            opts.inJustDecodeBounds = false;
            thum = BitmapFactory.decodeFile(path, opts);
            FileOutputStream fos;
            try {
                File fout = new File(mCacheDir, "" + (mSeqNum++) + ".jpg");
                fos = new FileOutputStream(fout);
                thum.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                fos.flush();
                fos.close();
                path_big = fout.getAbsolutePath();
                Log.d(TAG, "[big img:" + thum.getWidth() + "x" + thum.getHeight() + "]" + path_big);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (orgHeight > Constants.THUMB_MAX_HEIGHT || orgWidth > Constants.THUMB_MAX_WIDTH) {
            double ratio = 1.0 * orgWidth/orgHeight;
            double w = Constants.THUMB_MAX_WIDTH;
            double h = Constants.THUMB_MAX_HEIGHT;
            double x = w/ratio;
            if (x > h) {
                w = h * ratio;
            }
            if (thum == null) {
                BitmapFactory.Options opt = new BitmapFactory.Options();
                thum = BitmapFactory.decodeFile(path, opt);
            }
            Bitmap t = ThumbnailUtils.extractThumbnail(thum, (int)w, (int)h);
            FileOutputStream fos;
            try {
                File fout = new File(mCacheDir, "" + (mSeqNum++) + ".jpg");
                fos = new FileOutputStream(fout);
                t.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                t.recycle();
                fos.flush();
                fos.close();
                path_small = fout.getAbsolutePath();
                Log.d(TAG, "[small img:" + (int)w + "x" + (int)h + "]" + path_small);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (thum != null) {
            thum.recycle();
        }

        PicPair pair = new PicPair();
        pair.mPic = new File(path_big);
        pair.mThumb = new File(path_small);
        return pair;
    }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = ((Activity) mContext).getMenuInflater();
	    inflater.inflate(R.menu.context_menu_delete, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    switch (item.getItemId()) {
	        case R.id.item_delete:
	            deletePicture(info.id);
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}

	private void deletePicture(long id) {
		if (id != mPicturesUri.size()) {
			mPicturesUri.remove((int)id);
			mAdapter.notifyDataSetChanged();
		}
	}



}
