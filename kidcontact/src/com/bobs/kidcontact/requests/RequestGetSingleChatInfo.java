package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.IdProxy;
import com.bobs.kidcontact.model.SingleChatInfo;

/**
 * Created by hzheng3 on 13-12-23.
 */
public final class RequestGetSingleChatInfo extends CookieGsonJsonRequest<SingleChatInfo> {
    public RequestGetSingleChatInfo(IdProxy input, Response.Listener<SingleChatInfo> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_INFO_FOR_SINGLE_CHAT),
                input, SingleChatInfo.class, listener, errorListener);
    }
}
