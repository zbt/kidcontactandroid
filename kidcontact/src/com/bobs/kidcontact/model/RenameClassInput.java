package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 2014/6/9.
 */
public class RenameClassInput {
    @SerializedName("newName")
    public String mNewName;
}
