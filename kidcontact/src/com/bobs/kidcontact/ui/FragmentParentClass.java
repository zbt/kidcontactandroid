package com.bobs.kidcontact.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.chat.service.BaiduInfoHolder;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.model.IdProxy;
import com.bobs.kidcontact.model.Like;
import com.bobs.kidcontact.model.Moment;
import com.bobs.kidcontact.model.Reply;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestAddMomentReply;
import com.bobs.kidcontact.requests.RequestDeleteMoment;
import com.bobs.kidcontact.requests.RequestGetMoments;
import com.bobs.kidcontact.requests.RequestGetMoments.MomentPage;
import com.bobs.kidcontact.requests.RequestLikeMoment;
import com.bobs.kidcontact.ui.view.CommentView;
import com.bobs.kidcontact.ui.view.MomentListView;
import com.bobs.kidcontact.ui.view.MomentPicturesView;
import com.bobs.kidcontact.ui.view.MomentView;
import com.bobs.kidcontact.ui.view.UndoBarController;
import com.bobs.kidcontact.utils.DebugLog;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.util.EncodingUtils;

import de.greenrobot.event.EventBus;

/**
 * Created by junyongl on 13-12-25. zhanglong
 */
public class FragmentParentClass extends FragmentBase implements FragmentAddReply.OnReplyPublishedListener, MomentListView.IXListViewListener {
    private static final String TAG = "FragmentParentClass";
    private static final String MOMENTS_CACHE_FILE = "_moments.cache";
    private static final int MSG_COUNT_MARK_LIMIT = 99;
    private ArrayList<Moment> mMoments = new ArrayList<Moment>();
    private Context mContext;
    private Boolean mIsInFlight = false;
    private long mLastId = 0;
    private long mLatestId = 0;
    private String mLastUpdate = null;
    private MomentAdapter mAdapter;
    private int mCurrentReplyPosition;
    private MomentListView mListView;
    private TextView mNewClassMsgCountMark;
    private TextView mNewLeaveCountMark;
    private int mDeletePosition;
    private boolean isConfirmDelete = true;
    private TextView mTextMomentNewNotify;
    private IChatService.Stub mChatServiceStub;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isConfirmDelete) {
                deleteMoment(msg.arg1);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        EventBus.getDefault().register(this);
        getChatServiceStubAsync();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_parent_class, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        
        mTextMomentNewNotify = (TextView) view.findViewById(R.id.text_moment_new_notify);
        mListView = (MomentListView) view.findViewById(R.id.list_class_moment);
        mListView.setXListViewListener(this);
        mListView.setPullLoadEnable(true);
        mListView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent arg1) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentAddReply fragment = (FragmentAddReply)fragmentManager.findFragmentByTag("reply");
                if (fragment != null){
                	FragmentTransaction transaction = fragmentManager.beginTransaction();
                	transaction.remove(fragment);
                	transaction.commit();
                }
                try{
	                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
	                boolean isOpen=imm.isActive(); 
	                if (isOpen == true){
	                	imm.hideSoftInputFromWindow(((Activity)v.getContext()).getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	                }
                }catch(Exception e){
                	DebugLog.d("InputMethod Close error.");
                }
                
				return false;
			}
		});

        View headView = mListView.getHeaderView();
//        mListView.addHeaderView(headView);
        mListView.setDivider(mContext.getResources().getDrawable(R.drawable.divider));
        headView.findViewById(R.id.class_calendar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityClassCalendar.class);
                startActivity(intent);
            }
        });
        headView.findViewById(R.id.class_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityClassMenu.class);
                startActivity(intent);
            }
        });
        TextView btnMember = (TextView) headView.findViewById(R.id.class_member);
        if (KidContactApp.getInstance().getRole() == Constants.TYPE_TEACHER) {
            btnMember.setText("请假记录");
        }
        btnMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (KidContactApp.getInstance().getRole() == Constants.TYPE_TEACHER) {
                    intent = new Intent(mContext, ActivityLeaveRequestsList.class);
                } else {
                    intent = new Intent(mContext, ActivityClassMember.class);
                }
                startActivity(intent);
            }
        });
        mNewClassMsgCountMark = (TextView) headView.findViewById(R.id.text_corner_mark_class_chat);
        mNewLeaveCountMark = (TextView) headView.findViewById(R.id.text_corner_mark_leave);
        (headView.findViewById(R.id.bt_class_chat_room)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityGroupChat.class);
                startActivity(intent);
            }
        });

        mAdapter = new MomentAdapter(mContext);
        
        mListView.setAdapter(mAdapter);
        if (mMoments.size()==0){
	        //Read cached first page 
	        Gson gson = new Gson();
	        String cache = readMomentsCache();
	        if (!cache.equals("")){
		        ArrayList<Moment> ms = (ArrayList<Moment>)gson.fromJson(cache, new TypeToken<ArrayList<Moment>>(){}.getType());
		        mLastId = ms.get(ms.size() - 1).mId;
		        mMoments.addAll(ms);
		        mAdapter.notifyDataSetChanged();
	        }
        }
        
        // Get the first page from server
        onRefresh();
    }

    @Override
    public void OnReplyPublished(Reply reply) {
        mMoments.get(mCurrentReplyPosition).mReplies.add(reply);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        mListView.setRefreshTime(KidContactApp.getSharePrefUtil().getLastRefreshClassTime());
//        if (KidContactApp.getSharePrefUtil().getHasNewMoment()) {
//            onRefresh();
//        }
    }

    public void onEvent(UpdateMomentUIEvent event) {
        onRefresh();
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        loadNextPage(0);
    }

    @Override
    public void onLoadMore() {
        loadNextPage(mLastId);
    }
    
    public void writeMomentsCache(String cache){ 
        try{ 
        	FileOutputStream fout = mContext.openFileOutput(MOMENTS_CACHE_FILE + String.valueOf(KidContactApp.getSharePrefUtil().getClassId()), mContext.MODE_PRIVATE);
         	byte [] bytes = cache.getBytes(); 
         	fout.write(bytes); 
         	fout.close(); 
        }
        catch(Exception e){
        	e.printStackTrace(); 
        } 
    }
    
    public String readMomentsCache(){
    	String cache = ""; 
    	Log.d("JOEYADD",String.valueOf(KidContactApp.getSharePrefUtil().getClassId()));
        try{ 
	        FileInputStream fin = mContext.openFileInput(MOMENTS_CACHE_FILE + String.valueOf(KidContactApp.getSharePrefUtil().getClassId())); 
	        int length = fin.available(); 
	        byte [] buffer = new byte[length]; 
	        fin.read(buffer);
	        cache = EncodingUtils.getString(buffer, "UTF-8"); 
	        fin.close();
        }catch(Exception e){
        	//Get Cache file exception
        }
        int rn = cache.indexOf("\r\n", 0);
        String cacheData = null;
        if (rn==-1){
        	return cache;
        }else{
        	mLastUpdate = cache.substring(0,rn);
        	cacheData = cache.substring(rn + 2);
        }
        return cacheData; 
    }

    public void loadNextPage(final long offsetId) {
        Log.d(TAG, "Load next page + from : " + offsetId);
        if (mIsInFlight) {
            return;
        }
        final RequestUtil<MomentPage> requestUtil = new RequestUtil<MomentPage>() {
            @Override
            public Request<MomentPage> onPrepareRequest(Response.Listener<MomentPage> listener, Response.ErrorListener errListener) {
            	if (offsetId == 0){
            		return new RequestGetMoments(offsetId, mLastUpdate, listener, errListener);
            	}
            	return new RequestGetMoments(offsetId, null, listener, errListener);
            }

            @Override
            public void onSuccess(MomentPage response) {
                mIsInFlight = false;
                mListView.stopRefresh();
                mListView.stopLoadMore();
                if (KidContactApp.getSharePrefUtil().getHasNewMoment()) {
                    KidContactApp.getSharePrefUtil().setHasNewMoment(false);
                }
                
                String refreshDate = new Date().toLocaleString();
                mListView.setRefreshTime(refreshDate);
                KidContactApp.getSharePrefUtil().setLastRefreshClassTime(refreshDate);
                if (response == null || response.mMomentsInPage.size() ==0) {
                    Toast.makeText(mContext, mContext.getString(R.string.no_more_data), Toast.LENGTH_SHORT).show();
                    onSuccessLast();
                    return;
                }

                ArrayList<Moment> moments = response.mMomentsInPage;
                if (moments != null && moments.size() > 0) {
                    if (offsetId == 0) {
                        if (mLatestId == moments.get(0).mId) {
                            Toast.makeText(mContext, mContext.getString(R.string.isTheLatestData), Toast.LENGTH_SHORT).show();
                            onSuccessLast();
                            return;
                        } else {
                            mLatestId = moments.get(0).mId;
                            mMoments.clear();
                        }
                        String strLastUpdate = moments.get(0).mLastUpdated;
                        for (Moment m : moments){
                        	if (m.mLastUpdated.compareTo(strLastUpdate)>0){
                        		strLastUpdate = m.mLastUpdated;
                        	}
                        }
                        //Write the first page to cache file
                        Gson gson = new Gson();
                        String js =  gson.toJson(moments);
                        mLastUpdate = strLastUpdate;
                        strLastUpdate = strLastUpdate + "\r\n";
                        writeMomentsCache(strLastUpdate+js);
                    }
                    mLastId = moments.get(moments.size() - 1).mId;
                }
                Log.d(TAG, "received Moment Page:");

                if (moments != null) {
                    Log.d(TAG, "\t num of moments = "+ moments.size());
                    fillMomentArray(response.mMomentsInPage);
                }
                onSuccessLast();
                
            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
                Log.e(TAG, RequestUtil.interpretErr2String(e));
                mIsInFlight = false;
                mListView.stopLoadMore();
                mListView.stopRefresh();
            }

            private void onSuccessLast() {
                // Handle database and notification on success
                mTextMomentNewNotify.setVisibility(View.GONE);
                if (null != mChatServiceStub) {
                    try {
                        mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_MOMENT_NEW);
                        mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_MOMENT_UPDATE);
                    } catch (RemoteException e) {
                        Log.e(TAG, "failed to delete CtrlMessages by type: CtrlMessage.TYPE_MOMENT_NEW " +
                                "and CtrlMessage.TYPE_MOMENT_UPDATE" + e.getMessage());
                    }
                }
            }
        };
        requestUtil.add2queue();
        mIsInFlight = true;
    }

    @Override
    protected void refreshViewsAsync() {
        super.refreshViewsAsync();
        new Thread(new Runnable() {

            @Override
            public void run() {
                if (null != ((ActionBarActivityBase) getActivity()).getChatServiceStubWait(1000)) {
                    FragmentParentClass.this.mEventHandlerForView.sendEmptyMessage(Events.EVENT_CHAT_SERVICE_BOUND);
                } else {
                    Log.w(TAG, "failed to get chat service stub!");
                }
            }

        }).start();
    }

    @Override
    protected void refreshViews() {
        refreshNewClassMsgCountMark();
        refreshNewLeaveCountMark();
        refreshMomentNotify();
    }

    private void refreshMomentNotify() {
        // When we come here the mChatServiceStub is OK
        int momentUpdateCount = 0;
        try {
            momentUpdateCount = mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_MOMENT_NEW);
        } catch (RemoteException e) {
            Log.e(TAG, "failed to get CtrlMessage count with type: CtrlMessage.TYPE_MOMENT_NEW." + e.getMessage());
        }
        if (momentUpdateCount > 0) {
            mTextMomentNewNotify.setVisibility(View.VISIBLE);
        } else {
            mTextMomentNewNotify.setVisibility(View.GONE);
        }
    }

    /** Get the IChatService.Stub asynchronously */
    private void getChatServiceStubAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mChatServiceStub = ((ActionBarActivityBase) FragmentParentClass.this.getActivity()).getChatServiceStubWait(1000);
                if (null == mChatServiceStub) {
                    Log.w(TAG, "failed to get chat service stub!");
                } else {
                    Log.i(TAG, "bound to local service successfully.");
                }
            }
        }).start();
    }

    /** Refresh the new class msg count mark */
    private void refreshNewClassMsgCountMark() {
        int count = 0;
        try {
            IChatService.Stub stub = ((ActionBarActivityBase) getActivity()).getChatServiceStub();
            if (null != stub) {
                count = stub.getClassNewMsgCount();
            } else {
                Log.e(TAG, "failed to get the chat service stub!");
            }
        } catch (RemoteException e) {
            Log.e(TAG, "failed to get the new message count of the class: " + BaiduInfoHolder.getGroupTag());
            e.printStackTrace();
        }
        Log.d(TAG, "FragmentParentClass.refreshNewClassMsgCountMark(): count: " + count);
        if (count > 0 && count <= MSG_COUNT_MARK_LIMIT) {
            mNewClassMsgCountMark.setText("" + count);
            mNewClassMsgCountMark.setVisibility(View.VISIBLE);
        } else if (count > MSG_COUNT_MARK_LIMIT) {
            mNewClassMsgCountMark.setText("...");
            mNewClassMsgCountMark.setVisibility(View.VISIBLE);
        } else {
            mNewClassMsgCountMark.setText("");
            mNewClassMsgCountMark.setVisibility(View.INVISIBLE);
        }
    }
    
    /** Refresh the new leave count mark */
    private void refreshNewLeaveCountMark() {
        if (KidContactApp.getInstance().getRole() != Constants.TYPE_TEACHER) {
            return;
        }
        int count = 0;
        try {
            IChatService.Stub stub = ((ActionBarActivityBase) getActivity()).getChatServiceStub();
            if (null != stub) {
                count = stub.getCtrlMsgCountByType(CtrlMessage.TYPE_LEAVE_NEW);
            } else {
                Log.e(TAG, "failed to get the chat service stub!");
            }
        } catch (RemoteException e) {
            Log.e(TAG, "failed to get the new message count of the class: " + BaiduInfoHolder.getGroupTag());
            e.printStackTrace();
        }
        Log.d(TAG, "FragmentParentClass.refreshNewClassMsgCountMark(): count: " + count);
        if (count > 0 && count <= MSG_COUNT_MARK_LIMIT) {
            mNewLeaveCountMark.setText("" + count);
            mNewLeaveCountMark.setVisibility(View.VISIBLE);
        } else if (count > MSG_COUNT_MARK_LIMIT) {
            mNewLeaveCountMark.setText("...");
            mNewLeaveCountMark.setVisibility(View.VISIBLE);
        } else {
            mNewLeaveCountMark.setText("");
            mNewLeaveCountMark.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void refreshViews(Conversation conversation) {
        super.refreshViews(conversation);
        if (null == conversation) {
            return;
        }

        // Update new class message count mark
        int count = 0;
        if (conversation.getGroupChat()) {
            count = conversation.getNewMsgCount();
        }
        if (count > 0 && count <= MSG_COUNT_MARK_LIMIT) {
            mNewClassMsgCountMark.setText("" + count);
            mNewClassMsgCountMark.setVisibility(View.VISIBLE);
        } else if (count > MSG_COUNT_MARK_LIMIT) {
            mNewClassMsgCountMark.setText("...");
            mNewClassMsgCountMark.setVisibility(View.VISIBLE);
        } else {
            mNewClassMsgCountMark.setText("");
            mNewClassMsgCountMark.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void refreshViews(CtrlMessage msg) {
        super.refreshViews(msg);
        // We care about only CtrlMessage.TYPE_NOTICE_NEW and CtrlMessage.TYPE_NOTICE_UPDATE
        if (CtrlMessage.TYPE_MOMENT_NEW == msg.getType()) {
            mTextMomentNewNotify.setVisibility(View.VISIBLE);
        } else if (CtrlMessage.TYPE_LEAVE_NEW == msg.getType()) {
            refreshNewLeaveCountMark(); // we may have more than one
        }
        // TODO CtrlMessage.TYPE_NOTICE_UPDATE
    }

    private void fillMomentArray(ArrayList<Moment> moments) {
        if (moments == null || moments.isEmpty()) return;
        mMoments.addAll(moments);
        mAdapter.notifyDataSetChanged();

    }

    private void deleteMoment(final long id) {
        RequestUtil<CookieGsonRequest.Succeed> requestUtil = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                IdProxy idProxy = new IdProxy();
                idProxy.mId = id;
                return new RequestDeleteMoment(idProxy, listener, errListener);
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {

            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
            }
        };
        requestUtil.add2queue();
    }

    private class MomentAdapter extends BaseAdapter {
        private Context mContext;

        private MomentAdapter(Context mContext) {
            this.mContext = mContext;
        }

        @Override
        public int getCount() {
            return mMoments.size();
        }

        @Override
        public Object getItem(int position) {
            return mMoments.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            final Moment moment = (Moment) getItem(position);
//            if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_class_moment, parent, false);
            vh = new ViewHolder();
            vh.momentView = (MomentView) convertView.findViewById(R.id.moment_view);
            vh.addComment = (ImageView) convertView.findViewById(R.id.add_comment);
            vh.picturesView = (MomentPicturesView) convertView.findViewById(R.id.moment_pictures);
            vh.delete = (TextView) convertView.findViewById(R.id.delete);
            vh.addLike = (TextView) convertView.findViewById(R.id.add_favor);
            vh.likeCount= (TextView) convertView.findViewById(R.id.like_count);
            ArrayList<Reply> comments = moment.getReplies();
            ArrayList<Like> likes = moment.mLikes;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final float scale = mContext.getResources().getDisplayMetrics().density;
            int marginPixels = (int) (8 * scale + 0.5f);
                params.setMargins(marginPixels, 0, marginPixels, marginPixels);
            if (moment.mLikeCount > 0) {
                vh.likeCount.setText("+" + moment.mLikeCount);
                vh.likeCount.setVisibility(View.VISIBLE);
                if (isLiked(likes)) {
                    vh.addLike.setEnabled(false);
                }
            }
            CommentView commentView = null;
            if (comments != null && comments.size() > 0) {
                commentView = new CommentView(mContext, null);
                commentView.bind(comments);
            }
            if (likes != null && likes.size() > 0) {
                if (commentView == null) {
                    commentView = new CommentView(mContext, null);
                }
                commentView.bindLikes(likes);
            }
            if (commentView != null) {
                ((LinearLayout)convertView).addView(commentView, params);
            }
            vh.addLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addLikeToMoment(moment.mId, position);
                }
            });

                vh.addComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentAddReply fragment = (FragmentAddReply)fragmentManager.findFragmentByTag("reply");
                        if (fragment == null) {
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragment = new FragmentAddReply();
                            fragmentTransaction.add(R.id.comment_bar, fragment, "reply");
                            fragmentTransaction.commit();
                        }
                        fragment.setReplyRequest(moment.mId, RequestAddMomentReply.class);
                        fragment.setReplyPublishListener(FragmentParentClass.this);
                        mCurrentReplyPosition = position;
                    }
                });
                convertView.setTag(vh);
//            } else {
//                vh = (ViewHolder) convertView.getTag();
//            }
            if (moment.mCreatorId == KidContactApp.getInstance().getId()) {
                vh.delete.setVisibility(View.VISIBLE);
                vh.delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new UndoBarController.UndoBar((Activity)mContext)
                                .message("删除班级圈记录")
                                .listener(new UndoBarController.UndoListener() {
                                    @Override
                                    public void onUndo(Parcelable parcelable) {
                                        isConfirmDelete = false;
                                        Moment moment = (Moment)parcelable;
                                        mMoments.add(position, moment);
                                        mAdapter.notifyDataSetChanged();
                                    }
                                })
                                .token(moment)
                                .show();
                        Message msg = new Message();
                        msg.arg1 = moment.mId;
                        mDeletePosition = position;
                        mMoments.remove(position);
                        mAdapter.notifyDataSetChanged();
                        mHandler.sendMessageDelayed(msg, 6000);
                    }
                });

            }
            vh.momentView.bind(moment);
            vh.picturesView.setPictures(moment.getPictures());
            return convertView;
        }



        private final class ViewHolder {
            MomentView momentView;
            ImageView addComment;
            MomentPicturesView picturesView;
            TextView delete;
            TextView addLike;
            TextView likeCount;
        }
    }

    private boolean isLiked(ArrayList<Like> likes) {
        for(Like like : likes) {
            if (like.mCreatorId == KidContactApp.getInstance().getId()) {
                return true;
            }
        }
        return false;
    }

    private void addLikeToMoment(final long id, final int position) {
        RequestUtil<Like> addLikeRequest = new RequestUtil<Like>() {
            @Override
            public Request<Like> onPrepareRequest(Response.Listener<Like> listener, Response.ErrorListener errListener) {
                IdProxy input = new IdProxy();
                input.mId = id;
                return new RequestLikeMoment(input, listener, errListener);
            }

            @Override
            public void onSuccess(Like response) {
                mMoments.get(position).mLikeCount++;
                mMoments.get(position).mLikes.add(response);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
                DebugLog.e(e.toString());
            }
        };
        addLikeRequest.add2queue();
    }

}
