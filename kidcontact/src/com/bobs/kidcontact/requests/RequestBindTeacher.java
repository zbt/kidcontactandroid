package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 13-12-23.
 */
public final class RequestBindTeacher extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestBindTeacher(BindTeacher input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_BIND_TEACHER),
                input, Succeed.class, listener, errorListener);
    }

    public static class BindTeacher {
        @SerializedName("teacherId")
        public String mTeacherId;
        @SerializedName("name")
        public String mName;
    }
}
