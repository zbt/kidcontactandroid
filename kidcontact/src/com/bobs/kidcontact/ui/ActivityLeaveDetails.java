package com.bobs.kidcontact.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Leave;
import com.bobs.kidcontact.model.Reply;
import com.bobs.kidcontact.requests.RequestAddInfoId;
import com.bobs.kidcontact.requests.RequestAddLeaveReply;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.Gson;

/**
 * Created by junyongl on 14-2-13.
 */
public class ActivityLeaveDetails extends ActionBarActivityBase {

    private static final String TAG = "ActivityLeaveDetails";
    private Context mContext;
    private Leave mLeave;
    private TextView mTitle;
    private TextView mAuthor;
    private TextView mCreatedTime;
    private TextView mContent;
    private TextView mDateRange;
    private NetworkRoundImageView mAvatar;
    private TextView mResult;
    private LinearLayout mActBar;
    private Button mBtnAllow;
    private Button mBtnDecline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtil.setThemeForUser(this);
        setContentView(R.layout.activity_leave_details);
        mContext = this;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        Gson gson = new Gson();
        mLeave = gson.fromJson(bundle.getString("data-leave"), Leave.class);

        mTitle = (TextView)findViewById(R.id.tv_leave_title);
        mAuthor = (TextView) findViewById(R.id.notice_author);
        mCreatedTime = (TextView) findViewById(R.id.notice_created_time);
        mContent = (TextView) findViewById(R.id.tv_leave_content);
        mAvatar = (NetworkRoundImageView) findViewById(R.id.author_avatar);
        mDateRange = (TextView) findViewById(R.id.tv_date_range);
        mResult = (TextView) findViewById(R.id.result_text);
        mActBar = (LinearLayout) findViewById(R.id.layout_action);
        mBtnAllow = (Button) findViewById(R.id.bt_allow);
        mBtnDecline = (Button) findViewById(R.id.bt_decline);
        fillData();
        mBtnAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addLeaveReply("YES");
            }
        });
        mBtnDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addLeaveReply("NO");
            }
        });
    }

    private void addLeaveReply(final String action) {
        final RequestAddInfoId.InfoId info = new RequestAddInfoId.InfoId();
        info.mId = mLeave.mId;
        info.mInfo = action;
        RequestUtil<Reply> request = new RequestUtil<Reply>() {
            @Override
            public Request<Reply> onPrepareRequest(Response.Listener<Reply> listener, Response.ErrorListener errListener) {
                return new RequestAddLeaveReply(info, listener, errListener);
            }

            @Override
            public void onSuccess(Reply response) {
                disableActBtns();
                mResult.setVisibility(View.VISIBLE);
                mResult.setText(getLeaveStatus(response.mInfo));
            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
                Log.e(TAG, "" + RequestUtil.interpretErr2Res(e));
            }
        };
        request.add2queue();
    }

    private void disableActBtns() {
        mBtnAllow.setEnabled(false);
        mBtnDecline.setEnabled(false);
    }

    private void fillData() {
        if (mLeave.mTitle != null) {
            mTitle.setText(mLeave.mTitle);
        }

        if (mLeave.mCreatorName != null) {
            mAuthor.setText(mLeave.mCreatorName);
        }

        if (mLeave.mInfo != null) {
            Leave.LeaveContent leaveContent = new Gson().fromJson(mLeave.mInfo, Leave.LeaveContent.class);
            if (leaveContent.mLeaveMessage != null) {
                mContent.setText(leaveContent.mLeaveMessage);
            }
            if (leaveContent.mStartDate != null && leaveContent.mEndDate != null) {
                String dateRange = leaveContent.mStartDate.equals(leaveContent.mEndDate) ? leaveContent.mStartDate :
                        (leaveContent.mStartDate + " - " + leaveContent.mEndDate);
                mDateRange.setText(dateRange);
            }
        }

        if (mLeave.mDateCreated != null) {
            mCreatedTime.setText(UIUtil.dateToString(UIUtil.stringToDate(mLeave.mDateCreated,
                    Constants.DATA_DATE_FORMAT), mContext.getResources().
                    getString(R.string.notice_created_time_string_fromat)));
        }
        mAvatar.setImageUrl(Constants.getAvatarThumbURLOfCreator(mLeave.mCreatorId),
                KidContactApp.getInstance().getImageLoader());

        switch (KidContactApp.getInstance().getRole()) {
            case Constants.TYPE_TEACHER:
                if (mLeave.mReplies.size() > 0) {
                    mResult.setVisibility(View.VISIBLE);
                    mResult.setText(getLeaveStatus(mLeave.mReplies.get(0).mInfo));
                    disableActBtns();
                }
                break;
            case Constants.TYPE_PARENT:
                mActBar.setVisibility(View.GONE);
                if (mLeave.mReplies.size() > 0) {
                    mResult.setVisibility(View.VISIBLE);
                    mResult.setText(getLeaveStatus(mLeave.mReplies.get(0).mInfo));
                }
                break;
            default:
                break;
        }
    }

    private String getLeaveStatus(String info) {
        if (info.equals("YES")) {
            return getString(R.string.request_accepted);
        } else if (info.equals("NO")) {
            return getString(R.string.request_rejected);
        } else {
            return getString(R.string.unknown_status);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
