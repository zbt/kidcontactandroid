package com.bobs.kidcontact.chat.service;

import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
interface IChatService {
    List<Conversation> getAllConversations();

    List<ChatMessage> getMessages(long conversationId, long offset, long limit);

    long getMaxMessageId(in Conversation conversation);

    // Return value contains an unique identifier which will be used in callback to indicate fail or succeed
    // UI use this method to try to send message, service should synchronously insert the message into database
    // and return, once the message is successfully or failed to be sent out, service will send out broadcast
    // UI could use the broadcast to update UI
    ChatMessage sendMessage(in CloudMessage cloudMsg);

    Conversation getConversationByUserId(long userId);

    Conversation getConversationByGroupId(String groupId);

    void leaveGroups(in List<String> groupIds);

    void joinGroups(in List<String> groupIds);

    void onNotifyNewUser();

    void setChatStatus(boolean singleChat, boolean chatting, long userId, String groupId);

    void cancelNotification();

    void clearNotificationCache();

    boolean clearNewMsgCount(boolean singleChat, long userId, String groupId);

    int getClassNewMsgCount();

    int getAllSingleChatNewMsgCount();

    boolean deleteCtrlMsgsByType(int type);

    int getCtrlMsgCountByType(int type);

    boolean deleteGroupChatMsg(String groupId, long msgId);

    boolean deleteSingleChatMsg(long toUserId, long msgId);
}