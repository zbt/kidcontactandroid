package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.AssignmentZip;
import com.bobs.kidcontact.model.IdProxy;

/**
 * Created by hzheng3 on 14-4-16.
 */
public final class RequestZipAssignment extends RequestLikeId<AssignmentZip> {
    public RequestZipAssignment(IdProxy input, Response.Listener<AssignmentZip> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ZIP_ASSIGNMENT),
                input, AssignmentZip.class, listener, errorListener);
    }
}
