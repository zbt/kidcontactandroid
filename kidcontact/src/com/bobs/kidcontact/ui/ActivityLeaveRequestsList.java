package com.bobs.kidcontact.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteException;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.model.Leave;
import com.bobs.kidcontact.model.LeaveList;
import com.bobs.kidcontact.requests.RequestGetLeaves;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by junyongl on 14-2-12.
 */
public class ActivityLeaveRequestsList extends ActionBarActivityBase {
    private static final long REQUEST_TIMEOUT = 5000;
    private static final String TAG = "ActivityLeaveRequestsList";
    private ListView mListView;
    private LeaveRequestAdapter mAdapter;
    private ArrayList<Leave> mLeaves = new ArrayList<Leave>();
    private int mLastId;
    private RefreshLeaves mReq;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtil.setThemeForUser(this);
        mContext = this;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_leave_requests_list);
        mListView = (ListView) findViewById(R.id.list_leave_request);
        mAdapter = new LeaveRequestAdapter();
        mListView.setAdapter(mAdapter);
        mListView.setDivider(null);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(mContext, ActivityLeaveDetails.class);
                Gson gson = new Gson();
                String leaveString = gson.toJson(mLeaves.get(position), Leave.class);
                intent.putExtra("data-leave", leaveString);
                startActivity(intent);
            }
        });
    }
    
    @Override
    protected void onGetChatServiceStub(IChatService.Stub stub) {
        // We need to clear all "leave-update" CtrlMessage
        try {
            stub.deleteCtrlMsgsByType(CtrlMessage.TYPE_LEAVE_NEW);
            stub.deleteCtrlMsgsByType(CtrlMessage.TYPE_LEAVE_UPDATE);
        } catch (RemoteException e) {
            Log.e(TAG, "failed to delete CtrlMessage with the type: CtrlMessage.TYPE_LEAVE_UPDATE.");
        }
    }

    public class LeaveRequestAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mLeaves.size();
        }

        @Override
        public Object getItem(int position) {
            return mLeaves.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_leave_request, parent, false);
                vh = new ViewHolder();
                vh.viewRequestTitle = (TextView) convertView.findViewById(R.id.request_title);
                vh.viewRequestContent = (TextView) convertView.findViewById(R.id.request_content);
                vh.viewRequestStatus = (TextView) convertView.findViewById(R.id.request_status);
                convertView.setTag(vh);
            } else {
                vh = (ViewHolder) convertView.getTag();
            }
            Leave leave = (Leave) getItem(position);
            vh.viewRequestTitle.setText(leave.mTitle);
            Leave.LeaveContent leaveContent = new Gson().fromJson(leave.mInfo, Leave.LeaveContent.class);
            if (leaveContent.mStartDate != null && leaveContent.mEndDate != null) {
                String dateRange = leaveContent.mStartDate.equals(leaveContent.mEndDate) ? leaveContent.mStartDate :
                        (leaveContent.mStartDate + " - " + leaveContent.mEndDate);
                vh.viewRequestContent.setText("请假时间：" + dateRange);
            }
            if (leave.mReplies.size() > 0) {
                vh.viewRequestStatus.setText(leave.mReplies.get(0).mInfo);
            } else {
                vh.viewRequestStatus.setText("新");
            }
            if (position >= (getCount() - 1)) {
                requestLoadMore();
            }
            return convertView;
        }

        private class ViewHolder {
            TextView viewRequestTitle;
            TextView viewRequestContent;
            TextView viewRequestStatus;

        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mReq != null) {
            mReq.cancel();
            mReq = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        requestRefresh();
    }

    private void requestRefresh() {
        Log.d(TAG, "requestRefresh");
        if (mReq != null) {
            if (mLastId != 0) {
                mReq.cancel();
                mReq = null;
            }
        }
        if (mReq == null) {
            mLastId = 0;
            mReq = new RefreshLeaves(0);
            mReq.add2queue();
        }
    }

    private void requestLoadMore() {
        Log.d(TAG, "requestLoadMore");
        if (mReq != null) {
            if (mLastId == 0) {
                mReq.cancel();
                mReq = null;
            }
        }
        if (mReq == null) {
            mReq = new RefreshLeaves(mLastId);
            mReq.add2queue();
        }
    }

    private class RefreshLeaves extends RequestUtil<LeaveList> {
        private final int mId;

        public RefreshLeaves(int id) {
            super(REQUEST_TIMEOUT);
            mId = id;
        }

        @Override
        public Request<LeaveList> onPrepareRequest(Response.Listener<LeaveList> listener, Response.ErrorListener errListener) {
            return new RequestGetLeaves(mId, listener, errListener);
        }

        @Override
        public void onSuccess(LeaveList response) {
            if (mId == 0) {
                mLeaves.clear();
            }
            int cnt = response.mLeaves.size();
            if (cnt > 0) {
                mLastId = response.mLeaves.get(cnt -1).mId;
                mLeaves.addAll(response.mLeaves);
                mAdapter.notifyDataSetChanged();
            }
            mReq = null;
        }

        @Override
        public void onError(VolleyError e) {
            super.onError(e);
            Log.e(TAG, "" + RequestUtil.interpretErr2Res(e));
            mReq = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_leave_request:
                Intent intent = new Intent(mContext, ActivityLeaveRequest.class);
                startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_leave_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
