package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.IdProxy;

/**
 * Created by hzheng3 on 14-6-9.
 */
public final class RequestSwitchClass extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestSwitchClass(IdProxy input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_SWITCH_CLASS),
                input, Succeed.class, listener, errorListener);
    }
}
