package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by hzheng3 on 14-1-19.
 */
public class ChildList {
    @SerializedName("children")
    public ArrayList<Child> mChildren;

    public ArrayList<Child> getChildren() {
        if (mChildren.size() > 1) {
            Collections.sort(mChildren, new Child.ChildComparator());
        }
        return mChildren;
    }


}
