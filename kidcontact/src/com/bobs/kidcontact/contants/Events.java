package com.bobs.kidcontact.contants;

/**
 * @author zhanglong
 */
public class Events {
    public static final int EVENT_CONVERSATION_CHANGED = 0;
    public static final String KEY_CONVERSATION_CHANGED = "key_conversation_changed";
    public static final int EVENT_NOTICE_NEW = 1;
    public static final String KEY_NOTICE_NEW = "key_notice_new";
    public static final int EVENT_COMMENT_NEW = 2;
    public static final String KEY_COMMENT_NEW = "key_comment_new";
    public static final int EVENT_MOMENT_NEW = 3;
    public static final String KEY_MOMENT_NEW = "key_moment_new";
    public static final int EVENT_VERSION_NEW = 4;
    public static final String KEY_VERSION_NEW = "key_version_new";
    public static final int EVENT_CHAT_SERVICE_BOUND = 5;
    public static final String KEY_CHAT_SERVICE_BOUND = "key_chat_service_bound";
    public static final int EVENT_NETWORK_REQUEST_ERROR = 6;
    public static final String KEY_NETWORK_REQUEST_ERROR = "key_network_request_error";
    public static final int EVENT_REMOTE_LOGIN = 7;
    public static final String KEY_REMOTE_LOGIN = "key_remote_login";
    public static final int EVENT_CTRL_MSG_NEW = 8;
    public static final String KEY_CTRL_MSG = "key_ctrl_msg";
    public static final int EVENT_CTRL_MSG_DELETED = 9;
    public static final int EVENT_VERSION_UPDATED = 10;
    public static final int EVENT_VERSION_UPDATE_CANCELED = 11;
}
