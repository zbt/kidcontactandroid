package com.bobs.kidcontact;

public class Common {
    public static String getLastTrace() {
        Throwable e = new Throwable();
        StackTraceElement[] stackTrace = e.getStackTrace();
        String result = null;
        if (stackTrace.length > 1) {
            result = stackTrace[1].getClassName() + "." +
                     stackTrace[1].getMethodName() + "()";
        }
        return result;
    }

    public static void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    public static class SharedBoolean {
    	private Boolean mValue = false;
    	private Object mLock = new Object();
    	
    	public SharedBoolean(boolean defaultValue) {
    		mValue = defaultValue;
    	}
    	
    	public void setValue(boolean value) {
    		synchronized (mLock) {
    			mValue = value;
    		}
    	}
    	
    	public boolean getValue() {
    		synchronized (mLock) {
    			return mValue;
    		}
    	}
    }
}
