package com.bobs.kidcontact.chat.ui;

import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.model.User;
import com.bobs.kidcontact.ui.ActionBarActivityBase;
import com.bobs.kidcontact.ui.FragmentBase;
import com.bobs.kidcontact.ui.view.SlidingTabLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by junyongl on 14-2-24. zhanglong
 */
public class FragmentTabsChat extends FragmentBase {
    private static final String TAG = "FragmentTabsChat";
    private static final String KEY_TO_USERS = "key_to_users_list";
    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private ArrayList<User> mToUsers;
    private Map<Long, Integer> mUserIdTabMap = new HashMap<Long, Integer>();
    private Map<Integer, TextView> mTabCornerMarkMap = new HashMap<Integer, TextView>();
    private IChatService.Stub mChatServiceStub = null;

    public static FragmentTabsChat newInstance(ArrayList<User> users) {

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(KEY_TO_USERS, users);
        FragmentTabsChat fragment = new FragmentTabsChat();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToUsers = getArguments().getParcelableArrayList(KEY_TO_USERS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tabs_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new ChatFragmentPageAdapter(getChildFragmentManager()));
        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.fragment_tab, R.id.fragment_tab_text, R.id.fragment_tab_corner_mark);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    @Override
    public void onResume() {
        super.onResume();
        mTabCornerMarkMap = mSlidingTabLayout.getTabTitleMap();
        if (null == mTabCornerMarkMap) {
            Log.w(TAG, "failed to get corner marks!");
        }
    }

    @Override
    protected void refreshViewsAsync() {
        super.refreshViewsAsync();
        new Thread(new Runnable() {

            @Override
            public void run() {
                mChatServiceStub = ((ActionBarActivityBase) getActivity()).getChatServiceStubWait(1000);
                if (null != mChatServiceStub) {
                    FragmentTabsChat.this.mEventHandlerForView.sendEmptyMessage(Events.EVENT_CHAT_SERVICE_BOUND);
                } else {
                    Log.w(TAG, "failed to get chat service stub!");
                }
            }

        }).start();
    }

    @Override
    public void refreshViews(Conversation conversation) {
        super.refreshViews(conversation);
        if (null == conversation || conversation.getGroupChat()) {
            return;
        }
        long id = conversation.getUserId();
        if (!mUserIdTabMap.containsKey(id)) {
            return;
        }
        int conversationTab = mUserIdTabMap.get(id);
        if (conversationTab == mSlidingTabLayout.getCurrentTab() && conversation.getNewMsgCount() > 0) {
            if (null != mChatServiceStub) {
                try {
                    mChatServiceStub.clearNewMsgCount(true, conversation.getUserId(), "");
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            return;
        }
        int count = conversation.getNewMsgCount();
        TextView cornerMark = mTabCornerMarkMap.get(conversationTab);
        if (count > 0) {
            cornerMark.setVisibility(View.VISIBLE);
        } else {
            cornerMark.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void refreshViews() {
        super.refreshViews();
        if (null == mChatServiceStub) {
            Log.w(TAG, "chat service not bound!");
            return;
        }
        List<Conversation> conversations = null;
        try {
            conversations = mChatServiceStub.getAllConversations();
        } catch (RemoteException e) {
            e.printStackTrace();
            return;
        }
        if (null == conversations) {
            Log.w(TAG, "failed to get conversations!");
            return;
        }
        for (int i = 0; i < conversations.size(); i++) {
            refreshViews(conversations.get(i));
        }
    }

    class ChatFragmentPageAdapter extends FragmentPagerAdapter {

        public ChatFragmentPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return FragmentChat.newInstance(mToUsers.get(i));
        }

        @Override
        public int getCount() {
            return mToUsers.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            User user = mToUsers.get(position); // alias
            mUserIdTabMap.put(user.mId, position);
            return user.mName;
        }
    }
}
