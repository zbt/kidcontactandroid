package com.bobs.kidcontact.chat.service;

import java.lang.ref.WeakReference;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.Common.SharedBoolean;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestHeartBeat;
import com.bobs.kidcontact.utils.RequestUtil;

/**
 * We have heartbeat only when our app is in foreground.
 * @author zhanglong
 */
public class HeartbeatExecutor {
    private static final String TAG = "HeartbeatExecutor";
    private Thread mExeThread = null;
    private SharedBoolean mStopThread = new SharedBoolean(false);
    private Handler mHeartbeatHandler;
    private static HeartbeatExecutor mSingleExecutor;

    private HeartbeatExecutor() {
        mHeartbeatHandler = new HeartbeatHandler(this);
    }

    public static HeartbeatExecutor getInstance() {
        if (null == mSingleExecutor) {
            mSingleExecutor = new HeartbeatExecutor();
        }
        return mSingleExecutor;
    }

    /** Start heart beat loop. */
    public void execute() {
        if (null != mExeThread) {
            Log.w(TAG, Common.getLastTrace() + ": Heartbeat loop already started!");
            return;
        }
        mStopThread.setValue(false);
        mExeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!mStopThread.getValue()) {
                    if (CloudMessageManager.isAppForeground()) {
                        mHeartbeatHandler.sendEmptyMessage(HeartbeatHandler.MSG_REQUEST_HEARTBEAT);
                    }
                    Common.sleep(Constants.HEARTBEAT_PERIOD * 1000); // a heartbeat every Constants.HEARTBEAT_PERIOD seconds
                }
            }
        });
        mExeThread.start();
    }

    /** Stop heart beat loop synchronously. */
    public void stopSync() {
        if (null == mExeThread) {
            Log.w(TAG, Common.getLastTrace() + ": Heartbeat loop already stopped!");
            return;
        }
        mStopThread.setValue(true);
        try {
            mExeThread.join();
        } catch (InterruptedException e) {
            Log.e(TAG, "Heartbeat thread wait innterrupted: " + e.getMessage());
            return;
        }
        mExeThread = null;
    }

    /** Stop heart beat loop asynchronously. */
    public void stopAsync() {
        if (null == mExeThread) {
            Log.w(TAG, Common.getLastTrace() + ": Heartbeat loop already stopped!");
            return;
        }
        mStopThread.setValue(true);
    }

    /** Send a single heart beat request to the server. */
    private void requestHHeartbeat() {
        RequestUtil<CookieGsonRequest.Succeed> requestUtil = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                return new RequestHeartBeat(listener, errListener);
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {
                Log.d(TAG, "heartbeat...");
            }

            @Override
            public void onError(VolleyError e) {
                Log.w(TAG, Common.getLastTrace() + ": " + RequestUtil.interpretErr2Res(e));
            }
        };
        requestUtil.add2queue();
    }

    /** A handler used by heart beat thread. */
    static class HeartbeatHandler extends Handler {
        public static final int MSG_REQUEST_HEARTBEAT = 0;
        private WeakReference<HeartbeatExecutor> mExecutor;

        public HeartbeatHandler(HeartbeatExecutor executor) {
            mExecutor = new WeakReference<HeartbeatExecutor>(executor);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
            case MSG_REQUEST_HEARTBEAT:
                mExecutor.get().requestHHeartbeat();
                break;
            default:
                Log.w(TAG, Common.getLastTrace() + ": unknow message!");
                break;
            }
        }

    }
}
