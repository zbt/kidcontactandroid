package com.bobs.kidcontact.ui;

import java.net.HttpURLConnection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.baidu.frontia.FrontiaApplication;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestLogout;
import com.bobs.kidcontact.requests.LoginRequest.Login;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus.BoundStatus;
import com.bobs.kidcontact.utils.CrashLogger;
import com.bobs.kidcontact.utils.LoginUtil;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.SharePrefUtil;
import com.bobs.kidcontact.utils.UIUtil;
import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

public class KidContactApp extends Application {
    private static KidContactApp sApplication = null;
    public static final String SET_COOKIE_KEY = "Set-Cookie";
    private static final String COOKIE_KEY = "Cookie";
    //private static final String SESSION_COOKIE = "sessionid";
    private static final String JSESSION_COOKIE = "JSESSIONID";
    private static final String REMEMBER_ME_COOKIE = "grails_remember_me";
    public static final String PLUS = "&";
    private static final String TAG = "KidContactApp";

    private String mJsessionCookie;
    private String mRememberMeCookie;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private ImageLoader.ImageCache mImageCache;
    private final LruCache<String, Bitmap> mImageCacheLRU = new LruCache<String, Bitmap>(20);
    private Context mContext;
    private String mCloudId;

    // Baidu Push related below
    public final static String API_KEY = "sL8apC26Hkde2fHwctoxw9EE";
    public final static String SECRIT_KEY = "rwKPoAGvIF1urB2XN34az14bTVWUpPq0";
    public final static String APP_TAG_STRING = "KidContact";
    public static final String SP_FILE_NAME = "push_msg_sp";
    public static final int[] heads = { R.drawable.h0, R.drawable.h1,
            R.drawable.h2, R.drawable.h3, R.drawable.h4, R.drawable.h5,
            R.drawable.h6, R.drawable.h7, R.drawable.h8, R.drawable.h9,
            R.drawable.h10, R.drawable.h11, R.drawable.h12, R.drawable.h13,
            R.drawable.h14, R.drawable.h15, R.drawable.h16, R.drawable.h17,
            R.drawable.h18 };
    public static final int NUM_PAGE = 6;// 总共有多少页
    public static int NUM = 20;// 每页20个表情,还有最后一个删除button
    private Map<String, Integer> mFaceMap = new LinkedHashMap<String, Integer>();
    private String mCurrentUserId;

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FrontiaApplication.initFrontiaApplication(this);
        Thread.setDefaultUncaughtExceptionHandler(new CrashLogger(this));
        HttpURLConnection.setFollowRedirects(false);
        mContext = this;

        sApplication = this;
        mRequestQueue = Volley.newRequestQueue(this);

        mImageCache = new ImageLoader.ImageCache() {
            @Override
            public void putBitmap(String key, Bitmap value) {
                KidContactApp.this.mImageCacheLRU.put(key, value);
            }

            @Override
            public Bitmap getBitmap(String key) {
                return KidContactApp.this.mImageCacheLRU.get(key);
            }

            @Override
            public void removeBitmap(String key) {
                Log.i("ImageLoader", "RemoveBitmap " + key);
                KidContactApp.this.mImageCacheLRU.remove(key);
                //KidContactApp.this.mImageCacheLRU.evictAll();
            }
        };

        mImageLoader = new ImageLoader(mRequestQueue, mImageCache);

        // baidu push
        initFaceMap();
    }

    /**
     * @return instance of kidcontact application
     */
    public static KidContactApp getInstance() {
        return sApplication;
    }

    public void setCloudId(String cloudId) {
        mCloudId = cloudId;
    }
    public String getCloudId() {
        return mCloudId;
    }

    public static boolean hardwareEnterPressed(KeyEvent keyEvent) {
        if (keyEvent.getAction() == KeyEvent.ACTION_DOWN
                && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            return true;
        }
        return false;
    }

    public static SharePrefUtil getSharePrefUtil() {
        return new SharePrefUtil(getInstance().getApplicationContext());
    }

    /**
     * Checks the response headers for session cookie and saves it
     * if it finds it.
     *
     * @param headers Response Headers.
     */
    public final void checkSessionCookie(Map<String, String> headers) {
        mJsessionCookie = null;
        mRememberMeCookie = null;
        if (headers.containsKey(SET_COOKIE_KEY)) {
            String cookie = headers.get(SET_COOKIE_KEY);
            if (cookie.length() > 0) {
                String[] splitCookies = cookie.split(PLUS);

                for (String potentialCookie : splitCookies) {
                    String[] splitCookie = potentialCookie.split(";");
                    for (String JCookie : splitCookie) {
                        if (JCookie.contains("=")) {
                            String[] sessionID = JCookie.split("=");
                            if (sessionID.length == 2) {
                                if (sessionID[0].equals(JSESSION_COOKIE)) {
                                    mJsessionCookie = sessionID[1];
                                } else if (sessionID[0].equals(REMEMBER_ME_COOKIE)) {
                                    mRememberMeCookie = sessionID[1];
                                }
                            }
                        }
                    }
                }
            }
        }
        //Save the cookies to SharedPreferences
        Handler mainHandler = new Handler(sApplication.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if (mJsessionCookie != null) {
                    getSharePrefUtil().setJsessionCookies(mJsessionCookie);
                }
                if (mRememberMeCookie != null) {
                    getSharePrefUtil().setRememberMeCookies(mRememberMeCookie);
                }
            }
        };
        mainHandler.post(myRunnable);
    }

    /**
     * Adds session cookie to headers if exists.
     *
     * @param headers
     */
    public final void addSessionCookie(Map<String, String> headers) {
        if (mJsessionCookie == null) {
            mJsessionCookie = getSharePrefUtil().getJsessionCookie();
        }
        if (mRememberMeCookie == null) {
            mRememberMeCookie = getSharePrefUtil().getRememberMeCookie();
        }
        if (mJsessionCookie != null && mJsessionCookie.length() > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append(JSESSION_COOKIE);
            builder.append("=");
            builder.append(mJsessionCookie);
            if (headers.containsKey(COOKIE_KEY)) {
                builder.append("; ");
                builder.append(headers.get(COOKIE_KEY));
            }
            headers.put(COOKIE_KEY, builder.toString());
        }
        if (mRememberMeCookie != null && mRememberMeCookie.length() > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append(REMEMBER_ME_COOKIE);
            builder.append("=");
            builder.append(mRememberMeCookie);
            if (headers.containsKey(COOKIE_KEY)) {
                builder.append("; ");
                builder.append(headers.get(COOKIE_KEY));
            }
            headers.put(COOKIE_KEY, builder.toString());
        }
        Log.i(TAG, "Final Cookie is " + headers.get(COOKIE_KEY));
    }

	public int getRole() {
		return getSharePrefUtil().getRole();
	}

//	public void setRole(int mRole) {
//		this.mRole = mRole;
//	}

    public long getId() { return getSharePrefUtil().getUserID(); }

//    public void setId(int mId) { this.mId = mId; }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public static void logOut(final Context context) {
        RequestUtil<CookieGsonRequest.Succeed> requestUtil = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                return new RequestLogout(listener, errListener);
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {
                Log.i(TAG, "Logout successfully.");
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(context, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        requestUtil.add2queue();
    }
    
    /** Return to the sign-in page */
    @SuppressLint("InlinedApi")
    public static void returnToSigninPage(Context context) {
        getSharePrefUtil().clearSharedPreferences();
        Intent intent = new Intent(context, ActivitySignIn.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    
    public static abstract class AfterBackGroundLogin {
        public abstract void doAfterBackGroundLogin(BoundStatus bs);
    }
    
    /**
     * Supposed to be called by onError of normal requests(except login request)
     */
    public void doBackGroundLogin(final AfterBackGroundLogin afterBackGroundLogin) {
        Log.i(TAG, "doBackGroundLogin");
        if (!couldAutoSignin()) {
            returnToSigninPage(mContext);
            return;
        }
        String usrname = getSharePrefUtil().getUserName();
        String passwd = getSharePrefUtil().getPassword();
        LoginUtil requestLogin = new LoginUtil(new Login(usrname, passwd)) {
            @Override
            public void onSuccess(BoundStatus response) {
                super.onSuccess(response);
                if (afterBackGroundLogin != null) {
                    afterBackGroundLogin.doAfterBackGroundLogin(response);
                }
            }

            @SuppressLint("InlinedApi")
            @Override
            public void onError(VolleyError error) {
                super.onError(error);
                returnToSigninPage(mContext);
            }
        };
        requestLogin.login();
    }
    
    /** Check if we could sign-in automatically */
    public static boolean couldAutoSignin() {
        String usrname = getSharePrefUtil().getUserName();
        String passwd = getSharePrefUtil().getPassword();
        if (null == usrname || usrname.equals(SharePrefUtil.INVALID_STRING) || null == passwd || passwd.equals(SharePrefUtil.INVALID_STRING)) {
            return false;
        }
        return true;
    }

    public void saveBoundStatus(BoundStatus response) {
        if(response.mId != getSharePrefUtil().getUserID()) {
            getSharePrefUtil().setUserID(response.mId);
            saveUserAvatar(response.mId);
            getSharePrefUtil().setRole(response.mRole);
            getSharePrefUtil().setClassId(response.mClassId);
            if (response.mChildName != null) {
                getSharePrefUtil().setChildName(response.mChildName);
            }
            if (response.mChildBirthDay != null) {
                getSharePrefUtil().setChildBirth(response.mChildBirthDay);
            }
            if (response.mName != null) {
                if (response.isTeacher()) {
                    getSharePrefUtil().setTeacherName(response.mName);
                } else {
                    getSharePrefUtil().setParentName(response.mName);
                }
            }
            if (response.mTeacherId != null) {
                getSharePrefUtil().setTeacherID(response.mTeacherId);
            }
            getSharePrefUtil().setFirstUse(false);
            getSharePrefUtil().setSaveBoundState(true);
        }
    }
    
    public void cleanCacheOfCurrentUserAvatar() {
        String url = Constants.getAvatarURLOfCreator(getId());
        mImageLoader.clearCache(url);
        mRequestQueue.getCache().invalidate(url, true);
        String url1 = Constants.getAvatarThumbURLOfCreator(getId());
        mImageLoader.clearCache(url1);
        mRequestQueue.getCache().invalidate(url1, true);
    }

    public void saveUserAvatar(long userId) {
        mImageLoader.get(Constants.getAvatarURLOfCreator(userId), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response.getBitmap() != null) {
                    getSharePrefUtil().setAvatarUrl(UIUtil.saveToInternalSorage(KidContactApp.this, response.getBitmap()));
                }
            }
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }

    public Map<String, Integer> getFaceMap() {
        if (!mFaceMap.isEmpty())
            return mFaceMap;
        return null;
    }

    public String getCurrentUserId() {
        return mCurrentUserId;
    }
    public void setCurrentUserId(String id) {
        mCurrentUserId = id;
    }

    private void initFaceMap() {
        mFaceMap.put("[呲牙]", R.drawable.f000);
        mFaceMap.put("[调皮]", R.drawable.f001);
        mFaceMap.put("[流汗]", R.drawable.f002);
        mFaceMap.put("[偷笑]", R.drawable.f003);
        mFaceMap.put("[再见]", R.drawable.f004);
        mFaceMap.put("[敲打]", R.drawable.f005);
        mFaceMap.put("[擦汗]", R.drawable.f006);
        mFaceMap.put("[猪头]", R.drawable.f007);
        mFaceMap.put("[玫瑰]", R.drawable.f008);
        mFaceMap.put("[流泪]", R.drawable.f009);
        mFaceMap.put("[大哭]", R.drawable.f010);
        mFaceMap.put("[嘘]", R.drawable.f011);
        mFaceMap.put("[酷]", R.drawable.f012);
        mFaceMap.put("[抓狂]", R.drawable.f013);
        mFaceMap.put("[委屈]", R.drawable.f014);
        mFaceMap.put("[便便]", R.drawable.f015);
        mFaceMap.put("[炸弹]", R.drawable.f016);
        mFaceMap.put("[菜刀]", R.drawable.f017);
        mFaceMap.put("[可爱]", R.drawable.f018);
        mFaceMap.put("[色]", R.drawable.f019);
        mFaceMap.put("[害羞]", R.drawable.f020);

        mFaceMap.put("[得意]", R.drawable.f021);
        mFaceMap.put("[吐]", R.drawable.f022);
        mFaceMap.put("[微笑]", R.drawable.f023);
        mFaceMap.put("[发怒]", R.drawable.f024);
        mFaceMap.put("[尴尬]", R.drawable.f025);
        mFaceMap.put("[惊恐]", R.drawable.f026);
        mFaceMap.put("[冷汗]", R.drawable.f027);
        mFaceMap.put("[爱心]", R.drawable.f028);
        mFaceMap.put("[示爱]", R.drawable.f029);
        mFaceMap.put("[白眼]", R.drawable.f030);
        mFaceMap.put("[傲慢]", R.drawable.f031);
        mFaceMap.put("[难过]", R.drawable.f032);
        mFaceMap.put("[惊讶]", R.drawable.f033);
        mFaceMap.put("[疑问]", R.drawable.f034);
        mFaceMap.put("[睡]", R.drawable.f035);
        mFaceMap.put("[亲亲]", R.drawable.f036);
        mFaceMap.put("[憨笑]", R.drawable.f037);
        mFaceMap.put("[爱情]", R.drawable.f038);
        mFaceMap.put("[衰]", R.drawable.f039);
        mFaceMap.put("[撇嘴]", R.drawable.f040);
        mFaceMap.put("[阴险]", R.drawable.f041);

        mFaceMap.put("[奋斗]", R.drawable.f042);
        mFaceMap.put("[发呆]", R.drawable.f043);
        mFaceMap.put("[右哼哼]", R.drawable.f044);
        mFaceMap.put("[拥抱]", R.drawable.f045);
        mFaceMap.put("[坏笑]", R.drawable.f046);
        mFaceMap.put("[飞吻]", R.drawable.f047);
        mFaceMap.put("[鄙视]", R.drawable.f048);
        mFaceMap.put("[晕]", R.drawable.f049);
        mFaceMap.put("[大兵]", R.drawable.f050);
        mFaceMap.put("[可怜]", R.drawable.f051);
        mFaceMap.put("[强]", R.drawable.f052);
        mFaceMap.put("[弱]", R.drawable.f053);
        mFaceMap.put("[握手]", R.drawable.f054);
        mFaceMap.put("[胜利]", R.drawable.f055);
        mFaceMap.put("[抱拳]", R.drawable.f056);
        mFaceMap.put("[凋谢]", R.drawable.f057);
        mFaceMap.put("[饭]", R.drawable.f058);
        mFaceMap.put("[蛋糕]", R.drawable.f059);
        mFaceMap.put("[西瓜]", R.drawable.f060);
        mFaceMap.put("[啤酒]", R.drawable.f061);
        mFaceMap.put("[飘虫]", R.drawable.f062);

        mFaceMap.put("[勾引]", R.drawable.f063);
        mFaceMap.put("[OK]", R.drawable.f064);
        mFaceMap.put("[爱你]", R.drawable.f065);
        mFaceMap.put("[咖啡]", R.drawable.f066);
        mFaceMap.put("[钱]", R.drawable.f067);
        mFaceMap.put("[月亮]", R.drawable.f068);
        mFaceMap.put("[美女]", R.drawable.f069);
        mFaceMap.put("[刀]", R.drawable.f070);
        mFaceMap.put("[发抖]", R.drawable.f071);
        mFaceMap.put("[差劲]", R.drawable.f072);
        mFaceMap.put("[拳头]", R.drawable.f073);
        mFaceMap.put("[心碎]", R.drawable.f074);
        mFaceMap.put("[太阳]", R.drawable.f075);
        mFaceMap.put("[礼物]", R.drawable.f076);
        mFaceMap.put("[足球]", R.drawable.f077);
        mFaceMap.put("[骷髅]", R.drawable.f078);
        mFaceMap.put("[挥手]", R.drawable.f079);
        mFaceMap.put("[闪电]", R.drawable.f080);
        mFaceMap.put("[饥饿]", R.drawable.f081);
        mFaceMap.put("[困]", R.drawable.f082);
        mFaceMap.put("[咒骂]", R.drawable.f083);

        mFaceMap.put("[折磨]", R.drawable.f084);
        mFaceMap.put("[抠鼻]", R.drawable.f085);
        mFaceMap.put("[鼓掌]", R.drawable.f086);
        mFaceMap.put("[糗大了]", R.drawable.f087);
        mFaceMap.put("[左哼哼]", R.drawable.f088);
        mFaceMap.put("[哈欠]", R.drawable.f089);
        mFaceMap.put("[快哭了]", R.drawable.f090);
        mFaceMap.put("[吓]", R.drawable.f091);
        mFaceMap.put("[篮球]", R.drawable.f092);
        mFaceMap.put("[乒乓球]", R.drawable.f093);
        mFaceMap.put("[NO]", R.drawable.f094);
        mFaceMap.put("[跳跳]", R.drawable.f095);
        mFaceMap.put("[怄火]", R.drawable.f096);
        mFaceMap.put("[转圈]", R.drawable.f097);
        mFaceMap.put("[磕头]", R.drawable.f098);
        mFaceMap.put("[回头]", R.drawable.f099);
        mFaceMap.put("[跳绳]", R.drawable.f100);
        mFaceMap.put("[激动]", R.drawable.f101);
        mFaceMap.put("[街舞]", R.drawable.f102);
        mFaceMap.put("[献吻]", R.drawable.f103);
        mFaceMap.put("[左太极]", R.drawable.f104);

        mFaceMap.put("[右太极]", R.drawable.f105);
        mFaceMap.put("[闭嘴]", R.drawable.f106);
    }

//    /**
//     * Get current user name.
//     * @return the user name, or null if failed (eg. before "logged-in" state)
//     */
//    public String getUserName() {
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
//        return sp.getString("userName", "null0");
//    }

//    public String getLastUserName() {
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
//        return sp.getString("last_userName", "null1");
//    }
}
