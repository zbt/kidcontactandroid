package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CreateClassAndTeacher;

/**
 * Created by hzheng3 on 14-6-27.
 */
public final class RequestCreateClassAndTeacher extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestCreateClassAndTeacher(CreateClassAndTeacher input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_CREATE_CLASS_AND_TEACHER),
                input, Succeed.class, listener, errorListener);
    }
}
