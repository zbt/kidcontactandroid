package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.PicList;


/**
 * Created by hzheng3 on 14-6-9.
 */


public final class RequestAddOnePic extends RequestAddPic<PicList> {

    public RequestAddOnePic(PicPair input, Response.Listener<PicList> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_UPLOAD_PICS), input, PicList.class, listener, errorListener);
    }
}
