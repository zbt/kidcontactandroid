package com.bobs.kidcontact.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.Child;
import com.bobs.kidcontact.model.ChildList;
import com.bobs.kidcontact.model.CommentList;
import com.bobs.kidcontact.requests.RequestAddComment;
import com.bobs.kidcontact.requests.RequestAddComment.AddComment;
import com.bobs.kidcontact.requests.RequestGetChildren;
import com.bobs.kidcontact.ui.ActivityPublish.IPublish;
import com.bobs.kidcontact.ui.teacher.ActivityChildSelector;
import com.bobs.kidcontact.ui.view.FlowLayout;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;

import de.greenrobot.event.EventBus;

/**
 * Created by junyongl on 14-1-6.
 */
public class FragmentPublishReview extends Fragment implements IPublish {
    private static final String TAG = "FragmentPublishReview";
    private Context mContext;
    private RatingBar mLearning;
    private RatingBar mEating;
    private RatingBar mSleeping;
    private EditText mEtInfo;
    private ReqAddReview mReq;
    private ArrayList<Child> mChildren = new ArrayList<Child>();
    private FlowLayout mTargetContainer;
    private Child mInitChild;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mInitChild = new Gson().fromJson(getArguments().getString("child"), Child.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publish_review, container, false);
        mTargetContainer = (FlowLayout)view.findViewById(R.id.target_container);
        mLearning = (RatingBar) view.findViewById(R.id.rating_learning);
        mEating = (RatingBar) view.findViewById(R.id.rating_eating);
        mSleeping = (RatingBar) view.findViewById(R.id.rating_sleeping);
        mEtInfo = (EditText) view.findViewById(R.id.et_publish_review);
        if (mInitChild != null) {
            Log.d(TAG, "Get the Init user " + mInitChild.mName);
            mChildren.add(mInitChild);
        }
        initRecipientsView();
        return view;
    }

    private void initRecipientsView() {
        mTargetContainer.removeAllViews();
        final float scale = mContext.getResources().getDisplayMetrics().density;
        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams (ViewGroup.LayoutParams.WRAP_CONTENT, (int) (32 * scale));
        params.setMargins(4,4,4,4);
        for (int i = 0; i < mChildren.size(); i++) {
            Child c = mChildren.get(i);
            Button btRecipient = new Button(mContext);
            btRecipient.setBackgroundResource(R.drawable.button_recipient);
            btRecipient.setText(c.mName);
            btRecipient.setGravity(Gravity.CENTER);
            btRecipient.setTextSize((float) 13.0);
            mTargetContainer.addView(btRecipient, params);
        }
        Button btAdd = new Button(mContext);
        btAdd.setBackgroundResource(R.drawable.button_recipient);
        btAdd.setText(mContext.getString(R.string.add_recipient));
        btAdd.setGravity(Gravity.CENTER);
        btAdd.setTextSize((float) 13.0);
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChildList childList = new ChildList();
                childList.mChildren = mChildren;
                Intent intent = new Intent(mContext, ActivityChildSelector.class);
                intent.putExtra("selected_children", new Gson().toJson(childList));
                startActivityForResult(intent, 1);
            }
        });
        mTargetContainer.addView(btAdd, params);
        mTargetContainer.invalidate();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            ChildList childList = new Gson().fromJson(data.getStringExtra("selected_children"), ChildList.class);
            mChildren = childList.mChildren;
            initRecipientsView();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
	public void publish() {
	    mReq = new ReqAddReview(mChildren);
	    mReq.add2queue();
	}

    @Override
    public void onStop() {
        super.onStop();
        if (mReq != null) {
            mReq.cancel();
            mReq = null;
        }
    }

    private class ReqAddReview extends RequestUtil<CommentList> {
        private final AddComment mComment;

        public ReqAddReview(List<Child> children) {
            super(3000);
            mComment = new AddComment();
            mComment.mClassStar = (int) Math.floor(mLearning.getRating() * 2);
            mComment.mEatStar = (int) Math.floor(mEating.getRating() * 2);
            mComment.mSleepStar = (int) Math.floor(mSleeping.getRating() * 2);
            mComment.mChidren = new ArrayList<Long>();
            for (int i = 0; i != children.size(); i++) {
                mComment.mChidren.add(children.get(i).mId);
            }
            mComment.mInfo = mEtInfo.getText().toString();
        }

        @Override
        public Request<CommentList> onPrepareRequest(Listener<CommentList> listener,
                ErrorListener errListener) {
            return new RequestAddComment(mComment, listener, errListener);
        }

        @Override
        public void onSuccess(CommentList response) {
            getActivity().finish();
            Toast.makeText(mContext, R.string.review_add_success, Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new UpdateKidDetailUIEvent());
        }

        @Override
        public void onError(VolleyError e) {
            Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            super.onError(e);
        }
    }
}
