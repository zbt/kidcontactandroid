package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-14.
 */
public class TagProxy {
    @SerializedName("tag")
    public Tag mTag;
}
