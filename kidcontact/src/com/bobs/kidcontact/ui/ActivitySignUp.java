package com.bobs.kidcontact.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.requests.LoginRequest.Login;
import com.bobs.kidcontact.requests.RequestAddUser;
import com.bobs.kidcontact.requests.RequestAddUser.AddUser;
import com.bobs.kidcontact.requests.RequestAddUser.Password;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus.BoundStatus;
import com.bobs.kidcontact.ui.teacher.ActivityTeacherMain;
import com.bobs.kidcontact.utils.LoginUtil;
import com.bobs.kidcontact.utils.RequestUtil;

public class ActivitySignUp extends CommonActivityBase {
    private EditText mEtUsrname;
    private EditText mEtRegCode;
    private EditText mEtPasswd;
    private Button mBtRegcode;
    private Button mBtLogin;
    private LoginUtil mReqLogin;
    private ReqAddUser mReqAddUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mEtUsrname = (EditText) findViewById(R.id.et_username);
        mEtRegCode = (EditText) findViewById(R.id.et_invite_code);
        mEtPasswd = (EditText) findViewById(R.id.et_password);

        // hide the unused button
        ((Button) findViewById(R.id.bt_right)).setVisibility(View.GONE);

        mBtLogin = (Button) findViewById(R.id.bt_left);
        mBtLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String usrname = getPhoneNumber();
                if (usrname == null) {
                    Toast.makeText(ActivitySignUp.this, R.string.addusr_err_usrname, Toast.LENGTH_SHORT).show();
                    return;
                }
                String passcode = getPasswd();
                if (passcode == null) {
                    Toast.makeText(ActivitySignUp.this, R.string.addusr_err_smscode, Toast.LENGTH_SHORT).show();
                    return;
                }
                mReqLogin = new LoginUtil(new Login(usrname, passcode)) {
                    @Override
                    public void onSuccess(BoundStatus response) {
                        super.onSuccess(response);
                        //Temporarily save the name and password
                        saveNamePWD();
                        KidContactApp.getSharePrefUtil().setUserID(response.mId);
                        KidContactApp.getSharePrefUtil().setRole(response.mRole);
                        if (response.isBounded()) {
                            if (response.isTeacher()) {
                                Intent intent = new Intent(ActivitySignUp.this, ActivityTeacherMain.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else if (response.isParent()) {
                                Intent intent = new Intent(ActivitySignUp.this, ActivityParentMain.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                throw new RuntimeException("any other role?");
                            }
                        } else {
                            if (response.isParent()) {
                                Intent intent = new Intent(ActivitySignUp.this, ActivitySignUpKidInfo.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                            if (response.isTeacher()) {
                                Intent intent = new Intent(ActivitySignUp.this, ActivitySignUpTeacherInfo.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onError(VolleyError error) {
                        super.onError(error);
                        enableInput();
                        Toast.makeText(ActivitySignUp.this, RequestUtil.interpretErr2ResForLogin(error), Toast.LENGTH_SHORT).show();
                    }
                };
                mReqLogin.login();
                disableInput();
            }
        });

        mBtRegcode = (Button) findViewById(R.id.bt_sms_code);
        mBtRegcode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addUser()) {
                    disableInput();
                }
            }
        });
    }

    private void enableInput() {
        mEtUsrname.setEnabled(true);
        mEtRegCode.setEnabled(true);
        mEtPasswd.setEnabled(true);
        mBtRegcode.setEnabled(true);
        mBtLogin.setEnabled(true);
    }

    private void disableInput() {
        mEtUsrname.setEnabled(false);
        mEtRegCode.setEnabled(false);
        mEtPasswd.setEnabled(false);
        mBtRegcode.setEnabled(false);
        mBtLogin.setEnabled(false);
    }
    private String getPhoneNumber() {
        String usrname = mEtUsrname.getText().toString().trim();
        if (usrname.length() == 11 && usrname.matches("[0-9]+")) {
            return usrname;
        }
        return null;
    }

    private String getPasswd() {
        String passwd = mEtPasswd.getText().toString();
        if (passwd.length() > 0) {
            return passwd;
        }
        return null;
    }

    private String getRegCode() {
        String smscode = mEtRegCode.getText().toString();
        if (smscode.length() > 0) {
            return smscode;
        }
        return null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReqLogin != null) {
            mReqLogin.cancel();
            mReqLogin = null;
        }
        if (mReqAddUser != null) {
            mReqAddUser.cancel();
            mReqAddUser = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableInput();
    }

    private boolean addUser() {
        String phoneNumber = getPhoneNumber();
        if (phoneNumber == null) {
            Toast.makeText(this, R.string.addusr_err_usrname, Toast.LENGTH_SHORT).show();
            return false;
        }
        String regcode = getRegCode();
        if (regcode == null) {
            Toast.makeText(this, R.string.addusr_err_regcode, Toast.LENGTH_SHORT).show();
            return false;
        }

        mReqAddUser = new ReqAddUser(phoneNumber, regcode);
        mReqAddUser.add2queue();

        return true;
    }

    private class ReqAddUser extends RequestUtil<Password> {
        private final AddUser mUsr = new AddUser();

        public ReqAddUser(String usrname, String regcode) {
            super(3000);
            mUsr.mPhoneNumber = usrname;
            mUsr.mRegCode = regcode;
        }

        @Override
        public Request<Password> onPrepareRequest(Listener<Password> listener,
                ErrorListener errListener) {
            return new RequestAddUser(mUsr, listener, errListener);
        }

        @Override
        public void onSuccess(Password response) {
            enableInput();
            Toast.makeText(ActivitySignUp.this, R.string.addusr_sms_sent, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(VolleyError e) {
            enableInput();
            Toast.makeText(ActivitySignUp.this, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
        }

    }


    //Temp to save the Name and Password to SharePreference
    private void saveNamePWD() {
        KidContactApp.getSharePrefUtil().setUserName(getPhoneNumber());
        KidContactApp.getSharePrefUtil().setPassword(getPasswd());
        KidContactApp.getSharePrefUtil().setSaveState(true);
    }
}
