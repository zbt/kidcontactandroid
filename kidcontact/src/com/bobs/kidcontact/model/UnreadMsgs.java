package com.bobs.kidcontact.model;

import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 2014/5/20.
 */
public class UnreadMsgs {
    @SerializedName("unReadMessages")
    public ArrayList<CloudMessage> mCloudMessages;
}
