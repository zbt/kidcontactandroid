package com.bobs.kidcontact.ui.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.bobs.kidcontact.R;

/**
 * Created by junyongl on 14-3-7.
 */
public class FragmentPictureSelector extends DialogFragment {
    DialogInterface.OnClickListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("照片")
               .setItems(R.array.picture_picker_array, mListener);
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        try {
//            mListener = (DialogInterface.OnClickListener) activity;
//        } catch (ClassCastException e) {
//            throw new ClassCastException(activity.toString()
//                    + " must implement DialogInterface.OnClickListener");
//        }
    }

    public void setOnClickListener(DialogInterface.OnClickListener listener) {
        mListener = listener;
    }
}
