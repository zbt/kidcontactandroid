package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 2014/6/27.
 */
public class JoinClassInput {
    @SerializedName("regCode")
    public String mRegCode;
    @SerializedName("id")
    public long mId;
}
