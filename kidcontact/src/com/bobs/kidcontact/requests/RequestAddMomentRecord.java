package com.bobs.kidcontact.requests;

import android.util.Log;

import com.android.volley.Response;
import com.bobs.kidcontact.model.Tag;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.Gson;

import org.apache.http.entity.mime.content.StringBody;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-6.
 */

/**
 * Base class for AddMoment and AddRecord
 * The moment is submitted with multipart-form request which contain following parts
 * MUST: "info":"xxxxxx"
 * OPTIONAL: "tags":"["","",""]"
 * OPTIONAL: picture related
 *
 * The record is submitted with another optional part
 * OPTIONAL: "children":[1,2,3]
 */
public abstract class RequestAddMomentRecord<T> extends CookieMuiltiPicUploadRequest<T> {
    private static final String TAG = "RequestAddMomentRecord";
    String mInfo;
    ArrayList<Tag> mTags;

    public RequestAddMomentRecord(String url, String info, ArrayList<Tag> tags,
                                  ArrayList<PicPair> picPairs, Class<T> objectClass,
                                  Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(url, picPairs, objectClass, listener, errorListener);
        mInfo = UIUtil.encoder(info);
        mTags = tags;
        buildAdditionalInfo();
    }

    @SuppressWarnings("deprecation")
    protected void buildAdditionalInfo() {
        if (mInfo != null) {
            Log.i(TAG, "Moment info is " + mInfo);
            try {
                entity.addPart("info", new StringBody(mInfo));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "add moment info failed " + e.getMessage());
            }
        }
        if (mTags != null && !mTags.isEmpty()) {
            String tagInfo = getTagsInfo();
            Log.i(TAG, "Tag info is " + tagInfo);
            try {
                entity.addPart("tags", new StringBody(tagInfo));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "add tags failed " + e.getMessage());
            }
        }
    }

    private String getTagsInfo() {
        Gson gson = new Gson();
        ArrayList<String> tagStrings = new ArrayList<String>();
        for (Tag tag : mTags) {
            tagStrings.add(tag.tag);
        }
        return gson.toJson(tagStrings);
    }
}
