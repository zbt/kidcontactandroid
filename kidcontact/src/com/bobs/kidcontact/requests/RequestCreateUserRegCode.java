package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CreateClassAndTeacher;
import com.bobs.kidcontact.model.RegcodeProxy;

/**
 * Created by hzheng3 on 14-6-27.
 */
public final class RequestCreateUserRegCode extends CookieGsonJsonRequest<RegcodeProxy> {
    public RequestCreateUserRegCode(CreateClassAndTeacher input, Response.Listener<RegcodeProxy> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_CREATE_USER_REG_CODE),
                input, RegcodeProxy.class, listener, errorListener);
    }
}
