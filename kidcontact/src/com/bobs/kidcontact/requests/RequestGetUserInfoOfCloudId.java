package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CloudId;
import com.bobs.kidcontact.model.User;

public final class RequestGetUserInfoOfCloudId extends CookieGsonJsonRequest<User> {
    public RequestGetUserInfoOfCloudId(CloudId input, Response.Listener<User> listener, Response.ErrorListener errorListener) {
        super(getURL(), input, User.class, listener, errorListener);
    }

    public static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_GET_USERINFO_OF_CLOUDID);
    }
}
