package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-11.
 */
public class AckNotice extends NoticeBase {
    @SerializedName("ackNoticeReplies")
    public ArrayList<Reply> mReplies;
}
