package com.bobs.kidcontact.chat.unifiedmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhanglong on 2014-05-08.
 */
public class Aps implements Parcelable {

    /** A string to be shown in the client notification */
    @SerializedName("alert")
    private String mAlert = "";

    public Aps() {
    }

    public void setAlert(String alert) {
        mAlert = alert;
    }

    public String getAlert() {
        return mAlert;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mAlert);
    }

    private Aps(Parcel in) {
        this.mAlert = in.readString();
    }

    public static final Parcelable.Creator<Aps> CREATOR = new Parcelable.Creator<Aps>() {

        @Override
        public Aps createFromParcel(Parcel source) {
            return new Aps(source);
        }

        @Override
        public Aps[] newArray(int size) {
            return new Aps[size];
        }

    };
}
