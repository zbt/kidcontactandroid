package com.bobs.kidcontact.ui.teacher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.model.Child;
import com.bobs.kidcontact.model.ChildList;
import com.bobs.kidcontact.model.PicProxy;
import com.bobs.kidcontact.requests.RequestGetChildren;
import com.bobs.kidcontact.ui.ActionBarActivityBase;
import com.bobs.kidcontact.ui.FragmentBase;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;
import java.util.ArrayList;

/**
 * Created by junyongl on 14-1-14. zhanglong
 */
public class FragmentTeacherMember extends FragmentBase implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "FragmentTeacherMember";
    private static final int MSG_COUNT_MARK_LIMIT = 99;
    private Context mContext;
    private ArrayList<Child> mChildren = new ArrayList<Child>();
    private int gridWidth;
    private ImageLoader mImageLoader;
    private ClassMemberAdapter mAdapter;
    private IChatService.Stub mChatServiceStub = null;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        final float widthpixels = mContext.getResources().getDisplayMetrics().widthPixels;
        final float scale = mContext.getResources().getDisplayMetrics().density;
        gridWidth = (int) ((widthpixels - 32* scale)/3 -8);
        mImageLoader = KidContactApp.getInstance().getImageLoader();
    }

    @Override
    public void onResume() {
        super.onResume();
        getChatServiceStubAsync();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teacher_member, container, false);
        GridView gridView = (GridView) view.findViewById(R.id.grid_member);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorScheme(R.color.progress_scheme1, R.color.progress_scheme2,
                R.color.progress_scheme3, R.color.progress_scheme4);
        mAdapter = new ClassMemberAdapter();
        gridView.setAdapter(mAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (KidContactApp.getInstance().getRole() == Constants.TYPE_TEACHER) {
                    Intent intent = new Intent(mContext, ActivityChildDetails.class);
                    intent.putExtra("child", new Gson().toJson(mChildren.get(position)));
                    startActivity(intent);
                }
            }
        });
        refreshMemberList();
        return view;
    }

    private void refreshMemberList() {
        RequestUtil<ChildList> request = new RequestUtil<ChildList>() {
            @Override
            public Request<ChildList> onPrepareRequest(Response.Listener<ChildList> listener, Response.ErrorListener errListener) {
                return new RequestGetChildren(listener, errListener);
            }

            @Override
            public void onSuccess(ChildList response) {
                mSwipeRefreshLayout.setRefreshing(false);
                if (response.mChildren.size() == 0) {
                    return;
                }
                mChildren.clear();
                mChildren.addAll(response.getChildren());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(VolleyError e) {
                mSwipeRefreshLayout.setRefreshing(false);
                super.onError(e);
                Log.e(TAG, "" + RequestUtil.interpretErr2Res(e));
            }
        };
        request.add2queue();
    }

    /**
     * Since the chat service stub may not be got in time, we should do it
     * in a background loop.
     */
    private void getChatServiceStubAsync() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                int count = 0;
                do {
                    Common.sleep(10);
                    mChatServiceStub =((ActionBarActivityBase) getActivity()).getChatServiceStub();
                    count++;
                } while (null == mChatServiceStub && count < 100);
                if (null != mChatServiceStub) {
                    Log.i(TAG, "bound to chat service.");
                } else {
                    Log.e(TAG, "failed to get the stub of chat service!");
                }
            }

        }).start();
    }

    @Override
    public void onRefresh() {
        refreshMemberList();
    }

    private class ClassMemberAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mChildren.size();
        }

        @Override
        public Object getItem(int i) {
            return mChildren.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder = new ViewHolder();

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_member, viewGroup, false);
                viewHolder.mAvatar = (NetworkRoundImageView) convertView.findViewById(R.id.member_avatar);
                viewHolder.mAvatar.setLayoutParams(new FrameLayout.LayoutParams(gridWidth, gridWidth));
                viewHolder.mAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
                viewHolder.mAvatar.setPadding(4, 4, 4, 4);
                viewHolder.mCornerMark = (TextView) convertView.findViewById(R.id.text_corner_mark_member);
                viewHolder.mName = (TextView) convertView.findViewById(R.id.member_name);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.mAvatar.setDefaultImageResId(R.drawable.ic_default_image);
            ArrayList<PicProxy> childrenPic = ((Child)getItem(position)).mPics;
            if (childrenPic.size() != 0) {
                if (childrenPic.get(0).mPicture.picSize != 0) {
                    viewHolder.mAvatar.setImageUrl(childrenPic.get(0).mPicture.getPicURL(), mImageLoader);
                } else if (childrenPic.get(0).mPicture.mThumbSize != 0) {
                    viewHolder.mAvatar.setImageUrl(childrenPic.get(0).mPicture.getPicThumbURL(), mImageLoader);
                }
            } else {
                viewHolder.mAvatar.setImageResource(R.drawable.ic_avatar);
            }
            if (((Child) getItem(position)).mName != null) {
                viewHolder.mName.setText(((Child) getItem(position)).mName);
            }
            if (null != mChatServiceStub) {
                Conversation conversation = null;
                try {
                    conversation = mChatServiceStub.getConversationByUserId(((Child) getItem(position)).mParents.get(0).mId);
                } catch (RemoteException e) {
                    Log.e(TAG, "failed to get conversation by user id: " + e.getMessage());
                }
                if (null != conversation) {
                    updateCornerMark(viewHolder.mCornerMark, conversation.getNewMsgCount());
                }
            }
            return convertView;
        }

        private class ViewHolder {
            public NetworkRoundImageView mAvatar;
            public TextView mCornerMark;
            public TextView mName;
        }
    }

    @Override
    protected void refreshViewsAsync() {
        super.refreshViewsAsync();
        new Thread(new Runnable() {

            @Override
            public void run() {
                if (null != ((ActionBarActivityBase) getActivity()).getChatServiceStubWait(1000)) {
                    FragmentTeacherMember.this.mEventHandlerForView.sendEmptyMessage(Events.EVENT_CHAT_SERVICE_BOUND);
                } else {
                    Log.w(TAG, "failed to get chat service stub!");
                }
            }

        }).start();
    }

    @Override
    protected void refreshViews() {
        super.refreshViews();
        refreshViews((Conversation) null);
    }

    @Override
    protected void refreshViews(Conversation conversation) {
        super.refreshViews(conversation);
        Log.d(TAG, "FragmentTeacherMember.refreshViews()");
        refreshMemberViews();
    }

    /**
     * Update the given corner mark with the given update count
     * @param cornerMark a corner mark to update
     * @param count the count of new update
     */
    private void updateCornerMark(TextView cornerMark, int count) {
        if (count > 0 && count <= MSG_COUNT_MARK_LIMIT) {
            cornerMark.setText("" + count);
            cornerMark.setVisibility(View.VISIBLE);
        } else if (count > MSG_COUNT_MARK_LIMIT) {
            cornerMark.setText("...");
            cornerMark.setVisibility(View.VISIBLE);
        } else {
            cornerMark.setText("");
            cornerMark.setVisibility(View.INVISIBLE);
        }
    }

    /** Refresh the member views */
    private void refreshMemberViews() {
        mAdapter.notifyDataSetChanged();
    }
}
