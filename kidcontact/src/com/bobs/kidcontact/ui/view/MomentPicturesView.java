package com.bobs.kidcontact.ui.view;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.PicProxy;
import com.bobs.kidcontact.model.Picture;
import com.bobs.kidcontact.ui.ActivityPicturesViewer;
import com.bobs.kidcontact.ui.KidContactApp;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by junyongl on 14-1-1.
 */
public class MomentPicturesView extends LinearLayout {
    private static final String TAG = "MomentPicturesView";
    private ArrayList<PicProxy> mPictures = new ArrayList<PicProxy>();
    private Context mContext;
    private ImageLoader mImageLoader;
    private NetworkImageView singleImageView;
    private GridView gridView;
    private ImageAdapter imageAdapter;
    private int gridWidth;
    private float widthpixels;
    private float scale;
    private PicProxy.PicProxyComparator mPicProxyComparator = new PicProxy.PicProxyComparator();

    public MomentPicturesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mImageLoader = KidContactApp.getInstance().getImageLoader();
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_moment_pictures, this, true);
        singleImageView = (NetworkImageView) findViewById(R.id.single_view);
        gridView = (GridView) findViewById(R.id.grid_view);
        imageAdapter = new ImageAdapter(context);
        gridView.setAdapter(imageAdapter);
        widthpixels = mContext.getResources().getDisplayMetrics().widthPixels;
        scale = mContext.getResources().getDisplayMetrics().density;
        gridWidth = (int) ((widthpixels - 32* scale)/3 -8);
        Log.d(TAG, "The picture grid width:" + gridWidth);



    }

    public void setPictures(ArrayList<PicProxy> pictures){
        if (pictures.size() > 1) {
            Collections.sort(pictures, mPicProxyComparator);
        }
        Log.d(TAG, "pictures Size" + pictures.size());
        mPictures = pictures;
        final Type listOfMPicture = new TypeToken<ArrayList<Picture>>(){}.getType();
        final Gson gson = new Gson();
        if (mPictures.size() > 0 ){
            if (mPictures.size() == 1) {
                singleImageView.setDefaultImageResId(R.drawable.ic_default_image);
                if (mPictures.get(0).getPicture().mThumbSize != 0) {
                    singleImageView.setImageUrl(mPictures.get(0).getPicture().getPicThumbURL(), mImageLoader);
                } else if (mPictures.get(0).getPicture().picSize != 0) {
                    singleImageView.setImageUrl(mPictures.get(0).getPicture().getPicURL(), mImageLoader);
                }

                gridView.setVisibility(View.GONE);
                int h = (int)widthpixels*2/3;
                singleImageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,h));
                singleImageView.setPadding((int)(4*scale), (int)(4*scale), (int)(4*scale), (int)(4*scale));
                singleImageView.setClickable(true);
                singleImageView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, ActivityPicturesViewer.class);
                        String mPicturesJson = gson.toJson(mPictures, listOfMPicture);
                        intent.putExtra("key_pictures", mPicturesJson);
                        mContext.startActivity(intent);
                    }
                });
            } else {
                singleImageView.setVisibility(View.GONE);
                gridView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        gridWidth * ((mPictures.size() + 2) / 3) + 4));
                imageAdapter.notifyDataSetChanged();
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(mContext, ActivityPicturesViewer.class);
                        String mPicturesJson = gson.toJson(mPictures, listOfMPicture);
                        intent.putExtra("key_pictures", mPicturesJson);
                        intent.putExtra("key_position", position);
                        mContext.startActivity(intent);
                    }
                });
            }
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return mPictures.size();
        }

        @Override
        public Object getItem(int position) {
            return mPictures.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            NetworkImageView imageView;

            if (convertView == null) {
                imageView = new NetworkImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(gridWidth, gridWidth));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(4, 0, 4, 4);
            } else {
                imageView = (NetworkImageView) convertView;
            }
            imageView.setDefaultImageResId(R.drawable.ic_default_image);
            if (((PicProxy)getItem(position)).getPicture().mThumbSize != 0) {
                imageView.setImageUrl(((PicProxy)getItem(position)).getPicture().getPicThumbURL(), mImageLoader);
            } else if (((PicProxy)getItem(position)).getPicture().picSize != 0) {
                imageView.setImageUrl(((PicProxy)getItem(position)).getPicture().getPicURL(), mImageLoader);
            }
            return imageView;
        }
    }


}
