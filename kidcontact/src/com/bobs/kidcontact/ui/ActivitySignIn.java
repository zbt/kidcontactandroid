package com.bobs.kidcontact.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.baidu.android.pushservice.PushManager;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.service.BaiduInfoHolder;
import com.bobs.kidcontact.chat.service.HeartbeatExecutor;
import com.bobs.kidcontact.chat.service.Utils;
import com.bobs.kidcontact.model.CloudId;
import com.bobs.kidcontact.model.CloudTag;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestGetClassChatTag;
import com.bobs.kidcontact.requests.RequestUpdateCloudId;
import com.bobs.kidcontact.requests.LoginRequest.Login;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus.BoundStatus;
import com.bobs.kidcontact.ui.teacher.ActivityTeacherMain;
import com.bobs.kidcontact.ui.view.ActivityForgotPassword;
import com.bobs.kidcontact.utils.L;
import com.bobs.kidcontact.utils.LoginUtil;
import com.bobs.kidcontact.utils.RequestUtil;

public class ActivitySignIn extends Activity {
    private static final String TAG = "ActivitySignIn";
    private Button mBtSignin;
    private Button mBtSignup;
    private EditText mEtUsrname;
    private EditText mEtPasswd;
    private LoginUtil mReqLogin;
    private Context mContext;
    private BoundStatus mCachedResponse = null;
    private TextView mForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        mEtUsrname = (EditText) findViewById(R.id.et_username);
        mEtPasswd = (EditText) findViewById(R.id.et_password);
        mBtSignin = (Button) findViewById(R.id.bt_left);
        mBtSignup = (Button) findViewById(R.id.bt_right);
        mForgotPassword = (TextView) findViewById(R.id.forgot_password);

        mBtSignin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        mBtSignup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySignIn.this,
                        ActivitySignUp.class);
                startActivity(intent);
            }
        });

        mForgotPassword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ActivityForgotPassword.class);
                startActivity(intent);
            }
        });

        mContext = getApplicationContext();

        //temp to load name and password
        loadNamePWD();
    }



    /** Start to do our login tasks */
    private void login() {
        String usrname = getUsrname();
        if (usrname == null) {
            Toast.makeText(this, R.string.signin_warning_usrname, Toast.LENGTH_SHORT).show();
            return;
        }
        String passwd = getPasswd();
        if (passwd == null) {
            Toast.makeText(this, R.string.signin_warning_passwd, Toast.LENGTH_SHORT).show();
            return;
        }
        L.d(TAG, "login info [" + usrname + "|" + passwd + "]");

        disableInput();

        // login task1
        mReqLogin = new LoginUtil(new Login(usrname, passwd)) {
            @Override
            public void onSuccess(BoundStatus response) {
                super.onSuccess(response);
                // Temp to save name and password
                saveNamePWD();
                BaiduInfoHolder.setMyUserId(response.mId);
                KidContactApp.getInstance().saveBoundStatus(response);
//                KidContactApp.getInstance().setRole(response.mRole);
//                KidContactApp.getInstance().setId(response.mId);
                mCachedResponse = response;
                afterLoginResponse();
            }

            @Override
            public void onError(VolleyError error) {
                super.onError(error);
                enableInput();
                Toast.makeText(ActivitySignIn.this, RequestUtil.interpretErr2ResForLogin(error), Toast.LENGTH_SHORT).show();
            }
        };
        mReqLogin.login();
    }

    /** Do something after the login response */
    private void afterLoginResponse() {
        new TagRequestUtil().add2queue();
        new UpdateCloudIdRequestUtil().add2queue();
    }

    /** Do something after login succeed */
    private void onLoginSucceed() {
        if (mCachedResponse.isBounded()) {
            if (mCachedResponse.isTeacher()) {
                Intent intent = new Intent(ActivitySignIn.this, ActivityTeacherMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else if (mCachedResponse.isParent()) {
                Intent intent = new Intent(ActivitySignIn.this, ActivityParentMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                throw new RuntimeException("any other role?");
            }
        } else {
            if (mCachedResponse.isParent()) {
                Intent intent = new Intent(ActivitySignIn.this, ActivitySignUpKidInfo.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP  );
                startActivity(intent);
            } else if (mCachedResponse.isTeacher()) {
                Intent intent = new Intent(ActivitySignIn.this, ActivitySignUpTeacherInfo.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }
    }

    /**
     * Validate and return user name
     *
     * @return valid usrname string or null if validation failed
     */
    private String getUsrname() {
        String usrname = mEtUsrname.getText().toString().trim();
        if (usrname.length() == 11 && usrname.matches("[0-9]+")) {
            return usrname;
        }
        return null;
    }

    /**
     * Validate and return password
     *
     * @return valid password string or null if validation failed
     */
    private String getPasswd() {
        String passwd = mEtPasswd.getText().toString();
        if (passwd.length() > 0) {
            return passwd;
        }
        return null;
    }

    private void disableInput() {
        mEtUsrname.setEnabled(false);
        mEtPasswd.setEnabled(false);
        mBtSignin.setEnabled(false);
        mBtSignup.setEnabled(false);
    }

    private void enableInput() {
        mEtUsrname.setEnabled(true);
        mEtPasswd.setEnabled(true);
        mBtSignin.setEnabled(true);
        mBtSignup.setEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReqLogin != null) {
            mReqLogin.cancel();
            mReqLogin = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableInput();
    }

    //Temp to save the Name and Password to SharePreference
    private void saveNamePWD() {
        KidContactApp.getSharePrefUtil().setLastUserName();
        KidContactApp.getSharePrefUtil().setUserName(getUsrname());
        KidContactApp.getSharePrefUtil().setPassword(getPasswd());
        KidContactApp.getSharePrefUtil().setSaveState(true);
    }
    //Temp to load the Name and Password from SharePreference
    private void loadNamePWD(){
        Boolean isSave = KidContactApp.getSharePrefUtil().getSaveState();
        if(isSave) {
            mEtUsrname.setText(KidContactApp.getSharePrefUtil().getUserName());
            mEtPasswd.setText(KidContactApp.getSharePrefUtil().getPassword());
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * A RequestUtil for get cloud tag (group id).
     * @author zhanglong
     *
     */
    private class TagRequestUtil extends RequestUtil<CloudTag> {

        @Override
        public Request<CloudTag> onPrepareRequest(
                Listener<CloudTag> listener, ErrorListener errListener) {
            return new RequestGetClassChatTag(listener, errListener);
        }

        @Override
        public void onSuccess(CloudTag response) {
            if (null == response.mCloudTag || response.equals("")) {
                Log.e(TAG, "failed to get cloud tag!");
                // It's serious that we maybe unable to do class-chat，but
                // anyway we should move on.
                Toast.makeText(mContext, R.string.class_get_info_fail, Toast.LENGTH_LONG).show();
            }
            BaiduInfoHolder.setGroupTag(response.mCloudTag);

            // Temporary solution for joining class chat, not perfect
            ArrayList<String> groups = new ArrayList<String>();
            groups.add(response.mCloudTag);
            PushManager.setTags(mContext, groups);
            onLoginSucceed();
        }

        @Override
        public void onError(VolleyError e) {
            Log.w(TAG, RequestUtil.interpretErr2String(e));
            Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            // It's serious that we maybe unable to do class-chat，but
            // anyway we should move on.
            onLoginSucceed();
        }

    }
    
    /**
     * A request util for update cloud id.
     * @author zhanglong
     */
    public static class UpdateCloudIdRequestUtil extends RequestUtil<CookieGsonRequest.Succeed> {
        @Override
        public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
            CloudId cloudId = new CloudId();
            cloudId.mCloudId = KidContactApp.getInstance().getCloudId();
            Log.i(TAG, "about to update cloud id: " + cloudId.mCloudId);
            return new RequestUpdateCloudId(cloudId, listener, errListener);
        }

        @Override
        public void onSuccess(CookieGsonRequest.Succeed response) {
            Log.i(TAG, "updated cloud id");
            HeartbeatExecutor.getInstance().execute();
        }

        @Override
        public void onError(VolleyError e) {
            // super.onError(e);
            Log.w(TAG, "volley error: " + RequestUtil.interpretErr2String(e));
        }
    }
}
