package com.bobs.kidcontact.ui;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.AckNotice;
import com.bobs.kidcontact.model.PublicNotice;
import com.bobs.kidcontact.model.Vote;
import com.bobs.kidcontact.requests.RequestAddAckNotice;
import com.bobs.kidcontact.requests.RequestAddInfoTitle.InfoTitle;
import com.bobs.kidcontact.requests.RequestAddPublicNotice;
import com.bobs.kidcontact.requests.RequestAddVote;
import com.bobs.kidcontact.ui.view.VoteOptionView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;

import de.greenrobot.event.EventBus;

/**
 * Created by junyongl on 14-1-9.
 */
public class FragmentPublishNotice extends Fragment implements AdapterView.OnItemSelectedListener, ActivityPublish.IPublish {
    private static final int TYPE_NONE = 0;
    private static final int TYPE_NORMAL = 1;
    private static final int TYPE_VOTE = 2;
    private static final long REQUEST_TIMEOUT = 5000;
    private static final String TAG = "FragmentPublishNotice";
    private int mType = TYPE_NONE;
    private Context mContext;
    private LinearLayout voteOptionsContainer;
    private Button addOption;
    private CheckBox isNeedReceipt;
    private EditText mEtTitle;
    private EditText mEtContent;
    private RequestUtil<?> mReq;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publish_notice, container, false);
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner_notice_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                R.array.notice_type, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        voteOptionsContainer = (LinearLayout) view.findViewById(R.id.vote_options_container);


        addOption = (Button) view.findViewById(R.id.bt_add_option);
        addOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VoteOptionView optionView = new VoteOptionView(mContext, null);
                optionView.setBackgroundColor(Color.WHITE);
                final float scale = mContext.getResources().getDisplayMetrics().density;
                optionView.setPadding((int)(8*scale), (int)(8*scale), (int)(8*scale), (int)(8*scale));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.
                        LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                voteOptionsContainer.addView(optionView, layoutParams);
            }
        });
        isNeedReceipt = (CheckBox) view.findViewById(R.id.check_receipt);

        mEtTitle = (EditText) view.findViewById(R.id.et_notice_title);
        mEtContent = (EditText) view.findViewById(R.id.et_notice_content);

        return view;
    }

    public ArrayList<String> getVoteOptions() {
        ArrayList<String> optionStrings = new ArrayList<String>();
        for (int i = 0; i < voteOptionsContainer.getChildCount(); i++) {
            VoteOptionView optionView = (VoteOptionView) voteOptionsContainer.getChildAt(i);
            if (optionView.getOptionContent() != null && optionView.getOptionContent().length() > 0) {
                optionStrings.add(optionView.getOptionContent());
            }
        }
        return optionStrings;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getItemAtPosition(position).equals(
                mContext.getResources().getStringArray(R.array.notice_type)[0])) {
            //Select Normal Notice
            voteOptionsContainer.setVisibility(View.GONE);
            addOption.setVisibility(View.GONE);
            isNeedReceipt.setVisibility(View.VISIBLE);
            mType = TYPE_NORMAL;
        } else {
            //Select the Vote Notice
            voteOptionsContainer.setVisibility(View.VISIBLE);
            addOption.setVisibility(View.VISIBLE);
            isNeedReceipt.setVisibility(View.GONE);
            mType = TYPE_VOTE;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void publish() {
        switch (mType) {
        case TYPE_NORMAL:
            InfoTitle info = new InfoTitle();
            info.mTitle = mEtTitle.getText().toString();
            info.mInfo = mEtContent.getText().toString();
            publishNormal(info);
            break;
        case TYPE_VOTE:
            InfoTitle voteInfo = new InfoTitle();
            voteInfo.mTitle = mEtTitle.getText().toString();
            Vote.VoteContent content = new Vote.VoteContent();
            content.mContent = mEtContent.getText().toString();
            content.mOptions = getVoteOptions();
            voteInfo.mInfo = new Gson().toJson(content);
            publishVote(voteInfo);
            break;
        default:
            throw new RuntimeException("who am I?");
        }
    }

    private void publishNormal(final InfoTitle info) {
        if (isNeedReceipt.isChecked()) {
            mReq = new RequestUtil<AckNotice>(REQUEST_TIMEOUT) {
                @Override
                public Request<AckNotice> onPrepareRequest(
                        Listener<AckNotice> listener, ErrorListener errListener) {
                    return new RequestAddAckNotice(info, listener, errListener);
                }

                @Override
                public void onSuccess(AckNotice response) {
                    getActivity().finish();
                    Toast.makeText(mContext, R.string.publish_success, Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new UpdateStatusUIEvent());
                }

                @Override
                public void onError(VolleyError e) {
                    Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                    super.onError(e);
                }
            };
        } else {
            mReq = new RequestUtil<PublicNotice>(REQUEST_TIMEOUT) {
                @Override
                public Request<PublicNotice> onPrepareRequest(
                        Listener<PublicNotice> listener,
                        ErrorListener errListener) {
                    return new RequestAddPublicNotice(info, listener, errListener);
                }

                @Override
                public void onSuccess(PublicNotice response) {
                    getActivity().finish();
                    Toast.makeText(mContext, R.string.publish_success, Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new UpdateStatusUIEvent());
                }

                @Override
                public void onError(VolleyError e) {
                    Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                    super.onError(e);
                }
            };
        }
        mReq.add2queue();
    }

    private void publishVote(final InfoTitle info) {
        mReq = new RequestUtil<Vote>(REQUEST_TIMEOUT) {

            @Override
            public Request<Vote> onPrepareRequest(Listener<Vote> listener, ErrorListener errListener) {
                return new RequestAddVote(info, listener, errListener);
            }

            @Override
            public void onSuccess(Vote response) {
                getActivity().finish();
                Toast.makeText(mContext, R.string.publish_success, Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new UpdateStatusUIEvent());
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        mReq.add2queue();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mReq != null) {
            mReq.cancel();
            mReq = null;
        }
    }
}
