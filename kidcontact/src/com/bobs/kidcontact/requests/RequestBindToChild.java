package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 13-12-23.
 */
public final class RequestBindToChild extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestBindToChild(BindToChild input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_BIND_TO_CHILD),
                input, Succeed.class, listener, errorListener);
    }

    public static class BindToChild {
        @SerializedName("childBirthDay")
        public String mChildBirthDay;
        @SerializedName("childName")
        public String mChildName;
        @SerializedName("name")
        public String mName;
    }
}
