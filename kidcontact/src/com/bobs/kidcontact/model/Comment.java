package com.bobs.kidcontact.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hzheng3 on 14-1-14.
 */
public class Comment implements Parcelable {
    @SerializedName("id")
    public int mId;
    @SerializedName("creatorId")
    public int mCreatorId;
    @SerializedName("creatorName")
    public String mCreatorName;
    @SerializedName("info")
    public String mInfo;
    @SerializedName("dateCreated")
    public String mDateCreated;
    @SerializedName("lastUpdated")
    public String mLastUpdated;
    @SerializedName("commentReplies")
    public ArrayList<Reply> mReplies;
    @SerializedName("likes")
    public int mLikes;
    @SerializedName("eatStar")
    public int mEatStar;
    @SerializedName("sleepStar")
    public int mSleepStar;
    @SerializedName("classStar")
    public int mClassStar;


    public ArrayList<Reply> getReplies() {
        return mReplies;
    }

    public void setReplies(ArrayList<Reply> mReplies) {
        this.mReplies = mReplies;
    }

    public Date getCreatedDate() {
        return UIUtil.stringToDate(mDateCreated, Constants.DATA_DATE_FORMAT);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeInt(this.mCreatorId);
        dest.writeString(this.mCreatorName);
        dest.writeString(this.mInfo);
        dest.writeString(this.mDateCreated);
        dest.writeString(this.mLastUpdated);
        dest.writeSerializable(this.mReplies);
        dest.writeInt(this.mLikes);
        dest.writeInt(this.mEatStar);
        dest.writeInt(this.mSleepStar);
        dest.writeInt(this.mClassStar);
    }

    public Comment() {
    }

    @SuppressWarnings("unchecked")
    private Comment(Parcel in) {
        this.mId = in.readInt();
        this.mCreatorId = in.readInt();
        this.mCreatorName = in.readString();
        this.mInfo = in.readString();
        this.mDateCreated = in.readString();
        this.mLastUpdated = in.readString();
        this.mReplies = (ArrayList<Reply>) in.readSerializable();
        this.mLikes = in.readInt();
        this.mEatStar = in.readInt();
        this.mSleepStar = in.readInt();
        this.mClassStar = in.readInt();
    }

    public static Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}
