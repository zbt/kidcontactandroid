package com.bobs.kidcontact.requests;

import android.util.Log;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.utils.UIUtil;

import org.apache.http.entity.mime.content.StringBody;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-6.
 */

/**
 * The bind request is submitted with multipart-form request which contain following parts
 * Please only supply one PicPair in ArrayList<PicPair> argument
 * MUST: "name" : "xxxxxx"
 * MUST :"teacherId" : "xxxxxx"
 * MUST: picture related
  */
public final class RequestBindTeacherWithAvatar extends CookieMuiltiPicUploadRequest<CookieGsonRequest.Succeed> {
    private static final String TAG = "RequestBindToChildWithAvatar";

    private String mName;
    private String mTeacherId;
    public RequestBindTeacherWithAvatar(String teacherId, String name,
                                        ArrayList<PicPair> picPairs,
                                        Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(getURL(), picPairs, Succeed.class, listener, errorListener);

        mTeacherId = UIUtil.encoder(teacherId);
        mName = UIUtil.encoder(name);
        buildTeacherInfo();
    }

    @SuppressWarnings("deprecation")
    private void buildTeacherInfo() {
        if (mTeacherId != null && !mTeacherId.isEmpty()
                && mName != null && !mName.isEmpty()) {
            try {
                entity.addPart("teacherId", new StringBody(mTeacherId));
                entity.addPart("name", new StringBody(mName));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "add teacher info failed " + e.getMessage());
            }
        }
    }

    private static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_BIND_TEACHER_WITH_AVATAR);
    }
}
