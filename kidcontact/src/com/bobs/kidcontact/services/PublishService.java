package com.bobs.kidcontact.services;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.util.EncodingUtils;

import com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest.PicPair;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class PublishService extends Service {

	/*
	[{mid:mid,
	  pics:[{picPath:abspath,thumbPath:abspath, id:0},
	  {picPath:abspath,thumbPath:abspath, id:0}]
	  },
	  {mid:mid,
	   pics:[]
	  }
	  ]
	*/
	
	public class MomentItem{
		@SerializedName("mid")
		public long mid;
		
		@SerializedName("pics")
		public List<PicItem> pics;
	}
	
	public class PicItem {
		@SerializedName("pid")
		public long pid;
		
		@SerializedName("picPath")
		public String picPath;
		
		@SerializedName("thumbPath")
		public String thumbPath;
	}
	final String PUBLISH_CACHE_FILE = "publish_queue.cache";
	final private List<MomentItem> queue = Collections.synchronizedList(new LinkedList<MomentItem>());
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void writeMomentsCache(String cache){ 
        try{ 
        	FileOutputStream fout = this.openFileOutput(PUBLISH_CACHE_FILE, this.MODE_PRIVATE);
         	byte [] bytes = cache.getBytes(); 
         	fout.write(bytes); 
         	fout.close(); 
        }
        catch(Exception e){
        	e.printStackTrace(); 
        } 
    }
    
    public String readMomentsCache(){
    	String cache = ""; 
        try{ 
	        FileInputStream fin = this.openFileInput(PUBLISH_CACHE_FILE); 
	        int length = fin.available(); 
	        byte [] buffer = new byte[length]; 
	        fin.read(buffer);
	        cache = EncodingUtils.getString(buffer, "UTF-8"); 
	        fin.close();
        }catch(Exception e){
        	//Get Cache file exception
        }
        return cache; 
    }
    
	private void loadQueue(){
		Gson gson = new Gson();
        String cache = readMomentsCache();
        if (!cache.equals("")){
	        List<MomentItem> q = (LinkedList<MomentItem>)gson.fromJson(cache, new TypeToken<LinkedList<MomentItem>>(){}.getType());
	        queue.clear();
	        queue.addAll(q);
        }
	}
	
	private void saveQueue(){
		Gson gson = new Gson();
        String js =  gson.toJson(queue);
        writeMomentsCache(js);
	}
	
	public void addPic2Queue(long mid, List<PicPair> pics){
		MomentItem mi = new MomentItem();
		mi.mid = mid;
		mi.pics = new ArrayList<PicItem>();
		for (PicPair pic:pics){
			PicItem pi = new PicItem();
			pi.picPath = pic.mPic.getAbsolutePath();
			pi.thumbPath = pic.mThumb.getAbsolutePath();
			pi.pid = 0;
			mi.pics.add(pi);
		}
		queue.add(mi);
	}
	
	public void addPic2Queue(MomentItem mi){
		queue.add(mi);
	}
	
	public void clearQueue(){
		queue.clear();
	}
	
	public List<MomentItem> queryQueue(){
		return queue;
	}

}
