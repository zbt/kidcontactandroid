package com.bobs.kidcontact.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Picture;
import com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest;
import com.bobs.kidcontact.requests.RequestUpdateAvatar;
import com.bobs.kidcontact.ui.view.FragmentPictureSelector;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by junyongl on 13-12-26.
 */
public class ActivityParentSettings extends ActionBarActivityBase implements DialogInterface.OnClickListener {
    private LinearLayout mAvatarArea;
    private NetworkRoundImageView mAvatarImg;
    private Context mContext;
    private File cacheAvatarFile;
    private Bitmap mBitmap;
    private ProgressDialog mDialog;
    private EditText mEtKidName;
    private EditText mEtKidBirth;
    private EditText mEtParentName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_parent_settings);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        mEtKidName = (EditText) findViewById(R.id.et_kid_name);
        mEtKidName.setEnabled(false);
        mEtKidBirth = (EditText) findViewById(R.id.et_kid_birthday);
        mEtKidBirth.setEnabled(false);
        mEtParentName = (EditText) findViewById(R.id.et_parent_appellation);
        mAvatarArea = (LinearLayout) findViewById(R.id.avatar_container);
        mAvatarArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentPictureSelector fragment = new FragmentPictureSelector();
                fragment.show(getSupportFragmentManager(), "select_picture");
                fragment.setOnClickListener(ActivityParentSettings.this);
            }
        });

        mAvatarImg = (NetworkRoundImageView) findViewById(R.id.user_avatar);
        mAvatarImg.setImageResource(R.drawable.ic_avatar);
        mAvatarImg.setImageUrl(Constants.getAvatarThumbURLOfCreator(KidContactApp.getInstance().getId()),
                KidContactApp.getInstance().getImageLoader());
        mAvatarImg.setDefaultImageResId(R.drawable.ic_avatar);
        mDialog = new ProgressDialog(mContext) {
            @Override
            protected void onStop() {
                super.onStop();
            }
        };
        findViewById(R.id.bt_update_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityUpdatePassword.class);
                startActivity(intent);
            }
        });
        loadBoundStates();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_PICK) {
            Uri photoUri = data.getData();
            if (cacheAvatarFile.length() > 0) {
                mBitmap = BitmapFactory.decodeFile(cacheAvatarFile.getPath());
                mAvatarImg.setImageBitmap(mBitmap);
                updateAvatar();
            } else if (photoUri != null) {
                startActivityForResult(UIUtil.getCropIntent(photoUri), Constants.REQUEST_CROP) ;
            }
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CAPTURE) {
            Uri tempUri = Uri.fromFile(cacheAvatarFile);
            startActivityForResult(UIUtil.getCaptureCropIntent(tempUri), Constants.REQUEST_CROP) ;
        } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CROP) {
            Bundle extras = data.getExtras();

            if (extras != null) {
                mBitmap = extras.getParcelable("data");
                mAvatarImg.setImageBitmap(mBitmap);
                FileOutputStream fOut = null;
                try {
                    fOut = new FileOutputStream(cacheAvatarFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                mBitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                try {
                    fOut.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                updateAvatar();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private CookieMuiltiPicUploadRequest.PicPair getAvatarImagePair() {
        File cacheAvatarThumb = null;
        Bitmap t = ThumbnailUtils.extractThumbnail(mBitmap, 64, 64);
        FileOutputStream fos;
        try {
            cacheAvatarThumb = UIUtil.getAvatarImageTempFile("avatar_thumb");
            fos = new FileOutputStream(cacheAvatarThumb);
            t.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            t.recycle();
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (mBitmap != null) {
            mBitmap.recycle();
        }
        if ( cacheAvatarThumb != null && cacheAvatarFile != null){
            CookieMuiltiPicUploadRequest.PicPair pair = new CookieMuiltiPicUploadRequest.PicPair();
            pair.mPic = cacheAvatarFile;
            pair.mThumb = cacheAvatarThumb;
            return pair;
        } else {
            return null;
        }
    }

    private void updateAvatar(){
        AsyncTask<Void, String, RequestUtil<Picture>> mUpdateAvatarTask = new AsyncTask<Void, String, RequestUtil<Picture>>() {
            @Override
            protected RequestUtil<Picture> doInBackground(Void... params) {
                final ArrayList<CookieMuiltiPicUploadRequest.PicPair> pics = new ArrayList<CookieMuiltiPicUploadRequest.PicPair>();
                CookieMuiltiPicUploadRequest.PicPair picPair = getAvatarImagePair();
                if (picPair == null) {
                    return null;
                }
                pics.add(picPair);
                RequestUtil<Picture> req = new RequestUtil<Picture>() {
                    @Override
                    public Request<Picture> onPrepareRequest(
                            Response.Listener<Picture> listener, Response.ErrorListener errListener) {
                        return new RequestUpdateAvatar(pics, listener, errListener);
                    }

                    @Override
                    public void onSuccess(Picture response) {
                        KidContactApp.getInstance().saveUserAvatar(KidContactApp.getInstance().getId());
                        mDialog.dismiss();
                        Toast.makeText(mContext, R.string.avatar_update_success, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(VolleyError e) {
                        super.onError(e);
                        mDialog.dismiss();
                        Toast.makeText(mContext, R.string.avatar_update_fail, Toast.LENGTH_SHORT).show();
                    }
                };
                req.setTimeout(10000);
                return req;
            }

            @Override
            protected void onCancelled() {
                super.onCancelled();
                mDialog.dismiss();
            }

            @Override
            protected void onPostExecute(RequestUtil<Picture> result) {
                super.onPostExecute(result);
                if (result != null) {
                    mDialog.setMessage("uploading ...");
                    result.add2queue();
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mDialog.setMessage("Begin to upload ...");
                mDialog.show();
            }

            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);
                if (values != null && values.length > 0) {
                    mDialog.setMessage(values[0]);
                }
            }
        };
        mUpdateAvatarTask.execute();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadBoundStates(){
        Boolean isSave = KidContactApp.getSharePrefUtil().getSaveBoundState();
        if(isSave) {
            mEtKidName.setText(KidContactApp.getSharePrefUtil().getChildName());
            mEtKidBirth.setText(KidContactApp.getSharePrefUtil().getChildBirth());
            mEtParentName.setText(KidContactApp.getSharePrefUtil().getParentName());
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        cacheAvatarFile = UIUtil.getAvatarImageTempFile("cacheAvatar");
        switch (which) {
            //index of "picture_picker_array"
            case 0:
                //Take picture via camera
                Intent intent_capture = UIUtil.getCaptureImageIntent(cacheAvatarFile);
                if (intent_capture != null) {
                    startActivityForResult(intent_capture, Constants.REQUEST_CAPTURE);
                }
                break;
            case 1:
                //Select picture from gallery
                Intent intent = UIUtil.getUserImageIntent(cacheAvatarFile);
                if (intent != null) {
                    startActivityForResult(intent, Constants.REQUEST_PICK);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
