package com.bobs.kidcontact.requests;


import com.android.volley.Response;
import com.bobs.kidcontact.model.Reply;

/**
 * Created by hzheng3 on 14-1-11.
 */
public abstract class RequestAddReply extends RequestAddInfoId<Reply> {
    public RequestAddReply(String url, InfoId input, Response.Listener<Reply> listener, Response.ErrorListener errorListener) {
        super(url, input, Reply.class, listener, errorListener);
    }
}
