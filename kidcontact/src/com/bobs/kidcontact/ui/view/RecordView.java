package com.bobs.kidcontact.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Moment;
import com.bobs.kidcontact.model.Record;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.UIUtil;

/**
 * Created by junyongl on 13-12-27.
 */
public class RecordView extends LinearLayout {
    private static final String TAG = "MomentView";
    private Context mContext;
    private LayoutInflater mInflater;
    private LinearLayout mViewContainer;
    private TextView mAuthor;
    private TextView mCreatedTime;
    private NetworkRoundImageView mAvatar;
    private TextView mContent;
    private FrameLayout mPicturesContainer;
    private TextView mAddFavor;
    private ImageView mAddComment;

    public RecordView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewContainer = (LinearLayout) mInflater.inflate(R.layout.view_moment, null);
        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mAuthor = (TextView) mViewContainer.findViewById(R.id.moment_author);
        mCreatedTime = (TextView) mViewContainer.findViewById(R.id.moment_created_time);
        mAvatar = (NetworkRoundImageView) mViewContainer.findViewById(R.id.user_avatar);
        mContent = (TextView) mViewContainer.findViewById(R.id.moment_content);
        mAddFavor = (TextView) mViewContainer.findViewById(R.id.add_favor);

        mAddComment = (ImageView) mViewContainer.findViewById(R.id.add_comment);
//        mImageViews = (MomentPicturesView) mViewContainer.findViewById(R.id.moment_pictures);
        addView(mViewContainer, params);

    }

    public void bind(Record record){
        if (record.getAuthor() != null) {
            mAuthor.setText(record.getAuthor());
        }
        if (record.getContent() != null) {
            mContent.setText(record.getContent());
        }
        mAvatar.setImageUrl(Constants.getAvatarThumbURLOfCreator(record.mCreatorId),
                KidContactApp.getInstance().getImageLoader());

        String dateFormat = mContext.getString(R.string.moment_created_time_string_fromat);
        mCreatedTime.setText(UIUtil.dateToString(record.getCreatedDate(), dateFormat));

    }

}
