package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.TimeTable;


/**
 * Created by hzheng3 on 14-6-9.
 */


public final class RequestAddTimeTable extends RequestAddPic<TimeTable> {

    public RequestAddTimeTable(PicPair input, Response.Listener<TimeTable> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_UPLOAD_TIME_TABLE_PIC), input, TimeTable.class, listener, errorListener);
    }
}
