package com.bobs.kidcontact.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestUpdatePassword;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;

/**
 * Created by junyongl on 14-2-11.
 */
public class ActivityUpdatePassword extends ActionBarActivityBase {
    Button mBtnUpdate;
    Button mBtnCancel;
    EditText mPassword;
    EditText mPasswordVerify;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtil.setThemeForUser(this);
        mContext = this;
        setContentView(R.layout.activity_update_password);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        mBtnUpdate = (Button) findViewById(R.id.bt_left);
        mBtnUpdate.setText(getString(R.string.update));
        mBtnCancel = (Button) findViewById(R.id.bt_right);
        mBtnCancel.setText(getString(R.string.cancel));
        mPassword = (EditText) findViewById(R.id.et_password);
        mPasswordVerify = (EditText) findViewById(R.id.et_password_verify);
        mBtnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePassword();
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void updatePassword() {
        final String password = preCheckPassword();
        if (password != null) {
            final RequestUpdatePassword.UpdatePassword updatePassword = new RequestUpdatePassword.UpdatePassword();
            updatePassword.mPasswd = password;
            RequestUtil<CookieGsonRequest.Succeed> requestUtil = new RequestUtil<CookieGsonRequest.Succeed>() {
                @Override
                public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {

                    return new RequestUpdatePassword(updatePassword, listener, errListener);
                }

                @Override
                public void onSuccess(CookieGsonRequest.Succeed response) {
                    Toast.makeText(mContext, R.string.password_changed, Toast.LENGTH_SHORT).show();
                    onBackPressed();

                }

                @Override
                public void onError(VolleyError e) {
                    super.onError(e);
                    Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                }
            };
            requestUtil.add2queue();
        }
    }

    private String preCheckPassword() {
        if (getPasswd()!= null && getPasswdVerify() != null && getPasswd().equals(getPasswdVerify())) {
            return getPasswd();
        } else {
            Toast.makeText(mContext, R.string.password_confirm_fail, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    private String getPasswd() {
        String passwd = mPassword.getText().toString();
        if (passwd.length() > 0) {
            return passwd;
        }
        return null;
    }

    private String getPasswdVerify() {
        String passwd = mPasswordVerify.getText().toString();
        if (passwd.length() > 0) {
            return passwd;
        }
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
