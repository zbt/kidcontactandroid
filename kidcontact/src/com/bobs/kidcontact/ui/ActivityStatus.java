package com.bobs.kidcontact.ui;

import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.widget.TextView;

import com.bobs.kidcontact.R;

public class ActivityStatus extends ActionBarActivityBase {
	private TextView mTextView1;
	public static final String EXTRA_INFO = "extra_info";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        mTextView1 = (TextView) findViewById(R.id.tv_for_debug);
        Bundle extra = getIntent().getExtras();
        if (extra == null) return;

        String value = extra.getString(EXTRA_INFO);
        if (value != null) {
        	mTextView1.setText(value);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_status, menu);
        return true;
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }

}
