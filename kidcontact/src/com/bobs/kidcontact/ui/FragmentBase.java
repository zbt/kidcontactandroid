package com.bobs.kidcontact.ui;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import java.lang.ref.WeakReference;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.model.Comment;
import com.bobs.kidcontact.model.Moment;
import com.bobs.kidcontact.model.Notice;
import com.bobs.kidcontact.model.Version;

public abstract class FragmentBase extends Fragment {
    private static final String TAG = "FragmentBase";
    protected EventHandlerForView mEventHandlerForView = null;
    
    public Handler getEventHandlerForView() {
        return mEventHandlerForView;
    }

    /**
     * A handler for events that are associated with views.
     * @author zhanglong
     */
    public static class EventHandlerForView extends Handler {
        WeakReference<FragmentBase> mWeakContext = null;

        public EventHandlerForView(FragmentBase context) {
            mWeakContext = new WeakReference<FragmentBase>(context);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
            case Events.EVENT_CHAT_SERVICE_BOUND:
                mWeakContext.get().refreshViews();
            case Events.EVENT_CONVERSATION_CHANGED:
                Bundle dataConv = msg.getData();
                if (null == dataConv) {
                    break;
                }
                Conversation conversation = dataConv.getParcelable(Events.KEY_CONVERSATION_CHANGED);
                if (null == conversation) {
                    break;
                }
                mWeakContext.get().refreshViews(conversation);
                break;
            case Events.EVENT_CTRL_MSG_NEW:
                Bundle dataCtrlMsg = msg.getData();
                if (null == dataCtrlMsg) {
                    break;
                }
                CtrlMessage ctrlMsg = dataCtrlMsg.getParcelable(Events.KEY_CTRL_MSG);
                if (null == ctrlMsg) {
                    break;
                }
                mWeakContext.get().refreshViews(ctrlMsg);
                break;
            case Events.EVENT_CTRL_MSG_DELETED:
                Bundle dataCtrlMsgDel = msg.getData();
                if (null == dataCtrlMsgDel) {
                    break;
                }
                CtrlMessage ctrlMsgDel = dataCtrlMsgDel.getParcelable(Events.KEY_CTRL_MSG);
                if (null == ctrlMsgDel) {
                    break;
                }
                mWeakContext.get().refreshViewsDel(ctrlMsgDel);
                break;
            case Events.EVENT_VERSION_UPDATED:
                // TODO get the version object
                Version version = new Version();
                mWeakContext.get().refreshViews(version);
                break;
            case Events.EVENT_VERSION_UPDATE_CANCELED:
                mWeakContext.get().refreshViewsVersionUpdateCancel();
                break;
            case Events.EVENT_NETWORK_REQUEST_ERROR:
                Bundle dataRequest = msg.getData();
                if (null == dataRequest) {
                    break;
                }
                int type = dataRequest.getInt(Events.KEY_NETWORK_REQUEST_ERROR);
                mWeakContext.get().onNetworkRequestError(type);
                break;
            default:
                // Should not come here!
                Log.w(TAG, "unrecognized event!");
                break;
            }
        }
    }

    /** Refresh the views according to a newly changed conversation */
    protected void refreshViews(Conversation conversation) {
        Log.i(TAG, "FragmentBase.refreshViews()");
    }
    
    /** Refresh the views according to a newly updated notice */
    protected void refreshViews(Notice notice) {
    }
    
    /** Refresh the views according to a newly updated comment */
    protected void refreshViews(Comment comment) {
    }
    
    /** Refresh the views according to a newly updated moment */
    protected void refreshViews(Moment moment) {
    }
    
    /** Refresh the views according to a newly updated version */
    protected void refreshViews(Version version) {
    }
    
    /** Refresh the views according to a newly received CtrlMessage */
    protected void refreshViews(CtrlMessage msg) {
        Log.d(TAG, "received a CtrlMessage.");
    }
    
    /** Refresh the views according to a newly deleted CtrlMessage */
    protected void refreshViewsDel(CtrlMessage msg) {
        Log.d(TAG, "handle delete CtrlMessage.");
    }
    
    /** Refresh the views on canceling version update */
    protected void refreshViewsVersionUpdateCancel() {
    }
    
    /** Refresh the views */
    protected void refreshViews() {
    }
    
    /** Refresh the views asynchronously */
    protected void refreshViewsAsync() {
    }
    
    /** Do something on a network error */
    protected void onNetworkRequestError(int type) {
    }

    @Override
    public void onResume() {
        super.onResume();
        mEventHandlerForView = new EventHandlerForView(this);
        refreshViewsAsync();
    }

}
