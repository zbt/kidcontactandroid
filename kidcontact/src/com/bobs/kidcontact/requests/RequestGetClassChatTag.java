package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CloudTag;

/**
 * Get cloud tag for class chat
 */
public final class RequestGetClassChatTag extends CookieGsonRequest<CloudTag> {
    public RequestGetClassChatTag(Response.Listener<CloudTag> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getURL(),
                CloudTag.class, listener, errorListener);
    }

    public static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_GET_CLASS_CHAT_TAG);
    }
}
