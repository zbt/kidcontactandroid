package com.bobs.kidcontact.requests;

import java.util.ArrayList;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CommentList;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-11.
 */

/**
 * Only supposed to be called by teacher
 */
public final class RequestAddComment extends CookieGsonJsonRequest<CommentList> {
    public RequestAddComment(AddComment input,
                             Response.Listener<CommentList> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ADD_COMMENT),
                input, CommentList.class, listener, errorListener);
    }

    public static class AddComment {
        @SerializedName("info")
        public String mInfo;
        @SerializedName("eatStar")
        public int mEatStar;
        @SerializedName("classStar")
        public int mClassStar;
        @SerializedName("sleepStar")
        public int mSleepStar;
        @SerializedName("children")
        public ArrayList<Long> mChidren;
    }
}