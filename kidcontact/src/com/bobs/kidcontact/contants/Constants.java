package com.bobs.kidcontact.contants;

import com.bobs.kidcontact.utils.L;

public final class Constants {
    public static final String HTTP = "http";
    public static final String SERVER_IP = "106.186.25.55";
    public static final String PORT = "8080";
    public static final String ROOT_PATH = "KidContact";

    public static final String SEPERATER = "/";
    public static final String AND = "&";
    public static final String QUESTION = "?";
    public static final String EQUAL = "=";

    public static final int QUERY_PAGE_SIZE = 10;
    public static final String URL_TAG = "URL";
    public static final int PER_PAGE = 100;

    public static final boolean DEBUG = true;
    public static final int DEVICE_TYPE_ANDROID = 3;
    public static final int HEARTBEAT_PERIOD = 5; // seconds

    public static String getFunctionURL(String function) {
        String ret;
        String http, ip, port, rootPath;

        http = HTTP;
        ip = SERVER_IP;
        port = PORT;
        rootPath = ROOT_PATH;

        if (FUNC_LOGIN.equals(function)) {
            ret = http + "://" + ip + ":" + port + "/" + rootPath + "/" + function;
        } else {
            ret = http + "://" + ip + ":" + port + "/" + rootPath + "/api/rest/" + function;
        }
        L.i(URL_TAG, "URL of function " + function + " is " + ret);
        return ret;
    }

    private static String _getAvatarURLOfCreator(long creatorId, boolean thumbnail) {
        String url = Constants.getFunctionURL(Constants.FUNC_GET_CREATOR_AVATAR);
        url += QUESTION + "creatorId" + EQUAL + creatorId + AND + "thumbnail" + EQUAL + thumbnail;
        return url;
    }
    public static String getAvatarURLOfCreator(long creatorId) {
        return _getAvatarURLOfCreator(creatorId, false);
    }
    public static String getAvatarThumbURLOfCreator(long creatorId) {
        return _getAvatarURLOfCreator(creatorId, true);
    }

    public static final int TYPE_TEACHER = 1;
    public static final int TYPE_PARENT = 2;

    public static final String FUNC_LOGIN = "j_spring_security_check";
    public static final String FUNC_ADDUSER = "addUser";
    public static final String FUNC_CHECKBOUND_STATUS = "checkBoundStatus";
    public static final String FUNC_BIND_TEACHER = "bindTeacher";
    public static final String FUNC_BIND_TO_CHILD = "bindToChild";
    public static final String FUNC_GETMOMENT="getMoments";
    public static final String FUNC_GETPIC = "getPic";
    public static final String FUNC_ADDMOMENT = "addMoment";
    public static final String FUNC_ADD_ACK_NOTICE = "addAckNotice";
    public static final String FUNC_ADD_PUBLIC_NOTICE = "addPublicNotice";
    public static final String FUNC_GET_NOTICES = "getNotices";
    public static final String FUNC_ADDRECORD = "addRecord";
    public static final String FUNC_GET_COMMENT_RECORDS = "getCommentRecords";
    public static final String FUNC_ADD_COMMENT = "addComment";
    public static final String FUNC_UPDATE_AVATAR = "updateAvatar";
    public static final String FUNC_BIND_CHILD_WITH_AVATAR = "bindToChildWithAvatar";
    public static final String FUNC_BIND_TEACHER_WITH_AVATAR = "bindTeacherWithAvatar";
    public static final String FUNC_GET_CREATOR_AVATAR = "getCreatorAvatar";
    public static final String FUNC_GET_CHILDREN = "getChildren";
    public static final String FUNC_ADD_ACKNOTICE_REPLY = "addAckNoticeReply";
    public static final String FUNC_ADD_VOTE = "addVote";
    public static final String FUNC_ADD_VOTE_REPLY = "addVoteReply";
    public static final String FUNC_ADD_RECORD_REPLY = "addRecordReply";
    public static final String FUNC_ADD_COMMENT_REPLY = "addCommentReply";
    public static final String FUNC_ADD_MOMENT_REPLY = "addMomentReply";
    public static final String FUNC_UPDATE_PASSWD = "updatePassword";
    public static final String FUNC_DELETE_VOTE = "deleteVote";
    public static final String FUNC_DELETE_ASSIGNMENT = "deleteAssignment";
    public static final String FUNC_DELETE_PUBLIC_NOTICE = "deletePublicNotice";
    public static final String FUNC_DELETE_ACK_NOTICE = "deleteAckNotice";
    public static final String FUNC_DELETE_MOMENT = "deleteMoment";
    public static final String FUNC_DELETE_RECORD = "deleteRecord";
    public static final String FUNC_DELETE_COMMENT = "deleteComment";
    public static final String FUNC_LOGOUT = "logout";
    public static final String FUNC_ADD_LEAVE = "addLeave";
    public static final String FUNC_ADD_LEAVE_REPLY = "addLeaveReply";
    public static final String FUNC_GET_LEAVES = "getLeaves";
    public static final String FUNC_ADD_ASSIGNMENT = "addAssignment";
    public static final String FUNC_ADD_ASSIGNMENTREPLY = "addAssignmentReply";
    public static final String FUNC_UPDATE_CLOUDID = "updateCloudId";
    public static final String FUNC_GET_USERINFO_OF_CLOUDID = "getUserInfoOfCloudId";
    public static final String FUNC_GET_TEACHERS = "getTeachers";
    public static final String FUNC_INFO_FOR_SINGLE_CHAT = "getInfoForSingleChat";
    public static final String FUNC_GET_CLASS_CHAT_TAG = "getClassChatTag";
    public static final String FUNC_GET_BOUND_STATUS = "getBoundStatus";
    public static final String FUNC_RESET_PASSWORD = "resetPassword";
    public static final String FUNC_LIKE_MOMENT = "likeMoment";
    public static final String FUNC_LIKE_RECORD = "likeRecord";
    public static final String FUNC_ZIP_ASSIGNMENT = "zipAssignment";
    public static final String FUNC_HEART_BEAT = "heartBeat";
    public static final String FUNC_SEND_SINGLE_CLOUD_MSG = "sendSingleCloudMsg";
    public static final String FUNC_GET_UNREAD_MSG = "getUnreadMsg";
    public static final String FUNC_SEND_CLASS_CHAT_MSG = "sendClassChatMsg";
    public static final String FUNC_UPLOAD_PICS = "uploadPics";
    public static final String FUNC_UPLOAD_EAT_MENU_PIC = "uploadEatMenuPic";
    public static final String FUNC_UPLOAD_TIME_TABLE_PIC = "uploadTimeTablePic";
    public static final String FUNC_RENAME_CLASS = "renameClass";
    public static final String FUNC_GET_EAT_MENU = "getEatMenu";
    public static final String FUNC_GET_TIME_TABLE = "getTimeTable";
    public static final String FUNC_ASSOCIATE_MOMENT_PICS = "associateMomentPics";
    // for teacher switch
    public static final String FUNC_CREATE_USER_REG_CODE = "createUserRegCode";
    public static final String FUNC_CREATE_CLASS_AND_TEACHER = "createKlassAndTeacher";
    public static final String FUNC_GET_ALL_MY_CLASSES = "getAllMyKlasses";
    public static final String FUNC_GET_MY_CURRENT_CLASS = "getMyCurrentKlass";
    public static final String FUNC_JOIN_CLASS = "joinKlass";
    public static final String FUNC_SWITCH_CLASS = "switchKlass";

    public static final String KEY_ERRORCODE = "errCode";
    public static final String KEY_DESCRIPTION = "errMsg";
    public static final String KEY_PAGE = "page";

    public static final String DATA_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final int REQUEST_CROP = 2;
    public static final int REQUEST_PICK = 1;
    public static final int REQUEST_CAPTURE = 3;

    public static enum PublishType {
        MOMENT, REVIEW, NOTICE, VOTE, RECORD, ASSIGNMENT, CLASSMENU, CLASSCAL,CLASSNAME
    }

    public static final int PIC_MAX_WIDTH = 800;
    public static final int PIC_MAX_HEIGHT = 600;
    public static final int THUMB_MAX_WIDTH = 400;
    public static final int THUMB_MAX_HEIGHT = 300;
}
