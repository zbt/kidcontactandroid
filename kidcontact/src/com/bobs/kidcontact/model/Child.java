package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by hzheng3 on 14-1-19.
 */
public class Child {
    @SerializedName("id")
    public long mId;
    @SerializedName("name")
    public String mName;
    @SerializedName("birth")
    public String mBirth;
    @SerializedName("parents")
    public ArrayList<Parent> mParents;
    @SerializedName("childPics")
    public ArrayList<PicProxy> mPics;

    public transient boolean isSelected = false;

    public static class ChildComparator implements Comparator<Child> {
        @Override
        public int compare(Child child, Child child1) {
            if( child.mId < child1.mId)
                return -1;
            else if (child.mId > child1.mId)
                return 1;
            else
                return 0;
        }
    }
}
