package com.bobs.kidcontact.ui;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.utils.RequestUtil;

/**
 * Created by leon on 14-4-27.
 */
public class ActivityHarvardConsult extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_harvard_consult);
    }
}
