package com.bobs.kidcontact.chat.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.ui.KidContactApp;

/**
 * A util class who is responsible for managing remote login event.
 * @author zhanglong
 */
public class RemoteLoginManager {
    private static final String TAG = "RemoteLoginManager";
    
    /** Check if this CtrlMessage indicates a remote login event */
    public static boolean check(CtrlMessage msg) {
        return CtrlMessage.TYPE_REMOTE_LOGIN == msg.getType();
    }
    
    public static void handle(Context context) {
        KidContactApp.logOut(context);
        context.stopService(new Intent(context, ChatService.class));
        Log.i(TAG, "log out.");
    }
}
