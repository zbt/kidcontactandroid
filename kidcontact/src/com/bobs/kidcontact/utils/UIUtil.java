package com.bobs.kidcontact.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.net.ParseException;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.format.DateUtils;
import android.util.Log;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.ui.KidContactApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by junyongl on 14-1-13.
 */
public class UIUtil {

    private static final String TAG = "UIUtil";

    public static Date stringToDate(String dateString, String dateFormatString) {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(dateFormatString);
        format.setTimeZone(TimeZone.getTimeZone("CST"));
        try {
            date = format.parse(dateString);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String dateToString(Date date, String dateFormatString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
        dateFormat.setTimeZone(TimeZone.getDefault());
        String dateString = "";
        try {
            dateString = dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateString;
    }

    /**
     * Set the activity theme according to the Account type
     *
     * @param activity
     */
    public static void setThemeForUser(ActionBarActivity activity) {
        KidContactApp sApp = KidContactApp.getInstance();
        switch (sApp.getRole()) {
            case Constants.TYPE_TEACHER :
                activity.setTheme(R.style.Theme_teacher);
                break;
            case Constants.TYPE_PARENT :
                activity.setTheme(R.style.Theme_parent);
        }

    }

    public static Intent getUserImageIntent(File file) {
        if (file.getPath() != null) {
            Uri tempUri = Uri.fromFile(file);
            Intent pickCropImageIntent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickCropImageIntent.setType("image/*");
            pickCropImageIntent.putExtra("crop", "true");
            pickCropImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
            pickCropImageIntent.putExtra("outputX", 96);
            pickCropImageIntent.putExtra("outputY", 96);
            pickCropImageIntent.putExtra("aspectX", 1);
            pickCropImageIntent.putExtra("aspectY", 1);
            pickCropImageIntent.putExtra("scale", true);
            pickCropImageIntent.putExtra("outputFormat",
                    Bitmap.CompressFormat.JPEG.toString());
            return pickCropImageIntent;
        } else {
            return null;
        }
    }

    public static Intent getCaptureImageIntent(File file) {
        if (file.getPath() != null) {
            Uri tempUri = Uri.fromFile(file);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, tempUri);
            return intent;
        } else {
            return null;
        }
    }

    public static Intent getCaptureCropIntent(Uri photoUri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(photoUri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("outputX", 200);
        intent.putExtra("outputY", 200);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("scale", true);
        intent.putExtra("return-data", true);
        return intent;
    }

	public static Intent getCropIntent(Uri photoUri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setData(photoUri);
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 200);
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("scale", true);
		intent.putExtra("return-data", true);
		return intent;
	}

    public static File getAvatarImageTempFile(String fileName) {
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED)) {
            File tempFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + fileName + ".jpg");
            return tempFile;
        } else {
            Log.d(TAG, "Could not set the temp file");
            return null;
        }
    }

    public static String encoder(String string) {
        try {
            return URLEncoder.encode(string, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return string;
    }

    public static String decoder(String string) {
        try {
            return URLDecoder.decode(string, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return string;
    }

    public static CharSequence getRelativeDateTimeString(Context context, long time) {
        return DateUtils.getRelativeDateTimeString(context, time, DateUtils.MINUTE_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, 0);
    }

    public static String saveToInternalSorage(Context context, Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(context);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath();
    }

    public static Bitmap loadImageFromStorage(String path)
    {
        try {
            File f = new File(path, "profile.jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            return UIUtil.getRoundImage(b);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getRoundImage(Bitmap source) {
        if (source != null) {
            Bitmap result = Bitmap.createBitmap(source.getWidth(),
                    source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

            RectF rect = new RectF(0, 0, source.getWidth(), source.getHeight());
            paint.setColor(Color.BLACK);
            float radius = source.getWidth() / 2.0f;
            canvas.drawRoundRect(rect, radius, radius, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(source, 0, 0, paint);
            paint.setXfermode(null);

            return result;
        } else {
            return null;
        }
    }

}
