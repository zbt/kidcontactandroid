package com.bobs.kidcontact.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.Reply;
import com.bobs.kidcontact.requests.RequestAddCommentReply;
import com.bobs.kidcontact.requests.RequestAddInfoId;
import com.bobs.kidcontact.requests.RequestAddMomentReply;
import com.bobs.kidcontact.requests.RequestAddRecordReply;
import com.bobs.kidcontact.utils.RequestUtil;

/**
 * Created by junyongl on 13-12-31.
 */
public class FragmentAddReply extends Fragment {
    private long mId;
    private Class<?> mClass;
    private EditText mInfoEdit;
    private OnReplyPublishedListener mCallback;
    private Context mContext;

    public interface OnReplyPublishedListener {
        public void OnReplyPublished(Reply reply);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_comment, container, false);
        Button addReply = (Button) view.findViewById(R.id.btn_add_reply);
        mInfoEdit = (EditText) view.findViewById(R.id.et_reply_info);
        addReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishReply();
            }
        });
        return view;
    }

    public void setReplyPublishListener(OnReplyPublishedListener listener) {
        mCallback = listener;
    }

    private void publishReply() {
        if (getReplyInfo() != null && mId != 0) {
            RequestAddInfoId.InfoId infoId = new RequestAddInfoId.InfoId();
            infoId.mInfo = getReplyInfo();
            infoId.mId = mId;
            addReplyRequest(infoId);
        }
    }

    private String getReplyInfo() {
        String replyInfo = mInfoEdit.getText().toString().trim();
        if (replyInfo.length() > 0 ){
            return  replyInfo;
        }
        return null;
    }

    private void addReplyRequest(final RequestAddInfoId.InfoId infoId) {
        RequestUtil<Reply> replyRequest = new RequestUtil<Reply>() {
            @Override
            public Request<Reply> onPrepareRequest(Response.Listener<Reply> listener, Response.ErrorListener errListener) {
                if (mClass.getName() == RequestAddMomentReply.class.getName()) {
                    return new RequestAddMomentReply(infoId, listener, errListener);
                } else if (mClass.getName() == RequestAddRecordReply.class.getName()){
                    return new RequestAddRecordReply(infoId, listener, errListener);
                } else if (mClass.getName() == RequestAddCommentReply.class.getName()){
                    return new RequestAddCommentReply(infoId, listener, errListener);
                }
                return null;
            }

            @Override
            public void onSuccess(Reply response) {
                mCallback.OnReplyPublished(response);
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mInfoEdit.getWindowToken(), 0);
                mInfoEdit.setText("");
                getFragmentManager().popBackStack();
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        if (replyRequest != null) {
            replyRequest.add2queue();
        }
    }

    public void setReplyRequest(long id, Class<?> clazz) {
        mId = id;
        mClass = clazz;
    }
}
