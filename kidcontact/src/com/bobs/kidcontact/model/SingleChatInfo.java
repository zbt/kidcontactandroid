package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-23.
 */
public class SingleChatInfo {
    @SerializedName("myUserId")
    public Long mMyUserId;
    @SerializedName("myCloudId")
    public String mMyCloudId;
    @SerializedName("myDeviceType")
    public int mMyDeviceType;
    @SerializedName("targetUserId")
    public Long mTargetUserId;
    @SerializedName("targetCloudId")
    public String mTargetCloudId;
    @SerializedName("targetDeviceType")
    public int mTargetDeviceType;

    public boolean hasNull() {
        if (mMyUserId != null && mMyCloudId != null && mTargetUserId != null && mTargetCloudId != null) {
            return false;
        } else {
            return true;
        }

    }
}
