package com.bobs.kidcontact.chat.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import android.content.Context;
import android.content.Intent;
import android.net.ParseException;
import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;

public class Utils {
    public static final String ACTION_CONVERSATION_CHANGED = "conversation_changed";
    public static final String ACTION_CONVERSATION_DELETED = "conversation_deleted";
    public static final String ACTION_MESSAGE_CHANGED = "message_changed";
    public static final String ACTION_MESSAGE_DELETED = "message_deleted";
    public static final String ACTION_GROUP_JOINED = "group_joined";
    public static final String ACTION_GROUP_LEFT = "group_left";
    public static final String ACTION_CTRL_MSG_CHANGED = "ctrl_msg_changed";
    public static final String ACTION_CTRL_MSG_DELETED = "ctrl_msg.deleted";
    public static final String ACTION_AUTO_LOGIN = "auto_login";

    public static final String KEY_MESSAGE = "message";
    public static final String KEY_CONVERSATION = "conversation";
    public static final String KEY_MESSAGE_ID = "message_id";
    public static final String KEY_CONVERSATION_ID = "conversation_id";
    public static final String KEY_GROUP_ID = "group_id";
    public static final String KEY_CTRL_MSG = "key_ctrl_msg";

    public static Date stringToDate(String dateString, String dateFormatString) {
        Date date = new Date(System.currentTimeMillis());
        SimpleDateFormat format = new SimpleDateFormat(dateFormatString);
        try {
            date = format.parse(dateString);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    public static String dateToString(Date date, String dateFormatString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
        String dateString = "";
        try {
            dateString = dateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateString;
    }

    public static void sendConversationChangedBroadcast(Context context, Conversation conversation) {
        Intent i = new Intent(ACTION_CONVERSATION_CHANGED);
        i.putExtra(KEY_CONVERSATION, conversation);
        context.sendBroadcast(i);
    }

    public static void sendMessageChangedBroadcast(Context context, ChatMessage chatMessage) {
        Intent i = new Intent(ACTION_MESSAGE_CHANGED);
        i.putExtra(KEY_MESSAGE, chatMessage);
        context.sendBroadcast(i);
    }

    public static void sendMessageDeletedBroadcast(Context context, ChatMessage chatMessage) {
        Intent i = new Intent(ACTION_MESSAGE_DELETED);
        i.putExtra(KEY_MESSAGE, chatMessage);
        context.sendBroadcast(i);
    }

    public static void sendGroupJoinedBroadcast(Context context, String gourpId) {
        Intent i = new Intent(ACTION_GROUP_JOINED);
        i.putExtra(KEY_GROUP_ID, gourpId);
        context.sendBroadcast(i);
    }

    public static void sendGroupLeftBroadcast(Context context, String gourpId) {
        Intent i = new Intent(ACTION_GROUP_LEFT);
        i.putExtra(KEY_GROUP_ID, gourpId);
        context.sendBroadcast(i);
    }

    public static void sendConversationDeletedBroadcast(Context context, Conversation conversation) {
        Intent i = new Intent(ACTION_CONVERSATION_DELETED);
        i.putExtra(KEY_CONVERSATION, conversation);
        context.sendBroadcast(i);
    }

    public static void sendCtrlMessageChangedBroadcast(Context context, CtrlMessage msg) {
        Intent i = new Intent(ACTION_CTRL_MSG_CHANGED);
        i.putExtra(KEY_CTRL_MSG, msg);
        context.sendBroadcast(i);
    }

    public static void sendCtrlMessageDeletedBroadcast(Context context, CtrlMessage msg) {
        Intent i = new Intent(ACTION_CTRL_MSG_DELETED);
        i.putExtra(KEY_CTRL_MSG, msg);
        context.sendBroadcast(i);
    }

}
