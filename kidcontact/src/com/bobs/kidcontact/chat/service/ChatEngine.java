package com.bobs.kidcontact.chat.service;

import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;
import com.bobs.kidcontact.ui.ActivitySignIn;
import com.bobs.kidcontact.ui.KidContactApp;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by hzheng3 on 14-2-19.
 */
public class ChatEngine implements IChatEngine {
    // private static final String TAG = "ChatEngine";
    Gson mGson;
    private CloudMessageManager mCloudMessageManager;
    private ChatEngine(CloudMessageManager cloudMessageManager) {
        mCloudMessageManager = cloudMessageManager;
        mGson = new Gson();
    }
    private static ChatEngine sInstance;
    public static ChatEngine getInstance(CloudMessageManager cloudMessageManager) {
        if (sInstance == null) {
            sInstance = new ChatEngine(cloudMessageManager);
        }
        return sInstance;
    }
    
    public void release() {
        if (null != mCloudMessageManager) {
            mCloudMessageManager.release();
        }
        if (null != sInstance) {
            sInstance = null;
        }
    }

    @Override
    public void onChatEngineConnectSucceeded(final String id, String requestId) {
        KidContactApp.getInstance().setCloudId(id);
        new ActivitySignIn.UpdateCloudIdRequestUtil().add2queue();
    }

    @Override
    public void onChatEngineConnectFailed(String id, String requestId) {

    }

    @Override
    public void onChatEngineDisconnectSucceeded(String requestId) {
    }

    @Override
    public void onChatEngineDisconnectFailed(String requestId) {

    }

    @Override
    public void onGroupListSucceeded(List<String> groups, String requestId) {

    }

    @Override
    public void onGroupListFailed(List<String> groups, String requestId) {

    }

    @Override
    public void onGroupJoinSucceeded(List<String> groups, String requestId) {

    }

    @Override
    public void onGroupJoinFailed(List<String> groups, String requestId) {

    }

    @Override
    public void onGroupLeaveSucceeded(List<String> groups, String requestId) {

    }

    @Override
    public void onGroupLeaveFailed(List<String> groups, String requestId) {

    }

    @Override
    public void onMessage(String msg) {
        CloudMessage cloudMessage = mGson.fromJson(msg, CloudMessage.class);
        mCloudMessageManager.onCloudMessage(cloudMessage);
    }

    @Override
    public void sendCloudMessage(CloudMessage cloudMessage, ChatMessage chatMessage) {
        mCloudMessageManager.sendOutCloudMessage(cloudMessage, chatMessage);
    }

    @Override
    public void joinGroups(List<String> groups) {
        mCloudMessageManager.joinGroups(groups);
    }

    @Override
    public void leaveGroups(List<String> groups) {
        mCloudMessageManager.leaveGroups(groups);
    }
}
