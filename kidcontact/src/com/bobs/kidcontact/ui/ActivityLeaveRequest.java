package com.bobs.kidcontact.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.Leave;
import com.bobs.kidcontact.requests.RequestAddInfoTitle;
import com.bobs.kidcontact.requests.RequestAddLeave;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.Gson;
import com.squareup.timessquare.CalendarPickerView;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by junyongl on 14-1-23.
 */
public class ActivityLeaveRequest extends ActionBarActivityBase {
    private static final String TAG = "ActivityLeaveRequest";
    private RequestAddInfoTitle.InfoTitle mInfo;
    private Context mContext;
    private EditText mEtLeaveMessage;
    private RelativeLayout mDueDateSelector;
    private TextView mDateRangeTV;
    private CalendarPickerView dialogView;
    private AlertDialog theDialog;
    private Date mStartDate;
    private Date mEndDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_leave_request);
        mEtLeaveMessage = (EditText) findViewById(R.id.et_leave_message);
        mDateRangeTV = (TextView) findViewById(R.id.tv_due_date);
        mDueDateSelector = (RelativeLayout) findViewById(R.id.request_due_date);
        final Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, 1);

        final Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        mDueDateSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogView = (CalendarPickerView) getLayoutInflater().inflate(R.layout.dialog_due_date_picker, null, false);
                dialogView.init(lastYear.getTime(), nextYear.getTime()) //
                        .withSelectedDate(new Date())
                        .inMode(CalendarPickerView.SelectionMode.RANGE);
                theDialog =
                        new AlertDialog.Builder(mContext).setTitle("选择请假时间")
                                .setView(dialogView)
                                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int which) {
                                        dialogInterface.dismiss();
                                    }
                                })
                                .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        List<Date> selectedDates = dialogView.getSelectedDates();
                                        if (selectedDates.size() > 0) {
                                            mStartDate = selectedDates.get(0);
                                            mEndDate = selectedDates.get(selectedDates.size() - 1);
                                            updateDateRangeText();
                                        }
                                        dialog.dismiss();
                                    }
                                })
                                .create();
                theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override public void onShow(DialogInterface dialogInterface) {
                        Log.d(TAG, "onShow: fix the dimens!");
                        dialogView.fixDialogDimens();
                    }
                });
                theDialog.show();
            }
        });
        findViewById(R.id.bt_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prepareContent()) {
                    sendLeaveRequest();
                }
            }
        });
    }

    private void updateDateRangeText() {
        if (getLeaveStartDate() != null && getLeaveEndDate() != null) {
            String dateRange = getLeaveStartDate().equals(getLeaveEndDate()) ?
                    getLeaveStartDate() : (getLeaveStartDate() + " - " + getLeaveEndDate());
            mDateRangeTV.setText(dateRange);
        }
    }

    private boolean prepareContent() {
        if (getLeaveStartDate() == null) {
            Toast.makeText(mContext, R.string.leave_no_start_date, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (getLeaveEndDate() == null) {
            Toast.makeText(mContext, R.string.leave_no_end_date, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (getLeaveMessage() == null) {
            Toast.makeText(mContext, R.string.leave_no_reason, Toast.LENGTH_SHORT).show();
            return false;
        }

        mInfo = new RequestAddInfoTitle.InfoTitle();
        Leave.LeaveContent content = new Leave.LeaveContent();
        content.mStartDate = getLeaveStartDate();
        content.mEndDate = getLeaveEndDate();
        content.mLeaveMessage = getLeaveMessage();
        String info = new Gson().toJson(content, Leave.LeaveContent.class);
        mInfo.mInfo = info;
        mInfo.mTitle = getLeaveTitle();
        return true;
    }

    private String getLeaveTitle() {
        String childName = KidContactApp.getSharePrefUtil().getChildName();
        return (childName != null) ? new String(childName + getString(
                R.string.end_of_leave_request_title)):getString(R.string.leave_request);
    }

    private String getLeaveMessage() {
        String message = mEtLeaveMessage.getText().toString().trim();
        if (message.length() > 0) {
            return message;
        }
        return null;
    }

    private String getLeaveEndDate() {
        if (mEndDate == null) {
            return null;
        }
        return UIUtil.dateToString(mEndDate, mContext.getResources().
                getString(R.string.leave_request_time_string_fromat));
    }

    private String getLeaveStartDate() {
        if (mStartDate == null) {
            return null;
        }
        return UIUtil.dateToString(mStartDate, mContext.getResources().
                getString(R.string.leave_request_time_string_fromat));
    }

    private void sendLeaveRequest() {
        RequestUtil<Leave> request = new RequestUtil<Leave>() {
            @Override
            public Request<Leave> onPrepareRequest(Response.Listener<Leave> listener, Response.ErrorListener errListener) {
                return new RequestAddLeave(mInfo, listener, errListener);
            }

            @Override
            public void onSuccess(Leave response) {
                ((Activity)mContext).finish();
                Toast.makeText(mContext, "Success!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
                Log.e(TAG, "" + RequestUtil.interpretErr2Res(e));
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            }
        };
        request.add2queue();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
