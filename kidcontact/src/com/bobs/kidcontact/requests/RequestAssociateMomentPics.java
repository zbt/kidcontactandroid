package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.AssociatePicInput;
import com.bobs.kidcontact.model.Moment;

/**
 * Created by hzheng3 on 14-6-9.
 */
public final class RequestAssociateMomentPics extends CookieGsonJsonRequest<Moment> {
    public RequestAssociateMomentPics(AssociatePicInput input, Response.Listener<Moment> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ASSOCIATE_MOMENT_PICS),
                input, Moment.class, listener, errorListener);
    }
}
