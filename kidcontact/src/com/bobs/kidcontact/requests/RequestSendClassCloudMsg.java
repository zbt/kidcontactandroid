package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.MessageProxy;

/**
 * Created by hzheng3 on 14-05-21.
 */
public final class RequestSendClassCloudMsg extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestSendClassCloudMsg(MessageProxy input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_SEND_CLASS_CHAT_MSG),
                input, Succeed.class, listener, errorListener);
    }
}
