package com.bobs.kidcontact.chat.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.service.BaiduInfoHolder;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.chat.service.Utils;
import com.bobs.kidcontact.chat.ui.adapter.FaceAdapter;
import com.bobs.kidcontact.chat.ui.adapter.FacePageAdapter;
import com.bobs.kidcontact.chat.ui.adapter.MessageAdapter;
import com.bobs.kidcontact.chat.ui.view.MsgListView;
import com.bobs.kidcontact.chat.ui.view.MsgListViewHeader;
import com.bobs.kidcontact.model.IdProxy;
import com.bobs.kidcontact.model.SingleChatInfo;
import com.bobs.kidcontact.model.User;
import com.bobs.kidcontact.requests.RequestGetSingleChatInfo;
import com.bobs.kidcontact.ui.FragmentBase;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;
import com.bobs.kidcontact.chat.ui.view.CirclePageIndicator;
import com.bobs.kidcontact.chat.ui.view.JazzyViewPager;
import java.util.ArrayList;
import java.util.List;
import com.bobs.kidcontact.chat.ui.view.MsgListView.IXListViewListener;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.ui.ActionBarActivityBase;

/**
 * Created by leon on 14-2-23. Maintained by zhanglong
 */
public class FragmentChat extends FragmentBase implements View.OnClickListener, View.OnTouchListener, IXListViewListener, MsgListView.OnXScrollListener{
    private static final String KEY_TO_USER = "key_message_to";
    private static final String KEY_GROUP_TAG = "key_group_tag";
    private static final String KEY_TYPE = "key_type";
    private static final int REQUEST_GET_SINGLE_CHAT_INFO = 0;
    private static final int TYPE_SINGLE_CHAT = 0;
    private static final int TYPE_GROUP_CHAT = 1;
    private static final String TAG = "FragmentChat";
    private JazzyViewPager.TransitionEffect mEffects[] = { JazzyViewPager.TransitionEffect.Standard,
            JazzyViewPager.TransitionEffect.Tablet, JazzyViewPager.TransitionEffect.CubeIn,
            JazzyViewPager.TransitionEffect.CubeOut, JazzyViewPager.TransitionEffect.FlipVertical,
            JazzyViewPager.TransitionEffect.FlipHorizontal, JazzyViewPager.TransitionEffect.Stack,
            JazzyViewPager.TransitionEffect.ZoomIn, JazzyViewPager.TransitionEffect.ZoomOut,
            JazzyViewPager.TransitionEffect.RotateUp, JazzyViewPager.TransitionEffect.RotateDown,
            JazzyViewPager.TransitionEffect.Accordion, };// 表情翻页效果
    private Context mContext;
    private LinearLayout faceLinearLayout;
    private JazzyViewPager faceViewPager;
    private int currentPage = 0;
    private boolean isFaceShow = false;
    private Button sendBtn;
    private ImageButton faceBtn;
    private EditText msgEt;
    private WindowManager.LayoutParams params;
    private InputMethodManager imm;
    private MessageAdapter adapter;
    private MsgListView mMsgListView;
    private User mToUser;
    private long mConversationId;
    private long mMaxMessageId = Long.MAX_VALUE;
    private long mLastMessageId = -1L;
    private IChatService.Stub mChatServiceStub;
    private SingleChatInfo mChatInfo;
    private int mChatType = TYPE_SINGLE_CHAT;
    private String mGroupTag = "";
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleMsgBoadcast(context, intent);
        }
    };
    private int mScrollState;
    private boolean isScrollCheck = true;

    /** For single chat */
    public static FragmentChat newInstance(User toUser) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE, TYPE_SINGLE_CHAT);
        bundle.putString(KEY_TO_USER, new Gson().toJson(toUser));
        FragmentChat fragment = new FragmentChat();
        fragment.setArguments(bundle);
        return fragment;
    }

    /** For group(class) chat */
    public static FragmentChat newInstance(String tag) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_TYPE, TYPE_GROUP_CHAT);
        bundle.putString(KEY_GROUP_TAG, tag);
        FragmentChat fragment = new FragmentChat();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        Bundle bundle = getArguments(); // alias
        mChatType = bundle.getInt(KEY_TYPE);
        if (TYPE_SINGLE_CHAT == mChatType) {
            mToUser = new Gson().fromJson(bundle.getString(KEY_TO_USER), User.class);
            prepareSingleChat();
        } else {
            mGroupTag = bundle.getString(KEY_GROUP_TAG);
            prepareGroupChat();
        }
        getChatServiceStubAsync();
    }

    /** Check the status of chat service stub */
    boolean checkChatServiceStub () {
        if (null == mChatServiceStub) {
            return false;
        }
        return true;
    }

    /**
     * Since the chat service stub may not be got in time, we should do it
     * in a background loop.
     */
    private void getChatServiceStubAsync() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                mChatServiceStub =((ActionBarActivityBase) getActivity()).getChatServiceStubWait(1000);
                if (null != mChatServiceStub) {
                    Log.i(TAG, "bound to chat service.");
                } else {
                    Log.e(TAG, "failed to get chat service stub!");
                }
            }

        }).start();
    }

    /** Clear new message count for current conversation */
    private boolean clearNewMsgCount() {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return false;
        }
        boolean result = false;
        try {
            result = mChatServiceStub.clearNewMsgCount(TYPE_SINGLE_CHAT == mChatType,
                    null == mToUser ? -1 : mToUser.mId, mGroupTag);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return result;
    }
    /** Join the given groups */
    private boolean joinGroups(List<String> groups) {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return false;
        }
        if (TYPE_GROUP_CHAT == mChatType) {
            try {
                mChatServiceStub.joinGroups(groups);
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /** Tell chat service that we're chatting */
    private boolean syncChattingStatusWithService() {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return false;
        }
        try {
            if (TYPE_GROUP_CHAT == mChatType) {
                mChatServiceStub.setChatStatus(false, true, null == mToUser ? -1L : mToUser.mId, mGroupTag);
            } else {
                mChatServiceStub.setChatStatus(true, true, null == mToUser ? -1L : mToUser.mId, mGroupTag);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /** Tell chat service that we're leaving */
    private boolean syncLeftStatusWithService() {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return false;
        }
        try {
            if (TYPE_GROUP_CHAT == mChatType) {
                mChatServiceStub.setChatStatus(false, false, null == mToUser ? -1L : mToUser.mId, mGroupTag);
            } else {
                mChatServiceStub.setChatStatus(true, false, null == mToUser ? -1L : mToUser.mId, mGroupTag);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /** Do something after receiving message broadcast */
    private void handleMsgBoadcast(Context context, Intent intent) {
        if (intent.getAction().equals(Utils.ACTION_CONVERSATION_CHANGED)) {
            Conversation conversation = intent.getExtras().getParcelable(Utils.KEY_CONVERSATION);
            if (TYPE_SINGLE_CHAT == mChatType) {
                if (conversation != null && conversation.getUserId() == mToUser.mId) {
                    initData();
                }
            } else {
                if (null != conversation && conversation.getGroupId().equals(mGroupTag)) {
                    initData();
                }
            }
        } else if (intent.getAction().equals(Utils.ACTION_MESSAGE_CHANGED)) {
            Log.d(TAG, "Get the Broadcast of Message Changed");
            //TODO Message Changed
        }
    }

    /** Do some preparation for single chat */
    private void prepareSingleChat() {
        RequestUtil<SingleChatInfo> requestUtil = new RequestUtil<SingleChatInfo>() {

            @Override
            public Request<SingleChatInfo> onPrepareRequest(Response.Listener<SingleChatInfo> listener, Response.ErrorListener errListener) {
                IdProxy idProxy = new IdProxy();
                idProxy.mId = mToUser.mId;
                return new RequestGetSingleChatInfo(idProxy, listener, errListener);
            }

            @Override
            public void onSuccess(SingleChatInfo response) {
                Log.i(TAG, "request util succeed.");
                if (response.mMyCloudId != null && response.mMyUserId != null
                        && response.mTargetCloudId != null && response.mTargetUserId != null) {
                    mChatInfo = response; // why MyCloundId equals TargetCloundId if we switch the user ?!!!
                    BaiduInfoHolder.setMyUserId(response.mMyUserId);
                    BaiduInfoHolder.setCloudId(response.mMyCloudId);
                    BaiduInfoHolder.getInstance().setCloudId(response.mMyUserId, response.mMyCloudId);
                    BaiduInfoHolder.getInstance().setCloudId(response.mTargetUserId, response.mTargetCloudId);
                    // Continue
                } else {
                    Log.i(TAG, "abnormal response : mMyCloundId: " + response.mMyCloudId + ", mMyUserId: " + response.mMyUserId +
                            ", mTargetCloundId: " + response.mTargetCloudId + ", mTargetUserId: " + response.mTargetUserId);
                }
            }

            @Override
            public void onError(VolleyError e) {
                Log.w(TAG, RequestUtil.interpretErr2String(e));
                super.onError(e);
                Message msg = mEventHandlerForView.obtainMessage(Events.EVENT_NETWORK_REQUEST_ERROR);
                Bundle data = new Bundle();
                data.putInt(Events.KEY_NETWORK_REQUEST_ERROR, REQUEST_GET_SINGLE_CHAT_INFO);
                msg.setData(data);
                mEventHandlerForView.sendMessage(msg);
            }
        };
        requestUtil.add2queue();
    }

    /** Do some preparation for group chat */
    private void prepareGroupChat() {

    }

    @Override
    public void onNetworkRequestError(int type) {
        {// TODO temporary code
            Toast.makeText(mContext, R.string.error_network, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        initView(view);
        initFacePage(view);
    }

    @Override
    protected void refreshViewsAsync() {
        super.refreshViewsAsync();
        new Thread(new Runnable() {

            @Override
            public void run() {
                if (null != ((ActionBarActivityBase) getActivity()).getChatServiceStubWait(1000)) {
                    FragmentChat.this.mEventHandlerForView.sendEmptyMessage(Events.EVENT_CHAT_SERVICE_BOUND);
                } else {
                    Log.w(TAG, "failed to get chat service stub!");
                }
            }

        }).start();
    }

    @Override
    protected void refreshViews() {
        if (checkChatServiceStub()) {
            List<String> groups = new ArrayList<String>();
            groups.add(mGroupTag);
            joinGroups(groups);
            syncChattingStatusWithService();
            clearNewMsgCount();
            initData();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        syncLeftStatusWithService();
    }

    @Override
    public void onStart() {
        super.onStart();
        registerReceiver();
    }

    @Override
    public void onStop() {
        super.onStop();
        mContext.unregisterReceiver(mReceiver);
    }

    public void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Utils.ACTION_MESSAGE_CHANGED);
        filter.addAction(Utils.ACTION_CONVERSATION_CHANGED);
        mContext.registerReceiver(mReceiver, filter);
    }

    /**
     * Initialize message data for current chat instance. This will be called
     * in two situations:
     * 1. when the user first enter this chat page
     * 2. messages are updated for this conversation
     */
    public void initData() {
        Conversation conversation = getConversation();
        initMsgUI(conversation);
    }

    // TODO
    public ChatMsgLongClickListener newMsgLongClickListener() {
        return new ChatMsgLongClickListener();
    }

    /** Get the conversation for current FragmentChat instance */
    private Conversation getConversation() {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return null;
        }
        Conversation conversation = null;
        try {
            if (TYPE_SINGLE_CHAT == mChatType) {
                conversation = mChatServiceStub.getConversationByUserId(mToUser.mId);
            } else {
                conversation = mChatServiceStub.getConversationByGroupId(mGroupTag);
            }
            if (null != conversation) {
                mConversationId = conversation.getId();
                mMaxMessageId = mChatServiceStub.getMaxMessageId(conversation);
                if (-1L == mLastMessageId) {
                    mLastMessageId = mMaxMessageId;
                }
            } else {
                Log.i(TAG, "No Conversation for current chat instance.");
            }
        } catch (RemoteException e) {
            Log.e(TAG, "failed to get the conversation for current chat instance: " + e.getMessage());
            // This is serious that we can't do anything else, so return
            return null;
        }
        return conversation;
    }

    /** Initialize the message UI with a given conversation */
    private boolean initMsgUI(Conversation conversation) {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return false;
        }
        if (null != conversation) {
            try {
                List<ChatMessage> messageList = mChatServiceStub.getMessages(conversation.getId(), mMaxMessageId, 10);
                if (messageList != null && messageList.size() > 0) {
                    mLastMessageId = messageList.get(0).getId();
                    adapter.setMessageList(messageList);
                    if (messageList.size() == 10) {
                        mMsgListView.setPullLoadEnable(true);
                    }
                    scrollListViewToBottom();
                    Log.d(TAG, "Update the message List: " + messageList.size());
                }
            } catch (RemoteException e) {
                Log.e(TAG, "failed to initialize the message UI: " + e.getMessage());
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    private void initView(View view) {
        imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        params = ((Activity)mContext).getWindow().getAttributes();

        mMsgListView = (MsgListView) view.findViewById(R.id.msg_listView);
        // 触摸ListView隐藏表情和输入法
        mMsgListView.setOnTouchListener(this);
        mMsgListView.setPullLoadEnable(false);
        mMsgListView.setXListViewListener(this);
        mMsgListView.setOnScrollListener(this);
        adapter = new MessageAdapter(mContext, new ArrayList<ChatMessage>());
        adapter.setMsgLongClickListener(new ChatMsgLongClickListener());
        mMsgListView.setAdapter(adapter);
//        mMsgListView.setSelection(adapter.getCount() - 1);
        sendBtn = (Button) view.findViewById(R.id.send_btn);
        faceBtn = (ImageButton) view.findViewById(R.id.face_btn);
        msgEt = (EditText) view.findViewById(R.id.msg_et);
        faceLinearLayout = (LinearLayout) view.findViewById(R.id.face_ll);
        faceViewPager = (JazzyViewPager) view.findViewById(R.id.face_pager);
        msgEt.setOnTouchListener(this);
        msgEt.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (params.softInputMode == WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
                            || isFaceShow) {
                        faceLinearLayout.setVisibility(View.GONE);
                        isFaceShow = false;
                        // imm.showSoftInput(msgEt, 0);
                        return true;
                    }
                }
                return false;
            }
        });
        msgEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    sendBtn.setEnabled(true);
                } else {
                    sendBtn.setEnabled(false);
                }
            }
        });
        faceBtn.setOnClickListener(this);
        sendBtn.setOnClickListener(this);
    }

    /** Initialize the face page which is used to show messages*/
    private void initFacePage(View view) {
        List<View> lv = new ArrayList<View>();
        for (int i = 0; i < KidContactApp.NUM_PAGE; ++i)
            lv.add(getGridView(i));
        FacePageAdapter adapter = new FacePageAdapter(lv, faceViewPager);
        faceViewPager.setAdapter(adapter);
        faceViewPager.setCurrentItem(currentPage);
        //TODO to edit the face page transition Effect
        faceViewPager.setTransitionEffect(mEffects[3]);
        CirclePageIndicator indicator = (CirclePageIndicator) view.findViewById(R.id.indicator);
        indicator.setViewPager(faceViewPager);
        adapter.notifyDataSetChanged();
        faceLinearLayout.setVisibility(View.GONE);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int arg0) {
                currentPage = arg0;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // do nothing
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // do nothing
            }
        });

    }

    /** Get a GridView for all faces */
    private GridView getGridView(int i) {
        GridView gv = new GridView(mContext);
        gv.setNumColumns(7);
        gv.setSelector(new ColorDrawable(Color.TRANSPARENT));// 屏蔽GridView默认点击效果
        gv.setBackgroundColor(Color.TRANSPARENT);
        gv.setCacheColorHint(Color.TRANSPARENT);
        gv.setHorizontalSpacing(1);
        gv.setVerticalSpacing(1);
        gv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        gv.setGravity(Gravity.CENTER);
        gv.setAdapter(new FaceAdapter(mContext, i));
        gv.setOnTouchListener(forbidenScroll());
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                int count = currentPage * KidContactApp.NUM + arg2;
                String key = FaceAdapter.getFaceList().get(count).getKey();
                if (arg2 == KidContactApp.NUM) {// 删除键的位置
                    int selection = msgEt.getSelectionStart();
                    String text = msgEt.getText().toString();
                    if (selection > 0) {
                        String text2 = text.substring(selection - 1);
                        if ("]".equals(text2)) {
                            int start = text.lastIndexOf("[");
                            int end = selection;
                            msgEt.getText().delete(start, end);
                            return;
                        }
                        msgEt.getText().delete(selection - 1, selection);
                    }
                } else {
                    // 下面这部分，在EditText中显示表情
                    Bitmap bitmap = BitmapFactory.decodeResource(
                            getResources(), (Integer) KidContactApp
                            .getInstance().getFaceMap().values()
                            .toArray()[count]);
                    if (bitmap != null) {
                        int rawHeigh = bitmap.getHeight();
                        int rawWidth = bitmap.getHeight();
                        int newHeight = 40;
                        int newWidth = 40;
                        // 计算缩放因子
                        float heightScale = ((float) newHeight) / rawHeigh;
                        float widthScale = ((float) newWidth) / rawWidth;
                        // 新建立矩阵
                        Matrix matrix = new Matrix();
                        matrix.postScale(heightScale, widthScale);
                        // 设置图片的旋转角度
                        // matrix.postRotate(-30);
                        // 设置图片的倾斜
                        // matrix.postSkew(0.1f, 0.1f);
                        // 将图片大小压缩
                        // 压缩后图片的宽和高以及kB大小均会变化
                        Bitmap newBitmap = Bitmap.createBitmap(bitmap, 0, 0,
                                rawWidth, rawHeigh, matrix, true);
                        ImageSpan imageSpan = new ImageSpan(mContext,
                                newBitmap);
                        String emojiStr = key;
                        SpannableString spannableString = new SpannableString(
                                emojiStr);
                        spannableString.setSpan(imageSpan,
                                emojiStr.indexOf('['),
                                emojiStr.indexOf(']') + 1,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        msgEt.append(spannableString);
                    } else {
                        String ori = msgEt.getText().toString();
                        int index = msgEt.getSelectionStart();
                        StringBuilder stringBuilder = new StringBuilder(ori);
                        stringBuilder.insert(index, key);
                        msgEt.setText(stringBuilder.toString());
                        msgEt.setSelection(index + key.length());
                    }
                }
            }
        });
        return gv;
    }

    /** 防止乱pageview乱滚动 */
    private View.OnTouchListener forbidenScroll() {
        return new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    return true;
                }
                return false;
            }
        };
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.face_btn:
                if (!isFaceShow) {
                    imm.hideSoftInputFromWindow(msgEt.getWindowToken(), 0);
                    try {
                        Thread.sleep(80);// 解决此时会黑一下屏幕的问题
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    faceLinearLayout.setVisibility(View.VISIBLE);
                    isFaceShow = true;
                } else {
                    faceLinearLayout.setVisibility(View.GONE);
                    isFaceShow = false;
                }
                break;
            case R.id.send_btn:// 发送消息
                if (TYPE_SINGLE_CHAT == mChatType) {
                    if (mChatInfo == null || mChatInfo.hasNull()) {
                        Toast.makeText(mContext,R.string.chat_no_chatinfo, Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                String msg = msgEt.getText().toString().trim();
                msgEt.setText("");
                handleMsgSending(msg);
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (view.getId()) {
            case R.id.msg_listView:
                imm.hideSoftInputFromWindow(msgEt.getWindowToken(), 0);
                faceLinearLayout.setVisibility(View.GONE);
                isFaceShow = false;
                break;
            case R.id.msg_et:
                imm.showSoftInput(msgEt, 0);
                faceLinearLayout.setVisibility(View.GONE);
                isFaceShow = false;
                break;

            default:
                break;
        }
        return false;
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "Start to refresh data");

    }

    @Override
    public void onLoadMore() {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return;
        }
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    List<ChatMessage> messageList = mChatServiceStub.getMessages(mConversationId, mLastMessageId - 1, 10);
                    if (messageList != null && messageList.size() >0 ) {
                        mLastMessageId = messageList.get(0).getId();
                        mMsgListView.stopLoadMore();
                        adapter.addMessageList(messageList);
                        mMsgListView.setSelectionFromTop(messageList.size()+1, mMsgListView.getHeaderViewHeight());
                        mMsgListView.setHeaderViewStatus(MsgListViewHeader.STATE_NORMAL);
                        Log.d(TAG, "Load More message List: " + messageList.size());
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.no_more_data), Toast.LENGTH_SHORT).show();
                        mMsgListView.stopLoadMore();
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                isScrollCheck = true;
            }
        }, 1000);

    }

    private void scrollListViewToBottom() {
        mMsgListView.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                mMsgListView.setSelection(adapter.getCount() - 1);
            }
        });
    }

    @Override
    public void onXScrolling(View view) {
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        mScrollState = scrollState;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
         if (view.getFirstVisiblePosition() == 0 && mScrollState == SCROLL_STATE_FLING && firstVisibleItem == 0 && isScrollCheck) {
             Log.d(TAG, "onScroll hint");
//             ((MsgListView)view).smoothScrollToPositionFromTop();
             isScrollCheck = false;
             ((MsgListView) view).setSelectionFromTop(0, mMsgListView.getHeaderViewHeight());
             mMsgListView.setHeaderViewStatus(MsgListViewHeader.STATE_LOADING);
             onLoadMore();
         }
    }

    /**
     * Handle the message sending.
     * @param msg the message to be sent
     */
    private void handleMsgSending(String msg) {
        if (!checkChatServiceStub()) {
            Log.e(TAG, Common.getLastTrace() + ": chat service stub is null!");
            return;
        }
        if ( TYPE_SINGLE_CHAT == mChatType) { // Single chat
            CloudMessage message = CloudMessage.initSingleChatMsg(mChatInfo.mMyUserId, mChatInfo.mTargetUserId, msg,
                    ChatMessage.MSG_TYPE_TEXT, mChatInfo.mTargetDeviceType);
            ChatMessage chatMessage = null;
            try {
                chatMessage = mChatServiceStub.sendMessage(message);
            } catch (RemoteException e) {
                Log.e(TAG, "failed to send single message: " + e.getMessage());
                return;
            }
            if (chatMessage != null) {
                adapter.updateMessage(chatMessage); // view
                scrollListViewToBottom();
            }
        } else { // Group chat
           CloudMessage cloudMsg = CloudMessage.initGroupChatMsg(((KidContactApp) getActivity().getApplicationContext()).getId(), mGroupTag, msg, ChatMessage.MSG_TYPE_TEXT);
           ChatMessage chatMessage = null;
           try {
               chatMessage = mChatServiceStub.sendMessage(cloudMsg);
           } catch (RemoteException e) {
               Log.e(TAG, "failed to send group message: " + e.getMessage());
               return;
           }
           if (null != chatMessage) {
               adapter.updateMessage(chatMessage);
               scrollListViewToBottom();
           }
        }
    }

    /** A concrete listener for message long click */
    private class ChatMsgLongClickListener extends MessageAdapter.MsgLongClickListener {

        @Override
        public boolean onLongClick(View v) {
            new AlertDialog.Builder(mContext)
                .setMessage(R.string.del_msg_content)
                .setNegativeButton(R.string.del_msg_cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.del_msg_ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Now delete this message
                        if (null == mChatServiceStub) {
                            Log.w(TAG, "chat service stub is null!");
                            return;
                        }
                        if (-1 == mMsgId) {
                            Log.w(TAG, "wrong message id!");
                            return;
                        }
                        if (FragmentChat.TYPE_GROUP_CHAT == FragmentChat.this.mChatType) {
                            if (null == FragmentChat.this.mGroupTag || FragmentChat.this.mGroupTag.equals("")) {
                                Log.w(TAG, "wrong group tag!");
                                return;
                            }
                            try {
                                mChatServiceStub.deleteGroupChatMsg(FragmentChat.this.mGroupTag, mMsgId);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                                // Failed to delete a message is that serious, so we move on
                            }
                        } else if (FragmentChat.TYPE_SINGLE_CHAT == FragmentChat.this.mChatType) {
                            if (null == FragmentChat.this.mToUser || FragmentChat.this.mToUser.mId < 0) {
                                Log.w(TAG, "wrong toUser id!");
                                return;
                            }
                            try {
                                mChatServiceStub.deleteSingleChatMsg(FragmentChat.this.mToUser.mId, mMsgId);
                            } catch (RemoteException e) {
                                e.printStackTrace();
                                // Failed to delete a message is that serious, so we move on
                            }
                        }
                    }
                }).create().show();
            return true;
        }

    }
}
