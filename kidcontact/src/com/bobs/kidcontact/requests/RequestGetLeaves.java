package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.LeaveList;

/**
 * Created by hzheng3 on 14-1-14.
 */
public class RequestGetLeaves extends CookieGsonRequest<LeaveList> {
    private static final String IDOFFSET = "idOffset";

    public RequestGetLeaves(long idOffset, Response.Listener<LeaveList> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getCRsURL(idOffset),
                LeaveList.class, listener, errorListener);
    }

    public static String getCRsURL(long idOffset) {
        return Constants.getFunctionURL(Constants.FUNC_GET_LEAVES)
                + Constants.QUESTION
                + IDOFFSET + Constants.EQUAL + idOffset;
    }
}