package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.IdProxy;
import com.bobs.kidcontact.model.Like;

/**
 * Created by hzheng3 on 14-3-23.
 */
public final class RequestLikeMoment extends RequestLikeId<Like> {
    public RequestLikeMoment(IdProxy input, Response.Listener<Like> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_LIKE_MOMENT),
                input, Like.class, listener, errorListener);
    }
}
