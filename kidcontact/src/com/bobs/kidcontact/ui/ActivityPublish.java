package com.bobs.kidcontact.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.utils.UIUtil;

/**
 * Created by junyongl on 13-12-31.
 */
public class ActivityPublish extends ActionBarActivityBase {
    private TextView mActionBarTitle;
	private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtil.setThemeForUser(this);
        final LayoutInflater inflater = (LayoutInflater) getSupportActionBar()
                .getThemedContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View customActionBarView = inflater.inflate(
                R.layout.actionbar_custom_view_done_cancel, null);

        customActionBarView.findViewById(R.id.actionbar_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cancel
                finish();
            }
        });

        customActionBarView.findViewById(R.id.actionbar_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //publish the content
                if (mFragment != null) {
                    ((IPublish) mFragment).publish();
                }
            }
        });

        mActionBarTitle = (TextView) customActionBarView.findViewById(R.id.actionbar_title);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                        | ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setCustomView(customActionBarView,
                new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        setContentView(R.layout.activity_publish);

        Bundle bundle = getIntent().getExtras();

        if (bundle.getInt("publish_type") == Constants.PublishType.MOMENT.ordinal()) {
             mFragment = new FragmentPublishMoment();
        } else if (bundle.getInt("publish_type") == Constants.PublishType.REVIEW.ordinal()) {
            mFragment = new FragmentPublishReview();
            mActionBarTitle.setText(getString(R.string.title_publish_review));
        } else if (bundle.getInt("publish_type") == Constants.PublishType.NOTICE.ordinal()) {
            mFragment = new FragmentPublishNotice();
            mActionBarTitle.setText(getString(R.string.title_publish_notice));
        } else if (bundle.getInt("publish_type") == Constants.PublishType.ASSIGNMENT.ordinal()) {
            mFragment = new FragmentPublishAssignment();
            mActionBarTitle.setText(getString(R.string.title_publish_notice));
        } else if (bundle.getInt("publish_type") == Constants.PublishType.RECORD.ordinal()) {
            mFragment = new FragmentPublishMoment();
        } else if (bundle.getInt("publish_type") == Constants.PublishType.CLASSMENU.ordinal()) {
            mFragment = new FragmentPublishClassMenu();
            mActionBarTitle.setText(getString(R.string.title_publish_classmenu));
        } else if (bundle.getInt("publish_type") == Constants.PublishType.CLASSCAL.ordinal()) {
        	mFragment = new FragmentPublishClassMenu();
            mActionBarTitle.setText(getString(R.string.title_publish_classcal));
        } else if (bundle.getInt("publish_type") == Constants.PublishType.CLASSNAME.ordinal()) {
        	mFragment = new FragmentPublishClassMenu();
            mActionBarTitle.setText(getString(R.string.title_publish_class_name));
        }else {
            mFragment = new FragmentPublishMoment();
        }
        mFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frame_publish_content, mFragment);
        fragmentTransaction.commit();
    }

    public interface IPublish {
        void publish();
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }

}
