package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.AckNotice;

/**
 * Created by hzheng3 on 14-1-11.
 */
public final class RequestAddAckNotice extends RequestAddInfoTitle<AckNotice> {
    public RequestAddAckNotice(InfoTitle input, Response.Listener<AckNotice> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ADD_ACK_NOTICE),
                input, AckNotice.class, listener, errorListener);
    }
}
