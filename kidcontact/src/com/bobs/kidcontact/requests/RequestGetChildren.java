package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.ChildList;

/**
 * Suppose to be called by teacher role
 */
public final class RequestGetChildren extends CookieGsonRequest<ChildList> {
    public RequestGetChildren(Response.Listener<ChildList> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getURL(),
                ChildList.class, listener, errorListener);
	}

    public static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_GET_CHILDREN);
    }
}
