package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.EatMenu;

/**
 * Created by hzheng3 on 14-6-9.
 */
public final class RequestGetEatMenu extends CookieGsonRequest<EatMenu> {

    public RequestGetEatMenu(Response.Listener<EatMenu> listener, Response.ErrorListener errorListener) {
        super(Method.GET, Constants.getFunctionURL(Constants.FUNC_GET_EAT_MENU),
                EatMenu.class, listener, errorListener);
    }
}