package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.IdProxy;

public final class RequestGetBoundStatus extends CookieGsonJsonRequest<RequestCheckBoundStatus.BoundStatus> {
    public RequestGetBoundStatus(IdProxy input, Response.Listener<RequestCheckBoundStatus.BoundStatus> listener, Response.ErrorListener errorListener) {
        super(getURL(), input, RequestCheckBoundStatus.BoundStatus.class, listener, errorListener);
    }

    public static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_GET_BOUND_STATUS);
    }
}
