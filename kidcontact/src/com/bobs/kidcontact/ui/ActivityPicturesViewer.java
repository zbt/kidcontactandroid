package com.bobs.kidcontact.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.PicProxy;
import com.bobs.kidcontact.ui.view.NetworkImageViewWithCallBack;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by junyongl on 14-1-3.
 */
public class ActivityPicturesViewer extends ActionBarActivityBase implements NetworkImageViewWithCallBack.ResponseObserver {
    private ArrayList<PicProxy> mPictures = new ArrayList<PicProxy>();
    private int mPosition;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures_viewer);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        mProgressBar = (ProgressBar) findViewById(R.id.loading_progress);
        Type listOfMPicture = new TypeToken<ArrayList<PicProxy>>(){}.getType();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String picturesJson = getIntent().getExtras().getString("key_pictures");
            Gson gson = new Gson();
            mPictures = gson.fromJson(picturesJson, listOfMPicture);
            mPosition = getIntent().getExtras().getInt("key_position");
        }
        PicturesViewerAdapter adapter = new PicturesViewerAdapter();
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(mPosition);

    }

    @Override
    public void onError() {

    }

    @Override
    public void onSuccess() {
        mProgressBar.setVisibility(View.GONE);
    }

    private class PicturesViewerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mPictures.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            NetworkImageViewWithCallBack imageView = new NetworkImageViewWithCallBack(container.getContext());
            imageView.setImageUrl(mPictures.get(position).getPicture().getPicURL(), KidContactApp.getInstance().getImageLoader());
            container.addView(imageView, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
