package com.bobs.kidcontact.chat.engine;

import java.util.List;

/**
 * Created by hzheng3 on 14-2-18.
 */
public interface IPushEngine {
    public boolean sendMsgToUser(long userId, int deviceType, String msg);
    public boolean sendMsgToGroup(String groupId, String msg);
    public void joinGroups(List<String> groups);
    public void leaveGroups(List<String> groups);
}
