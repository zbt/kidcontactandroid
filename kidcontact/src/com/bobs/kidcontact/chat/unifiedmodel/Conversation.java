package com.bobs.kidcontact.chat.unifiedmodel;

import java.security.InvalidParameterException;
import java.util.Date;
import com.bobs.kidcontact.chat.database.ChatDB;
import com.bobs.kidcontact.chat.service.Utils;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hzheng3 on 14-2-18.
 */
public class Conversation implements Parcelable {
    // Every conversation has its own table to store messages
    // For group chat, table name is mGroupId; For single chat, table name is mUserId;
    // And there's also a conversation table to store conversation status

    public static final String FIELD_NAME_ID = "_id";
    public static final String FIELD_NAME_DISPLAY_NAME = "display_name";
    public static final String FIELD_NAME_ATTENDEES = "attendees";
    public static final String FIELD_NAME_GROUP_CHAT = "group_chat";
    public static final String FIELD_NAME_GROUP_CHAT_JOINED = "group_chat_joined";
    public static final String FIELD_NAME_GROUP_ID = "group_id";
    public static final String FIELD_NAME_USER_ID = "user_id";
    public static final String FIELD_NAME_NEW_MSG_COUNT = "new_msg_count";
    public static final String FIELD_NAME_LAST_MSG_DATE = "last_msg_date";
    public static final String FIELD_NAME_NOTIFY_ENABLED = "notify_enabled";
    public static final String FIELD_NAME_TABLE_NAME = "table_name";
    private static final String DATE_FORMAT_STRING = "yy-MM-dd HH:mm";

    private long mId = ChatDB.EMPTY_DB_ID;

    // cannot be null or empty
    private String mDisplayName = "";

    // cannot be null or empty, JSON string, example [1, 5, 10]
    private String mAttendees = "";

    // whether or not this conversation is group chat or not
    private boolean mGroupChat = false;

    // indicates whether user has joined group chat successfully
    private boolean mGroupChatJoined = false;

    // can be empty if it's not group chat; For baidu, it indicates cloud tag name
    private String mGroupId = "";

    // can be -1 if it's not single user chat; It is kidcontact user id, must use API to map it to cloud user id
    private long mUserId = 0;

    // indicates incoming new/unread messages, should be reset to 0 if user opens this conversation
    private int mNewMsgCount = 0;

    // latest incoming message, SimpleDateFormat is like "yy-MM-dd HH:mm", can'r be null or empty
    private String mLastMsgDate = "";

    // whether or not to notify user when new message comes
    private boolean mNotifyEnabled = false;

    // can be either mGroupId or mUserId
    private String mTableName = "";

    public long getId() {
        return mId;
    }

    public String getDisplayName() {
        return mDisplayName;
    }

    public String getAttendees() {
        return mAttendees;
    }

    public boolean getGroupChat() {
        return mGroupChat;
    }

    public boolean getGroupChatJoined() {
        return mGroupChatJoined;
    }

    public String getGroupId() {
        return mGroupId;
    }

    public long getUserId() {
        return mUserId;
    }

    public int getNewMsgCount() {
        return mNewMsgCount;
    }

    public String getLastMsgDate() {
        return mLastMsgDate;
    }

    public boolean getNotifyEnabled() {
        return mNotifyEnabled;
    }

    public String getTableName() {
        return mTableName;
    }

    public void setId(long id) {
        mId = id;
    }

    /**
     * @param name can't be null or empty
     * @throws InvalidParameterException
     */
    public void setDisplayName(String name) throws InvalidParameterException {
//        if (null == name || name.equals("")) {
//            throw new InvalidParameterException("name can't be null or empty!");
//        }
        mDisplayName = name;
    }

    /**
     * @param attendees can't be null or empty
     * @throws InvalidParameterException
     */
    public void setAttendees(String attendees) throws InvalidParameterException {
//        if (null == attendees || attendees.equals("")) {
//            throw new InvalidParameterException("attendees can't be null or empty!");
//        }
        mAttendees = attendees;
    }

    public void setGroupChat(boolean groupChat) {
        mGroupChat = groupChat;
    }

    public void setGroupChatJoined(boolean joined) {
        mGroupChatJoined = joined;
    }

    public void setGroupId(String id) {
        if (null != id) {
            mGroupId = id;
        } else {
            mGroupId = "";
        }
    }

    public void setUserId(long id) {
        mUserId = id;
    }

    public void setNewMsgCount(int count) {
        mNewMsgCount = count;
    }

    /**
     * @param date can't be null or empty
     * @throws InvalidParameterException
     */
    public void setLastMsgDate(String date) throws InvalidParameterException {
        if (null == date || date.equals("")) {
            throw new InvalidParameterException("date can't be null or empty");
        }
        mLastMsgDate = date;
    }

    public void setNotifyEnabled(boolean enabled) {
        mNotifyEnabled = enabled;
    }

    public void setTableName(String name) throws InvalidParameterException {
        if (null == name || name.equals("")) {
            throw new InvalidParameterException("name can't be null or empty!");
        }
        mTableName = name;
    }

    /**
     * Judge if this conversation is valid.
     */
    public boolean isValid() {
//        if (null == mDisplayName || mDisplayName.equals("") || null == mAttendees || mAttendees.equals("")
//                || null == mGroupId || null == mLastMsgDate || mLastMsgDate.equals("")) {
//            return false;
//        }
        return true;
    }

    public static Date stringToDate(String dateString) {
        return Utils.stringToDate(dateString, DATE_FORMAT_STRING);
    }

    public static String dateToString(Date date) {
        return Utils.dateToString(date, DATE_FORMAT_STRING);
    }

    // Below TODO
    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mDisplayName);
        dest.writeString(mAttendees);
        if (mGroupChat)
            dest.writeInt(1);
        else
            dest.writeInt(0);
        if (mGroupChatJoined)
            dest.writeInt(1);
        else
            dest.writeInt(0);
        dest.writeString(mGroupId);
        dest.writeLong(mUserId);
        dest.writeInt(mNewMsgCount);
        dest.writeString(mLastMsgDate);
        if (mNotifyEnabled)
            dest.writeInt(1);
        else
            dest.writeInt(0);
        dest.writeString(mTableName);
    }

    public Conversation() {
    }

    public Conversation(Parcel in){
        mId = in.readLong();
        mDisplayName = in.readString();
        mAttendees = in.readString();
        int groupChat = in.readInt();
        if (groupChat == 1)
            mGroupChat = true;
        else
            mGroupChat = false;
        int groupchatJoined = in.readInt();
        if (groupchatJoined == 1)
            mGroupChatJoined = true;
        else
            mGroupChatJoined = false;
        mGroupId = in.readString();
        mUserId = in.readLong();
        mNewMsgCount = in.readInt();
        mLastMsgDate = in.readString();
        int notifyEnabled = in.readInt();
        if (notifyEnabled == 1)
            mNotifyEnabled = true;
        else
            mNotifyEnabled = false;
        mTableName = in.readString();
    }
    public static final Parcelable.Creator<Conversation> CREATOR = new Parcelable.Creator<Conversation>() {

        public Conversation createFromParcel(Parcel in) {
            return new Conversation(in);
        }

        public Conversation[] newArray(int size) {
            return new Conversation[size];
        }
    };
}
