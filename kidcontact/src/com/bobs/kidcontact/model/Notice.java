package com.bobs.kidcontact.model;

import android.util.Log;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.ui.KidContactApp;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

/**
 * Created by junyongl on 14-1-8.
 */
public class Notice {
    @SerializedName("id")
    public long mId;
    @SerializedName("publicNotice")
    public PublicNotice mPublicNotice;
    @SerializedName("ackNotice")
    public AckNotice mAckNotice;
    @SerializedName("vote")
    public Vote mVote;
    @SerializedName("questionary")
    public Questionary mQuestionary;
    @SerializedName("assignment")
    public Assignment mAssignment;

    public transient String mContent;
    public transient String mTitle;
    public transient String mCreatedDate;
    public transient String mType;
    private static final String TAG = "Notice";

    public final long getId() {
        return mId;
//
//        if (mPublicNotice != null) {
//            return mPublicNotice.mId;
//        } else if (mAckNotice != null) {
//            return mAckNotice.mId;
//        } else if (mVote != null) {
//            return mVote.mId;
//        } else if (mQuestionary != null) {
//            return mQuestionary.mId;
//        } else {
//            throw new RuntimeException("tell me what happened!!!");
//        }
    }

    public final void fillContent() {
        if (mPublicNotice != null) {
            mTitle = mPublicNotice.mTitle;
            mContent = mPublicNotice.mInfo;
            mCreatedDate = mPublicNotice.mDateCreated;
            mType = KidContactApp.getInstance().getString(R.string.notice);
        } else if (mAckNotice != null) {
            mTitle = mAckNotice.mTitle;
            mContent = mAckNotice.mInfo;
            mCreatedDate = mAckNotice.mDateCreated;
            mType = KidContactApp.getInstance().getString(R.string.ack_notice);
        } else if (mVote != null) {
            mTitle = mVote.mTitle;
            Vote.VoteContent content = new Gson().fromJson(mVote.mInfo, Vote.VoteContent.class);
            mContent = content.mContent;
            mCreatedDate = mVote.mDateCreated;
            mType = KidContactApp.getInstance().getString(R.string.vote_notice);
        } else if (mQuestionary != null) {
            mTitle = mQuestionary.mTitle;
            mContent = mQuestionary.mInfo;
            mCreatedDate = mQuestionary.mDateCreated;
            mType = KidContactApp.getInstance().getString(R.string.questionary_notice);
        } else if (mAssignment != null) {
            mTitle = mAssignment.mTitle;
            Assignment.AssignmentContent content =  new Gson().fromJson(mAssignment.mInfo, Assignment.AssignmentContent.class);
            mContent = content.mContent;
            mCreatedDate = mAssignment.mDateCreated;
            mType = KidContactApp.getInstance().getString(R.string.assignment);
        } else {
            throw new RuntimeException("tell me what happened!!!");
        }
    }
}
