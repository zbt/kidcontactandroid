package com.bobs.kidcontact.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bobs.kidcontact.R;

/**
 * Created by junyongl on 2014/4/14.
 * Modified by Joey Chen on 2014/7/6
 */
public class ActivityIntro extends FragmentActivity {
    private static final int INTRO_PAGES_AMOUNT = 4;
    private ViewPager mViewPager;
    private RelativeLayout mPageLayout;
    private LinearLayout mIndicatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        mViewPager = (ViewPager) findViewById(R.id.pager);
        IntroCollectionPagerAdapter adapter = new IntroCollectionPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
    }

    public class IntroCollectionPagerAdapter extends FragmentStatePagerAdapter {

        public IntroCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new FragmentIntroPage();
            Bundle args = new Bundle();
            args.putInt(FragmentIntroPage.ARG_PAGE_NUMBER, i + 1);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return INTRO_PAGES_AMOUNT;
        }
    }

    @SuppressLint("ValidFragment")
	public class FragmentIntroPage extends Fragment {

        public static final String ARG_PAGE_NUMBER = "arg_page_number";
        private int mPageNumber;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Bundle args = getArguments();
            if (args != null) {
                mPageNumber = args.getInt(ARG_PAGE_NUMBER);
            }
            return inflater.inflate(
                    R.layout.fragment_intro_page, container, false);
        }

        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            mPageLayout = (RelativeLayout) view.findViewById(R.id.intro_page_layout);
            mIndicatorLayout = (LinearLayout) view.findViewById(R.id.indicator_bullets);
            initIndicatorBullets();
            switch (mPageNumber) {
                case 1: //First Page
                    mPageLayout.setBackgroundResource(R.drawable.guide_01);
                    break;
                case 2: //Second Page
                    mPageLayout.setBackgroundResource(R.drawable.guide_02);
                    break;
                case 3: //Second Page
                    mPageLayout.setBackgroundResource(R.drawable.guide_03);
                    break;
                case 4: //Second Page
                    mPageLayout.setBackgroundResource(R.drawable.guide_04);
                    break;
                default:
                    break;
            }
            if (mPageNumber == INTRO_PAGES_AMOUNT) {
            	TextView createClass = (TextView) view.findViewById(R.id.txt_create_class);
            	createClass.setVisibility(View.VISIBLE);
            	createClass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ActivityCreateClass.class);
                        startActivity(intent);
                    }
                });
                View importPanel = ((ViewStub) view.findViewById(R.id.bottom_buttons)).inflate();
                Button btLeft = (Button) importPanel.findViewById(R.id.bt_left);
                btLeft.setText("现有用户登录");
                btLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ActivitySignIn.class);
                        startActivity(intent);
                    }
                });
                Button btRight = (Button) importPanel.findViewById(R.id.bt_right);
                btRight.setText("新用户注册");
                btRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), ActivitySignUp.class);
                        startActivity(intent);
                    }
                });
            }
        }

        private void initIndicatorBullets() {
            for (int i = 0; i < INTRO_PAGES_AMOUNT; i ++) {
                ImageView indicator = new ImageView(getActivity());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                        (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(4, 4, 4, 4);
                indicator.setImageResource(R.drawable.indicator_bullet);
                if (i + 1 == mPageNumber) {
                    indicator.setSelected(true);
                }
                mIndicatorLayout.addView(indicator, layoutParams);
            }
        }
    }
}
