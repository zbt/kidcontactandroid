package com.bobs.kidcontact.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.chat.service.HeartbeatExecutor;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.model.Version;
import com.bobs.kidcontact.ui.view.UIButton;
import com.bobs.kidcontact.utils.UpdateManager;

/**
 * Created by junyongl on 13-12-25. zhanglong
 */
public class FragmentParentMore extends FragmentBase {
    private static final String TAG = "FragmentParentMore";
    private UIButton mUserSettings;
    private UIButton mHarvard;
    private UIButton mCheckUpdateBtn;
    private IChatService.Stub mChatServiceStub;
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        getChatServiceStubAsync();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_more, container, false);
        mUserSettings = (UIButton) view.findViewById(R.id.user_settings);
        mUserSettings.addClickListener(new UIButton.ClickListener() {
            @Override
            public void onClick(View view) {
                //Launch Settings Activity
                Intent intent = new Intent(getActivity(), ActivityParentSettings.class);
                startActivity(intent);
            }
        });
        view.findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActionBarActivityBase) getActivity()).shouldStopChatService(true);
                HeartbeatExecutor.getInstance().stopSync();
                KidContactApp.getSharePrefUtil().clearSharedPreferences();
                KidContactApp.logOut(getActivity());
                KidContactApp.returnToSigninPage(getActivity());
            }
        });

        mCheckUpdateBtn = ((UIButton)view.findViewById(R.id.check_update));
        mCheckUpdateBtn.addClickListener(new UIButton.ClickListener() {
            @Override
            public void onClick(View v) {
                if (!mCheckUpdateBtn.isClickable()) {// already clicked, do nothing
                    return;
                }
                mCheckUpdateBtn.setClickable(false);
                UpdateManager updateManager = UpdateManager.getInstance(getActivity(), getEventHandlerForView());
                updateManager.checkUpdate();
            }
        });

        /*
        mHarvard = (UIButton) view.findViewById(R.id.more_harvard);
        mHarvard.addClickListener(new UIButton.ClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityHarvardConsult.class);
            }
        });
        */

        return view;
    }

    /** Get the IChatService.Stub asynchronously */
    private void getChatServiceStubAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mChatServiceStub = ((ActionBarActivityBase) mContext).getChatServiceStubWait(1000);
                if (null == mChatServiceStub) {
                    Log.w(TAG, "failed to get chat service stub!");
                } else {
                    Log.i(TAG, "bound to local service successfully.");
                }
            }
        }).start();
    }

    @Override
    protected void refreshViewsAsync() {
        super.refreshViewsAsync();
        new Thread(new Runnable() {

            @Override
            public void run() {
                if (null != ((ActionBarActivityBase) getActivity()).getChatServiceStubWait(1000)) {
                    FragmentParentMore.this.mEventHandlerForView.sendEmptyMessage(Events.EVENT_CHAT_SERVICE_BOUND);
                } else {
                    Log.w(TAG, "failed to get chat service stub!");
                }
            }

        }).start();
    }

    @Override
    protected void refreshViews() {
        // the mChatServiceStub is OK, when we come here
        int versionUpdateCount = 0;
        try {
            if (null != mChatServiceStub) {
                versionUpdateCount = mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_VERSION_NEW);
            } else {
                Log.w(TAG, "chat service stub is null!");
            }
        } catch (RemoteException e) {
            Log.e(TAG, "failed to get CtrlMessage count with type: CtrlMessage.TYPE_VERSION_NEW." + e.getMessage());
        }
        if (versionUpdateCount > 0) {
            mCheckUpdateBtn.showCornerMark();
        } else {
            mCheckUpdateBtn.hideCornerMark();
        }
    }

    @Override
    protected void refreshViews(CtrlMessage msg) {
        super.refreshViews(msg);
        if (CtrlMessage.TYPE_VERSION_NEW == msg.getType()) {
            mCheckUpdateBtn.showCornerMark();
        }
    }

    @Override
    protected void refreshViews(Version version) {
        // TODO handle new version
        if (null != mChatServiceStub) {
            try {
                mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_VERSION_NEW);
            } catch (RemoteException e) {
                Log.e(TAG, "failed to delete ctrl message with type: CtrlMessage.TYPE_VERSION_NEW");
            }
        }
        mCheckUpdateBtn.setClickable(true);
    }

    @Override
    protected void refreshViewsDel(CtrlMessage msg) {
        super.refreshViewsDel(msg);
        if (CtrlMessage.TYPE_VERSION_NEW == msg.getType()) {
            mCheckUpdateBtn.hideCornerMark();
        }
    }
    
    @Override
    protected void refreshViewsVersionUpdateCancel() {
        mCheckUpdateBtn.setClickable(true);
        if (null != mChatServiceStub) {
            try {
                mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_VERSION_NEW);
            } catch (RemoteException e) {
                Log.e(TAG, "failed to delete ctrl message with type: CtrlMessage.TYPE_VERSION_NEW");
            }
        }
    }
}
