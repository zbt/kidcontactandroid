package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 2014/6/27.
 */
public class RegcodeProxy {
    @SerializedName("regCode")
    public String mRegcode;
}
