package com.bobs.kidcontact.ui;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.PicProxy;
import com.bobs.kidcontact.model.TimeTable;
import com.bobs.kidcontact.requests.RequestGetTimeTable;
import com.bobs.kidcontact.ui.view.NetworkImageViewWithCallBack;
import com.bobs.kidcontact.utils.RequestUtil;

import de.greenrobot.event.EventBus;


/**
 * Created by junyongl on 14-1-22.
 * Modified by Joey 14-6-14
 */
public class ActivityClassCalendar extends ActionBarActivityBase implements NetworkImageViewWithCallBack.ResponseObserver {

	private ArrayList<PicProxy> mPictures = new ArrayList<PicProxy>();
    private int mPosition;
    private ProgressBar mProgressBar;
    private ViewPager viewPager;
    private Context mContext;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.class_calendar);
        mContext = this;
        
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        mProgressBar = (ProgressBar) findViewById(R.id.loading_progress);
        EventBus.getDefault().register(this);
        onRefresh();
    }

	private void onRefresh() {
		RequestUtil<TimeTable> getTimeTable = new RequestUtil<TimeTable>() {
            @Override
            public Request<TimeTable> onPrepareRequest(Response.Listener<TimeTable> listener, Response.ErrorListener errListener) {
                return new RequestGetTimeTable(listener, errListener);
            }

            @Override
            public void onSuccess(TimeTable response) {
              mPictures = response.mTimeTablePics;
              mPosition = 0;
	          PicturesViewerAdapter adapter = new PicturesViewerAdapter();
	          viewPager.setAdapter(adapter);
	          if (mPictures.size()>=1){
	        	  viewPager.setCurrentItem(mPosition);
	          }
            }

            @Override
            public void onError(VolleyError e) {
            	mProgressBar.setVisibility(View.GONE);
            	Toast.makeText(mContext, R.string.class_timetable_fail, Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        
        getTimeTable.add2queue();
	}
    
    @Override
    public void onError() {

    }

    @Override
    public void onSuccess() {
        mProgressBar.setVisibility(View.GONE);
    }

    private class PicturesViewerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mPictures.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            NetworkImageViewWithCallBack imageView = new NetworkImageViewWithCallBack(container.getContext());
            imageView.setImageUrl(mPictures.get(position).getPicture().getPicURL(), KidContactApp.getInstance().getImageLoader());
            container.addView(imageView, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view == o;
        }
    }
    
    public void onEvent(UpdateClassCalUIEvent event) {
    	onRefresh();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_class_cal_update:
            	Intent intent = new Intent(this, ActivityPublish.class);
                intent.putExtra("publish_type", Constants.PublishType.CLASSCAL.ordinal());
                startActivityForResult(intent, Constants.PublishType.CLASSCAL.ordinal());
            	break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	if (KidContactApp.getInstance().getRole() == Constants.TYPE_TEACHER) {
    		MenuInflater inflater = getMenuInflater();
        	inflater.inflate(R.menu.activity_class_cal, menu);
    	}
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
