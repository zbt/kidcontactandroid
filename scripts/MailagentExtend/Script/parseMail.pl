#!/usr/bin/perl

open(FILE_AUTH_INFO, ">"."authInfo") or die "failed to open file: ".$!;
our $sender = "";
while (<>) {
	if ($_ =~ /From:/ && $_ =~ /</ && $_ =~ />/) {
		$sender = $_;
		$sender =~ s/.*<//;
		$sender =~ s/>.*//;
		chomp($sender);
 	}
	if ($_ =~ /=REG-\d{11}-REG=/) {
                my $phone = $_; 
                $phone =~ s/.*=REG-//;
                $phone =~ s/-REG=.*//;
                print FILE_AUTH_INFO $sender.":".$phone;
		break;
        }  	
}
close(FILE_AUTH_INFO);
