package com.bobs.kidcontact.chat.unifiedmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhanglong on 2014-05-08.
 */
public class CloudMessage implements Parcelable {

    /**
     * The unique id of each cloud message, generated and wrote by the server.
     * It can be used to distinguish a cloud message from another.
     */
    // @SerializedName("uniqueId")
    @SerializedName("a")
    private String mUniqueId;

    /**
     * The time stamp of this cloud message, generated and wrote by the server.
     */
    // @SerializedName("timestamp")
    @SerializedName("b")
    private Long mTimestamp;

    /**
     * Indicates whether current user has cached cloud messages in the server.
     * Wrote by the server.
     */
    // @SerializedName("needPull")
    @SerializedName("c")
    private Boolean mNeedPull = Boolean.valueOf(false);

    /**
     * Used by Baidu Push engine to generate notification in the client.
     * Necessary for IOS client, can be ignored by android client.
     */
    @SerializedName("aps")
    private Aps mAps;

    /**
     * Our main content of this cloud message, a payload can be a single chat message,
     * a group chat message, or a control message. Check detail in {@link #CloudPayload}.
     */
    // @SerializedName("payload")
    @SerializedName("e")
    private CloudPayload mPayload;

    // not intend to be transmitted
    private transient int mToDeviceType;
    public static int DEVICE_TYPE_UNKNOWN = -1;

    /** CloudMessage should not be instantiated by outside directly. */
    private CloudMessage() {
    }

    public String getUniqueId() {
        return mUniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.mUniqueId = uniqueId;
    }

    public Long getTimestamp() {
        return mTimestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.mTimestamp = timestamp;
    }

    public Boolean getNeedPull() {
        return mNeedPull;
    }

    public void setNeedPull(Boolean needPull) {
        this.mNeedPull = needPull;
    }

    public Aps getAps() {
        return mAps;
    }

    public void setAps(Aps aps) {
        this.mAps = aps;
    }

    public CloudPayload getPayload() {
        return mPayload;
    }

    public void setPayload(CloudPayload payload) {
        this.mPayload = payload;
    }

    public int getToDeviceType() {
        return mToDeviceType;
    }

    public void setToDeviceType(int type) {
        this.mToDeviceType = type;
    }

    public static CloudMessage initSingleChatMsg(long fromUserId, long toUserId, String info, int msgType, int toDeviceType) {
        // Initialize Aps
        Aps aps = new Aps();
        aps.setAlert(info);

        // Initialize payload
        CloudPayload payload = new CloudPayload();
        payload.mCloudMsgType = CloudPayload.CLOUD_TYPE_GROUP_OP_NONE;
        payload.mFromUserId = fromUserId;
        payload.mToUserId = toUserId;
        payload.mMsgInfo = info;
        payload.mMsgType = msgType;

        // Initialize CloudMessage
        CloudMessage cloudMessage = new CloudMessage();
        cloudMessage.setAps(aps);
        cloudMessage.setPayload(payload);
        cloudMessage.setToDeviceType(toDeviceType);
        return cloudMessage;
    }

    /** Create a CloudMessage for a group chat message. */
    public static CloudMessage initGroupChatMsg(long fromUserId, String groupId, String info, int msgType) {
        // Initialize Aps
        Aps aps = new Aps();
        aps.setAlert(info);

        // Initialize payload
        CloudPayload payload = new CloudPayload();
        payload.mFromUserId = fromUserId;
        payload.mGroupId = groupId;
        payload.mMsgInfo = info;
        payload.mMsgType = msgType;
        payload.mCloudMsgType = CloudPayload.CLOUD_TYPE_GROUP_OP_MESSAGE;

        // Initialize CloudMessage
        CloudMessage cloudMsg = new CloudMessage();
        cloudMsg.setAps(aps);
        cloudMsg.setPayload(payload);
        cloudMsg.setToDeviceType(CloudMessage.DEVICE_TYPE_UNKNOWN);
        return cloudMsg;
    }

    public static CloudMessage CtrlMsg2CloudMsg(CtrlMessage cm) {
        // Initialize Aps
        Aps aps = new Aps();
        aps.setAlert(cm.getDescription());

        // Initialize payload
        CloudPayload payload = new CloudPayload();
        payload.mCloudMsgType = CloudPayload.CLOUD_TYPE_CTRL;
        payload.mCtrlMsg = cm;
        payload.mFromUserId = cm.getFromUserId();

        // Initialize CloudMessage
        CloudMessage cloudMsg = new CloudMessage();
        cloudMsg.setAps(aps);
        cloudMsg.setPayload(payload);
        return cloudMsg;
    }

    public ChatMessage newChatMessage() {
        return mPayload.newChatMessage();
    }

    public Conversation newConversation() {
        return mPayload.newConversation();
    }

    /** To judge if current cloud message's payload is a chat message. */
    public boolean isChatMessage() {
        return mPayload.isChatMessage();
    }

    public boolean isCtrlMessage() {
        return mPayload.isCtrlMessage();
    }

    public boolean isGroupChatMessage() {
        return mPayload.isGroupChatMessage();
    }

    public boolean isSingleChatMessage() {
        return mPayload.isSingleChatMessage();
    }

    /** Check if this message is sent by myself */
    public boolean isSentByMyself() {
        return mPayload.isSentByMyself();
    }

    public String getConversationName() {
        return mPayload.getConversationName();
    }

    public CtrlMessage toCtrlMsg() {
        return mPayload.toCtrlMsg();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUniqueId);
        dest.writeLong(mTimestamp);
        dest.writeInt(mNeedPull ? 1 : 0);
        dest.writeParcelable(mAps, flags);
        dest.writeParcelable(mPayload, flags);
    }

    private CloudMessage(Parcel in) {
        this.mUniqueId = in.readString();
        this.mTimestamp = in.readLong();
        this.mNeedPull = 0 == in.readInt() ? false : true;
        this.mAps = in.readParcelable(Aps.class.getClassLoader());
        this.mPayload = in.readParcelable(CloudPayload.class.getClassLoader());
    }

    public static final Parcelable.Creator<CloudMessage> CREATOR = new Parcelable.Creator<CloudMessage>() {

        public CloudMessage createFromParcel(Parcel in) {
            return new CloudMessage(in);
        }

        public CloudMessage[] newArray(int size) {
            return new CloudMessage[size];
        }
    };
}
