package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-2-11.
 */
public class Leave extends NoticeBase {
    @SerializedName("leaveReplies")
    public ArrayList<Reply> mReplies;

    public static class LeaveContent {
        @SerializedName("leaveStartDate")
        public String mStartDate;
        @SerializedName("leaveEndDate")
        public String mEndDate;
        @SerializedName("leaveMessage")
        public String mLeaveMessage;

    }
}
