package com.bobs.kidcontact.utils;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.chat.service.HeartbeatExecutor;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CloudId;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestUpdateCloudId;
import com.bobs.kidcontact.requests.CookieGsonRequest.EmptyInput;
import com.bobs.kidcontact.requests.CookieGsonRequest.Succeed;
import com.bobs.kidcontact.requests.LoginRequest;
import com.bobs.kidcontact.requests.LoginRequest.Login;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus.BoundStatus;
import com.bobs.kidcontact.ui.KidContactApp;


public class LoginUtil {
    private static final String TAG = "LoginUtil";
    private static final long TIMEOUT_SIGNIN = 5000;
    private static final long TIMEOUT_BOND = 5000;
    private final Login mLogin;
    private final RequestUtil<CookieGsonRequest.Succeed> mSignin;
    private final RequestUtil<BoundStatus> mBond;
    private final RequestUtil<CookieGsonRequest.Succeed> mUpdateCloudId;

    public LoginUtil(Login login) {
        mLogin = login;
        mSignin = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<Succeed> onPrepareRequest(Listener<Succeed> listener,
                    ErrorListener errListener) {
                return new LoginRequest(mLogin, listener, errListener);
            }

            @Override
            public void onSuccess(Succeed response) {
                Log.d(TAG, "Login success, checking bond state...");
                mBond.add2queue();
                LoginUtil.this.onProgressUpdate("Login success, checking bond state...");
                
                String id = KidContactApp.getInstance().getCloudId();
                if (id != null && !id.isEmpty()) {
                    mUpdateCloudId.add2queue();
                }
            }

            @Override
            public void onError(VolleyError e) {
                LoginUtil.this.onError(e);
                LoginUtil.this.onProgressUpdate("Signin error: " + RequestUtil.interpretErr2String(e));
            }
        };
        mSignin.setTimeout(TIMEOUT_SIGNIN);
        mBond = new RequestUtil<BoundStatus>() {
            @Override
            public Request<BoundStatus> onPrepareRequest(
                    Listener<BoundStatus> listener, ErrorListener errListener) {
                return new RequestCheckBoundStatus(new EmptyInput(), listener, errListener);
            }

            @Override
            public void onSuccess(BoundStatus response) {
                Log.d(TAG, "Bond state checked: " + response.isBounded());
                LoginUtil.this.onSuccess(response);
                LoginUtil.this.onProgressUpdate("Signin successfully");
            }

            @Override
            public void onError(VolleyError e) {
                LoginUtil.this.onError(e);
                LoginUtil.this.onProgressUpdate("Bonding error: " + RequestUtil.interpretErr2String(e));
            }
        };
        mBond.setTimeout(TIMEOUT_BOND);
        
        mUpdateCloudId = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                
                    CloudId cloudId = new CloudId();
                    cloudId.mCloudId = KidContactApp.getInstance().getCloudId();
                    Log.d(TAG, "current cloud id: " + cloudId.mCloudId);
                    return new RequestUpdateCloudId(cloudId, listener, errListener);
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {
                Log.i(TAG, "updated cloud id.");
                HeartbeatExecutor.getInstance().execute();
            }

            @Override
            public void onError(VolleyError e) {
            }
        };
        mUpdateCloudId.setTimeout(TIMEOUT_BOND);
    }

    public final void login() {
        mSignin.add2queue();
        onProgressUpdate("Trying to login into server ...");
    }

    /**
     * WARNING: call me from main thread only
     */
    public final void cancel() {
        mSignin.cancel();
        mBond.cancel();
        mUpdateCloudId.cancel();
        onProgressUpdate("Login canceled");
    }

    public void onProgressUpdate(String msg) {
        if (Constants.DEBUG) {
            Log.d(TAG, msg);
        }
    }

    public void onSuccess(BoundStatus response) {

    }

    public void onError(VolleyError error) {

    }
}
