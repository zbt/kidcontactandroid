package com.bobs.kidcontact.chat.service;

import android.util.Log;

import com.bobs.kidcontact.KCPushMessageReceiver;

import java.util.List;

/**
 * Created by hzheng3 on 14-2-19.
 */
public class BaiduPushMsgEventHandler implements KCPushMessageReceiver.EventHandler {
    IChatEngine mChatEngine;
    private static final String TAG = "BaiduPushMsgEventHandler";
    public BaiduPushMsgEventHandler(IChatEngine chatEngine) {
        mChatEngine = chatEngine;
    }
    @Override
    public void onBind(int errorCode, String appId, String userId, String channelId, String requestId) {
        if (errorCode != 0) {
            mChatEngine.onChatEngineConnectFailed(userId, requestId);
        } else {
            Log.i(TAG, "onChatEngineConnectSucceeded " + userId);
            mChatEngine.onChatEngineConnectSucceeded(userId, requestId);
        }
    }

    @Override
    public void onUnbind(int errorCode, String requestId) {
        if (errorCode != 0)
            mChatEngine.onChatEngineDisconnectFailed(requestId);
        else
            mChatEngine.onChatEngineDisconnectSucceeded(requestId);
    }

    @Override
    public void onDelTags(int errorCode, List<String> successTags, List<String> failTags, String requestId) {
        if (errorCode != 0)
            mChatEngine.onGroupLeaveFailed(failTags, requestId);
        else
            mChatEngine.onGroupLeaveSucceeded(successTags, requestId);
    }

    @Override
    public void onListTags(int errorCode, List<String> tags, String requestId) {
        if (errorCode != 0)
            mChatEngine.onGroupListFailed(tags, requestId);
        else
            mChatEngine.onGroupLeaveSucceeded(tags, requestId);
    }

    @Override
    public void onSetTags(int errorCode, List<String> successTags, List<String> failTags, String requestId) {
        if (errorCode != 0)
            mChatEngine.onGroupJoinFailed(failTags, requestId);
        else
            mChatEngine.onGroupJoinSucceeded(successTags, requestId);
    }

    @Override
    public void onMessage(String message, String customContentString) {
        mChatEngine.onMessage(message);
    }

    @Override
    public void onNotificationClicked(String title, String description, String customContentString) {
        // nothing
    }
}
