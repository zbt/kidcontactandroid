package com.bobs.kidcontact.requests;

import android.util.Log;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.RecordList;
import com.bobs.kidcontact.model.Tag;
import com.google.gson.Gson;

import org.apache.http.entity.mime.content.StringBody;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-6.
 */

/**
 * The record is submitted with multipart-form request which contain following parts
 * MUST: "info":"xxxxxx"
 * OPTIONAL: "tags":"["","",""]"
 * OPTIONAL: picture related
 * OPTIONAL: "children":[1,2,3]
 */
public final class RequestAddRecord extends RequestAddMomentRecord<RecordList> {
    private static final String TAG = "RequestAddRecord";

    ArrayList<Long> mChildren;

    /**
     * NOTE: If role is teacher, please supply children list, otherwise, it means add record
     * for *all* children with the same information
     * If role is parent, no need to supply children list, because parent is already bound to child
     * @param recordInfo
     * @param tags
     * @param picPairs
     * @param children
     * @param listener
     * @param errorListener
     */
    public RequestAddRecord(String recordInfo, ArrayList<Tag> tags,
                            ArrayList<PicPair> picPairs, ArrayList<Long> children,
                            Response.Listener<RecordList> listener, Response.ErrorListener errorListener) {
        super(getAddRecordURL(), recordInfo, tags,
                picPairs, RecordList.class, listener, errorListener);
        mChildren = children;
        buildChildInfo();
    }

    @SuppressWarnings("deprecation")
    private void buildChildInfo() {
        if (mChildren != null && !mChildren.isEmpty()) {
            String childrenInfo = getChildrenInfo();
            Log.i(TAG, "Children info is " + childrenInfo);
            try {
                entity.addPart("children", new StringBody(childrenInfo));
            } catch (UnsupportedEncodingException e) {
                Log.e(TAG, "add children failed " + e.getMessage());
            }
        }
    }

    private String getChildrenInfo() {
        Gson gson = new Gson();
        ArrayList<Long> childList = new ArrayList<Long>();
        for (Long child : mChildren) {
            childList.add(child);
        }
        return gson.toJson(childList);
    }

    private static String getAddRecordURL() {
        return Constants.getFunctionURL(Constants.FUNC_ADDRECORD);
    }
}
