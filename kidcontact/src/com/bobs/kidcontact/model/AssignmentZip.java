package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-4-16.
 */
public class AssignmentZip {
    @SerializedName("assignmentPath")
    public String mAssignmentPath;
}
