package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.model.CommentRecordList;

/**
 * Created by hzheng3 on 14-1-14.
 */
public final class RequestTeacherGetCommentRecord extends RequestGetCommentRecord {

    public RequestTeacherGetCommentRecord(long idOffset, long childId, Response.Listener<CommentRecordList> listener, Response.ErrorListener errorListener) {
        super(idOffset, childId, listener, errorListener);
    }
}