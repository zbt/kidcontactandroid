package com.bobs.kidcontact.chat.service;

import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;

import java.util.List;

/**
 * Created by hzheng3 on 14-2-19.
 */
public interface IChatEngine {
    public void onChatEngineConnectSucceeded(String id, String requestId);
    public void onChatEngineConnectFailed(String id, String requestId);
    public void onChatEngineDisconnectSucceeded(String requestId);
    public void onChatEngineDisconnectFailed(String requestId);
    public void onGroupListSucceeded(List<String> groups, String requestId);
    public void onGroupListFailed(List<String> groups, String requestId);
    public void onGroupJoinSucceeded(List<String> groups, String requestId);
    public void onGroupJoinFailed(List<String> groups, String requestId);
    public void onGroupLeaveSucceeded(List<String> groups, String requestId);
    public void onGroupLeaveFailed(List<String> groups, String requestId);
    public void onMessage(String msg);
    public void sendCloudMessage(CloudMessage cloudMessage, ChatMessage chatMessage);
    public void joinGroups(List<String> groups);
    public void leaveGroups(List<String> groups);
}
