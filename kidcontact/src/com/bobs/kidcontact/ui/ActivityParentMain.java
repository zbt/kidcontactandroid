package com.bobs.kidcontact.ui;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.chat.service.IChatService.Stub;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.utils.DebugLog;

import static com.bobs.kidcontact.contants.Constants.PublishType;
import static com.bobs.kidcontact.contants.Constants.PublishType.MOMENT;

/**
 * Created by junyongl on 13-12-25. zhanglong
 */
public class ActivityParentMain extends ActionBarActivityBase implements ActionBar.TabListener {
    private ParentMainPagerAdapter mPagerAdapter;
    private ViewPager mViewpager;
    private ActionBar mActionBar;
    @SuppressLint("UseSparseArrays")
    private Map<Integer, Fragment> mTabFragmentMap = new HashMap<Integer, Fragment>();
    private TextView mTab0 = null;
    private TextView mTabCornerMark0 = null;
    private TextView mTab1 = null;
    private TextView mTabCornerMark1 = null;
    private TextView mTab2 = null;
    private TextView mTabCornerMark2 = null;
    private TextView mTab3 = null;
    private TextView mTabCornerMark3 = null;
    private ViewEventHandler mViewEventHandler = null;
    private int mNewClassMsgCount = 0;
    private int mNewClassMomentCount = 0;
    private int mTab0UpdateCount = 0;
    private int mTab1UpdateCount = 0;
    private int mTab2UpdateCount = 0;
    private int mTab3UpdateCount = 0;
    private static boolean isExit = false;
    private static Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_parent_main);
        mActionBar = getSupportActionBar();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mActionBar.setHomeButtonEnabled(false);

        mPagerAdapter = new ParentMainPagerAdapter(getSupportFragmentManager());

        mViewpager = (ViewPager) findViewById(R.id.pager);
        mViewpager.setAdapter(mPagerAdapter);
        mViewpager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                getSupportActionBar().setSelectedNavigationItem(position);
            }
        });

        // Initialize all the tab views
        mActionBar.addTab(mActionBar.newTab().setCustomView(R.layout.activity_tab).setTabListener(this));
        mActionBar.addTab(mActionBar.newTab().setCustomView(R.layout.activity_tab).setTabListener(this));
        mActionBar.addTab(mActionBar.newTab().setCustomView(R.layout.activity_tab).setTabListener(this));
        mActionBar.addTab(mActionBar.newTab().setCustomView(R.layout.activity_tab).setTabListener(this));
        mTab0 = (TextView) mActionBar.getTabAt(0).getCustomView().findViewById(R.id.activity_tab_text);
        mTabCornerMark0 = (TextView) mActionBar.getTabAt(0).getCustomView().findViewById(R.id.activity_tab_corner_mark);
        mTab0.setText(getString(R.string.status));
        mTab1 = (TextView) mActionBar.getTabAt(1).getCustomView().findViewById(R.id.activity_tab_text);
        mTabCornerMark1 = (TextView) mActionBar.getTabAt(1).getCustomView().findViewById(R.id.activity_tab_corner_mark);
        mTab1.setText(getString(R.string.kid));
        mTab2 = (TextView) mActionBar.getTabAt(2).getCustomView().findViewById(R.id.activity_tab_text);
        mTabCornerMark2 = (TextView) mActionBar.getTabAt(2).getCustomView().findViewById(R.id.activity_tab_corner_mark);
        mTab2.setText(getString(R.string.clazz));
        mTab3 = (TextView) mActionBar.getTabAt(3).getCustomView().findViewById(R.id.activity_tab_text);
        mTabCornerMark3 = (TextView) mActionBar.getTabAt(3).getCustomView().findViewById(R.id.activity_tab_corner_mark);
        mTab3.setText(getString(R.string.more));

        mViewEventHandler = new ViewEventHandler(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_parent_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_publish_moment:
                publish(MOMENT);
                return true;
            case R.id.menu_publish_review:
                publish(PublishType.REVIEW);
                return true;
            case R.id.menu_publish_notice:
                publish(PublishType.NOTICE);
                return true;
            case R.id.menu_publish_assignment:
                publish(PublishType.ASSIGNMENT);
                return true;
            case R.id.menu_publish_record:
                publish(PublishType.RECORD);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void publish(PublishType type) {
        Intent intent = new Intent(this, ActivityPublish.class);
        switch (type) {
        case MOMENT:
            intent.putExtra("publish_type", MOMENT.ordinal());
            break;

        case REVIEW:
            intent.putExtra("publish_type", PublishType.REVIEW.ordinal());
            break;

        case NOTICE:
            intent.putExtra("publish_type", PublishType.NOTICE.ordinal());
            break;

        case ASSIGNMENT:
            intent.putExtra("publish_type", PublishType.ASSIGNMENT.ordinal());
            break;

        case RECORD:
            intent.putExtra("publish_type", PublishType.RECORD.ordinal());
            break;
        default:
            break;
        }
        startActivityForResult(intent, type.ordinal());
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        switch (mActionBar.getSelectedTab().getPosition()) {
            case 1:
                menu.findItem(R.id.menu_publish_record).setVisible(true);
                break;
            case 2:
                menu.findItem(R.id.menu_publish_moment).setVisible(true);
                break;
            default:
                break;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @SuppressLint("NewApi")
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewpager.setCurrentItem(tab.getPosition());
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }
        try{
        	Context c = ActivityParentMain.this;
            InputMethodManager imm = (InputMethodManager)c.getSystemService(Context.INPUT_METHOD_SERVICE);
            boolean isOpen=imm.isActive(); 
            if (isOpen == true){
            	imm.hideSoftInputFromWindow(((Activity)c).getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }catch(Exception e){
        	DebugLog.d("InputMethod Close error.");
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public Handler getViewEventHandler() {
        return mViewEventHandler;
    }

    public class ParentMainPagerAdapter extends FragmentPagerAdapter {

        private ParentMainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment result = null;
            switch (i) {
                case TAB_POSITION_PARENT_STATUS:
                    result = new FragmentParentStatus();
                    mTabFragmentMap.remove(TAB_POSITION_PARENT_STATUS);
                    mTabFragmentMap.put(TAB_POSITION_PARENT_STATUS, result);
                    break;
                case 1:
                    result = new FragmentParentKid();
                    mTabFragmentMap.remove(TAB_POSITION_PARENT_KID);
                    mTabFragmentMap.put(TAB_POSITION_PARENT_KID, result);
                    break;
                case 2:
                    result = new FragmentParentClass();
                    mTabFragmentMap.remove(TAB_POSITION_PARENT_CLASS);
                    mTabFragmentMap.put(TAB_POSITION_PARENT_CLASS, result);
                    break;
                case 3:
                    result = new FragmentParentMore();
                    mTabFragmentMap.remove(TAB_POSITION_PARENT_MORE);
                    mTabFragmentMap.put(TAB_POSITION_PARENT_MORE, result);
                    break;
                default:
                    break;
            }
            return result;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    @Override
    protected void refreshViewsAsync() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                // Sleep a while to make sure that the service's been bound
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                refreshUnhandledUpdates();
            }

        }).start();
    }

    /**
     * Refresh all the unhandled updates and notify the UI. This may be slow, you
     * should not call it in your UI thread.
     */
    private void refreshUnhandledUpdates() {
        // Get unhandled updates for all tabs
        int tab0UpdateCount = getTab0UpdateCount();
        int tab1UpdateCount = getTab1UpdateCount();
        int tab2UpdateCount = getTab2UpdateCount();
        int tab3UpdateCount = getTab3UpdateCount();

        // Notify the UI to update
        Bundle data = new Bundle();
        data.putInt(KEY_TAB0_UPDATE_COUNT, tab0UpdateCount);
        data.putInt(KEY_TAB1_UPDATE_COUNT, tab1UpdateCount);
        data.putInt(KEY_TAB2_UPDATE_COUNT, tab2UpdateCount);
        data.putInt(KEY_TAB3_UPDATE_COUNT, tab3UpdateCount);
        Message msg = new Message();
        msg.what = EVENT_ALL_UPDATE_COUNT;
        msg.setData(data);
        mViewEventHandler.sendMessage(msg);
    }

    /** Get the update count for tab0 */
    private int getTab0UpdateCount() {
        int newNoticeCount = 0;
        if (null != mChatServiceStub) {
            try {
                newNoticeCount = mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_NOTICE_NEW);
            } catch (RemoteException e) {
                Log.e(TAG, "failed to get CtrlMessage count by type: CtrlMessage.TYPE_NOTICE_NEW." + e.getMessage());
            }
        } else {
            Log.w(TAG, Common.getLastTrace() + ": failed to get chat service stub!");
        }
        return newNoticeCount;
    }

    /** Get the update count for tab1 */
    private int getTab1UpdateCount() {
        // Check single chat (all chat with teachers) update count
        int singleChatNewMsgCount = 0;
        try {
            Stub stub = getChatServiceStub();
            if (null != stub) {
                singleChatNewMsgCount = stub.getAllSingleChatNewMsgCount();
            } else {
                Log.w(TAG, "chat service not bound!");
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            // We should move on anyway
        }

        // Check leave update count
        int leaveUpdateCount = 0;
        try {
            if (null != mChatServiceStub) {
                leaveUpdateCount = mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_LEAVE_UPDATE);
            } else {
                Log.w(TAG, "chat service not bound!");
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            // We should move on anyway
        }

        // TODO CHECK NEW COMMENT
        int newCommentCount = 0;

        return singleChatNewMsgCount + leaveUpdateCount + newCommentCount;
    }

    /** Get the udpate count for tab2 */
    private int getTab2UpdateCount() {
        // Get the class chat new message count
        int classChatNewMsgCount = 0;
        int momentUpcateCount = 0;
        try {
            if (null != mChatServiceStub) {
                classChatNewMsgCount = mChatServiceStub.getClassNewMsgCount();
                momentUpcateCount = mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_MOMENT_NEW);
                // TODO CtrlMessage.TYPE_MOMENT_UPDATE count
            } else {
                Log.w(TAG, "chat service not bound!");
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            // We should move on anyway
        }

        return classChatNewMsgCount + momentUpcateCount;
    }

    /** Get the update count for tab3 */
    private int getTab3UpdateCount() {
        int versionUpdateCount = 0;
        try {
            if (null != mChatServiceStub) {
                versionUpdateCount = mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_VERSION_NEW);
            } else {
                Log.w(TAG, "chat service not bound!");
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            // We should move on anyway
        }
        return versionUpdateCount;
    }

    /** Update the one of the corner mark */
    private void refreshCornerMark(int tabPosition, int unhandledUpdateCount) {
        switch (tabPosition) {
        case TAB_POSITION_PARENT_STATUS:
            mTabCornerMark0.setVisibility(unhandledUpdateCount > 0 ? View.VISIBLE : View.INVISIBLE);
            break;
        case TAB_POSITION_PARENT_KID:
            mTabCornerMark1.setVisibility(unhandledUpdateCount > 0 ? View.VISIBLE : View.INVISIBLE);
            break;
        case TAB_POSITION_PARENT_CLASS:
            mTabCornerMark2.setVisibility(unhandledUpdateCount > 0 ? View.VISIBLE : View.INVISIBLE);
            break;
        case TAB_POSITION_PARENT_MORE:
            mTabCornerMark3.setVisibility(unhandledUpdateCount > 0 ? View.VISIBLE : View.INVISIBLE);
            break;
        default:
            // Should not come here !
            Log.w(TAG, "wrong tab position!");
            break;
        }
    }

    @Override
    protected void refreshViews(Conversation conversation) {
        if (null == conversation) {
            return;
        }
        int count = conversation.getNewMsgCount();
        count = count < 0 ? 0 : count;
        if (conversation.getGroupChat()) {
            refreshCornerMark(TAB_POSITION_PARENT_CLASS, count);

            // Notify parent class fragment
            FragmentParentClass parentClass = (FragmentParentClass) mTabFragmentMap.get(TAB_POSITION_PARENT_CLASS);
            if (null != parentClass) {
                Message msg = new Message();
                msg.what = Events.EVENT_CONVERSATION_CHANGED;
                Bundle data = new Bundle();
                data.putParcelable(Events.KEY_CONVERSATION_CHANGED, conversation);
                msg.setData(data);
                parentClass.getEventHandlerForView().sendMessage(msg);
            }
        } else {
            refreshCornerMark(TAB_POSITION_PARENT_KID, count);

            // Notify parent kid fragment
            Message msg = new Message();
            msg.what = Events.EVENT_CONVERSATION_CHANGED;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CONVERSATION_CHANGED, conversation);
            msg.setData(data);
            FragmentParentKid parentKid = (FragmentParentKid) mTabFragmentMap.get(TAB_POSITION_PARENT_KID);
            if (null != parentKid) {
                parentKid.getEventHandlerForView().sendMessage(msg);
            }
        }
    }

    @Override
    protected void refreshViews(CtrlMessage msg) {
        super.refreshViews(msg);
        refreshViewsAsync();

        // We don't assume what kind of types the sub fragment(s) care about, so notify every time
        FragmentParentStatus status = (FragmentParentStatus) mTabFragmentMap.get(TAB_POSITION_TEACHER_STATUS);
        if (null != status) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_NEW;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            status.getEventHandlerForView().sendMessage(msgOut);
        }
        FragmentParentKid kid = (FragmentParentKid) mTabFragmentMap.get(TAB_POSITION_PARENT_KID);
        if (null != kid) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_NEW;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            kid.getEventHandlerForView().sendMessage(msgOut);
        }
        FragmentParentClass cls = (FragmentParentClass) mTabFragmentMap.get(TAB_POSITION_PARENT_CLASS);
        if (null != cls) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_NEW;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            cls.getEventHandlerForView().sendMessage(msgOut);
        }
        FragmentParentMore more = (FragmentParentMore) mTabFragmentMap.get(TAB_POSITION_PARENT_MORE);
        if (null != more) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_NEW;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            more.getEventHandlerForView().sendMessage(msgOut);
        }
    }

    @Override
    protected void refreshViewsDel(CtrlMessage msg) {
        super.refreshViewsDel(msg);
        refreshViewsAsync();
        // TODO CtrlMessage.TYPE_NOTICE_UPDATE, CtrlMessage.TYPE_MOMENT_UPDATE
        // We don't assume what kind of types the sub fragment(s) care about, so notify every time
        FragmentParentStatus status = (FragmentParentStatus) mTabFragmentMap.get(TAB_POSITION_PARENT_STATUS);
        if (null != status) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_DELETED;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            status.getEventHandlerForView().sendMessage(msgOut);
        }
        FragmentParentKid kid = (FragmentParentKid) mTabFragmentMap.get(TAB_POSITION_PARENT_KID);
        if (null != kid) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_DELETED;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            kid.getEventHandlerForView().sendMessage(msgOut);
        }
        FragmentParentClass cls = (FragmentParentClass) mTabFragmentMap.get(TAB_POSITION_PARENT_CLASS);
        if (null != cls) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_DELETED;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            cls.getEventHandlerForView().sendMessage(msgOut);
        }
        FragmentParentMore more = (FragmentParentMore) mTabFragmentMap.get(TAB_POSITION_PARENT_MORE);
        if (null != more) {
            Message msgOut = new Message();
            msgOut.what = Events.EVENT_CTRL_MSG_DELETED;
            Bundle data = new Bundle();
            data.putParcelable(Events.KEY_CTRL_MSG, msg);
            msgOut.setData(data);
            more.getEventHandlerForView().sendMessage(msgOut);
        }
    }

    /** A handler used to handle all the view event of this activity */
    public static class ViewEventHandler extends Handler {
        private WeakReference<ActivityParentMain> mWeakParent = null;
        // private int mUnhandledUpdateCount = 0;

        public ViewEventHandler(ActivityParentMain parentMain) {
            mWeakParent = new WeakReference<ActivityParentMain>(parentMain);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
            case EVENT_NEW_MSG_COUNT:
                Bundle data0 = msg.getData();
                mWeakParent.get().mNewClassMsgCount = data0.getInt(KEY_NEW_MSG_COUNT);
                int tabPosition = data0.getInt(KEY_TAB_POSITION);
                mWeakParent.get().refreshCornerMark(tabPosition, mWeakParent.get().mNewClassMsgCount +
                        mWeakParent.get().mNewClassMomentCount);
                break;
            case EVENT_ALL_UPDATE_COUNT:
                Bundle data1 = msg.getData();
                mWeakParent.get().mTab0UpdateCount = data1.getInt(KEY_TAB0_UPDATE_COUNT);
                mWeakParent.get().mTab1UpdateCount = data1.getInt(KEY_TAB1_UPDATE_COUNT);
                mWeakParent.get().mTab2UpdateCount = data1.getInt(KEY_TAB2_UPDATE_COUNT);
                mWeakParent.get().mTab3UpdateCount = data1.getInt(KEY_TAB3_UPDATE_COUNT);
                mWeakParent.get().refreshCornerMark(TAB_POSITION_PARENT_STATUS, mWeakParent.get().mTab0UpdateCount);
                mWeakParent.get().refreshCornerMark(TAB_POSITION_PARENT_KID, mWeakParent.get().mTab1UpdateCount);
                mWeakParent.get().refreshCornerMark(TAB_POSITION_PARENT_CLASS, mWeakParent.get().mTab2UpdateCount);
                mWeakParent.get().refreshCornerMark(TAB_POSITION_PARENT_MORE, mWeakParent.get().mTab3UpdateCount);
                break;
            default:
                break;
            }
        }

    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        int count = fragmentManager.getBackStackEntryCount();
        if (count == 0) {
            exit();
        } else {
            super.onBackPressed();
        }
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), R.string.exit_app,
                    Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            DebugLog.d("exit application");
            this.finish();
        }
    }
}
