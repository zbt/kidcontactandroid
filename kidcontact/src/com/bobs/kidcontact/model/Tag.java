package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-6.
 * Shared for MomentTag, RecordTag, PictureTag
 */
public class Tag {
    @SerializedName("tag")
    public String tag;
}
