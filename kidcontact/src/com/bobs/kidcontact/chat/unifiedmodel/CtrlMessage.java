package com.bobs.kidcontact.chat.unifiedmodel;

import com.bobs.kidcontact.ui.KidContactApp;
import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import android.os.Parcelable;
import com.bobs.kidcontact.R;

/**
 *
 * @author zhanglong
 */
public class CtrlMessage implements Parcelable {
    public static final String FIELD_NAME_ID = "_id";
    public static final String FIELD_NAME_TYPE = "type";
    public static final String FIELD_NAME_FROM_USER_ID = "from_user_id";
    public static final String FIELD_NAME_TO_USER_ID = "to_user_id";
    public static final String FIELD_NAME_DATA_ID = "data_id";
    public static final int TYPE_INVALID = -1;
    public static final int TYPE_NOTICE_NEW = 0;
    public static final int TYPE_NOTICE_UPDATE = 1;
    public static final int TYPE_COMMENT_NEW = 2;
    public static final int TYPE_COMMENT_UPDATE = 3;
    public static final int TYPE_RECORD_NEW = 4;
    public static final int TYPE_RECORD_UPDATE = 5;
    public static final int TYPE_MOMENT_NEW = 6;
    public static final int TYPE_MOMENT_UPDATE = 7;
    public static final int TYPE_LEAVE_NEW = 8;
    public static final int TYPE_LEAVE_UPDATE = 9;
    public static final int TYPE_VERSION_NEW = 10;
    public static final int TYPE_REMOTE_LOGIN = 11;
    public static final int TYPE_KID_ENTER_KINDERGARDEN = 12;
    public static final int TYPE_KID_LEAVE_KINDERGARDEN = 13;

    // @SerializedName("id")
    @SerializedName("a")
    long mId = -1L;

    // @SerializedName("type")
    @SerializedName("b")
    int mType = TYPE_INVALID;

    // @SerializedName("fromUserId")
    @SerializedName("c")
    long mFromUserId = -1L;

    // @SerializedName("toUserId")
    @SerializedName("d")
    long mToUserId = -1L;

    // @SerializedName("dataId")
    @SerializedName("e")
    long mDataId = -1L;

    public void setId(long id) {
        mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setType(int type) {
        mType = type;
    }

    public int getType() {
        return mType;
    }

    public void setFromUserId(long id) {
        mFromUserId = id;
    }

    public long getFromUserId() {
        return mFromUserId;
    }

    public void setToUserId(long id) {
        mToUserId = id;
    }

    public long getToUserId() {
        return mToUserId;
    }

    public void setDataId(long id) {
        mDataId = id;
    }

    public long getDataId() {
        return mDataId;
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeInt(mType);
        dest.writeLong(mFromUserId);
        dest.writeLong(mToUserId);
        dest.writeLong(mDataId);
    }

    public CtrlMessage() {
    }

    public CtrlMessage(Parcel in) {
        mId = in.readLong();
        mType = in.readInt();
        mFromUserId = in.readLong();
        mToUserId = in.readLong();
        mDataId = in.readLong();
    }

    /**
     * Create a control message for a newly created notice.
     * @param fromUserId the id of the user that make this notice
     * @param dataId the id of the newly made notice
     * @return a CtrlMessage
     */
    public static CtrlMessage createNoticeNew(long fromUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_NOTICE_NEW);
        msg.setFromUserId(fromUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     *Create a control message for a newly updated notice
     *@param fromUserId the id of the user who made this update
     *@param dataId the id of the newly updated notice
     *@return a CtrlMessage
     */
    public static CtrlMessage createNoticeUpdate(long fromUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_NOTICE_UPDATE);
        msg.setFromUserId(fromUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     *Create a control message for a newly created comment.
     *@param fromUserId the id of the user who committed this comment
     *@param toUserId the id of the user who was commented
     *@param dataId the id of newly created comment
     *@return a CtrlMessage
     */
    public static CtrlMessage createCommentNew(long fromUserId, long toUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_COMMENT_NEW);
        msg.setFromUserId(fromUserId);
        msg.setToUserId(toUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a newly updated comment.
     * @param fromUserId the id of the user who updated this comment
     * @param toUserId the id of the user who was commented
     * @param dataId the id of the newly updated comment
     * @return a CtrlMessage
     */
    public static CtrlMessage createCommentUpdate(long fromUserId, long toUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_COMMENT_UPDATE);
        msg.setFromUserId(fromUserId);
        msg.setToUserId(toUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a newly created record.
     * @param fromUserId the id of the user who committed this record
     * @param toUserId the id of the user who was recorded
     * @param dataId the id of the newly created record
     * @return a CtrlMessage
     */
    public static CtrlMessage createRecordNew(long fromUserId, long toUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_RECORD_NEW);
        msg.setFromUserId(fromUserId);
        msg.setToUserId(toUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a newly updated record.
     * @param fromUserId the id of the user who updated the record
     * @param toUserId the id of the user who was recorded
     * @param dataId the id of the newly updated record
     * @return a CtrlMessage
     */
    public static CtrlMessage createRecordUpdate(long fromUserId, long toUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_RECORD_UPDATE);
        msg.setFromUserId(fromUserId);
        msg.setToUserId(toUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a newly created moment.
     * @param fromUserId the id of the user who committed this comment.
     * @param dataId the id of the newly created comment
     * @return a CtrlMessage
     */
    public static CtrlMessage createMomentNew(long fromUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_MOMENT_NEW);
        msg.setFromUserId(fromUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a newly updated moment.
     * @param fromUserId the id of the user who updated this moment
     * @param dataId the id of the newly updated moment
     * @return a CtrlMessage
     */
    public static CtrlMessage createMomentUpdate(long fromUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_MOMENT_UPDATE);
        msg.setFromUserId(fromUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a newly created leave.
     * @param fromUserId the id of the user who commited leave
     * @param dataId the id of the newly created leave
     * @return a CtrlMessage
     */
    public static CtrlMessage createLeaveNew(long fromUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_LEAVE_NEW);
        msg.setFromUserId(fromUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a newly updated leave.
     * @param fromUserId the id of the user who updated the leave
     * @param dataId the id of the newly updated leave
     * @return a CtrlMessage
     */
    public static CtrlMessage createLeaveUpdate(long fromUserId, long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_LEAVE_UPDATE);
        msg.setFromUserId(fromUserId);
        msg.setDataId(dataId);
        return msg;
    }

    /**
     * Create a control message for a new version.
     * @param dataId the id of the newly committed version
     * @return a CtrlMessage
     */
    public static CtrlMessage createVersionNew(long dataId) {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_VERSION_NEW);
        msg.setDataId(dataId);
        return msg;
    }

    public static CtrlMessage createKidEnterGarden() {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_KID_ENTER_KINDERGARDEN);
        return msg;
    }

    public static CtrlMessage createKidLeaveGarden() {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_KID_LEAVE_KINDERGARDEN);
        return msg;
    }

    /**
     * Create a control message for a remote login.
     * @return a CtrlMessage
     */
    public static CtrlMessage createRemoteLogin() {
        CtrlMessage msg = new CtrlMessage();
        msg.setType(TYPE_REMOTE_LOGIN);
        return msg;
    }

    /** Get a human readable description of this control message. */
    public String getDescription() {
        String des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_default);
        switch (mType) {
        case CtrlMessage.TYPE_COMMENT_NEW:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_coment_new);
            break;
        case CtrlMessage.TYPE_COMMENT_UPDATE:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_coment_update);
            break;
        case CtrlMessage.TYPE_LEAVE_NEW:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_leave_update);
            break;
        case CtrlMessage.TYPE_LEAVE_UPDATE:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_leave_update);
            break;
        case CtrlMessage.TYPE_MOMENT_NEW:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_moment_new);
            break;
        case CtrlMessage.TYPE_MOMENT_UPDATE:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_moment_update);
            break;
        case CtrlMessage.TYPE_NOTICE_NEW:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_notice_new);
            break;
        case CtrlMessage.TYPE_NOTICE_UPDATE:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_notice_update);
            break;
        case CtrlMessage.TYPE_RECORD_NEW:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_record_new);
            break;
        case CtrlMessage.TYPE_RECORD_UPDATE:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_record_update);
            break;
        case CtrlMessage.TYPE_REMOTE_LOGIN:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_remote_login);
            break;
        case CtrlMessage.TYPE_VERSION_NEW:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_version_new);
            break;
        case CtrlMessage.TYPE_KID_ENTER_KINDERGARDEN:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_kid_enter_kindergarden);
            break;
        case CtrlMessage.TYPE_KID_LEAVE_KINDERGARDEN:
            des = KidContactApp.getInstance().getString(R.string.ctrl_msg_des_kid_leave_kindergarden);
            break;
        default:
            break;
        }
        return des;
    }

    public static final Parcelable.Creator<CtrlMessage> CREATOR = new Parcelable.Creator<CtrlMessage>() {

        public CtrlMessage createFromParcel(Parcel in) {
            return new CtrlMessage(in);
        }

        public CtrlMessage[] newArray(int size) {
            return new CtrlMessage[size];
        }
    };

/******************************For Server********************/
    /** Get a human readable description of this control message. */
    public String getDescriptionForServer() {
        String des = CtrlMessage.DESCRIPTION_DEFAULT;
        switch (mType) {
        case CtrlMessage.TYPE_COMMENT_NEW:
            des = CtrlMessage.DESCRIPTION_COMENT_NEW;
            break;
        case CtrlMessage.TYPE_COMMENT_UPDATE:
            des = CtrlMessage.DESCRIPTION_COMENT_UPDATE;
            break;
        case CtrlMessage.TYPE_LEAVE_NEW:
            des = CtrlMessage.DESCRIPTION_LEAVE_NEW;
            break;
        case CtrlMessage.TYPE_LEAVE_UPDATE:
            des = CtrlMessage.DESCRIPTION_LEAVE_UPDATE;
            break;
        case CtrlMessage.TYPE_MOMENT_NEW:
            des = CtrlMessage.DESCRIPTION_MOMENT_NEW;
            break;
        case CtrlMessage.TYPE_MOMENT_UPDATE:
            des = CtrlMessage.DESCRIPTION_MOMENT_UPDATE;
            break;
        case CtrlMessage.TYPE_NOTICE_NEW:
            des = CtrlMessage.DESCRIPTION_NOTICE_NEW;
            break;
        case CtrlMessage.TYPE_NOTICE_UPDATE:
            des = CtrlMessage.DESCRIPTION_NOTICE_UPDATE;
            break;
        case CtrlMessage.TYPE_RECORD_NEW:
            des = CtrlMessage.DESCRIPTION_RECORD_NEW;
            break;
        case CtrlMessage.TYPE_RECORD_UPDATE:
            des = CtrlMessage.DESCRIPTION_RECORD_UPDATE;
            break;
        case CtrlMessage.TYPE_REMOTE_LOGIN:
            des = CtrlMessage.DESCRIPTION_REMOTE_LOGIN;
            break;
        case CtrlMessage.TYPE_VERSION_NEW:
            des = CtrlMessage.DESCRIPTION_VERSION_NEW;
            break;
        case CtrlMessage.TYPE_KID_ENTER_KINDERGARDEN:
            des = CtrlMessage.DESCRIPTION_KID_ENTER_KINDERGARDEN;
            break;
        case CtrlMessage.TYPE_KID_LEAVE_KINDERGARDEN:
            des = CtrlMessage.DESCRIPTION_KID_LEAVE_KINDERGARDEN;
            break;
        default:
            break;
        }
        return des;
    }

    private static final String DESCRIPTION_DEFAULT = "您有新的提醒";
    private static final String DESCRIPTION_COMENT_NEW = "有老师对您的孩子发表了新的评论";
    private static final String DESCRIPTION_COMENT_UPDATE = "有老师更新了对您的孩子的评论";
    private static final String DESCRIPTION_LEAVE_NEW = "您新的请假记录未处理";
    private static final String DESCRIPTION_LEAVE_UPDATE = "您的请假记录已经被处理";
    private static final String DESCRIPTION_MOMENT_NEW = "您有新的班级记录";
    private static final String DESCRIPTION_MOMENT_UPDATE = "您的班级记录有更新";
    private static final String DESCRIPTION_NOTICE_NEW = "您有新的班级动态";
    private static final String DESCRIPTION_NOTICE_UPDATE = "您的班级动态有更新";
    private static final String DESCRIPTION_RECORD_NEW = "有新的小朋友记录";
    private static final String DESCRIPTION_RECORD_UPDATE = "小朋友记录有更新";
    private static final String DESCRIPTION_REMOTE_LOGIN = "您的账号在另外一台设备上登录";
    private static final String DESCRIPTION_VERSION_NEW = "新版本可用";
    private static final String DESCRIPTION_KID_ENTER_KINDERGARDEN = "您的孩子已进入幼儿园";
    private static final String DESCRIPTION_KID_LEAVE_KINDERGARDEN = "您的孩子已离开幼儿园";
}
