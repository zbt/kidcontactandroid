package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;

/**
 * Created by hzheng3 on 13-12-23.
 */
public final class RequestLogout extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestLogout(Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_LOGOUT),
                null, Succeed.class, listener, errorListener);
    }

}
