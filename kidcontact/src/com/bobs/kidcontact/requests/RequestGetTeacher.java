package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.TeacherList;

public final class RequestGetTeacher extends CookieGsonRequest<TeacherList> {
    public RequestGetTeacher(Response.Listener<TeacherList> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getURL(), TeacherList.class, listener, errorListener);
    }

    public static String getURL() {
        return Constants.getFunctionURL(Constants.FUNC_GET_TEACHERS);
    }
}
