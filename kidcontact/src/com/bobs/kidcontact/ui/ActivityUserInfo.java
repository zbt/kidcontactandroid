package com.bobs.kidcontact.ui;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.IdProxy;
import com.bobs.kidcontact.requests.RequestGetBoundStatus;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus.BoundStatus;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The main activity to show the detail information of a user
 * @author zhanglong
 */
public class ActivityUserInfo extends ActionBarActivityBase {
    public static final String TAG = "ActivityUserInfo";
    public static final String KEY_USER_ID = "key_user_id";
    private NetworkRoundImageView mIconUser = null;
    private TextView mName = null;
    private TextView mId = null;
    private TextView mRole = null;
    private TextView mKidName = null;
    private long mUserId = -1L;
    BoundStatus mResStatus = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtil.setThemeForUser(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_user_info);
        initViews();
        mUserId = getIntent().getLongExtra(KEY_USER_ID, -1L);
        new UserInfoRequestUtil().add2queue();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
    }

    /** Initialize all views of this activity */
    private void initViews() {
        // Get all instances
        mIconUser = (NetworkRoundImageView) findViewById(R.id.icon_user);
        mName = (TextView) findViewById(R.id.text_user_name);
        mId = (TextView) findViewById(R.id.text_user_id);
        mRole = (TextView) findViewById(R.id.text_user_role);
        mKidName = (TextView) findViewById(R.id.text_kid_name);
    }

    private void refreshViews(BoundStatus status) {
        if (null == status) {
            return;
        }

        // Update UI
        mIconUser.setImageUrl(Constants.getAvatarThumbURLOfCreator(status.mId),
                KidContactApp.getInstance().getImageLoader());
        mName.setText("姓名：" + status.mName);
        mId.setText("ID：" + status.mId);
        mRole.setText("身份：" + (1 == status.mRole ? "老师" : "家长"));
        if (null != status.mChildName && !status.mChildName.equals("")) {
            mKidName.setText("孩子：" + status.mChildName);
        } else {
            mKidName.setVisibility(View.INVISIBLE);
        }
    }
    
    
    /**
     * A RequestUtil for get bound user info.
     * @author zhanglong
     *
     */
    private class UserInfoRequestUtil extends RequestUtil<BoundStatus> {

        @Override
        public Request<BoundStatus> onPrepareRequest(
                Listener<BoundStatus> listener, ErrorListener errListener) {
            IdProxy idProxy = new IdProxy();
            idProxy.mId = mUserId;
            return new RequestGetBoundStatus(idProxy, listener, errListener);
        }

        @Override
        public void onSuccess(BoundStatus response) {
            if (null == response) {
                Log.e(TAG, "failed to get cloud tag!");
                return;
            }
            refreshViews(response);
        }

        @Override
        public void onError(VolleyError e) {
            Toast.makeText(ActivityUserInfo.this, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            Log.w(TAG, RequestUtil.interpretErr2String(e));
            super.onError(e);
        }

    }

}
