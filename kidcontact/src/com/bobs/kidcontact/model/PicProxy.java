package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by hzheng3 on 14-1-14.
 * Shared for MomemntPic, RecordPic, ChildPic, UserPic
 */
public class PicProxy  {
    @SerializedName("pic")
    public Picture mPicture;

    public Picture getPicture() {
        return mPicture;
    }

    public static class PicProxyComparator implements Comparator<PicProxy> {
        @Override
        public int compare(PicProxy picProxy, PicProxy picProxy2) {
            if( picProxy.mPicture.mId < picProxy2.mPicture.mId)
                return -1;
            else if (picProxy.mPicture.mId > picProxy2.mPicture.mId)
                return 1;
            else
                return 0;
        }
    }
}
