package com.bobs.kidcontact;

import java.util.List;

import android.content.Context;
import android.util.Log;

import com.baidu.frontia.api.FrontiaPushMessageReceiver;


public class KCPushMessageReceiver extends FrontiaPushMessageReceiver {
	public static final String TAG = "KCPushMessageReceiver";
	public static EventHandler mHandler;

	public static abstract interface EventHandler {
		public abstract void onBind(int errorCode, String appId, String userId,
	            String channelId, String requestId);

		public abstract void onUnbind(int errorCode, String requestId);

		public abstract void onDelTags(int errorCode, List<String> successTags,
	            List<String> failTags, String requestId);

		public abstract void onListTags(int errorCode, List<String> tags,
	            String requestId);

		public abstract void onSetTags(int errorCode, List<String> successTags,
	            List<String> failTags, String requestId);

		public abstract void onMessage(String message, String customContentString);

		public abstract void onNotificationClicked(String title, String description,
	            String customContentString);
	}

	private void dumpList(List<String> strs) {
	    for (String str : strs){
	        Log.i(TAG, str + "\n");
	    }
	}

	public static final void registerEventHandler(EventHandler eventHandler) {
	    Log.i(TAG, "Registering event handler");
	    mHandler = eventHandler;
	}
	public static final void unRegisterEventHandler() {
	    mHandler = null;
	}
    @Override
    public void onBind(Context arg0, int errorCode, String appId, String userId,
            String channelId, String requestId) {
        Log.i(TAG, "onBind\nerrorCode: " + errorCode +
                   "\nappId: " + appId +
                   "\nuserId: " + userId +
                   "\nchannelId: " + channelId +
                   "\nrequestId: " + requestId);
        if (mHandler != null) {
            mHandler.onBind(errorCode, appId, userId, channelId, requestId);
        }
    }

    @Override
    public void onDelTags(Context arg0, int errorCode, List<String> successTags,
            List<String> failTags, String requestId) {
        Log.i(TAG, "onDelTags\nerrorCode: " + errorCode +
                   "\nsuccesstags: ");
        dumpList(successTags);
        Log.i(TAG, "failTags: ");
        dumpList(failTags);
        Log.i(TAG, "\nrequestId: " + requestId);
        if (mHandler != null) {
            mHandler.onDelTags(errorCode, successTags, failTags, requestId);
        }
    }

    @Override
    public void onListTags(Context arg0, int errorCode, List<String> tags,
            String requestId) {
        Log.i(TAG, "onListTags\nerrorCode: " + errorCode +
                "\ntags: ");
        dumpList(tags);
        Log.i(TAG, "\nrequestId: " + requestId);
        if (mHandler != null) {
            mHandler.onListTags(errorCode, tags, requestId);
        }
    }

    @Override
    public void onMessage(Context arg0, String message, String customContentString) {
        Log.i(TAG, "onMessage\nmessage: " + message +

                "\ncustomContentString: " + customContentString);
        if (mHandler != null) {
            mHandler.onMessage(message, customContentString);
        }
    }

    @Override
    public void onNotificationClicked(Context arg0, String title, String description,
            String customContentString) {
        Log.i(TAG, "onNotificationClicked\ntitle: " + title +
                "\ndescription: " + description +
                "\ncustomContentString: " + customContentString);
        if (mHandler != null) {
            mHandler.onNotificationClicked(title, description, customContentString);
        }
    }

    @Override
    public void onSetTags(Context arg0, int errorCode, List<String> successTags,
            List<String> failTags, String requestId) {
        Log.i(TAG, "onSetTags\nerrorCode: " + errorCode +
                "\nsuccesstags: ");
        dumpList(successTags);
        Log.i(TAG, "failTags: ");
        dumpList(failTags);
        Log.i(TAG, "\nrequestId: " + requestId);
        if (mHandler != null) {
            mHandler.onSetTags(errorCode, successTags, failTags, requestId);
        }
    }

    @Override
    public void onUnbind(Context arg0, int errorCode, String requestId) {
        Log.i(TAG, "onUnBind\nerrorCode: " + errorCode +

                "\nrequestId: " + requestId);
        if (mHandler != null) {
            mHandler.onUnbind(errorCode, requestId);
        }
    }
}
