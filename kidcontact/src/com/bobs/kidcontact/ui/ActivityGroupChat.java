package com.bobs.kidcontact.ui;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.ui.FragmentChat;
import com.bobs.kidcontact.model.CloudTag;
import com.bobs.kidcontact.requests.RequestGetClassChatTag;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * The main activity for group chat (class chat).
 * @author zhanglong
 *
 */
public class ActivityGroupChat extends ActionBarActivityBase {
    public static final String TAG = "ActivityGroupChat";
    private Context mContext = null;
    private FragmentManager mFragmentManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIUtil.setThemeForUser(this);
        mContext = this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_group_chat);
        mFragmentManager = getSupportFragmentManager();
        new TagRequestUtil().add2queue();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
//    /** Return to the login (sign in) page */
//    private void returnToLoginPage() {
//        Intent intent = new Intent(mContext, ActivitySignIn.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        mContext.startActivity(intent);
//    }

    /**
     * A RequestUtil for get cloud tag.
     * @author zhanglong
     *
     */
    private class TagRequestUtil extends RequestUtil<CloudTag> {

        @Override
        public Request<CloudTag> onPrepareRequest(
                Listener<CloudTag> listener, ErrorListener errListener) {
            return new RequestGetClassChatTag(listener, errListener);
        }

        @Override
        public void onSuccess(CloudTag response) {
            if (null == response.mCloudTag) {
                Log.e(TAG, "failed to get cloud tag!");
                // It's serious that we can't go to next step
                Toast.makeText(mContext, R.string.chat_group_init_fail, Toast.LENGTH_LONG).show();
                ActivityGroupChat.this.finish();
                return;
            }
            FragmentChat fragment = FragmentChat.newInstance(response.mCloudTag);
            mFragmentManager.beginTransaction().replace(R.id.fragment_group_chat, fragment).commit();
        }

        @Override
        public void onError(VolleyError e) {
            Log.w(TAG, RequestUtil.interpretErr2String(e));
            super.onError(e);
            Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }
}
