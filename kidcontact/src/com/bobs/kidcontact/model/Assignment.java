package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-2-12.
 */
public class Assignment extends NoticeBase {
    @SerializedName("assignmentReplies")
    public ArrayList<AssignmentReply> mAssignmentReplies;

    public static class AssignmentContent {
        @SerializedName("assignmentContent")
        public String mContent;
        @SerializedName("assignmentDueDate")
        public String mDueDate;
        @SerializedName("isNeedAttachment")
        public boolean isAttachmentNeed;
    }
}
