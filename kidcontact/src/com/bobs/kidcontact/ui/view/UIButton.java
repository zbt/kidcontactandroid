package com.bobs.kidcontact.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bobs.kidcontact.R;


/**
 * Created by junyongl on 13-12-26. zhanglong
 */
public class UIButton extends LinearLayout {
    private String mTitle;
    private String mSubtitle;
    private int mImage;
    private LayoutInflater mInflater;
    private LinearLayout mButtonContainer;
    private ClickListener mClickListener;
    private TextView mCornerMark;

    public UIButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setClickable(true);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mButtonContainer = (LinearLayout) mInflater.inflate(R.layout.item_ui_button, null);
        mCornerMark = (TextView) mButtonContainer.findViewById(R.id.ui_btn_corner_mark);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.UIButton, 0, 0);
        mTitle = a.getString(R.styleable.UIButton_bt_title);
        mSubtitle = a.getString(R.styleable.UIButton_bt_subtitle);
        mImage = a.getResourceId(R.styleable.UIButton_bt_image, -1);

        if (mTitle != null) {
            ((TextView) mButtonContainer.findViewById(R.id.ui_button_title)).setText(mTitle.toString());
        } else {
            ((TextView) mButtonContainer.findViewById(R.id.ui_button_title)).setText("Title");
        }

        if (mSubtitle != null) {
            ((TextView) mButtonContainer.findViewById(R.id.ui_button_subtitle)).setText(mSubtitle.toString());
        } else {
            mButtonContainer.findViewById(R.id.ui_button_subtitle).setVisibility(View.GONE);
        }

        if (mImage > -1) {
            ((ImageView) mButtonContainer.findViewById(R.id.ui_button_image)).setImageResource(mImage);
        } else {
            mButtonContainer.findViewById(R.id.ui_button_image).setVisibility(View.GONE);
        }

        mButtonContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onClick(UIButton.this);
                }
            }
        });

        addView(mButtonContainer, params);
    }

    public interface ClickListener{
        void onClick(View view);
    }

    public void addClickListener(ClickListener listener) {
        this.mClickListener = listener;
    }

    public void removeClickListener() {
        this.mClickListener = null;
    }

    public void showCornerMark() {
        mCornerMark.setVisibility(View.VISIBLE);
    }

    public void hideCornerMark() {
        mCornerMark.setVisibility(View.INVISIBLE);
    }
}
