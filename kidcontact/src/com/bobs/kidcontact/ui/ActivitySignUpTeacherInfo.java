package com.bobs.kidcontact.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest;
import com.bobs.kidcontact.requests.RequestBindTeacher;
import com.bobs.kidcontact.requests.RequestBindTeacherWithAvatar;
import com.bobs.kidcontact.ui.teacher.ActivityTeacherMain;
import com.bobs.kidcontact.ui.view.FragmentPictureSelector;
import com.bobs.kidcontact.utils.L;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.UIUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ActivitySignUpTeacherInfo extends FragmentActivityBase implements
        DialogInterface.OnClickListener {
    private static final String TAG = "ActivitySignUpTeacherInfo";
    private EditText mEtTeacherName;
    private EditText mEtTeacherID;
    private Button mBtLogin;
    private LinearLayout mAvatarArea;
    private NetworkRoundImageView mAvatarImg;
    private File cacheAvatarFile;
    private RequestQueue mReqQueue;
    private final Handler mHandler = new Handler();
    private final Response.Listener<CookieGsonRequest.Succeed> mBoundListener = new Response.Listener<CookieGsonRequest.Succeed>() {
        @Override
        public void onResponse(CookieGsonRequest.Succeed response) {
            if (mBitmap != null) {
                mBitmap.recycle();
            }
            saveBoundStatus();
            Intent intent = new Intent(ActivitySignUpTeacherInfo.this, ActivityTeacherMain.class);
            intent.putExtra(ActivityStatus.EXTRA_INFO, "Teacher Role Logged in");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            mHandler.removeCallbacksAndMessages(null);

            L.d(TAG, "Success");
        }
    };
    private final Response.ErrorListener mErrListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            enableInput();
            Toast.makeText(ActivitySignUpTeacherInfo.this, R.string.signin_error, Toast.LENGTH_SHORT).show();
            mHandler.removeCallbacksAndMessages(null);

            L.d(TAG, "" + error.getMessage());
        }
    };
    private Bitmap mBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_teacher_info);

        // hide the unuse button
        ((Button) findViewById(R.id.bt_right)).setVisibility(View.GONE);

        mEtTeacherName = (EditText) findViewById(R.id.et_teacher_name);
        mEtTeacherID = (EditText) findViewById(R.id.et_teacher_id);
        mBtLogin = (Button) findViewById(R.id.bt_left);
        mReqQueue = KidContactApp.getInstance().getRequestQueue();
        mBtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boundTeacher();
            }
        });
        mAvatarArea = (LinearLayout) findViewById(R.id.avatar_container);
        mAvatarArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentPictureSelector fragment = new FragmentPictureSelector();
                fragment.show(getSupportFragmentManager(), "select_picture");
                fragment.setOnClickListener(ActivitySignUpTeacherInfo.this);
            }
        });
        mAvatarImg = (NetworkRoundImageView) findViewById(R.id.user_avatar);
        mAvatarImg.setImageResource(R.drawable.ic_avatar);
        mAvatarImg.setImageUrl(null,
                KidContactApp.getInstance().getImageLoader());
        mAvatarImg.setDefaultImageResId(R.drawable.ic_avatar);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mReqQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableInput();
    }

    private void boundTeacher() {
        RequestBindTeacher.BindTeacher input = new RequestBindTeacher.BindTeacher();
        input.mName = getTeacherName();
        if (input.mName == null) {
            Toast.makeText(this, getString(R.string.teacher_info_err_name), Toast.LENGTH_SHORT).show();
            return;
        }
        input.mTeacherId = getTeacherID();
        if (input.mTeacherId == null) {
            Toast.makeText(this, getString(R.string.teacher_info_err_id), Toast.LENGTH_SHORT).show();
            return;
        }
        if (mBitmap != null) {
            mReqQueue.add(new RequestBindTeacherWithAvatar(input.mTeacherId, input.mName,
                    getAvatarImagePair(), mBoundListener, mErrListener));
        } else {
            mReqQueue.add(new RequestBindTeacher(input, mBoundListener, mErrListener));
        }
        disableInput();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onPause();
                onResume();
                Toast.makeText(ActivitySignUpTeacherInfo.this, R.string.signin_error, Toast.LENGTH_SHORT).show();
                L.d(TAG, "login timeout!");
                // Return to the launch screen page
                Intent intent = new Intent(ActivitySignUpTeacherInfo.this, ActivityLaunchScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                ActivitySignUpTeacherInfo.this.startActivity(intent);
            }
        }, 5000);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	 if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_PICK) {
             Uri photoUri = data.getData();
             if (cacheAvatarFile.length() > 0) {
                 mBitmap = BitmapFactory.decodeFile(cacheAvatarFile.getPath());
                 mAvatarImg.setImageBitmap(mBitmap);
             } else if (photoUri != null) {
                 startActivityForResult(UIUtil.getCropIntent(photoUri), Constants.REQUEST_CROP) ;
             }
         } else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CAPTURE) {
             Uri tempUri = Uri.fromFile(cacheAvatarFile);
             startActivityForResult(UIUtil.getCaptureCropIntent(tempUri), Constants.REQUEST_CROP) ;
         }  else if (resultCode == RESULT_OK && requestCode == Constants.REQUEST_CROP) {
             Bundle extras = data.getExtras();

             if (extras != null) {
                 mBitmap = extras.getParcelable("data");
                 mAvatarImg.setImageBitmap(mBitmap);
                 FileOutputStream fOut = null;
 				try {
 					fOut = new FileOutputStream(cacheAvatarFile);
 				} catch (FileNotFoundException e) {
 					e.printStackTrace();
 				}

                 mBitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                 try {
 					fOut.flush();
 				} catch (IOException e) {
 					e.printStackTrace();
 				}
                 try {
 					fOut.close();
 				} catch (IOException e) {
 					e.printStackTrace();
 				}
             }
         }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void disableInput() {
        mEtTeacherName.setEnabled(false);
        mEtTeacherID.setEnabled(false);
    }


    private void enableInput() {
        mEtTeacherName.setEnabled(true);
        mEtTeacherID.setEnabled(true);
    }

    private String getTeacherName() {
        String teacherName = mEtTeacherName.getText().toString().trim();
        if (teacherName.length() > 0 ){
            return  teacherName;
        }
        return null;
    }

    private String getTeacherID() {
        String teacherId = mEtTeacherID.getText().toString().trim();
        if (teacherId.length() > 0 ){
            return  teacherId;
        }
        return null;
    }

    private ArrayList<CookieMuiltiPicUploadRequest.PicPair> getAvatarImagePair() {
        ArrayList<CookieMuiltiPicUploadRequest.PicPair> pics = new ArrayList<CookieMuiltiPicUploadRequest.PicPair>();
        File cacheAvatarThumb = null;
        Bitmap t = ThumbnailUtils.extractThumbnail(mBitmap, 64, 64);
        FileOutputStream fos;
        try {
            cacheAvatarThumb = UIUtil.getAvatarImageTempFile("avatar_thumb");
            fos = new FileOutputStream(cacheAvatarThumb);
            t.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            t.recycle();
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if ( cacheAvatarThumb != null && cacheAvatarFile != null){
            CookieMuiltiPicUploadRequest.PicPair pair = new CookieMuiltiPicUploadRequest.PicPair();
            pair.mPic = cacheAvatarFile;
            pair.mThumb = cacheAvatarFile;
            pics.add(pair);
            return pics;
        } else {
            return null;
        }
    }

    private void saveBoundStatus() {
        KidContactApp.getSharePrefUtil().setTeacherName(getTeacherName());
        KidContactApp.getSharePrefUtil().setTeacherID(getTeacherID());
        KidContactApp.getSharePrefUtil().setSaveBoundState(true);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        cacheAvatarFile = UIUtil.getAvatarImageTempFile("cacheAvatar");
        switch (which) {
            //index of "picture_picker_array"
            case 0:
                //Take picture via camera
                Intent intent_capture = UIUtil.getCaptureImageIntent(cacheAvatarFile);
                if (intent_capture != null) {
                    startActivityForResult(intent_capture, Constants.REQUEST_CAPTURE);
                }
                break;
            case 1:
                //Select picture from gallery
                Intent intent = UIUtil.getUserImageIntent(cacheAvatarFile);
                if (intent != null) {
                    startActivityForResult(intent, Constants.REQUEST_PICK);
                }
                break;
            default:
                break;
        }
    }
}
