package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-2-23.
 */
public class TeacherList {
    @SerializedName("teachers")
    public ArrayList<Teacher> mTeachers;
}
