package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-11.
 */
public abstract class RequestAddInfoId<T> extends CookieGsonJsonRequest<T> {
    public RequestAddInfoId(String url, InfoId input, Class<T> objectClass,
                            Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(url, input, objectClass, listener, errorListener);
    }

    public static class InfoId {
        @SerializedName("info")
        public String mInfo;
        @SerializedName("id")
        public long mId;
    }
}