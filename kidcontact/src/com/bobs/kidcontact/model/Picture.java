package com.bobs.kidcontact.model;

import android.util.Log;

import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-1-6.
 */

/**
 * Picture is like below
 * pic: {
    id: 2
    picContentType: "image/png"
    picSize: 21316
    thumbContentType: null
    thumbSize: 0
 }
 */
public class Picture {
    public static final String KEY_PIC_ID = "picId";
    public static final String KEY_THUMB = "thumbnail";
    private static final String TAG = "Picture";

    @SerializedName("id")
    public int mId;
    @SerializedName("picContentType")
    public String mContentType;
    @SerializedName("picSize")
    public int picSize;
    @SerializedName("thumbContentType")
    public String mThumbContentType;
    @SerializedName("thumbSize")
    public int mThumbSize;
    @SerializedName("url")
    public String mUrl;

    transient public String picUrl;


    public String getPicURL() {
        String getPicPath = Constants.getFunctionURL(Constants.FUNC_GETPIC);
        String ret = getPicPath + Constants.QUESTION
                + KEY_PIC_ID + Constants.EQUAL + mId
                + Constants.AND
                + KEY_THUMB + Constants.EQUAL + "false";
        Log.d(TAG, "get the picURL" + ret + mContentType + mThumbContentType + picSize);
        return ret;
    }

    public String getPicThumbURL() {

//        return "http://d.hiphotos.baidu.com/image/w%3D2048/sign=704211bda41ea8d38a227304a332314e/1ad5ad6eddc451dac273aaedb4fd5266d016322a.jpg";

        String getPicPath = Constants.getFunctionURL(Constants.FUNC_GETPIC);
        String ret = getPicPath + Constants.QUESTION
                + KEY_PIC_ID + Constants.EQUAL + mId
                + Constants.AND
                + KEY_THUMB + Constants.EQUAL + "true";
        Log.d(TAG, "get the thumbURL" + ret+ mContentType + mThumbContentType + picSize);
        return ret;
    }
}
