package com.bobs.kidcontact.ui.teacher;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.Child;
import com.bobs.kidcontact.model.Comment;
import com.bobs.kidcontact.model.CommentRecord;
import com.bobs.kidcontact.model.CommentRecordList;
import com.bobs.kidcontact.model.IdProxy;
import com.bobs.kidcontact.model.Record;
import com.bobs.kidcontact.model.Reply;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestAddCommentReply;
import com.bobs.kidcontact.requests.RequestAddRecordReply;
import com.bobs.kidcontact.requests.RequestDeleteComment;
import com.bobs.kidcontact.requests.RequestDeleteMoment;
import com.bobs.kidcontact.requests.RequestDeleteRecord;
import com.bobs.kidcontact.requests.RequestTeacherGetCommentRecord;
import com.bobs.kidcontact.ui.FragmentAddReply;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.ui.UpdateKidDetailUIEvent;
import com.bobs.kidcontact.ui.view.CommentView;
import com.bobs.kidcontact.ui.view.MomentPicturesView;
import com.bobs.kidcontact.ui.view.RecordView;
import com.bobs.kidcontact.ui.view.ReviewView;
import com.bobs.kidcontact.ui.view.UndoBarController;
import com.bobs.kidcontact.utils.DebugLog;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by junyongl on 14-1-20.
 */
public class FragmentChildDetails extends Fragment implements FragmentAddReply.OnReplyPublishedListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "FragmentChildDetails";
    private static final int MSG_DELETE_COMMENT = 1;
    private static final int MSG_DELETE_RECORD = 2;
    private Context mContext;
    private RecordAdapter mAdapter;
    private ArrayList<CommentRecord> mCommentRecords = new ArrayList<CommentRecord>();
    private Child mChild;
    private Request<?> mInFlightRequest;
    private RequestQueue mReqQueue;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private long mLeastId;
    private final Response.Listener<CommentRecordList> mGetRCsListener = new Response.Listener<CommentRecordList>() {
        @Override
        public void onResponse(CommentRecordList response) {
            mInFlightRequest = null;
            mSwipeRefreshLayout.setRefreshing(false);
            if (response == null || response.mCommentRecords.size() == 0) return;
            if (mLeastId == 0) {
                mCommentRecords.clear();
            }
            ArrayList<CommentRecord> crs = response.mCommentRecords;
            if (crs != null && crs.size() > 0) {
                mLeastId = crs.get(crs.size() -1).getId();
                Log.d(TAG, "received CommentRecords:");
                Log.d(TAG, "\t num of Comment Record = "+ crs.size());
                fillCommentRecordArray(response.mCommentRecords);
            } else {
                return;
            }
        }
    };
    private int mCurrentReplyPosition;

    private void fillCommentRecordArray(ArrayList<CommentRecord> commentRecords) {
        mCommentRecords.addAll(commentRecords);
        mAdapter.notifyDataSetChanged();
    }

    private final Response.ErrorListener mGetRCsErrListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            mSwipeRefreshLayout.setRefreshing(false);
            Log.e(TAG, error.toString());
        }
    };
    private boolean isConfirmDelete = true;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (isConfirmDelete) {
                switch (msg.arg2) {
                    case MSG_DELETE_COMMENT:
                        deleteComment(msg.arg1);
                        break;
                    case MSG_DELETE_RECORD:
                        deleteRecord(msg.arg1);
                        break;
                    default:
                        break;
                }

            }
        }
    };

    private void deleteRecord(final long id) {
        RequestUtil<CookieGsonRequest.Succeed> requestUtil = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                IdProxy idProxy = new IdProxy();
                idProxy.mId = id;
                return new RequestDeleteRecord(idProxy, listener, errListener);
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {

            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
            }
        };
        requestUtil.add2queue();
    }

    private void deleteComment(final long id) {
        RequestUtil<CookieGsonRequest.Succeed> requestUtil = new RequestUtil<CookieGsonRequest.Succeed>() {
            @Override
            public Request<CookieGsonRequest.Succeed> onPrepareRequest(Response.Listener<CookieGsonRequest.Succeed> listener, Response.ErrorListener errListener) {
                IdProxy idProxy = new IdProxy();
                idProxy.mId = id;
                return new RequestDeleteComment(idProxy, listener, errListener);
            }

            @Override
            public void onSuccess(CookieGsonRequest.Succeed response) {

            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
            }
        };
        requestUtil.add2queue();
    }

    private int mDeletePosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mReqQueue = KidContactApp.getInstance().getRequestQueue();
        mChild = new Gson().fromJson(getArguments().getString("child"), Child.class);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_kid, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorScheme(R.color.progress_scheme1, R.color.progress_scheme2,
                R.color.progress_scheme3, R.color.progress_scheme4);
        ListView listView = (ListView) view.findViewById(R.id.list_kid_record);
        View headView = (View) inflater.inflate(R.layout.header_kid_detail, null);
        if (mChild.mPics.size() != 0 && mChild.mPics.get(0).mPicture.picSize != 0) {
            ((NetworkRoundImageView)headView.findViewById(R.id.child_avatar)).setImageUrl(mChild.mPics.get(0).mPicture.getPicURL(),
                    KidContactApp.getInstance().getImageLoader());
        }
        ((TextView)headView.findViewById(R.id.child_name)).setText(mChild.mName != null ? mChild.mName:"");
        listView.addHeaderView(headView);
        listView.setDivider(mContext.getResources().getDrawable(R.drawable.divider));

        mAdapter = new RecordAdapter();
        mAdapter.loadNextPage();
        listView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void OnReplyPublished(Reply reply) {
        if (mAdapter.getItemViewType(mCurrentReplyPosition) == 0) {
            mCommentRecords.get(mCurrentReplyPosition).mRecord.getReplies().add(reply);
        } else {
            mCommentRecords.get(mCurrentReplyPosition).mComment.getReplies().add(reply);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        DebugLog.d("SwipeRefreshLayout refreshed");
        mLeastId = 0;
        mAdapter.loadNextPage();
        mAdapter.notifyDataSetChanged();
    }

    private class RecordAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mCommentRecords.size();
        }

        @Override
        public Object getItem(int position) {
            return mCommentRecords.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            if (((CommentRecord)getItem(position)).mRecord != null) {
                // It is Record
                return 0;
            } else {
                //It is Comment
                return 1;
            }
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (getItemViewType(position) == 0) {
                RecordViewHolder recordvh = new RecordViewHolder();
                final Record record = ((CommentRecord) getItem(position)).mRecord;
//                if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_kid_record, parent, false);
                recordvh.recordView = (RecordView) convertView.findViewById(R.id.moment_view);
                recordvh.addReply = (ImageView) convertView.findViewById(R.id.add_comment);
                recordvh.picturesView = (MomentPicturesView) convertView.findViewById(R.id.moment_pictures);
                recordvh.addLike = (TextView) convertView.findViewById(R.id.add_favor);
                recordvh.delete = (TextView) convertView.findViewById(R.id.delete);
                recordvh.addLike.setVisibility(View.GONE);
                recordvh.recordView.bind(record);
                ArrayList<Reply> replies = record.getReplies();
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                final float scale = mContext.getResources().getDisplayMetrics().density;
                int marginPixels = (int) (8 * scale + 0.5f);
                params.setMargins(marginPixels, 0, marginPixels, marginPixels);
                if (replies.size() > 0) {
//                    for (int i = 0; i < replies.size(); i++) {
                        CommentView commentView = new CommentView(mContext, null);
                        commentView.bind(replies);
                        ((LinearLayout)convertView).addView(commentView, params);
//                    }
                }
                recordvh.picturesView.setPictures(record.getPictures());
                convertView.setTag(recordvh);
//                } else {
//                    recordvh = (RecordViewHolder)convertView.getTag();
//                }

                recordvh.addReply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentAddReply fragment = (FragmentAddReply)fragmentManager.findFragmentByTag("reply");
                        if (fragment == null) {
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragment = new FragmentAddReply();
                            fragmentTransaction.replace(R.id.kid_comment_bar, fragment, "reply");
                            fragmentTransaction.addToBackStack("reply");
                            fragmentTransaction.commit();
                        }
                        fragment.setReplyRequest(record.mId, RequestAddRecordReply.class);
                        fragment.setReplyPublishListener(FragmentChildDetails.this);
                        mCurrentReplyPosition = position;
                    }
                });
                if (record.mCreatorId == KidContactApp.getInstance().getId()) {
                    recordvh.delete.setVisibility(View.VISIBLE);
                    recordvh.delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new UndoBarController.UndoBar((Activity)mContext)
                                    .message("删除记录")
                                    .listener(new UndoBarController.UndoListener() {
                                        @Override
                                        public void onUndo(Parcelable parcelable) {
                                            isConfirmDelete = false;
                                            CommentRecord commentRecord = new CommentRecord();
                                            commentRecord.mRecord = (Record)parcelable;
                                            mCommentRecords.add(position, commentRecord);
                                            mAdapter.notifyDataSetChanged();
                                        }
                                    })
                                    .token(record)
                                    .show();
                            Message msg = new Message();
                            msg.arg1 = record.mId;
                            msg.arg2 = MSG_DELETE_RECORD;
                            mDeletePosition = position;
                            mCommentRecords.remove(position);
                            mAdapter.notifyDataSetChanged();
                            mHandler.sendMessageDelayed(msg, 6000);
                        }
                    });
                }
            } else  {
                CommentViewHolder commentvh = new CommentViewHolder();
                final Comment comment = ((CommentRecord) getItem(position)).mComment;
//                if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_kid_review, parent, false);

                commentvh.reviewView = (ReviewView) convertView.findViewById(R.id.kid_review_view);
                commentvh.addReply = (ImageView) convertView.findViewById(R.id.add_comment);
                commentvh.delete = (TextView) convertView.findViewById(R.id.delete);

                commentvh.reviewView.bind(comment);
                ArrayList<Reply> replies = comment.getReplies();
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                final float scale = mContext.getResources().getDisplayMetrics().density;
                int marginPixels = (int) (8 * scale + 0.5f);
                params.setMargins(marginPixels, 0, marginPixels, marginPixels);
                if (replies.size() > 0) {
//                    for (int i = 0; i < replies.size(); i++) {
                        CommentView commentView = new CommentView(mContext, null);
                        commentView.bind(replies);
                        ((LinearLayout)convertView).addView(commentView, params);
//                    }
                }
                convertView.setTag(commentvh);
//                } else {
//                    commentvh = (CommentViewHolder)convertView.getTag();
//                }
                commentvh.addReply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentAddReply fragment = (FragmentAddReply)fragmentManager.findFragmentByTag("reply");
                        if (fragment == null) {
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragment = new FragmentAddReply();
                            fragmentTransaction.replace(R.id.kid_comment_bar, fragment, "reply");
                            fragmentTransaction.addToBackStack("reply");
                            fragmentTransaction.commit();
                        }
                        fragment.setReplyRequest(comment.mId, RequestAddCommentReply.class);
                        fragment.setReplyPublishListener(FragmentChildDetails.this);
                        mCurrentReplyPosition = position;
                    }
                });
                if (comment.mCreatorId == KidContactApp.getInstance().getId()) {
                    commentvh.delete.setVisibility(View.VISIBLE);
                    commentvh.delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new UndoBarController.UndoBar((Activity)mContext)
                                    .message("删除点评")
                                    .listener(new UndoBarController.UndoListener() {
                                        @Override
                                        public void onUndo(Parcelable parcelable) {
                                            isConfirmDelete = false;
                                            CommentRecord commentRecord = new CommentRecord();
                                            commentRecord.mComment = (Comment)parcelable;
                                            mCommentRecords.add(position, commentRecord);
                                            mAdapter.notifyDataSetChanged();
                                        }
                                    })
                                    .token(comment)
                                    .show();
                            Message msg = new Message();
                            msg.arg1 = comment.mId;
                            msg.arg2 = MSG_DELETE_COMMENT;
                            mDeletePosition = position;
                            mCommentRecords.remove(position);
                            mAdapter.notifyDataSetChanged();
                            mHandler.sendMessageDelayed(msg, 6000);
                        }
                    });
                }

            }
            if (position > (getCount() - 2)) {
                Log.d(TAG, "At the end :" + position + " ------ Load the next page." + getCount());
                loadNextPage();
            }
            return convertView;
        }

        private final class RecordViewHolder {
            RecordView recordView;
            ImageView addReply;
            MomentPicturesView picturesView;
            TextView delete;
            TextView addLike;

        }

        private final class CommentViewHolder {
            ReviewView reviewView;
            ImageView addReply;
            TextView delete;

        }

        public void loadNextPage() {
            if (mInFlightRequest != null) {
                return;
            }
            mInFlightRequest = mReqQueue.add(new RequestTeacherGetCommentRecord(mLeastId, mChild.mId,
                    mGetRCsListener, mGetRCsErrListener));
        }
    }

    public void onEvent(UpdateKidDetailUIEvent event) {
        mLeastId = 0;
        mAdapter.loadNextPage();
    }
}
