package com.bobs.kidcontact.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.Assignment;
import com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest;
import com.bobs.kidcontact.requests.RequestAddAssignment;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.greenrobot.event.EventBus;

/**
 * Created by junyongl on 14-1-10.
 */
public class FragmentPublishAssignment extends Fragment implements DatePickerDialog.OnDateSetListener, ActivityPublish.IPublish {
    private static final String DATEPICKER_TAG = "due_date_picker";
    private Context mContext;
    private EditText mEtTitle;
    private EditText mEtContent;
    private CheckBox mIfUploadNeed;
    private TextView mTvDueDateText;
    private String mInfo;
    private String mTitle;
    private ArrayList<CookieMuiltiPicUploadRequest.PicPair> mPicpairs = new ArrayList<CookieMuiltiPicUploadRequest.PicPair>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        View view = inflater.inflate(R.layout.fragment_publish_assignment, container, false);
        mEtTitle = (EditText) view.findViewById(R.id.et_assignment_title);
        mEtContent = (EditText) view.findViewById(R.id.et_assignment_content);
        mIfUploadNeed = (CheckBox) view.findViewById(R.id.check_upload_link);
        mTvDueDateText = (TextView) view.findViewById(R.id.tv_due_date);
        final com.fourmob.datetimepicker.date.DatePickerDialog mDatePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);
        mTvDueDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatePickerDialog.setYearRange(2014, 2028);
                mDatePickerDialog.show(getFragmentManager(), DATEPICKER_TAG);
            }
        });
        return view;
    }


    @Override
    public void publish() {
        if (prepareContent()) {
            publishAssignment();
        }
    }

    private boolean prepareContent() {
        if (getTitle() == null) {
            Toast.makeText(mContext, R.string.assign_input_title,Toast.LENGTH_LONG).show();
            return false;
        } else {
            mTitle = getTitle();
        }
        Assignment.AssignmentContent assignmentContent = new Assignment.AssignmentContent();
        if (getContent() == null) {
            Toast.makeText(mContext, R.string.assign_input_content,Toast.LENGTH_LONG).show();
            return false;
        } else {
            assignmentContent.mContent = getContent();
        }
        assignmentContent.mDueDate = mTvDueDateText.getText().toString();
        assignmentContent.isAttachmentNeed = mIfUploadNeed.isChecked();
        mInfo = new Gson().toJson(assignmentContent, Assignment.AssignmentContent.class);
        return true;
    }

    private String getContent() {
        String content = mEtContent.getText().toString().trim();
        if (content.length() > 0) {
            return content;
        }
        return null;
    }

    private String getTitle() {
        String title = mEtTitle.getText().toString().trim();
        if (title.length() > 0) {
            return title;
        }
        return null;
    }

    private void publishAssignment() {
        RequestUtil<Assignment> request = new RequestUtil<Assignment>() {
            @Override
            public Request<Assignment> onPrepareRequest(Response.Listener<Assignment> listener, Response.ErrorListener errListener) {
                return new RequestAddAssignment(mInfo, mTitle, mPicpairs, listener, errListener);
            }

            @Override
            public void onSuccess(Assignment response) {
                getActivity().finish();
                Toast.makeText(mContext, "Assignment publish success!", Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new UpdateStatusUIEvent());
            }

            @Override
            public void onError(VolleyError e) {
                Toast.makeText(mContext, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
                super.onError(e);
            }
        };
        request.add2queue();
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        SimpleDateFormat dateFormat = new SimpleDateFormat(mContext.getString(R.string.assignment_due_data_format));
        mTvDueDateText.setText(dateFormat.format(calendar.getTime()));
    }
}
