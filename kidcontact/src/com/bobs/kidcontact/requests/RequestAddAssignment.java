package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Assignment;
import com.bobs.kidcontact.utils.UIUtil;

import org.apache.http.entity.mime.content.StringBody;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-2-12.
 */

/**
 * The moment is submitted with multipart-form request which contain following parts
 * MUST: "info":"xxxxxx"
 * MUST: "title":"xxxxxx"
 * OPTIONAL: picture related
 */
public final class RequestAddAssignment extends RequestAddMomentRecord<Assignment> {
    public RequestAddAssignment(String info, String title,
                                ArrayList<PicPair> picPairs,
                                Response.Listener<Assignment> listener, Response.ErrorListener errorListener) {
        super(getAddAssignmentURL(), info, null,
                picPairs, Assignment.class, listener, errorListener);
        buildTitleInfo(UIUtil.encoder(title));
    }

    @SuppressWarnings("deprecation")
    private void buildTitleInfo(String title) {
        try {
            entity.addPart("title", new StringBody(title));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private static String getAddAssignmentURL() {
        return Constants.getFunctionURL(Constants.FUNC_ADD_ASSIGNMENT);
    }
}
