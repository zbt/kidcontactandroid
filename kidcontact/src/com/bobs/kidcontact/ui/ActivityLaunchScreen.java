package com.bobs.kidcontact.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.baidu.android.pushservice.PushManager;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.service.BaiduInfoHolder;
import com.bobs.kidcontact.chat.service.HeartbeatExecutor;
import com.bobs.kidcontact.chat.service.Utils;
import com.bobs.kidcontact.model.CloudTag;
import com.bobs.kidcontact.requests.CookieGsonRequest;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus;
import com.bobs.kidcontact.requests.RequestGetClassChatTag;
import com.bobs.kidcontact.requests.RequestCheckBoundStatus.BoundStatus;
import com.bobs.kidcontact.ui.KidContactApp.AfterBackGroundLogin;
import com.bobs.kidcontact.ui.teacher.ActivityTeacherMain;
import com.bobs.kidcontact.utils.DebugLog;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;

/**
 * Created by junyongl on 14-3-14. zhanglong
 */
public class ActivityLaunchScreen extends Activity {
    private static final String TAG = "ActivityLaunchScreen";
    private BoundStatus mCachedResponse = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);
        ImageView avatarImg = (ImageView) findViewById(R.id.avatar);
        Bitmap bitmap = UIUtil.loadImageFromStorage(KidContactApp.getSharePrefUtil().getAvatarUrl());
        DebugLog.d("get the bitmap url " + KidContactApp.getSharePrefUtil().getAvatarUrl());
        if (bitmap != null) {
            DebugLog.d("get the bitmap " + bitmap.toString());
            avatarImg.setImageBitmap(bitmap);
        }
        if (isCookiesSaved()) {
            checkBoundStatus();
        } else {
            if (KidContactApp.getSharePrefUtil().isFirstUse()) {
                Intent intent = new Intent(this, ActivityIntro.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, ActivitySignIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }

        }
    }

    private void checkBoundStatus() {
        RequestUtil<BoundStatus> checkBoundStatus = new RequestUtil<BoundStatus>() {
            @Override
            public Request<BoundStatus> onPrepareRequest(Response.Listener<BoundStatus> listener,
                                                         Response.ErrorListener errListener) {
                return new RequestCheckBoundStatus(new CookieGsonRequest.EmptyInput(), listener, errListener);
            }

            @Override
            public void onSuccess(BoundStatus response) {
                Log.i(TAG, "onSucceed");
                KidContactApp.getInstance().saveBoundStatus(response);
                mCachedResponse = response;
                HeartbeatExecutor.getInstance().execute(); // restore heartbeat
                afterLoginResponse();
            }

            @Override
            public void onError(VolleyError e) {
                Log.i(TAG, "onError");
                super.onError(e, mMyAfterBackGroundLogin);
            }
        };
        checkBoundStatus.add2queue();
    }

    private boolean isCookiesSaved() {
        if (KidContactApp.getSharePrefUtil().getJsessionCookie() == null) {
            return false;
        }
        if (KidContactApp.getSharePrefUtil().getRememberMeCookie() == null) {
            return false;
        }
        return true;
    }

    /** Do something after login succeed */
    private void onLoginSucceed(BoundStatus boundStatus) {
        if (boundStatus.isBounded()) {
            if (boundStatus.isTeacher()) {
                Intent intent = new Intent(this, ActivityTeacherMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else if (boundStatus.isParent()) {
                Intent intent = new Intent(this, ActivityParentMain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            } else {
                throw new RuntimeException("any other role?");
            }
        } else {
            if (boundStatus.isParent()) {
                Intent intent = new Intent(this, ActivitySignUpKidInfo.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP  );
                startActivity(intent);
            } else if (boundStatus.isTeacher()) {
                Intent intent = new Intent(this, ActivitySignUpTeacherInfo.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }
        this.sendBroadcast(new Intent(Utils.ACTION_AUTO_LOGIN)); // Will be received and handled in chat service
    }

    /** Do something after the login response */
    private void afterLoginResponse() {
        new TagRequestUtil().add2queue();
    }
    
    /**
     * A RequestUtil for get cloud tag (group id).
     * @author zhanglong
     *
     */
    private class TagRequestUtil extends RequestUtil<CloudTag> {

        @Override
        public Request<CloudTag> onPrepareRequest(
                Listener<CloudTag> listener, ErrorListener errListener) {
            return new RequestGetClassChatTag(listener, errListener);
        }

        @Override
        public void onSuccess(CloudTag response) {
            if (null == response.mCloudTag || response.equals("")) {
                Log.e(TAG, "failed to get cloud tag!");
                // It's serious that we maybe unable to do class-chat，but
                // anyway we should move on.
                Toast.makeText(ActivityLaunchScreen.this, R.string.class_get_info_fail, Toast.LENGTH_LONG).show();
            }
            BaiduInfoHolder.setGroupTag(response.mCloudTag);
            Log.d(TAG, "set cloud tag: " + response.mCloudTag);

            // Temporary solution for joining class chat, not perfect
            ArrayList<String> groups = new ArrayList<String>();
            groups.add(response.mCloudTag);
            PushManager.setTags(ActivityLaunchScreen.this, groups);
            onLoginSucceed(mCachedResponse);
        }

        @Override
        public void onError(VolleyError e) {
            super.onError(e);
            Log.w(TAG, RequestUtil.interpretErr2String(e));
            Toast.makeText(ActivityLaunchScreen.this, RequestUtil.interpretErr2Res(e), Toast.LENGTH_SHORT).show();
            // It's serious that we maybe unable to do class-chat，but
            // anyway we should move on.
            onLoginSucceed(mCachedResponse);
        }

    }

    private class MyAfterBackGroundLogin extends AfterBackGroundLogin {

        @Override
        public void doAfterBackGroundLogin(BoundStatus bs) {
            onLoginSucceed(bs);
        }
    }
    private MyAfterBackGroundLogin mMyAfterBackGroundLogin = new MyAfterBackGroundLogin();


}
