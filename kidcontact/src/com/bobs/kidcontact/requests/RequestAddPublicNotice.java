package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.PublicNotice;

/**
 * Created by hzheng3 on 14-1-11.
 */
public final class RequestAddPublicNotice extends RequestAddInfoTitle<PublicNotice> {
    public RequestAddPublicNotice(InfoTitle input, Response.Listener<PublicNotice> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ADD_PUBLIC_NOTICE),
                input, PublicNotice.class, listener, errorListener);
    }
}
