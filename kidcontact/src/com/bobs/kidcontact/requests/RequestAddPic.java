package com.bobs.kidcontact.requests;

import com.android.volley.Response;


/**
 * Created by hzheng3 on 14-6-9.
 */


public abstract class RequestAddPic<T> extends CookieMuiltiPicUploadRequest<T> {

    public RequestAddPic(String url, PicPair input, Class<T> objectClass, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(url, input, objectClass, listener, errorListener);
    }
}
