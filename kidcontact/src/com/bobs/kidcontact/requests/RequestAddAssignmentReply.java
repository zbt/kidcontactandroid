package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.AssignmentReply;

import org.apache.http.entity.mime.content.StringBody;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-2-12.
 */

/**
 * The moment is submitted with multipart-form request which contain following parts
 * MUST: "info":"xxxxxx"
 * MUST: "assignmentId":"xxxxxx"
 * OPTIONAL: picture related
 */
public final class RequestAddAssignmentReply extends RequestAddMomentRecord<AssignmentReply> {
    public RequestAddAssignmentReply(String info, long assignmentId,
                                     ArrayList<PicPair> picPairs,
                                     Response.Listener<AssignmentReply> listener, Response.ErrorListener errorListener) {
        super(getAddAssignmentReplyURL(), info, null,
                picPairs, AssignmentReply.class, listener, errorListener);
        buildIdInfo(assignmentId);
    }

    @SuppressWarnings("deprecation")
    private void buildIdInfo(long title) {
        try {
            entity.addPart("assignmentId", new StringBody(String.valueOf(title)));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private static String getAddAssignmentReplyURL() {
        return Constants.getFunctionURL(Constants.FUNC_ADD_ASSIGNMENTREPLY);
    }
}
