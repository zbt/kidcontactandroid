package com.bobs.kidcontact.chat.service;

import java.util.Arrays;
import java.util.List;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.KCPushMessageReceiver;
import com.bobs.kidcontact.chat.engine.BaiduPushEngine;
import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;
import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.ui.KidContactApp;

/**
 * Created by hzheng3 on 14-2-18. Maintained by zhanglong.
 */
public class ChatService extends Service {

    private static final int MSG_ON_CHAT_ENGINE_CONNECTED_SUCCEEDED = 1;
    private static final int MSG_ON_CHAT_ENGINE_CONNECTED_FAILED = 2;
    private static final int MSG_ON_CHAT_ENGINE_DISCONNECTED_SUCCEEDED = 3;
    private static final int MSG_ON_CHAT_ENGINE_DISCONNECTED_FAILED = 4;
    private static final int MSG_ON_GROUP_LIST_SUCCEEDED = 5;
    private static final int MSG_ON_GROUP_LIST_FAILED = 6;
    private static final int MSG_ON_GROUP_JOIN_SUCCEEDED = 7;
    private static final int MSG_ON_GROUP_JOIN_FAILED = 8;
    private static final int MSG_ON_GROUP_LEAVE_SUCCEEDED = 9;
    private static final int MSG_ON_GROUP_LEAVE_FAILED = 10;
    private static final int MSG_ON_MESSAGE = 11;
    //private static final int MSG_SEND_MESSAGE_TO_USER = 12;
    //private static final int MSG_SEND_MESSAGE_TO_GROUP = 13;
    private static final int MSG_SEND_CLOUD_MESSAGE = 14;
    private static final int MSG_JOIN_GROUPS = 15;
    private static final int MSG_LEAVE_GROUPS = 16;

    private static final String KEY_ID = "key_id";
    private static final String KEY_REQUEST_ID = "key_request_id";
    private static final String KEY_GROUPS = "key_groups";
    private static final String KEY_MESSAGE = "key_message";
    //private static final String KEY_GROUP_ID = "key_group_id";
    private static final String KEY_CLOUD_MESSAGE = "key_cloud_message";
    private static final String KEY_CHAT_MESSAGE = "key_chat_message";
    private static final String TAG = "ChatService";


    //IPushEngine mIPushEngine;
    IChatEngine mIChatEngine;
    ChatEngineCallback mChatEngineCallback;
    CloudMessageManager mCloudMessageManager;

    Context mContext;
    ChatEngineHandlerThread mChatEngineHandlerThread;
    Handler mHandler;
    KCPushMessageReceiver.EventHandler mBaiduEventHandler;


    IChatService.Stub mChatServiceStub = null;
    private Boolean mIsSwitchingContext = false;
    private HeartbeatExecutor mHeartbeatExecutor;
    private AutoLoginReceiver mAutoLoginReceiver;

    @Override
    public void onCreate() {
        super.onCreate();
        doInitialize();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        doClear();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null != mChatServiceStub ? mChatServiceStub : createNewIBinder();
    }

    /** Do initialization for this service */
    private void doInitialize() {
        mContext = getApplicationContext();

        mCloudMessageManager = CloudMessageManager.getInstance(mContext,
                new BaiduPushEngine(mContext,
                        KidContactApp.API_KEY,
                        KidContactApp.SECRIT_KEY));
        mChatEngineCallback = ChatEngineCallback.getInstance(ChatEngine.getInstance(mCloudMessageManager));

        mChatEngineHandlerThread = new ChatEngineHandlerThread();
        mChatEngineHandlerThread.start();
        mHandler = new Handler(mChatEngineHandlerThread.getLooper(), mChatEngineCallback);
        mChatEngineHandlerThread.setHandler(mHandler);
        mIChatEngine = mChatEngineHandlerThread;

        mBaiduEventHandler = new BaiduPushMsgEventHandler(mIChatEngine);
        KCPushMessageReceiver.registerEventHandler(mBaiduEventHandler);
        Log.d(TAG, "onCreate");

        PushManager.startWork(getApplicationContext(),
                PushConstants.LOGIN_TYPE_API_KEY,
                KidContactApp.API_KEY);
        mHeartbeatExecutor = HeartbeatExecutor.getInstance(); // just hold it
        registerAutoLoginReceiver();
    }

    /** Do some clearing for this service */
    private void doClear() {
        unregisterAutoLoginReceiver();
        mHeartbeatExecutor = null;
        PushManager.stopWork(mContext);
        KCPushMessageReceiver.unRegisterEventHandler();
        mChatEngineHandlerThread.quit();
        mChatEngineHandlerThread = null;
        mChatEngineCallback.release();
        mCloudMessageManager.release();
        mCloudMessageManager = null;
    }

    class ChatEngineHandlerThread extends HandlerThread implements IChatEngine {
        Handler mHandler;

        public ChatEngineHandlerThread() {
            super("chatEngineHandlerThread");
        }

        public void setHandler(Handler handler) {
            mHandler = handler;
        }

        @Override
        public void onChatEngineConnectSucceeded(String id, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_CHAT_ENGINE_CONNECTED_SUCCEEDED);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_ID, id);
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onChatEngineConnectFailed(String id, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_CHAT_ENGINE_CONNECTED_FAILED);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_ID, id);
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onChatEngineDisconnectSucceeded(String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_CHAT_ENGINE_DISCONNECTED_SUCCEEDED);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onChatEngineDisconnectFailed(String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_CHAT_ENGINE_DISCONNECTED_FAILED);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onGroupListSucceeded(List<String> groups, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_GROUP_LIST_SUCCEEDED);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, groups.toArray(new String[groups.size()]));
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onGroupListFailed(List<String> groups, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_GROUP_LIST_FAILED);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, groups.toArray(new String[groups.size()]));
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onGroupJoinSucceeded(List<String> groups, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_GROUP_JOIN_SUCCEEDED);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, groups.toArray(new String[groups.size()]));
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onGroupJoinFailed(List<String> groups, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_GROUP_JOIN_FAILED);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, groups.toArray(new String[groups.size()]));
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onGroupLeaveSucceeded(List<String> groups, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_GROUP_LEAVE_SUCCEEDED);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, groups.toArray(new String[groups.size()]));
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onGroupLeaveFailed(List<String> groups, String requestId) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_ON_GROUP_LEAVE_FAILED);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, groups.toArray(new String[groups.size()]));
                bundle.putString(KEY_REQUEST_ID, requestId);
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void onMessage(String msg) {
            if (mHandler != null) {
                Message msg1 = mHandler.obtainMessage(MSG_ON_MESSAGE);
                Bundle bundle = new Bundle();
                bundle.putString(KEY_MESSAGE, msg);
                msg1.setData(bundle);
                mHandler.sendMessage(msg1);
            }
        }

        @Override
        public void sendCloudMessage(CloudMessage cloudMessage, ChatMessage chatMessage) {
            if (mHandler != null) {
                Message msg1 = mHandler.obtainMessage(MSG_SEND_CLOUD_MESSAGE);
                Bundle bundle = new Bundle();
                bundle.putParcelable(KEY_CLOUD_MESSAGE, cloudMessage);
                bundle.putParcelable(KEY_CHAT_MESSAGE, chatMessage);
                msg1.setData(bundle);
                mHandler.sendMessage(msg1);
            }
        }

//        @Override
//        public boolean sendMsgToUser(long userId, String msg) {
//            if (mHandler != null) {
//                Message msg1 = mHandler.obtainMessage(MSG_SEND_MESSAGE_TO_USER);
//                Bundle bundle = new Bundle();
//                bundle.putLong(KEY_ID, userId);
//                bundle.putString(KEY_MESSAGE, msg);
//                msg1.setData(bundle);
//                mHandler.sendMessage(msg1);
//                return true;
//            }
//            return false;
//        }
//
//        @Override
//        public boolean sendMsgToGroup(String groupId, String msg) {
//            if (mHandler != null) {
//                Message msg1 = mHandler.obtainMessage(MSG_SEND_MESSAGE_TO_GROUP);
//                Bundle bundle = new Bundle();
//                bundle.putString(KEY_GROUP_ID, groupId);
//                bundle.putString(KEY_MESSAGE, msg);
//                msg1.setData(bundle);
//                mHandler.sendMessage(msg1);
//                return true;
//            }
//            return false;
//        }
//
        @Override
        public void joinGroups(List<String> groups) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_JOIN_GROUPS);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, (String[]) (groups.toArray(new String[groups.size()])));
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }

        @Override
        public void leaveGroups(List<String> groups) {
            if (mHandler != null) {
                Message msg = mHandler.obtainMessage(MSG_LEAVE_GROUPS);
                Bundle bundle = new Bundle();
                bundle.putStringArray(KEY_GROUPS, (String[]) (groups.toArray()));
                msg.setData(bundle);
                mHandler.sendMessage(msg);
            }
        }
    }

    private static class ChatEngineCallback implements Callback {
        IChatEngine mChatEngine;

        private ChatEngineCallback(IChatEngine chatEngine) {
            mChatEngine = chatEngine;
        }
        private static ChatEngineCallback sInstance;
        public static ChatEngineCallback getInstance(IChatEngine chatEngine) {
            if (sInstance == null) {
                sInstance = new ChatEngineCallback(chatEngine);
            }
            return sInstance;
        }

        public void release() {
            if(null != mChatEngine) {
                ((ChatEngine) mChatEngine).release();
                mChatEngine = null;
            }
            if (null != sInstance) {
                sInstance = null;
            }
        }
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_ON_CHAT_ENGINE_CONNECTED_SUCCEEDED: {
                    Bundle bundle = msg.getData();
                    String id = bundle.getString(KEY_ID);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onChatEngineConnectSucceeded(id, requestId);
                    break;
                }
                case MSG_ON_CHAT_ENGINE_CONNECTED_FAILED: {
                    Bundle bundle = msg.getData();
                    String id = bundle.getString(KEY_ID);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onChatEngineConnectFailed(id, requestId);
                    break;
                }
                case MSG_ON_CHAT_ENGINE_DISCONNECTED_SUCCEEDED: {
                    Bundle bundle = msg.getData();
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onChatEngineDisconnectSucceeded(requestId);
                    break;
                }
                case MSG_ON_CHAT_ENGINE_DISCONNECTED_FAILED: {
                    Bundle bundle = msg.getData();
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onChatEngineDisconnectFailed(requestId);
                    break;
                }
                case MSG_ON_GROUP_LIST_SUCCEEDED: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onGroupListSucceeded(Arrays.asList(groups), requestId);
                    break;
                }
                case MSG_ON_GROUP_LIST_FAILED: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onGroupListFailed(Arrays.asList(groups), requestId);
                    break;
                }
                case MSG_ON_GROUP_JOIN_SUCCEEDED: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onGroupJoinSucceeded(Arrays.asList(groups), requestId);
                    break;
                }
                case MSG_ON_GROUP_JOIN_FAILED: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onGroupJoinFailed(Arrays.asList(groups), requestId);
                    break;
                }
                case MSG_ON_GROUP_LEAVE_SUCCEEDED: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onGroupLeaveSucceeded(Arrays.asList(groups), requestId);
                    break;
                }
                case MSG_ON_GROUP_LEAVE_FAILED: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    String requestId = bundle.getString(KEY_REQUEST_ID);
                    mChatEngine.onGroupLeaveFailed(Arrays.asList(groups), requestId);
                    break;
                }
                case MSG_ON_MESSAGE: {
                    Bundle bundle = msg.getData();
                    String message = bundle.getString(KEY_MESSAGE);
                    mChatEngine.onMessage(message);
                    break;
                }
                case MSG_SEND_CLOUD_MESSAGE: {
                    Bundle bundle = msg.getData();
                    CloudMessage cloudMessage = bundle.getParcelable(KEY_CLOUD_MESSAGE);
                    ChatMessage chatMessage = bundle.getParcelable(KEY_CHAT_MESSAGE);
                    mChatEngine.sendCloudMessage(cloudMessage, chatMessage);
                    break;
                }
                case MSG_JOIN_GROUPS: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    mChatEngine.joinGroups(Arrays.asList(groups));
                    break;
                }
                case MSG_LEAVE_GROUPS: {
                    Bundle bundle = msg.getData();
                    String[] groups = bundle.getStringArray(KEY_GROUPS);
                    mChatEngine.leaveGroups(Arrays.asList(groups));
                    break;
                }
            }
            return false;
        }

    }

    private IChatService.Stub createNewIBinder() {
        /**
         * Note: when we have detected that we were using another account to
         * log in, we'll switch the whole context for the new user. During the switching
         * , it's not safe to use any objects in the context, so before you use them, you
         * should check if they're null.
         */
        mChatServiceStub = new IChatService.Stub() {
            @Override
            public void leaveGroups(List<String> groupIds) throws RemoteException {
                if (null == mChatEngineHandlerThread) {
                    return;
                }
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        mChatEngineHandlerThread.leaveGroups(groupIds);
                    }
                } else {
                    mChatEngineHandlerThread.leaveGroups(groupIds);
                }
            }

            @Override
            public void joinGroups(List<String> groupIds) throws RemoteException {
                if (null == mChatEngineHandlerThread) {
                    return;
                }
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        mChatEngineHandlerThread.joinGroups(groupIds);
                    }
                } else {
                    mChatEngineHandlerThread.joinGroups(groupIds);
                }
            }

            @Override
            public List<ChatMessage> getMessages(
                    long conversationId, long offset, long limit) throws RemoteException {
                if (null == mCloudMessageManager) {
                    return null;
                }
                List<ChatMessage> result = null;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getMessages(conversationId, offset, limit);
                    }
                } else {
                    result = mCloudMessageManager.getMessages(conversationId, offset, limit);
                }
                return result;
            }

            @Override
            public List<Conversation> getAllConversations() throws RemoteException {
                if (null == mCloudMessageManager) {
                    return null;
                }
                List<Conversation> result = null;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getAllConversations();
                    }
                } else {
                    result = mCloudMessageManager.getAllConversations();
                }
                return result;
            }

            @Override
            public ChatMessage sendMessage(CloudMessage cloudMsg) throws RemoteException {
                if (null == mCloudMessageManager || null == mChatEngineHandlerThread) {
                    return null;
                }
                ChatMessage chatMessage = null;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        chatMessage = mCloudMessageManager.insertCloudMessageToDB(cloudMsg, false);
                        if (chatMessage != null) {
                            mChatEngineHandlerThread.sendCloudMessage(cloudMsg, chatMessage);
                        }
                    }
                } else {
                    chatMessage = mCloudMessageManager.insertCloudMessageToDB(cloudMsg, false);
                    if (chatMessage != null) {
                        mChatEngineHandlerThread.sendCloudMessage(cloudMsg, chatMessage);
                    }
                }
                return chatMessage;
            }

            @Override
            public Conversation getConversationByUserId(long userId) throws RemoteException {
                if (null == mCloudMessageManager) {
                    return null;
                }
                Conversation result = null;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getConversationByUserId(userId);
                    }
                } else {
                    result = mCloudMessageManager.getConversationByUserId(userId);
                }
                return result;
            }

            @Override
            public Conversation getConversationByGroupId(String groupId) throws RemoteException {
                if (null == mCloudMessageManager) {
                    return null;
                }
                Conversation result = null;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getConversationByGroupId(groupId);
                    }
                } else {
                    result = mCloudMessageManager.getConversationByGroupId(groupId);
                }
                return result;
            }

            @Override
            public void onNotifyNewUser() throws RemoteException {
                // we've changed the user in upper context, so update the service context
                mIsSwitchingContext = true;
                synchronized (mIsSwitchingContext) {
                    doClear();
                    doInitialize();
                    Log.i(TAG, "chat service updated.");
                }
                mIsSwitchingContext = false;
            }

            @Override
            public long getMaxMessageId(Conversation conversation)
                    throws RemoteException {
                if (null == mCloudMessageManager) {
                    return 0;
                }
                long result = 0;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getMaxMessageId(conversation);
                    }
                } else {
                    result = mCloudMessageManager.getMaxMessageId(conversation);
                }
                return result;
            }

            @Override
            public void setChatStatus(boolean singleChat, boolean chatting,
                    long userId, String groupId) throws RemoteException {
                if (null == mCloudMessageManager) {
                    return;
                }
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        mCloudMessageManager.setChatStatus(singleChat, chatting, userId, groupId);
                    }
                } else {
                    mCloudMessageManager.setChatStatus(singleChat, chatting, userId, groupId);
                }
            }

            @Override
            public void cancelNotification() throws RemoteException {
                if (null == mCloudMessageManager) {
                    return;
                }
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        mCloudMessageManager.cancelNotification();
                    }
                } else {
                    mCloudMessageManager.cancelNotification();
                }
            }

            @Override
            public void clearNotificationCache() throws RemoteException {
                if (null == mCloudMessageManager) {
                    return;
                }
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        mCloudMessageManager.clearNotificationCache();
                    }
                } else {
                    mCloudMessageManager.clearNotificationCache();
                }
            }

            @Override
            public boolean clearNewMsgCount(boolean singleChat, long userId, String groupId) {
                if (null == mCloudMessageManager) {
                    return false;
                }
                boolean result = false;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.clearNewMsgCount(singleChat, userId, groupId);
                    }
                } else {
                    result = mCloudMessageManager.clearNewMsgCount(singleChat, userId, groupId);
                }
                return result;
            }

            @Override
            public int getClassNewMsgCount() {
                if (null == mCloudMessageManager) {
                    return 0;
                }
                int result = 0;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getClassNewMsgCount();
                    }
                } else {
                    result = mCloudMessageManager.getClassNewMsgCount();
                }
                return result;
            }

            @Override
            public int getAllSingleChatNewMsgCount() throws RemoteException {
                if (null == mCloudMessageManager) {
                    return 0;
                }
                int result = 0;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getAllSingleChatNewMsgCount();
                    }
                } else {
                    result = mCloudMessageManager.getAllSingleChatNewMsgCount();
                }
                return result;
            }

            @Override
            public boolean deleteCtrlMsgsByType(int type)
                    throws RemoteException {
                if (null == mCloudMessageManager) {
                    return false;
                }
                boolean result = false;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.deleteCtrlMsgsByType(type);
                    }
                } else {
                    result = mCloudMessageManager.deleteCtrlMsgsByType(type);
                }
                return result;
            }

            @Override
            public int getCtrlMsgCountByType(int type) throws RemoteException {
                if (null == mCloudMessageManager) {
                    return 0;
                }
                int result = 0;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.getCtrlMsgCountByType(type);
                    }
                } else {
                    result = mCloudMessageManager.getCtrlMsgCountByType(type);
                }
                return result;
            }

            @Override
            public boolean deleteGroupChatMsg(String groupId, long msgId) throws RemoteException {
                if (null == mCloudMessageManager) {
                    return false;
                }
                boolean result = false;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.deleteGroupChatMsg(groupId, msgId);
                    }
                } else {
                    result = mCloudMessageManager.deleteGroupChatMsg(groupId, msgId);
                }
                return result;
            }

            @Override
            public boolean deleteSingleChatMsg(long toUserId, long msgId) throws RemoteException {
                if (null == mCloudMessageManager) {
                    return false;
                }
                boolean result = false;
                if (mIsSwitchingContext) {
                    synchronized (mIsSwitchingContext) {
                        result = mCloudMessageManager.deleteSingleChatMsg(toUserId, msgId);
                    }
                } else {
                    result = mCloudMessageManager.deleteSingleChatMsg(toUserId, msgId);
                }
                return result;
            }
        };
        return mChatServiceStub;
    }

    private void registerAutoLoginReceiver() {
        if (null != mAutoLoginReceiver) {
            Log.w(TAG, Common.getLastTrace() + "chat service already registered AutoLoginReceiver!");
            return;
        }
        mAutoLoginReceiver = new AutoLoginReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Utils.ACTION_AUTO_LOGIN);
        this.registerReceiver(mAutoLoginReceiver, filter);
    }

    /**
     * Make sure you've called {@link #registerAutologinReceiver()} before calling this method.
     */
    private void unregisterAutoLoginReceiver() {
        if (null == mAutoLoginReceiver) {
            Log.w(TAG, Common.getLastTrace() + ": chat service hasn't register AutoLoginReceiver yet!");
            return;
        }
        this.unregisterReceiver(mAutoLoginReceiver);
    }

    private class AutoLoginReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context arg0, Intent arg1) {
            if (null != mCloudMessageManager) {
                mCloudMessageManager.requestUnreadMsgs();
            }
        }

    }
}
