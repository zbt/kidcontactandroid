package com.bobs.kidcontact.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bobs.kidcontact.R;

/**
 * Created by junyongl on 14-1-9.
 */
public class VoteOptionView extends LinearLayout {
    private EditText mItemContent;

    public VoteOptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.CENTER_VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_vote_option, this, true);
        mItemContent = (EditText) findViewById(R.id.et_option_content);
    }

    public String getOptionContent() {
        return mItemContent.getText().toString().trim();
    }
}
