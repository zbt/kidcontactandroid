package com.bobs.kidcontact.chat.service;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.NotificationManager;
import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.chat.database.ChatDB;
import com.bobs.kidcontact.chat.database.CtrlDB;
import com.bobs.kidcontact.chat.engine.IPushEngine;
import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.chat.unifiedmodel.CloudMessage;
import com.bobs.kidcontact.chat.unifiedmodel.Conversation;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.model.UnreadMsgs;
import com.bobs.kidcontact.requests.RequestGetUnreadMsg;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;
import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.R;

/**
 * Created by hzheng3 on 14-2-20. Maintained by zhanglong.
 * This class should be thread safe
 */
public class CloudMessageManager {
    private static final String TAG = "CloudMessageManager";
    IPushEngine mIPushEngine;
    Context mContext;
    ChatDB mChatDB;
    CtrlDB mCtrlDB;
    String mDBLock = new String("dblock");
    String mCtrlDBLock = new String("ctrl_db_lock");
    String mNotificationLock = new String("ntflock");
    Gson mGson = new Gson();
    boolean mChatting = false;
    boolean mSingleChat = false;
    String mForegroundChatGroupId = "";
    long mForegroundChatUserId = -1L;
    NotificationManager mNtyManager = null;
    NotificationConstructor mNotificationConstructor = null;
    MediaPlayer mMsgSoundPlayer = null;
    private CloudMessageManager(Context context, IPushEngine pushEngine) {
        mIPushEngine = pushEngine;
        mContext = context;
        mNtyManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationConstructor = new NotificationConstructor(mContext);
        mMsgSoundPlayer = MediaPlayer.create(mContext, R.raw.msg_sound);
        synchronized (mDBLock) {
            mChatDB = new ChatDB(mContext);
        }
        synchronized (mCtrlDBLock) {
            mCtrlDB = new CtrlDB(mContext);
        }
        requestUnreadMsgs();
    }

    public static CloudMessageManager getInstance(Context context, IPushEngine pushEngine) {
        if(sInstance == null) {
            sInstance = new CloudMessageManager(context, pushEngine);
        }
        return sInstance;
    }
    private static CloudMessageManager sInstance;

    public Conversation getConversationByUserId(long userId) {
        synchronized (mDBLock) {
            return mChatDB.getConversationByName(""+userId);
        }
    }

    public synchronized void setChatStatus(boolean singleChat, boolean chatting,
            long userId, String groupId) {
        mSingleChat = singleChat;
        mChatting = chatting;
        mForegroundChatUserId = userId;
        mForegroundChatGroupId = groupId;
    }

    public synchronized boolean isChatting() {
        return mChatting;
    }

    public synchronized boolean isSingleChat() {
        return mSingleChat;
    }
    public synchronized long foregroundChatUserId() {
        return mForegroundChatUserId;
    }
    public synchronized String foregroundGroupChatId() {
        return mForegroundChatGroupId;
    }

    public Conversation getConversationByGroupId(String groupId) {
        synchronized (mDBLock) {
            return mChatDB.getConversationByGroupId(groupId);
        }
    }

    public List<ChatMessage> getMessages(long conversationId, long offset, long limit) {
        synchronized (mDBLock) {
            return mChatDB.getMessages(conversationId, offset, limit);
        }
    }

    public long getMaxMessageId(Conversation conversation) {
        synchronized (mDBLock) {
            return mChatDB.getMaxMessageId(conversation);
        }
    }

    public List<Conversation> getAllConversations() {
        synchronized (mDBLock) {
            return mChatDB.getAllConversations();
        }
    }

    /** Release all the resources that is hold by this instance */
    public void release() {
        synchronized (mDBLock) {
            if (null != mChatDB) {
                mChatDB.close();
                mChatDB = null;
            }
        }
        synchronized (mCtrlDBLock) {
            if (null != mCtrlDB) {
                mCtrlDB.close();
                mCtrlDB = null;
            }
        }
        if (null != sInstance) {
            sInstance = null;
        }
    }

    /**
     * According to cloud message information, insert this cloud message to database accordingly
     * @param cloudMessage
     * @return
     */
    public ChatMessage insertCloudMessageToDB(CloudMessage cloudMessage, boolean updateNewMsgCount) {
        ChatMessage chatMessage = cloudMessage.newChatMessage();
        if (chatMessage != null) {
            synchronized (mDBLock) {
                Conversation conversation = mChatDB.getConversationByName(chatMessage.getConversationName());
                if (conversation == null) {
                    conversation = cloudMessage.newConversation();
                }
                return mChatDB.addMsgToConversation(conversation, chatMessage, updateNewMsgCount);
            }
        } else {
            return null;
        }
    }

    /**
     * First according to cloudMessage property, use PushEngine to send out msg
     * Then, according to sending result, update chat message database
     * Last, send out broadcast
     * @param cloudMessage
     * @param chatMessage
     * @return
     */
    public boolean sendOutCloudMessage(CloudMessage cloudMessage, ChatMessage chatMessage) {
        String msg = mGson.toJson(cloudMessage);
        boolean ret = false;
        if (cloudMessage.isSingleChatMessage()) {
            ret = mIPushEngine.sendMsgToUser(cloudMessage.getPayload().mToUserId, cloudMessage.getToDeviceType(), msg);
        } else if (cloudMessage.isGroupChatMessage()) {
            ret = mIPushEngine.sendMsgToGroup(cloudMessage.getPayload().mGroupId, msg);
        }
        if (ret) {
            chatMessage.setStatus(ChatMessage.MSG_STATUS_SENT);
        } else {
            chatMessage.setStatus(ChatMessage.MSG_STATUS_FAILED);
        }
        synchronized (mDBLock) {
            mChatDB.updateChatMessage(chatMessage);
        }
        return ret;
    }

    /**
     * Handling incoming cloudMessage. We'll discard the message that is
     * sent by myself.
     * @param cloudMessage
     */
    public void onCloudMessage(CloudMessage cloudMessage) {
        if (!CloudMessageChecker.getInstance().check(cloudMessage)) {
            return;
        }
        if (cloudMessage.isChatMessage()) {
            // check if we need to update new message count
            boolean newMsgCount = false;
            if (cloudMessage.isGroupChatMessage()) {
                newMsgCount = !(mChatting && !mSingleChat);
            } else {
                newMsgCount = !(mChatting && mSingleChat && cloudMessage.getPayload().mFromUserId.equals(mForegroundChatUserId));
            }

            // Save message
            if (null == insertCloudMessageToDB(cloudMessage, newMsgCount)) {
                Log.e(TAG, "failed to insert message to database!");
                return;
            }
        } else if (cloudMessage.isCtrlMessage()) {
            Log.d(TAG, "received ctrl msg.");
            CtrlMessage msg = cloudMessage.toCtrlMsg();
            if (RemoteLoginManager.check(msg)) {
                RemoteLoginManager.handle(mContext);
            }
            synchronized (mCtrlDBLock) {
                mCtrlDB.insertCtrlMsg(msg); // we ignore the return value here because it's not that serious
            }
        }
        // Check and notify
        synchronized (mNotificationLock) {
            if (!isAppForeground()) {
                mNtyManager.notify(NotificationConstructor.NOTIFICATION_ID, mNotificationConstructor.newNotification(cloudMessage));
            }
            playMsgSound();
        }

        // Handle "needPull"
        if (cloudMessage.getNeedPull()) {
            requestUnreadMsgs();
        }
    }

    /** Cancel the existing notification */
    public void cancelNotification() {
        synchronized (mNotificationLock) {
            mNtyManager.cancel(NotificationConstructor.NOTIFICATION_ID);
        }
    }

    /** Clear all the notification cache */
    public void clearNotificationCache() {
        synchronized (mNotificationLock) {
            mNotificationConstructor.clear();
        }
    }

    /** Clear the new message count of a conversation */
    public boolean clearNewMsgCount(boolean singleChat, long userId, String groupId) {
        synchronized (mDBLock) {
            if (singleChat) {
                if (!mChatDB.clearNewMsgCountByUserId(userId)) {
                    Log.e(TAG, "failed to clear the new message count of the conversation with the user id: " + userId);
                    return false;
                }
            } else {
                if (!mChatDB.clearNewMsgCountByGroupId(groupId)) {
                    Log.e(TAG, "failed to clear the new message count of the conversation with the group id: " + groupId);
                    return false;
                }
            }
        }
        return true;
    }

    public int getClassNewMsgCount() {
        synchronized (mDBLock) {
            return mChatDB.getNewMsgCount(BaiduInfoHolder.getGroupTag());
        }
    }

    /**
     * Get the sum new message count of all the single chat.
     * @return an integer larger than zero if found some, 0 if not found, -1 if error
     */
    public int getAllSingleChatNewMsgCount() {
        List<Conversation> conversations = null;
        synchronized (mDBLock) {
            conversations = mChatDB.getAllConversations();
        }
        if (null == conversations) {
            return -1;
        }
        int result = 0;
        for (int i = 0; i < conversations.size(); i++) {
            if (!conversations.get(i).getGroupChat()){
                result += conversations.get(i).getNewMsgCount();
            }
        }
        return result;
    }

    /** Delete CtrlMessages by type */
    public boolean deleteCtrlMsgsByType(int type) {
        synchronized (mCtrlDBLock) {
            return mCtrlDB.deleteCtrlMsgsByType(type);
        }
    }

    public boolean deleteGroupChatMsg(String groupId, long msgId) {
        synchronized (mDBLock) {
            return mChatDB.deleteGroupChatMsg(groupId, msgId);
        }
    }

    public boolean deleteSingleChatMsg(long toUserId, long msgId) {
        synchronized (mDBLock) {
            return mChatDB.deleteSingleChatMsg(toUserId, msgId);
        }
    }

    /** Get the CtrlMessage count with a given type */
    public int getCtrlMsgCountByType(int type) {
        List<CtrlMessage> msgs = new ArrayList<CtrlMessage>();
        synchronized (mCtrlDBLock) {
            msgs = mCtrlDB.getCtrlMessagesByType(type);
        }
        if (null == msgs) {
            Log.e(TAG, Common.getLastTrace() + ": failed to get CtrlMessages by type: " + type);
            return 0;
        }
        return msgs.size();
    }

    public void joinGroups(List<String> groups) {
        mIPushEngine.joinGroups(groups);
    }

    public void leaveGroups(List<String> groups) {
        mIPushEngine.leaveGroups(groups);
    }

    public void requestUnreadMsgs() {
        new RequestGetMsgUtil().add2queue();
    }

    /** Play the message sound once */
    private void playMsgSound() {
        synchronized (mMsgSoundPlayer) {
            mMsgSoundPlayer.start();
        }
    }

    /** Check if the KidContact app is in foreground */
    public static boolean isAppForeground() {
        Context appCtx = KidContactApp.getInstance();
        if (null == appCtx) {
            return false;
        }
        ActivityManager am = (ActivityManager) appCtx.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (tasks.size() > 0 && tasks.get(0).topActivity.getPackageName().equals(appCtx.getPackageName())) {
            return true;
        }
        return false;
    }

    /** A request util for request unread messages. */
    public class RequestGetMsgUtil extends RequestUtil<UnreadMsgs> {
        private static final String TAG_MSG_REQUEST = "get_msg_request";
        private static final int NUM_PER_REQUEST = 30;

        @Override
        public Request<UnreadMsgs> onPrepareRequest(
                Listener<UnreadMsgs> listener, ErrorListener errListener) {
            Request<UnreadMsgs> request = new RequestGetUnreadMsg(NUM_PER_REQUEST, listener, errListener);
            request.setTag(TAG_MSG_REQUEST);
            return request;
        }

        @Override
        public void onSuccess(UnreadMsgs response) {
            List<CloudMessage> msgs = response.mCloudMessages; // alias
            int msgCount = msgs.size(); // alias
            // Sort it
            Collections.sort(msgs, new Comparator<CloudMessage>() {

                @Override
                public int compare(CloudMessage lhs, CloudMessage rhs) {
                    return (int) (lhs.getTimestamp() - rhs.getTimestamp());
                }

            });

            // Cancel all the same requests if we have no cache in the server
            if (msgCount < NUM_PER_REQUEST) {
                this.cancel(TAG_MSG_REQUEST);
            }

            // Now we parse each one of them
            for (int i = 0; i < msgCount; i++) {
                CloudMessageManager.this.onCloudMessage(msgs.get(i));
            }
        }

        @Override
        public void onError(VolleyError e) {
            Log.e(TAG, Common.getLastTrace() + "failed to get unread messages!");
            super.onError(e);
        }
    }

    /** Used to check the incoming CloudMessage. */
    private static class CloudMessageChecker {
        private static final int CACHE_SIZE = 10;
        private ArrayDeque<String> mUniqueIdCache = new ArrayDeque<String>();
        private static CloudMessageChecker mInstance;

        public static CloudMessageChecker getInstance() {
            if (null == mInstance) {
                mInstance = new CloudMessageChecker();
            }
            return mInstance;
        }

        /** To check if we should handle this CloudMessage */
        public boolean check(CloudMessage cloudMsg) {
            // Check sender
            if (cloudMsg.isSentByMyself()) {
                return false;
            }

            // Check unique id
            if (mUniqueIdCache.contains(cloudMsg.getUniqueId())) {
                return false;
            }

            // Cache unique id
            mUniqueIdCache.addLast(cloudMsg.getUniqueId());
            if (mUniqueIdCache.size() > CACHE_SIZE) {
                mUniqueIdCache.removeFirst();
            }

            return true;
        }
    }

//    /**
//     * The incoming cloud message is to new group
//     * Not usable for kid contact, since every one belongs to existing class group
//     * @param cloudMessage
//     */
//    private void onNewGroup(CloudMessage cloudMessage) {
//
//    }
//
//    /**
//     * The incoming cloud message is to indicate someone has left group chat
//     * Not usable for kid contact
//     * @param cloudMessage
//     */
//    private void onLeaveGroup(CloudMessage cloudMessage) {
//
//    }
//
//    /**
//     * This message is group chat message
//     * @param cloudMessage
//     */
//    private void onGroupMessage(CloudMessage cloudMessage) {
//
//    }
//
//    /**
//     * This message is single user chat message
//     * @param cloudMessage
//     */
//    private void onUserMessage(CloudMessage cloudMessage) {
//
//    }
}
