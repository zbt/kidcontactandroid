package com.bobs.kidcontact.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.EatMenu;
import com.bobs.kidcontact.model.TimeTable;
import com.bobs.kidcontact.requests.CookieMuiltiPicUploadRequest.PicPair;
import com.bobs.kidcontact.requests.RequestAddEatMenu;
import com.bobs.kidcontact.requests.RequestAddTimeTable;
import com.bobs.kidcontact.ui.ActivityPublish.IPublish;
import com.bobs.kidcontact.ui.view.FragmentPictureSelector;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;

import de.greenrobot.event.EventBus;

/**
 * Created by leon on 14-1-4.
 * Modified by Joey on 14-6-15.
 */
public class FragmentPublishClassMenu extends Fragment implements IPublish, DialogInterface.OnClickListener {
    private static final String TAG = "FragmentPublishClassMenu";
    private Context mContext;
    private AsyncTask<Void, String, RequestUtil<EatMenu>> mEatMenuTask;
    private AsyncTask<Void, String, RequestUtil<TimeTable>> mTimeTableTask;
    private RequestUtil<?> mPublishTask;
    private ProgressDialog mDialog;
    private File mCacheDir;
    private int mSeqNum = 0;
    private int mPublishType;
    private File cacheImageFile;
    private ImageView mImageView;
    private Uri mUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        Bundle bundle = getArguments();
        mPublishType = bundle.getInt("publish_type");
        mDialog = new ProgressDialog(mContext) {
            @Override
            protected void onStop() {
                super.onStop();
                FragmentPublishClassMenu.this.onStop();
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publish_classmenu, container, false);
        mImageView = (ImageView) view.findViewById(R.id.image_class_menu);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentPictureSelector fragment = new FragmentPictureSelector();
                fragment.show(getChildFragmentManager(), "picture_selector");
                fragment.setOnClickListener(FragmentPublishClassMenu.this);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_PICK:
                	if (data.getData()!=null)
                		mUri = data.getData();
                    Log.d(TAG, "Pick picture URI: " + mUri.toString());
                    break;
                case Constants.REQUEST_CAPTURE:
                    if (cacheImageFile != null) 
                        mUri = Uri.fromFile(cacheImageFile);
                    Log.d(TAG, "Pick picture URI: " + mUri.toString());
                    break;
                default:
                    break;
            }
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(mUri.getPath(), bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;
            int targetH = mContext.getResources().getDisplayMetrics().heightPixels - 100;
            int targetW = photoW * targetH/photoH;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;
            bmOptions.inPurgeable = true;

            Bitmap bitmap = BitmapFactory.decodeFile(mUri.getPath(), bmOptions);
            
            //bitmap = getThumbnailBitmap(mUri);        
            mImageView.setImageBitmap(bitmap);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            //index of "picture_picker_array"
            case 0:
                //Take picture via camera
                try {
                    cacheImageFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Intent intent_capture = UIUtil.getCaptureImageIntent(cacheImageFile);
                if (intent_capture != null) {
                    startActivityForResult(intent_capture, Constants.REQUEST_CAPTURE);
                }
                break;
            case 1:
                //Select picture from gallery
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, Constants.REQUEST_PICK);
                break;
            default:
                break;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private String getRealPathFromURI(Context context, Uri contentUri) {
        if (contentUri.getScheme().equals("file")) {
            return contentUri.getPath();
        }
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void publish() {
    	// Support to publish Class Menu and Class Calendar
        if (mPublishType == Constants.PublishType.CLASSMENU.ordinal()){
	        mEatMenuTask = new AsyncTask<Void, String, RequestUtil<EatMenu>>() {
	            @Override
	            protected RequestUtil<EatMenu> doInBackground(Void... params) {
	                
	                if (mUri == null) {
	                    return null;
	                }
	                prepareCache();
	                
	                if (isCancelled()) {
	                    return null;
	                }
	
	                publishProgress("准备 " + mUri.toString());
	                String abspath = getRealPathFromURI(mContext, mUri);
	                Log.d(TAG, "Adding " + abspath);
	                final PicPair pics = processImage(abspath);
	                
	                RequestUtil<EatMenu> req = new RequestUtil<EatMenu>() {
	                    @Override
	                    public Request<EatMenu> onPrepareRequest(
	                            Listener<EatMenu> listener, ErrorListener errListener) {
	                        
	                        return new RequestAddEatMenu(pics, listener, errListener);
	                    }
	
	                    @Override
	                    public void onSuccess(EatMenu response) {
	                        mDialog.dismiss();
	                        Toast.makeText(mContext, R.string.publish_success, Toast.LENGTH_SHORT).show();
	                        getActivity().finish();
	                        EventBus.getDefault().post(new UpdateEatMenuUIEvent());
	                    }
	
	                    @Override
	                    public void onError(VolleyError e) {
	                        mDialog.dismiss();
	                        Toast.makeText(mContext, R.string.publish_fail, Toast.LENGTH_SHORT).show();
	                        super.onError(e);
	                    }
	                };
	                req.setTimeout(10000);
	                return req;
	            }
	
	            @Override
	            protected void onCancelled() {
	                super.onCancelled();
	                mDialog.dismiss();
	            }
	
	            @Override
	            protected void onPostExecute(RequestUtil<EatMenu> result) {
	                super.onPostExecute(result);
	                if (result != null) {
	                    mPublishTask = result;
	                    mDialog.setMessage("上传 ...");
	                    mPublishTask.add2queue();
	                } else {
	                    mDialog.dismiss();
	                }
	            }
	
	            @Override
	            protected void onPreExecute() {
	                super.onPreExecute();
	                mDialog.setMessage("准备上传 ...");
	                mDialog.show();
	            }
	
	            @Override
	            protected void onProgressUpdate(String... values) {
	                super.onProgressUpdate(values);
	                if (values != null && values.length > 0) {
	                    mDialog.setMessage(values[0]);
	                }
	            }
	        };
	        mEatMenuTask.execute();
        }else if (mPublishType == Constants.PublishType.CLASSCAL.ordinal()){
        	mTimeTableTask = new AsyncTask<Void, String, RequestUtil<TimeTable>>() {
	            @Override
	            protected RequestUtil<TimeTable> doInBackground(Void... params) {
	                
	                if (mUri == null) {
	                    return null;
	                }
	                prepareCache();
	                
	                if (isCancelled()) {
	                    return null;
	                }
	
	                publishProgress("准备 " + mUri.toString());
	                String abspath = getRealPathFromURI(mContext, mUri);
	                Log.d(TAG, "Adding " + abspath);
	                final PicPair pics = processImage(abspath);
	                
	                RequestUtil<TimeTable> req = new RequestUtil<TimeTable>() {
	                    @Override
	                    public Request<TimeTable> onPrepareRequest(
	                            Listener<TimeTable> listener, ErrorListener errListener) {
	                        
	                        return new RequestAddTimeTable(pics, listener, errListener);
	                    }
	
	                    @Override
	                    public void onSuccess(TimeTable response) {
	                        mDialog.dismiss();
	                        Toast.makeText(mContext, R.string.publish_success, Toast.LENGTH_SHORT).show();
	                        getActivity().finish();
	                        EventBus.getDefault().post(new UpdateClassCalUIEvent());
	                    }
	
	                    @Override
	                    public void onError(VolleyError e) {
	                        mDialog.dismiss();
	                        Toast.makeText(mContext, R.string.publish_fail, Toast.LENGTH_SHORT).show();
	                        super.onError(e);
	                    }
	                };
	                req.setTimeout(10000);
	                return req;
	            }
	
	            @Override
	            protected void onCancelled() {
	                super.onCancelled();
	                mDialog.dismiss();
	            }
	
	            @Override
	            protected void onPostExecute(RequestUtil<TimeTable> result) {
	                super.onPostExecute(result);
	                if (result != null) {
	                    mPublishTask = result;
	                    mDialog.setMessage("上传 ...");
	                    mPublishTask.add2queue();
	                } else {
	                    mDialog.dismiss();
	                }
	            }
	
	            @Override
	            protected void onPreExecute() {
	                super.onPreExecute();
	                mDialog.setMessage("准备上传 ...");
	                mDialog.show();
	            }
	
	            @Override
	            protected void onProgressUpdate(String... values) {
	                super.onProgressUpdate(values);
	                if (values != null && values.length > 0) {
	                    mDialog.setMessage(values[0]);
	                }
	            }
	        };
	        mTimeTableTask.execute();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mEatMenuTask != null) {
        	mEatMenuTask.cancel(true);
        }
        if (mPublishTask != null) {
            mPublishTask.cancel();
        }
        mDialog.dismiss();
    }

    private void prepareCache() {
        if (mCacheDir == null) {
            mCacheDir = new File(mContext.getCacheDir(), TAG);
            if (!mCacheDir.exists()) {
                if (!mCacheDir.mkdirs()) {
                    throw new RuntimeException("Can't create cache for thumbnails");
                }
            }
        }
        File[] files = mCacheDir.listFiles();
        if (files != null) {
            for (File file : files) {
                file.delete();
            }
        }
    }

    private PicPair processImage(String path) {
        String path_big = path;
        String path_small = path;

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opts);
        int orgHeight = opts.outHeight;
        int orgWidth = opts.outWidth;
        Bitmap thum = null;

        if (orgHeight > Constants.PIC_MAX_HEIGHT || orgWidth > Constants.PIC_MAX_WIDTH) {
            int heightRatio = Math.round(orgHeight / (float) Constants.PIC_MAX_HEIGHT);
            int widthRatio = Math.round(orgWidth / (float) Constants.PIC_MAX_WIDTH);
            int resizeScale = (heightRatio > widthRatio) ? heightRatio : widthRatio;
            opts.inSampleSize = resizeScale;
            opts.inJustDecodeBounds = false;
            thum = BitmapFactory.decodeFile(path, opts);
            FileOutputStream fos;
            try {
                File fout = new File(mCacheDir, "" + (mSeqNum++) + ".jpg");
                fos = new FileOutputStream(fout);
                thum.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                fos.flush();
                fos.close();
                path_big = fout.getAbsolutePath();
                Log.d(TAG, "[big img:" + thum.getWidth() + "x" + thum.getHeight() + "]" + path_big);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (orgHeight > Constants.THUMB_MAX_HEIGHT || orgWidth > Constants.THUMB_MAX_WIDTH) {
            double ratio = 1.0 * orgWidth/orgHeight;
            double w = Constants.THUMB_MAX_WIDTH;
            double h = Constants.THUMB_MAX_HEIGHT;
            double x = w/ratio;
            if (x > h) {
                w = h * ratio;
            }
            if (thum == null) {
                BitmapFactory.Options opt = new BitmapFactory.Options();
                thum = BitmapFactory.decodeFile(path, opt);
            }
            Bitmap t = ThumbnailUtils.extractThumbnail(thum, (int)w, (int)h);
            FileOutputStream fos;
            try {
                File fout = new File(mCacheDir, "" + (mSeqNum++) + ".jpg");
                fos = new FileOutputStream(fout);
                t.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                t.recycle();
                fos.flush();
                fos.close();
                path_small = fout.getAbsolutePath();
                Log.d(TAG, "[small img:" + (int)w + "x" + (int)h + "]" + path_small);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (thum != null) {
            thum.recycle();
        }

        PicPair pair = new PicPair();
        pair.mPic = new File(path_big);
        pair.mThumb = new File(path_small);
        return pair;
    }

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = ((Activity) mContext).getMenuInflater();
	    inflater.inflate(R.menu.context_menu_delete, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    switch (item.getItemId()) {
	        case R.id.item_delete:
	            deletePicture(info.id);
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}

	private void deletePicture(long id) {
//		if (id != mPicturesUri.size()) {
//			mPicturesUri.remove((int)id);
//			mAdapter.notifyDataSetChanged();
//		}
	}



}
