package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.IdProxy;

/**
 * Created by hzheng3 on 14-2-2.
 */
public class RequestDeleteAssignment extends RequestDeleteEntityOfId {
    public RequestDeleteAssignment(IdProxy input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_DELETE_ASSIGNMENT), input, listener, errorListener);
    }
}
