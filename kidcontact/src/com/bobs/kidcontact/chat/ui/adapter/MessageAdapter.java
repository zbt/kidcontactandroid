package com.bobs.kidcontact.chat.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.ChatMessage;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.ui.ActivityUserInfo;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.UIUtil;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by leon on 14-2-23. zhanglong
 */
public class MessageAdapter extends BaseAdapter {
    public static final Pattern EMOTION_URL = Pattern.compile("\\[(\\S+?)\\]");
    private Context mContext;
    private List<ChatMessage> mMessageList;
    private LayoutInflater mInflater;
    private MsgLongClickListener mMsgLongClickListener = null;

    public MessageAdapter(Context context, List<ChatMessage> messageList) {
        mContext = context;
        mMessageList = messageList;
        mInflater = LayoutInflater.from(mContext);
    }

    public void setMessageList(List<ChatMessage> msgList) {
        mMessageList = msgList;
        notifyDataSetChanged();
    }

    public void updateMessage(ChatMessage message) {
        mMessageList.add(message);
        notifyDataSetChanged();
    }
    
    public void setMsgLongClickListener(MsgLongClickListener listener) {
        mMsgLongClickListener = listener;
    }

    @Override
    public int getCount() {
        return mMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMessageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ChatMessage msgItem = mMessageList.get(position);
        boolean isComMsg = msgItem.getIsInComing();
        ViewHolder holder = new ViewHolder();
//        if (convertView == null) {
//            holder = new ViewHolder();
            if (isComMsg) {
                convertView = mInflater.inflate(R.layout.item_chat_left, null);
            } else {
                convertView = mInflater.inflate(R.layout.item_chat_right, null);
            }
            holder.imageView = (NetworkRoundImageView) convertView.findViewById(R.id.user_avatar);
            holder.msg = (TextView) convertView.findViewById(R.id.message_content);
            holder.time = (TextView) convertView.findViewById(R.id.message_time);
            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
        holder.time.setText(UIUtil.getRelativeDateTimeString(mContext, ChatMessage.stringToDate(msgItem.getMsgDate()).getTime()));
        holder.msg.setText(convertNormalStringToSpannableString(msgItem.getInfo()),
                TextView.BufferType.SPANNABLE);
        if (null != mMsgLongClickListener) {
            mMsgLongClickListener.setMsgId(msgItem.getId());
            holder.msg.setOnLongClickListener(mMsgLongClickListener);
        }
        holder.imageView.setImageUrl(Constants.getAvatarThumbURLOfCreator(msgItem.getUserId()),
                KidContactApp.getInstance().getImageLoader());
        holder.imageView.setOnClickListener(new IconUserListener(msgItem.getUserId()));
        return convertView;
    }

    /**
     * 另外一种方法解析表情
     *
     * @param message
     *            传入的需要处理的String
     * @return
     */
    private CharSequence convertNormalStringToSpannableString(String message) {
        String hackTxt;
        if (message.startsWith("[") && message.endsWith("]")) {
            hackTxt = message + " ";
        } else {
            hackTxt = message;
        }
        SpannableString value = SpannableString.valueOf(hackTxt);

        Matcher localMatcher = EMOTION_URL.matcher(value);
        while (localMatcher.find()) {
            String str2 = localMatcher.group(0);
            int k = localMatcher.start();
            int m = localMatcher.end();
            // k = str2.lastIndexOf("[");
            // Log.i("way", "str2.length = "+str2.length()+", k = " + k);
            // str2 = str2.substring(k, m);
            if (m - k < 8) {
                if (KidContactApp.getInstance().getFaceMap()
                        .containsKey(str2)) {
                    int face = KidContactApp.getInstance().getFaceMap()
                            .get(str2);
                    Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), face);
                    Matrix matrix = new Matrix();
                    matrix.postScale((float) 35/bitmap.getWidth(), (float) 35/bitmap.getHeight());
                    Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    if (bitmap != null) {
                        ImageSpan localImageSpan = new ImageSpan(mContext,
                                scaledBitmap, ImageSpan.ALIGN_BASELINE);
                        value.setSpan(localImageSpan, k, m,
                                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }
            }
        }
        return value;
    }

    public void addMessageList(List<ChatMessage> messageList) {
        mMessageList.addAll(0, messageList);
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        NetworkRoundImageView imageView;
        TextView time;
        TextView msg;
    }

    /** Listener for user icon */
    private class IconUserListener implements OnClickListener {
        private long mUserId = -1L;

        public IconUserListener(long userId) {
            mUserId = userId;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, ActivityUserInfo.class);
            intent.putExtra(ActivityUserInfo.KEY_USER_ID, mUserId);
            mContext.startActivity(intent);
        }

    }

    /**
     * A abstract base class for message item long click listener. The user of {@link #MessageAdapter}
     * who cares about the long click event should extend this class and pass an instance to
     * {@link #MessageAdapter}.
     * @author zhanglong
     */
    public abstract static class MsgLongClickListener implements OnLongClickListener {
        protected long mMsgId = -1;

        /** Usually called by {@link #MessageAdapter} */
        public void setMsgId(long msgId) {
            mMsgId = msgId;
        }
    }
}
