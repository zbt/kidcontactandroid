#!/usr/bin/env python

import re
import sys

FIN = sys.argv[1]
FXML = "out.xml"
FJAVA1 = "out1.java"
FJAVA2 = "out2.java"

fxml = open(FXML, 'w')
fjava1 = open(FJAVA1, 'w')
fjava2 = open(FJAVA2, 'w')

with open(FIN) as fin:
    pt_num = re.compile(r'int\s+(ERR_\S+)\s*=\s*(\d+)')
    pt_msg = re.compile(r'\(\s*(ERR_\S+)\s*\)\s*:\s*\"(.*)\"')
    for line in fin:
        sobj = pt_num.search(line)
        if sobj:
            fjava1.write("public static final int " + sobj.group(1) + " = " + sobj.group(2) + ";\n")
            fjava2.write("mapping.put(" + sobj.group(1) + ", R.string." + sobj.group(1)+ ");\n")
        else:
            sobj = pt_msg.search(line)
            if sobj:
                fxml.write("<string name=\"" + sobj.group(1) + "\">" + sobj.group(2) + "</string>\n")

fxml.close()
fjava1.close()
fjava2.close()
