package com.bobs.kidcontact.ui;

import com.bobs.kidcontact.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityCreateClass extends Activity {

	
	private Button mBtCreate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_class);
		
		 ((Button) findViewById(R.id.bt_right)).setVisibility(View.GONE);

		 mBtCreate = (Button) findViewById(R.id.bt_left);
		 mBtCreate.setText(R.string.create_class_title);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

}
