package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.CommentRecordList;

/**
 * Created by hzheng3 on 14-1-14.
 */
public abstract class RequestGetCommentRecord extends CookieGsonRequest<CommentRecordList> {
    private static final String IDOFFSET = "idOffset";
    private static final String CHILDID = "childId";
    public static final long INVALID_ID = -1;
    public RequestGetCommentRecord(long idOffset, long childId, Response.Listener<CommentRecordList> listener, Response.ErrorListener errorListener) {
        super(Method.GET, getCRsURL(idOffset, childId),
                CommentRecordList.class, listener, errorListener);
    }

    public static String getCRsURL(long idOffset, long childId) {
        if (childId == INVALID_ID) {
            return Constants.getFunctionURL(Constants.FUNC_GET_COMMENT_RECORDS)
                + Constants.QUESTION
                + IDOFFSET + Constants.EQUAL + idOffset;
        } else {
            return Constants.getFunctionURL(Constants.FUNC_GET_COMMENT_RECORDS)
                    + Constants.QUESTION
                    + IDOFFSET + Constants.EQUAL + idOffset
                    + Constants.AND
                    + CHILDID + Constants.EQUAL + childId;
        }
    }
}