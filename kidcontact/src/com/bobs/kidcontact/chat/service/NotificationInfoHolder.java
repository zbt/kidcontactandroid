package com.bobs.kidcontact.chat.service;

/**
 * 
 * @author zhanglong
 */
public class NotificationInfoHolder {
    private int mId = 0;
    private String mContentTitle = "";
    private String mContentText = "";
    

    public void setId(int id) {
        mId = id;
    }
    
    public int getId() {
        return mId;
    }

    public void setContentTitle(String title) {
        mContentTitle = title;
    }
    
    public String getContentTitle() {
        return mContentTitle;
    }
    
    public void setContentText(String text) {
        mContentText = text;
    }
    
    public String getContentText() {
        return mContentText;
    }
}
