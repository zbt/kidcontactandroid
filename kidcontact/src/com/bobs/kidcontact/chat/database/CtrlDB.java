package com.bobs.kidcontact.chat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.bobs.kidcontact.Common;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.chat.service.Utils;
import com.bobs.kidcontact.ui.KidContactApp;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author zhanglong
 */
public class CtrlDB {
    private static final String TAG = "CtrlDB";
    private Context mContext = null;
    private SQLiteDatabase mDB = null;
    private static final String TABLE_NAME_CTRL_MSG = "ctrl_msg";
    private static final String SQL_CREATE_CTRL_MSG_TABLE = "create table IF NOT EXISTS " +
            TABLE_NAME_CTRL_MSG + " (" + CtrlMessage.FIELD_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            CtrlMessage.FIELD_NAME_TYPE + " INTEGER," + CtrlMessage.FIELD_NAME_FROM_USER_ID + " INTEGER," +
            CtrlMessage.FIELD_NAME_TO_USER_ID + " INTEGER," + CtrlMessage.FIELD_NAME_DATA_ID + " INTEGER" + ");";

    public CtrlDB(Context context) {
        mContext = context;
        mDB = mContext.openOrCreateDatabase("ctrl" + KidContactApp.getSharePrefUtil().getUserName() + ".db", Context.MODE_PRIVATE, null);
        Log.d(TAG, "current database name: " + "ctrl" + KidContactApp.getSharePrefUtil().getUserName() + ".db");
        createCtrlMsgTable();
    }

    /** Insert a new CtrlMessage into the database */
    public boolean insertCtrlMsg(CtrlMessage msg) {
        if (!checkStatus()) {
            return false;
        }
        if (-1 == mDB.insert(TABLE_NAME_CTRL_MSG, null, createContentValues(msg))) {
            return false;
        }
        Log.d(TAG, "sending ctrl msg changed broadcast.");
        Utils.sendCtrlMessageChangedBroadcast(mContext, msg);
        return true;
    }

    /** Delete a CtrlMessage from database */
    public boolean deleteCtrlMsg(CtrlMessage msg) {
        if (!checkStatus()) {
            return false;
        }
        if (mDB.delete(TABLE_NAME_CTRL_MSG, CtrlMessage.FIELD_NAME_ID + "=" + msg.getId(), null) == 0) {
            Log.w(TAG, "Can't delete the CtrlMessage with the id: " + msg.getId());
            return false;
        }
        Utils.sendCtrlMessageDeletedBroadcast(mContext, msg);
        return true;
    }

    /**
     * Delete all the CtrlMessages with the given type.
     * @param type should one of the CtrlMessage type
     * @return true if succeed, false otherwise
     */
    public boolean deleteCtrlMsgsByType(int type) {
        if (!checkStatus()) {
            return false;
        }
        List<CtrlMessage> msgs = getCtrlMessagesByType(type);
        if (mDB.delete(TABLE_NAME_CTRL_MSG, CtrlMessage.FIELD_NAME_TYPE + "=" + type, null) == 0) {
            Log.w(TAG, "Can't delete the CtrlMessages with the type: " + type);
            return false;
        }
        if (msgs.size() > 0) {
            Utils.sendCtrlMessageDeletedBroadcast(mContext, msgs.get(0));
        }
        return true;
    }

    /**
     * Update a CtrlMessage stored in the database.
     * @param msg must have a valid id.
     * @return true if succeed, false otherwise
     */
    public boolean updateCtrlMsg(CtrlMessage msg) {
        if (!checkStatus()) {
            return false;
        }
        if (msg.getId() <= 0) {
            return false;
        }
        if (mDB.update(TABLE_NAME_CTRL_MSG, createContentValues(msg), CtrlMessage.FIELD_NAME_ID + "=" + msg.getId(), null) <= 0) {
            Log.w(TAG, "Can't update the CtrlMessage with the given id: " + msg.getId());
            return false;
        }
        Utils.sendCtrlMessageChangedBroadcast(mContext, msg);
        return true;
    }

    /**
     * Get all CtrlMessages from database.
     * @return a List that contain CtrlMessages (may be empty), null if error
     */
    public List<CtrlMessage> getAllCtrlMessages() {
        if (!checkStatus()) {
            return null;
        }
        List<CtrlMessage> ctrlMsgs = new ArrayList<CtrlMessage>();
        Cursor cursor = mDB.query(TABLE_NAME_CTRL_MSG, null, null, null, null, null, "_id");
        if (null == cursor) {
            return null;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return ctrlMsgs;
        }
        while(cursor.moveToNext()) {
            CtrlMessage msg = new CtrlMessage();
            msg.setId(cursor.getLong(0));
            msg.setType(cursor.getInt(1));
            msg.setFromUserId(cursor.getLong(2));
            msg.setToUserId(cursor.getLong(3));
            msg.setDataId(cursor.getLong(4));
            ctrlMsgs.add(msg);
        }
        cursor.close();
        return ctrlMsgs;
    }

    /**
     * Get all CtrlMessages with the given type.
     * @param type should be one the CtrlMessage type
     * @return a list that contains CtrlMessages(might be empty), null if error
     */
    public List<CtrlMessage> getCtrlMessagesByType(int type) {
        if (!checkStatus()) {
            return null;
        }
        List<CtrlMessage> ctrlMsgs = new ArrayList<CtrlMessage>();
        Cursor cursor = mDB.query(TABLE_NAME_CTRL_MSG, null, CtrlMessage.FIELD_NAME_TYPE + "=" + type, null, null, null, "_id");
        if (null == cursor) {
            return null;
        }
        if (cursor.getCount() <= 0) {
            cursor.close();
            return ctrlMsgs;
        }
        while(cursor.moveToNext()) {
            CtrlMessage msg = new CtrlMessage();
            msg.setId(cursor.getLong(0));
            msg.setType(cursor.getInt(1));
            msg.setFromUserId(cursor.getLong(2));
            msg.setToUserId(cursor.getLong(3));
            msg.setDataId(cursor.getLong(4));
            ctrlMsgs.add(msg);
        }
        cursor.close();
        return ctrlMsgs;
    }

    /** Create a ContentValues from a given CtrlMessage */
    private ContentValues createContentValues(CtrlMessage msg) {
        ContentValues values = new ContentValues();
        values.put(CtrlMessage.FIELD_NAME_TYPE, msg.getType());
        values.put(CtrlMessage.FIELD_NAME_FROM_USER_ID, msg.getFromUserId());
        values.put(CtrlMessage.FIELD_NAME_TO_USER_ID, msg.getToUserId());
        values.put(CtrlMessage.FIELD_NAME_DATA_ID, msg.getDataId());
        return values;
    }

    /** Create ctrl_msg table */
    private boolean createCtrlMsgTable() {
        if (!checkStatus()) {
            return false;
        }

        try {
            mDB.execSQL(SQL_CREATE_CTRL_MSG_TABLE);
        } catch (SQLException e) {
            Log.e(TAG, Common.getLastTrace() + ": wrong SQL string: " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Check the status of the SQLiteDatabase instance
     * @return true if it is instantiated, false if not
     */
    private boolean checkStatus() {
        if (null == mDB) {
            Log.e(TAG, Common.getLastTrace() + ": SQLiteDatabase has not been created.");
            return false;
        }
        return true;
    }
    
    /** Do some clearing. */
    public void close() {
        if (null != mDB) {
            mDB.close();
            mDB = null;
        }
    }
}
