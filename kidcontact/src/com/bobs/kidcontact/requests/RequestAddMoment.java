package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Moment;
import com.bobs.kidcontact.model.Tag;

import java.util.ArrayList;

/**
 * Created by hzheng3 on 14-1-6.
 */

/**
 * The moment is submitted with multipart-form request which contain following parts
 * MUST: "info":"xxxxxx"
 * OPTIONAL: "tags":"["","",""]"
 * OPTIONAL: picture related
 */
public final class RequestAddMoment extends RequestAddMomentRecord<Moment> {
    public RequestAddMoment(String momentInfo, ArrayList<Tag> tags,
                            ArrayList<PicPair> picPairs,
                            Response.Listener<Moment> listener, Response.ErrorListener errorListener) {
        super(getAddMomentURL(), momentInfo, tags,
                picPairs, Moment.class, listener, errorListener);
    }

    private static String getAddMomentURL() {
        return Constants.getFunctionURL(Constants.FUNC_ADDMOMENT);
    }
}
