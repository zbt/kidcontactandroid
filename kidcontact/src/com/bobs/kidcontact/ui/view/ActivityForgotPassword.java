package com.bobs.kidcontact.ui.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.requests.RequestResetPassword;
import com.bobs.kidcontact.utils.DebugLog;
import com.bobs.kidcontact.utils.RequestUtil;

/**
 * Created by leon on 14-4-13.
 */
public class ActivityForgotPassword extends Activity {
    private EditText mNumber;
    private Button mReset;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_forgot_password);
        mNumber = (EditText) findViewById(R.id.et_phone_number);
        mReset = (Button) findViewById(R.id.reset_password);
        mReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = getNumber();
                if (number != null) {
                    forgotPassword(number);
                }
            }
        });
    }

    private String getNumber() {
        if (mNumber.getText() == null || mNumber.getText().toString().trim().isEmpty()) {
            Toast.makeText(mContext, getString(R.string.please_input_phone_number),Toast.LENGTH_LONG).show();
            return null;
        } else {
            return  mNumber.getText().toString().trim();
        }
    }

    private void forgotPassword(final String number) {
        RequestUtil forgotPasswordReq = new RequestUtil() {
            @Override
            public Request onPrepareRequest(Response.Listener listener, Response.ErrorListener errListener) {
                return new RequestResetPassword(number, listener, errListener);
            }

            @Override
            public void onSuccess(Object response) {
                DebugLog.d("Reset password succeed");
                onBackPressed();
            }

            @Override
            public void onError(VolleyError e) {
                DebugLog.d("Reset password failed");
                Toast.makeText(mContext, getString(R.string.please_input_the_right_password),Toast.LENGTH_LONG).show();
            }
        };
        forgotPasswordReq.add2queue();
    }


}
