package com.bobs.kidcontact.chat.unifiedmodel;

import java.security.InvalidParameterException;
import java.util.Date;

import com.bobs.kidcontact.chat.database.ChatDB;
import com.bobs.kidcontact.chat.service.Utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hzheng3 on 14-2-18.
 */
public class ChatMessage implements Parcelable {

    public static final String FIELD_NAME_ID = "_id";
    public static final String FIELD_NAME_INFO = "info";
    public static final String FIELD_NAME_IS_INCOMING = "is_incoming";
    public static final String FIELD_NAME_USER_ID = "user_id";
    public static final String FIELD_NAME_TYPE = "type";
    public static final String FIELD_NAME_MSG_DATE = "msg_date";
    public static final String FIELD_NAME_STATUS = "status";
    public static final String FIELD_NAME_CONVERSATION_NAME = "conversation_name";
    private static final String DATE_FORMAT_STRING = "yy-MM-dd HH:mm";

    private long mId = ChatDB.EMPTY_DB_ID;

    // cannot be null or empty
    private String mInfo = "";

    // whether or not this message is from other people
    private boolean mIsInComing = true;

    // cannot be empty, indicates who sent this message, it is kid contact user id, must use API to map to cloud user id
    private long mUserId = 0L;

    public static final int MSG_TYPE_TEXT = 1;
    public static final int MSG_TYPE_PIC = 2;
    public static final int MSG_TYPE_AUDIO = 3;
    // indicates the type of message, must be 1 or 2 or 3
    private int mType = 0;

    // date of the message, SimpleDateFormat is like "yy-MM-dd HH:mm"
    private String mMsgDate = "";

    public static final int MSG_STATUS_SENDING = 0;
    public static final int MSG_STATUS_SENT = 1;
    public static final int MSG_STATUS_FAILED = 2;
    private int mStatus = MSG_STATUS_SENT;

    // The name of the conversation table (that this messages belongs to) in conversations table
    // Together with mId of this message, could uniquely identify a message

    private String mConversationName = "";

    public long getId() {
        return mId;
    }

    public String getInfo() {
        return mInfo;
    }

    public boolean getIsInComing() {
        return mIsInComing;
    }

    public long getUserId() {
        return mUserId;
    }

    public int getType() {
        return mType;
    }

    public String getMsgDate() {
        return mMsgDate;
    }

    public int getStatus() {
        return mStatus;
    }

    public String getConversationName() {
        return mConversationName;
    }

    public void setId(long id) {
        mId = id;
    }

    /**
     * @param info can't be null or empty
     * @throws InvalidParameterException
     */
    public void setInfo(String info) throws InvalidParameterException {
        if (null == info || info.equals("")) {
            throw new InvalidParameterException("info can't be null or empty!");
        }
        mInfo = info;
    }

    public void setIsInComing(boolean isInComing) {
        mIsInComing = isInComing;
    }

    public void setUserId(long id) {
        mUserId = id;
    }

    public void setType(int type) {
        mType = type;
    }

    public void setMsgDate(String date) throws InvalidParameterException {
        if (null == date || date.equals("")) {
            throw new InvalidParameterException("date can't be null or empty!");
        }
        mMsgDate = date;
    }

    public void setStatus(int status) {
        mStatus = status;
    }

    public void setConversationName(String name) throws InvalidParameterException {
        if (null == name || name.equals("")) {
            throw new InvalidParameterException("name can't be null or empty");
        }
        mConversationName = name;
    }

    /** Check if this ChatMessage is valid. */
    public boolean isValid() {
        if (null == mInfo || mInfo.equals("") || null == mMsgDate || mMsgDate.equals("")) {
            return false;
        }
        return true;
    }

    public static Date stringToDate(String dateString) {
        return Utils.stringToDate(dateString, DATE_FORMAT_STRING);
    }

    public static String dateToString(Date date) {
        return Utils.dateToString(date, DATE_FORMAT_STRING);
    }


    // Below TODO
    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mInfo);
        if (mIsInComing) {
            dest.writeInt(1);
        } else {
            dest.writeInt(0);
        }
        dest.writeLong(mUserId);
        dest.writeInt(mType);
        dest.writeString(mMsgDate);
        dest.writeInt(mStatus);
        dest.writeString(mConversationName);
    }

    public ChatMessage() {
    }

    public ChatMessage(Parcel in){
        mId = in.readLong();
        mInfo = in.readString();
        int isInComing = in.readInt();
        if (1 == isInComing) {
            mIsInComing = true; 
        } else {
            mIsInComing = false;
        }
        mUserId = in.readLong();
        mType = in.readInt();
        mMsgDate = in.readString();
        mStatus = in.readInt();
        mConversationName = in.readString();
    }
    public static final Parcelable.Creator<ChatMessage> CREATOR = new Parcelable.Creator<ChatMessage>() {

        public ChatMessage createFromParcel(Parcel in) {
            return new ChatMessage(in);
        }

        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };
}
