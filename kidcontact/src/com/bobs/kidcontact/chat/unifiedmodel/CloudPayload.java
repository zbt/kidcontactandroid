package com.bobs.kidcontact.chat.unifiedmodel;

import java.util.ArrayList;
import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;
import com.bobs.kidcontact.ui.KidContactApp;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-19, maintained by zhanglong
 */
public class CloudPayload implements Parcelable {
    // @SerializedName("msgInfo")
    @SerializedName("a")
    public String mMsgInfo;

    // Message type, aligns with definition in ChatMessage.java
    // @SerializedName("msgType")
    @SerializedName("b")
    public Integer mMsgType;

    // Who send this message; it is kid contact user id
    // @SerializedName("fromUserId")
    @SerializedName("c")
    public Long mFromUserId;

    // This message is sent to whom; if it is group chat, can be null;
    // @SerializedName("toUserId")
    @SerializedName("d")
    public Long mToUserId;

    // Group chat related below
    // @SerializedName("groupDisplayName")
    @SerializedName("e")
    public String mGroupDisplayName;

    // @SerializedName("groupId")
    @SerializedName("f")
    public String mGroupId; // If this message is from group chat, this must be set

    // @SerializedName("cloudMsgType")
    @SerializedName("g")
    public int mCloudMsgType = CLOUD_TYPE_INVALID; // This is the first parameter to be checked
    public static final int CLOUD_TYPE_INVALID = -1; // Indicates no meaning here
    public static final int CLOUD_TYPE_GROUP_OP_NONE = 0; // Indicates this msg is single chat msg
    public static final int CLOUD_TYPE_GROUP_OP_NEW = 1; // This kind of message is not associated with tag
    public static final int CLOUD_TYPE_GROUP_OP_ADD = 2; //
    public static final int CLOUD_TYPE_GROUP_OP_LEAVE = 3; // This kind of message should associated with tag
    public static final int CLOUD_TYPE_GROUP_OP_MESSAGE = 4; // This kind of message should associated with tag
    public static final int CLOUD_TYPE_CTRL = 5; // Indicates this is an control message here

    // @SerializedName("ctrlMsg")
    @SerializedName("h")
    public CtrlMessage mCtrlMsg;
    public CtrlMessage toCtrlMsg() {
        return mCtrlMsg;
    }

    /**
     * For kid contact, GROUP_OP_NEW GROUP_OP_ADD GROUP_OP_LEAVE will not be used, since
     * no one could initiated new group chat or join/leave group chat,
     * everybody is associated with class chat, every device should bind to specific group
     * after cloud messaging is connected
     */

    // @SerializedName("attendees")
    @SerializedName("i")
    public ArrayList<Long> mAttendees = new ArrayList<Long>();

    public CloudPayload() {
    }
    public boolean isChatMessage() {
        return (isGroupChatMessage() || isSingleChatMessage());
    }

    public boolean isCtrlMessage() {
        return CLOUD_TYPE_CTRL == mCloudMsgType;
    }

    public boolean isGroupChatMessage() {
        return mCloudMsgType == CLOUD_TYPE_GROUP_OP_MESSAGE;
    }
    public boolean isSingleChatMessage() {
        return mCloudMsgType == CLOUD_TYPE_GROUP_OP_NONE;
    }

    /** Check if this message is sent by myself */
    public boolean isSentByMyself() {
        if (null == mFromUserId) {
            return false;
        }
        return KidContactApp.getSharePrefUtil().getUserID() == mFromUserId;
    }

    private boolean isInComingMessage() {
        return !(mFromUserId == KidContactApp.getSharePrefUtil().getUserID());
    }

    public String getConversationName() {
        if (isGroupChatMessage()) {
            return mGroupId;
        } else if (isSingleChatMessage()) {
            if (isInComingMessage()) {
                return ""+mFromUserId;
            } else {
                return ""+mToUserId;
            }
        } else {
            return null;
        }
    }

    public ChatMessage newChatMessage() {
        if (!isChatMessage()) {
            return null;
        }

        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setInfo(mMsgInfo);
        chatMessage.setType(mMsgType);

        if (isInComingMessage()) {
            chatMessage.setIsInComing(true);
            chatMessage.setStatus(ChatMessage.MSG_STATUS_SENT);
        } else {
            chatMessage.setIsInComing(false);
            chatMessage.setStatus(ChatMessage.MSG_STATUS_SENDING);
        }
        chatMessage.setUserId(mFromUserId);

        Date date = new Date();
        chatMessage.setMsgDate(ChatMessage.dateToString(date));
        chatMessage.setConversationName(getConversationName());
        return chatMessage;
    }

    public Conversation newConversation() {
        if (!isChatMessage()) {
            return null;
        }

        Conversation conversation = new Conversation();
        Date date = new Date();
        conversation.setLastMsgDate(Conversation.dateToString(date));
        if (isGroupChatMessage()) {
            conversation.setGroupId(mGroupId);
            conversation.setUserId(-1L);
            conversation.setGroupChat(true);
            conversation.setAttendees(new Gson().toJson(mAttendees));
            conversation.setDisplayName(mGroupDisplayName);
        } else {
            if (isInComingMessage()) {
                conversation.setUserId(mFromUserId);
            } else {
                conversation.setUserId(mToUserId);
            }
        }
        conversation.setTableName(getConversationName());
        return conversation;
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mMsgInfo);
        dest.writeInt(mMsgType);
        dest.writeLong(mFromUserId);
        dest.writeLong(mToUserId);
        dest.writeString(mGroupDisplayName);
        dest.writeString(mGroupId);
        dest.writeInt(mCloudMsgType);
        dest.writeParcelable(mCtrlMsg, flags);
        dest.writeList(mAttendees);
    }

    private CloudPayload(Parcel in) {
        this.mMsgInfo = in.readString();
        this.mMsgType = in.readInt();
        this.mFromUserId = in.readLong();
        this.mToUserId = in.readLong();
        this.mGroupDisplayName = in.readString();
        this.mGroupId = in.readString();
        this.mCloudMsgType = in.readInt();
        this.mCtrlMsg = in.readParcelable(CtrlMessage.class.getClassLoader());
        in.readList(mAttendees, Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<CloudPayload> CREATOR = new Parcelable.Creator<CloudPayload>() {

        @Override
        public CloudPayload createFromParcel(Parcel source) {
            return new CloudPayload(source);
        }

        @Override
        public CloudPayload[] newArray(int size) {
            return new CloudPayload[size];
        }
    };
}
