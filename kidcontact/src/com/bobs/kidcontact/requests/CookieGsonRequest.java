package com.bobs.kidcontact.requests;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.L;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hzheng3, base class for almost all requests
 */
public class CookieGsonRequest<T> extends Request<T> {
    private static final String TAG = "CookieGsonRequest";
    /**
     * Gson parser
     */
    protected final Gson mGson;

    /**
     * Class type for the response
     */
    private final Class<T> mClass;
    //MultipartEntity mE;
    /**
     * Callback for response delivery
     */
    private final Listener<T> mListener;

    public CookieGsonRequest(int method, String url, Class<T> objectClass,
                             Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        mGson = new Gson();
        mClass = objectClass;
        mListener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }

        KidContactApp.getInstance().addSessionCookie(headers);
        return headers;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            L.i(TAG, "Response is " + json + " Http status code is " + response.statusCode);
            if (response.statusCode != HttpStatus.SC_OK) {
                // may be tuned here
                KidContactError airportError = new KidContactError(response.statusCode, "Invalid HTTP Status " + response.statusCode);
                return Response.error(airportError);
            }
            // empty payload, together with status ok, indicates succeed
            if (response.statusCode == HttpStatus.SC_OK && (json == null || json.isEmpty())) {
                Succeed ret = new Succeed();
                // assert T must be Succeed.class when this happens
                assert (mClass.isInstance(ret));
                return (Response<T>) Response.success(ret,
                        getCacheEntry());
            }

            JSONObject object;
            try {
                object = new JSONObject(json);
            } catch (JSONException e) {
                L.e(TAG, "Invalid JSON String from server " + json + " " + e.getMessage());
                return Response.error(new ParseError(e));
            }

            try {
                // Let's first check if there's errorcode
                int errorCode = object.getInt(Constants.KEY_ERRORCODE);
                String errorMsg = object.getString(Constants.KEY_DESCRIPTION);
                L.e(TAG, "Operation error " + errorCode + " " + errorMsg);
                return Response.error(new KidContactError(errorCode, errorMsg));
            } catch (JSONException e) {
                // OK, since we can not get errorCode and errorMsg, we are good
                return Response.success(mGson.fromJson(json, mClass),
                        getCacheEntry());
            }
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    /**
     * Some common classes defined below
     */
    public static class Succeed {
    }

    /**
     * Empty JSON input
     */
    public static class EmptyInput {
    }

}
