package com.bobs.kidcontact.chat.service;

import java.util.HashMap;

/**
 * Created by hzheng3 on 14-2-19.
 */
public class BaiduInfoHolder {
    private static long sMyUserId;
    private static String sCloudId;
    private static String sGroupTag;
    private HashMap<Long, String> mIDMap = new HashMap<Long, String>();

    // my user id must be set when app connected to server
    public static void setMyUserId(long userId) {
        sMyUserId = userId;
    }

    public static long getMyUserId() {
        return sMyUserId;
    }

    public static void setCloudId(String cloudId) {
        sCloudId = cloudId;
    }

    public static String getCloudId() {
        return sCloudId;
    }

    public static void setGroupTag(String tag) {
        sGroupTag = tag;
    }

    public static String getGroupTag() {
        return sGroupTag;
    }

    private BaiduInfoHolder() {

    }

    public static BaiduInfoHolder getInstance() {
        if (sInstance == null) {
            sInstance = new BaiduInfoHolder();
        }
        return sInstance;
    }
    private static BaiduInfoHolder sInstance;

    synchronized public void setCloudId(long userId, String cloudId) {
        mIDMap.put(userId, cloudId);
    }
    synchronized public String userIdToCloudId(long userId) {
        return mIDMap.get(userId);
    }
}
