package com.bobs.kidcontact.ui.teacher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.model.Child;
import com.bobs.kidcontact.model.ChildList;
import com.bobs.kidcontact.model.PicProxy;
import com.bobs.kidcontact.requests.RequestGetChildren;
import com.bobs.kidcontact.ui.ActionBarActivityBase;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.RequestUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by junyongl on 14-1-21.
 */
public class ActivityChildSelector extends ActionBarActivityBase {
    private static final String TAG = "ActivityChildSelector";
    public ArrayList<Child> mSelectedChildren = new ArrayList<Child>();
    public ArrayList<Child> mChildren = new ArrayList<Child>();
    private Context mContext;
    private ClassMemberAdapter mAdapter;
    private int gridWidth;
    private ImageLoader mImageLoader;
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.fragment_teacher_member);
        mIntent = getIntent();
        mSelectedChildren = new Gson().fromJson(mIntent.getStringExtra("selected_children"),
                ChildList.class).mChildren;
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        GridView gridView = (GridView) findViewById(R.id.grid_member);

        final float widthpixels = mContext.getResources().getDisplayMetrics().widthPixels;
        final float scale = mContext.getResources().getDisplayMetrics().density;
        gridWidth = (int) ((widthpixels - 32* scale)/3 -8);
        mImageLoader = KidContactApp.getInstance().getImageLoader();

        mAdapter = new ClassMemberAdapter();
        gridView.setAdapter(mAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                ClassMemberAdapter.ViewHolder vh = (ClassMemberAdapter.ViewHolder) view.getTag();
                if (!vh.mCheck.isChecked()) {
//                    mSelectedChildren.add(mChildren.get(position));
                    mChildren.get(position).isSelected = true;
                    Log.d(TAG, "Select item" + mChildren.get(position).toString());
                } else {
//                    mSelectedChildren.remove(mChildren.get(position));
                    mChildren.get(position).isSelected = false;
                    Log.d(TAG, "Deselect item" + mChildren.get(position).toString());
                }
                vh.mCheck.setChecked(!vh.mCheck.isChecked());
            }
        });
        RequestUtil<ChildList> request = new RequestUtil<ChildList>() {
            @Override
            public Request<ChildList> onPrepareRequest(Response.Listener<ChildList> listener, Response.ErrorListener errListener) {
                return new RequestGetChildren(listener, errListener);
            }

            @Override
            public void onSuccess(ChildList response) {
                if (response.mChildren.size() == 0) {
                    return;
                }
                mChildren.clear();
                mChildren.addAll(response.getChildren());
                for (Child c : mSelectedChildren) {
                    updateSelectedChildren(c);
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(VolleyError e) {
                super.onError(e);
                Log.e(TAG, "" + RequestUtil.interpretErr2Res(e));
            }
        };
        request.add2queue();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_child_selector, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.ok:
                Log.d(TAG, "Selected children number: " + mSelectedChildren.size());
                mSelectedChildren.clear();
                for (Child c : mChildren) {
                    if (c.isSelected) {
                        mSelectedChildren.add(c);
                    }
                }
                ChildList childList = new ChildList();
                childList.mChildren = mSelectedChildren;
                String data = new Gson().toJson(childList, ChildList.class);
                mIntent.putExtra("selected_children", data);
                Log.d(TAG, "Selected children:" + data);
                setResult(RESULT_OK, mIntent);
                finish();
                return true;
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSelectedChildren(Child child) {
        for (Child c : mChildren) {
            if (c.mId == child.mId) {
                c.isSelected = true;
            }
        }
    }



    private class ClassMemberAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mChildren.size();
        }

        @Override
        public Object getItem(int i) {
            return mChildren.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder = new ViewHolder();

            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_child_selector, viewGroup, false);
                viewHolder.mAvatar = (NetworkRoundImageView) convertView.findViewById(R.id.member_avatar);
                viewHolder.mAvatar.setLayoutParams(new RelativeLayout.LayoutParams(gridWidth, gridWidth));
                viewHolder.mAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
                viewHolder.mAvatar.setPadding(8, 8, 8, 8);
                viewHolder.mName = (TextView) convertView.findViewById(R.id.member_name);
                viewHolder.mCheck = (CheckBox) convertView.findViewById(R.id.member_check);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.mAvatar.setDefaultImageResId(R.drawable.ic_default_image);
            ArrayList<PicProxy> childrenPic = ((Child)getItem(position)).mPics;
            if (childrenPic.size() != 0) {
                if (childrenPic.get(0).mPicture.picSize != 0) {
                    viewHolder.mAvatar.setImageUrl(childrenPic.get(0).mPicture.getPicURL(), mImageLoader);
                } else if (childrenPic.get(0).mPicture.mThumbSize != 0) {
                    viewHolder.mAvatar.setImageUrl(childrenPic.get(0).mPicture.getPicThumbURL(), mImageLoader);
                }
            } else {
                viewHolder.mAvatar.setImageResource(R.drawable.ic_avatar);
            }
            if (((Child) getItem(position)).mName != null) {
                viewHolder.mName.setText(((Child) getItem(position)).mName);
            }
            viewHolder.mCheck.setChecked(((Child) getItem(position)).isSelected);
            return convertView;
        }

        private class ViewHolder {
            public NetworkRoundImageView mAvatar;
            public TextView mName;
            public CheckBox mCheck;
        }
    }



    @Override
    public Handler getViewEventHandler() {
        // TODO Auto-generated method stub
        return null;
    }


}
