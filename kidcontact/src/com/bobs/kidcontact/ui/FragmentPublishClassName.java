package com.bobs.kidcontact.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.ui.ActivityPublish.IPublish;
import com.bobs.kidcontact.ui.view.FragmentPictureSelector;

public class FragmentPublishClassName extends Fragment implements IPublish {

	private Context mContext;
	private int mPublishType;
	private EditText mContent;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        Bundle bundle = getArguments();
        mPublishType = bundle.getInt("publish_type");
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_publish_class_name, container, false);
        mContent = (EditText) view.findViewById(R.id.new_class_name);
        //Todo Set Original class name

        return view;
    }
    
	@Override
	public void publish() {
		// TODO Auto-generated method stub

	}

}
