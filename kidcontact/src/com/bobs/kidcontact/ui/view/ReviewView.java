package com.bobs.kidcontact.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bobs.kidcontact.R;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Comment;
import com.bobs.kidcontact.ui.KidContactApp;
import com.bobs.kidcontact.utils.NetworkRoundImageView;
import com.bobs.kidcontact.utils.UIUtil;

/**
 * Created by junyongl on 13-12-27.
 */
public class ReviewView extends LinearLayout {
    private static final String TAG = "ReviewView";
    private Context mContext;
    private LayoutInflater mInflater;
    private LinearLayout mViewContainer;
    private TextView mAuthor;
    private TextView mCreatedTime;
    private NetworkRoundImageView mAvatar;
    private TextView mContent;
    private FrameLayout mPicturesContainer;
    private TextView mAddFavor;
    private ImageView mAddComment;
    private RatingBar mLearningRating;
    private RatingBar mEatingRating;
    private RatingBar mSleepingRating;
//    private MomentPicturesView mImageViews;

    public ReviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewContainer = (LinearLayout) mInflater.inflate(R.layout.view_review, null);
        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mAuthor = (TextView) mViewContainer.findViewById(R.id.moment_author);
        mCreatedTime = (TextView) mViewContainer.findViewById(R.id.moment_created_time);
        mAvatar = (NetworkRoundImageView) mViewContainer.findViewById(R.id.user_avatar);
        mContent = (TextView) mViewContainer.findViewById(R.id.moment_content);

        mAddComment = (ImageView) mViewContainer.findViewById(R.id.add_comment);

        mLearningRating = (RatingBar) mViewContainer.findViewById(R.id.rating_learning);
        mLearningRating.setIsIndicator(true);
        mEatingRating = (RatingBar) mViewContainer.findViewById(R.id.rating_eating);
        mEatingRating.setIsIndicator(true);
        mSleepingRating = (RatingBar) mViewContainer.findViewById(R.id.rating_sleeping);
        mSleepingRating.setIsIndicator(true);

        addView(mViewContainer, params);

    }

    public void bind(Comment comment){
        if (comment.mCreatorName != null) {
            mAuthor.setText(comment.mCreatorName);
        }
        if (comment.mInfo != null) {
            mContent.setText(comment.mInfo);
        }
        mAvatar.setImageUrl(Constants.getAvatarThumbURLOfCreator(comment.mCreatorId),
                KidContactApp.getInstance().getImageLoader());
        mLearningRating.setRating((float)comment.mClassStar / 2);
        mEatingRating.setRating((float)comment.mEatStar / 2);
        mSleepingRating.setRating((float)comment.mEatStar / 2);
        String dateFormat = mContext.getString(R.string.moment_created_time_string_fromat);
        mCreatedTime.setText(UIUtil.dateToString(comment.getCreatedDate(), dateFormat));
    }

}
