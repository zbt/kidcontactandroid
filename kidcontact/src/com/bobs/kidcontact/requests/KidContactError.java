package com.bobs.kidcontact.requests;

import com.android.volley.VolleyError;

/**
 * Created by hzheng3
 */
@SuppressWarnings("serial")
public final class KidContactError extends VolleyError {
    public final int mErrorCode;
    public final String mErrorMsg;

    public KidContactError(int errorCode, String errorMsg) {
        super(errorMsg);
        mErrorCode = errorCode;
        mErrorMsg = errorMsg;
    }
}
