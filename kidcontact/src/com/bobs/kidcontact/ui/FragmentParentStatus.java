package com.bobs.kidcontact.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.bobs.kidcontact.R;
import com.bobs.kidcontact.chat.unifiedmodel.CtrlMessage;
import com.bobs.kidcontact.chat.service.IChatService;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.contants.Events;
import com.bobs.kidcontact.model.Notice;
import com.bobs.kidcontact.model.PublicNotice;
import com.bobs.kidcontact.requests.RequestGetNotices;
import com.bobs.kidcontact.requests.RequestGetNotices.Notices;
import com.bobs.kidcontact.ui.view.XListView;
import com.bobs.kidcontact.utils.DebugLog;
import com.bobs.kidcontact.utils.RequestUtil;
import com.bobs.kidcontact.utils.UIUtil;
import com.google.gson.Gson;

import de.greenrobot.event.EventBus;

/**
 * Created by junyongl on 13-12-25. zhanglong
 */
public class FragmentParentStatus extends FragmentBase implements XListView.IXListViewListener {
    private static final String TAG = "FragmentParentStatus";
    private static final long REQEST_TIMEOUT = 5000;
    private Context mContext;
    private final ArrayList<Notice> mNotices = new ArrayList<Notice>();
    private NoticeAdapter mAdapter;
    private int mLastId = 0;
    private RefreshStatus mReq;
    private XListView mListView;
    private TextView mTextNoticeNewNotify;
    private View mStatusHeader;
    private IChatService.Stub mChatServiceStub;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        EventBus.getDefault().register(this);
        getChatServiceStubAsync();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_parent_status, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView = (XListView) view.findViewById(R.id.list_notice);
        mTextNoticeNewNotify = (TextView) view.findViewById(R.id.text_notice_new_notify);
        mStatusHeader = LayoutInflater.from(mContext).inflate(R.layout.item_notice, null);
        // mListView.addHeaderView(mStatusHeader);
        mAdapter = new NoticeAdapter();
        mListView.setAdapter(mAdapter);
        mListView.setDivider(mContext.getResources().getDrawable(R.drawable.divider));
        mListView.setXListViewListener(this);
        mListView.setPullLoadEnable(true);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(mContext, ActivityNoticeDetails.class);
                Gson gson = new Gson();
                String noticeString = gson.toJson(mNotices.get(i - 1), Notice.class); //FIXME Why the position start from 1 ?!
                intent.putExtra("data-notice", noticeString);
                startActivity(intent);
            }
        });
        if (mLastId == 0) {
            onRefresh();
        }
    }

    @Override
    public void onRefresh() {
        requestRefresh();
    }

    @Override
    public void onLoadMore() {
        requestLoadMore();
    }

    @Override
    public void onResume() {
        super.onResume();
        mListView.setRefreshTime(KidContactApp.getSharePrefUtil().getLastRefreshStatusTime());
    }

    /** Get the IChatService.Stub asynchronously */
    private void getChatServiceStubAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mChatServiceStub = ((ActionBarActivityBase) FragmentParentStatus.this.getActivity()).getChatServiceStubWait(1000);
                if (null == mChatServiceStub) {
                    Log.w(TAG, "failed to get chat service stub!");
                } else {
                    Log.i(TAG, "bound to local service successfully.");
                }
            }
        }).start();
    }

    private class NoticeAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mNotices.size();
        }

        @Override
        public Object getItem(int position) {
            return mNotices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.item_notice, parent, false);
                vh = new ViewHolder();
                vh.viewNoticeTitle = (TextView) convertView.findViewById(R.id.notice_title);
                vh.viewNoticeContent = (TextView) convertView.findViewById(R.id.notice_content);
                vh.viewNoticeCreatedDate = (TextView) convertView.findViewById(R.id.notice_created_date);
                vh.viewNoticeType = (TextView) convertView.findViewById(R.id.notice_type);
//                vh.viewDueDateLayout = (LinearLayout) convertView.findViewById(R.id.notice_due_date_layout);
                vh.viewDueDate = (TextView) convertView.findViewById(R.id.notice_due_date);
                convertView.setTag(vh);
            } else {
                vh = (ViewHolder) convertView.getTag();
            }
            Notice notice = (Notice)getItem(position);
            if (notice.mTitle != null) {
                vh.viewNoticeTitle.setText(notice.mTitle);
            }
            if (notice.mContent != null) {
                vh.viewNoticeContent.setText(notice.mContent);
            }
            if (notice.mCreatedDate != null) {
                vh.viewNoticeCreatedDate.setText(notice.mCreatedDate);
            }
            if (notice.mType != null) {
                vh.viewNoticeType.setText(notice.mType);
            }

            //update type background color
            if (notice.mPublicNotice != null) {
                vh.viewNoticeType.setBackgroundColor(mContext.getResources().getColor(R.color.type_public_notice));
            } else if (notice.mAckNotice != null) {
                vh.viewNoticeType.setBackgroundColor(mContext.getResources().getColor(R.color.type_ack_notice));
            } else if (notice.mVote != null) {
                vh.viewNoticeType.setBackgroundColor(mContext.getResources().getColor(R.color.type_vote));
            } else if (notice.mAssignment != null) {
                vh.viewNoticeType.setBackgroundColor(mContext.getResources().getColor(R.color.type_assignment));
            }
            return convertView;
        }

        private class ViewHolder {
            public TextView viewNoticeTitle;
            public TextView viewNoticeContent;
            public TextView viewNoticeCreatedDate;
            public TextView viewNoticeType;
//            public LinearLayout viewDueDateLayout;
            public TextView viewDueDate;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mReq != null) {
            mReq.cancel();
            mReq = null;
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    protected void refreshViewsAsync() {
        super.refreshViewsAsync();
        new Thread(new Runnable() {

            @Override
            public void run() {
                if (null != ((ActionBarActivityBase) getActivity()).getChatServiceStubWait(1000)) {
                    FragmentParentStatus.this.mEventHandlerForView.sendEmptyMessage(Events.EVENT_CHAT_SERVICE_BOUND);
                } else {
                    Log.w(TAG, "failed to get chat service stub!");
                }
            }

        }).start();
    }

    @Override
    protected void refreshViews() {
        int noticeUpdateCount = 0;
        try {
            if (null != mChatServiceStub) {
                noticeUpdateCount = mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_NOTICE_NEW);
            } else {
                Log.w(TAG, "chat srevice stub is null!");
            }
        } catch (RemoteException e) {
            Log.e(TAG, "failed to get CtrlMessage count with type: CtrlMessage.TYPE_NOTICE_NEW." + e.getMessage());
        }
        if (noticeUpdateCount > 0) {
            mTextNoticeNewNotify.setVisibility(View.VISIBLE);
        } else {
            mTextNoticeNewNotify.setVisibility(View.GONE);
        }
        refreshStatusHeader();
    }

    @Override
    protected void refreshViews(CtrlMessage msg) {
        super.refreshViews(msg);
        // We care about only CtrlMessage.TYPE_NOTICE_NEW and CtrlMessage.TYPE_NOTICE_UPDATE
        if (CtrlMessage.TYPE_NOTICE_NEW == msg.getType()) {
            mTextNoticeNewNotify.setVisibility(View.VISIBLE);
        } else if (CtrlMessage.TYPE_KID_ENTER_KINDERGARDEN == msg.getType() || CtrlMessage.TYPE_KID_LEAVE_KINDERGARDEN == msg.getType()) {
            refreshStatusHeader();
        }
        // TODO CtrlMessage.TYPE_NOTICE_UPDATE
    }

    public void onEvent(UpdateStatusUIEvent event){
        DebugLog.d("on Event trigger");
        requestRefresh();
    }

    private void requestRefresh() {
        Log.d(TAG, "requestRefresh");
        if (mReq != null) {
            if (mLastId != 0) {
                mReq.cancel();
                mReq = null;
            }
        }
        if (mReq == null) {
            mLastId = 0;
            mReq = new RefreshStatus(0);
            mReq.add2queue();
        }
    }

    private void requestLoadMore() {
        Log.d(TAG, "requestLoadMore");
        if (mReq != null) {
            if (mLastId == 0) {
                mReq.cancel();
                mReq = null;
            }
        }
        if (mReq == null) {
            mReq = new RefreshStatus(mLastId);
            mReq.add2queue();
        }
    }

    private class RefreshStatus extends RequestUtil<Notices> {
        private final int mId;

        public RefreshStatus(int id) {
            super(REQEST_TIMEOUT);
            mId = id;
            Log.d(TAG, "Load id = " + mId);
        }

        @Override
        public Request<Notices> onPrepareRequest(Listener<Notices> listener,
                ErrorListener errListener) {
            return new RequestGetNotices(mId, listener, errListener);
        }

        @Override
        public void onSuccess(Notices response) {
            if (mId ==0) {
                mListView.stopRefresh();
                mNotices.clear();
            } else {
                mListView.stopLoadMore();
            }
            List<Notice> list = response.mNotices;
            int cnt = list.size();
            if (cnt > 0) {
                mLastId = (int)list.get(cnt - 1).getId();
                String dateFormat = mContext.getString(R.string.leave_request_time_string_fromat);
                for (int i = 0; i < cnt; i++) {
                    Notice n = list.get(i);
                    n.fillContent();
                    n.mCreatedDate = UIUtil.dateToString(UIUtil.stringToDate(n.mCreatedDate, Constants.DATA_DATE_FORMAT), dateFormat);
                }
                mNotices.addAll(response.mNotices);
                mAdapter.notifyDataSetChanged();
                refreshStatusHeader();
                Log.d(TAG, "Fetched " + cnt + " items");
            } else {
                Toast.makeText(mContext, mContext.getResources().getString(R.string.no_more_data), Toast.LENGTH_LONG).show();
            }

            // Handle database and notification on success
            mTextNoticeNewNotify.setVisibility(View.GONE);
            if (null != mChatServiceStub) {
                try {
                    mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_NOTICE_NEW);
                    mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_NOTICE_UPDATE);
                } catch (RemoteException e) {
                    Log.e(TAG, "failed to delete CtrlMessages by type: CtrlMessage.TYPE_NOTICE_NEW " +
                            "and CtrlMessage.TYPE_NOTICE_UPDATE" + e.getMessage());
                }
            }
            String refreshDate = new Date().toLocaleString();
            mListView.setRefreshTime(refreshDate);
            KidContactApp.getSharePrefUtil().setLastRefreshStatusTime(refreshDate);
            mReq = null;
        }

        @Override
        public void onError(VolleyError e) {
            super.onError(e);
            Log.e(TAG, "" + RequestUtil.interpretErr2Res(e));
            mReq = null;
            mListView.stopLoadMore();
            mListView.stopRefresh();
        }
    }
    
    private void refreshStatusHeader() {
        Notice notice = null;
        try {
            if (null != mChatServiceStub) {
                if (mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_KID_ENTER_KINDERGARDEN) > 0) {
                    PublicNotice pNotice = new PublicNotice();
                    pNotice.mId = -1;
                    pNotice.mCreatorId = -1;
                    pNotice.mCreatorName = "server";
                    pNotice.mDateCreated = UIUtil.dateToString(new Date(), getResources().getString(R.string.notice_created_time_string_fromat));
                    pNotice.mInfo = this.getString(R.string.ctrl_msg_des_kid_enter_kindergarden);
                    pNotice.mLastUpdated = pNotice.mDateCreated;
                    pNotice.mTitle = "孩子在幼儿园";
                    notice = new Notice();
                    notice.mId = -1;
                    notice.mPublicNotice = pNotice;
                    notice.fillContent();
                    KidContactApp.getSharePrefUtil().setStatusHeader(new Gson().toJson(notice));
                } else if (mChatServiceStub.getCtrlMsgCountByType(CtrlMessage.TYPE_KID_LEAVE_KINDERGARDEN) > 0) {
                    PublicNotice pNotice = new PublicNotice();
                    pNotice.mId = -1;
                    pNotice.mCreatorId = -1;
                    pNotice.mCreatorName = "server";
                    pNotice.mDateCreated = UIUtil.dateToString(new Date(), getResources().getString(R.string.notice_created_time_string_fromat));
                    pNotice.mInfo = this.getString(R.string.ctrl_msg_des_kid_leave_kindergarden);
                    pNotice.mLastUpdated = pNotice.mDateCreated;
                    pNotice.mTitle = "孩子离开幼儿园";
                    notice = new Notice();
                    notice.mId = -1;
                    notice.mPublicNotice = pNotice;
                    notice.fillContent();
                    KidContactApp.getSharePrefUtil().setStatusHeader(new Gson().toJson(notice));
                }
                mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_KID_LEAVE_KINDERGARDEN);
                mChatServiceStub.deleteCtrlMsgsByType(CtrlMessage.TYPE_KID_ENTER_KINDERGARDEN);
            } else {
                Log.w(TAG, "chat srevice stub is null!");
            }
            if (null == notice) {
                String headerJson = KidContactApp.getSharePrefUtil().getStatusHeader();
                if (null != headerJson && !headerJson.isEmpty()) {
                    notice = new Gson().fromJson(headerJson, Notice.class);
                }
            }
            if (null != notice) {
                notice.mTitle = notice.mPublicNotice.mTitle;
                notice.mContent = notice.mPublicNotice.mInfo;
                notice.mType = "通知";
                if (mNotices.size() > 0 && mNotices.get(0).mId == -1) {
                    mNotices.remove(0);
                }
                mNotices.add(0, notice);
                mAdapter.notifyDataSetChanged();
            }
        } catch (RemoteException e) {
            Log.e(TAG, "failed to get CtrlMessage count with type: CtrlMessage.TYPE_NOTICE_NEW." + e.getMessage());
        }
    }
}
