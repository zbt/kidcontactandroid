package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 13-12-23.
 */
public final class RequestCheckBoundStatus extends CookieGsonJsonRequest<RequestCheckBoundStatus.BoundStatus> {
    public RequestCheckBoundStatus(EmptyInput input, Response.Listener<BoundStatus> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_CHECKBOUND_STATUS),
                input, BoundStatus.class, listener, errorListener);
    }

    public static class BoundStatus {
        @SerializedName("bounded")
        public boolean mBounded;
        @SerializedName("role")
        public int mRole;
        @SerializedName("id")
        public long mId;
        @SerializedName("classId")
        public long mClassId;
        @SerializedName("name")
        public String mName;
        @SerializedName("teacherId")
        public String mTeacherId;
        @SerializedName("childName")
        public String mChildName;
        @SerializedName("childBirthDay")
        public String mChildBirthDay;

        public boolean isTeacher() {
            return mRole == Constants.TYPE_TEACHER;
        }
        public boolean isParent() {
            return mRole == Constants.TYPE_PARENT;
        }

        public boolean isBounded() {
        	return mBounded;
        }
    }
}
