package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 13-12-23.
 */
public final class RequestAddUser extends CookieGsonJsonRequest<RequestAddUser.Password> {
    public RequestAddUser(AddUser input, Response.Listener<Password> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_ADDUSER),
                input, Password.class, listener, errorListener);
    }

    public static class AddUser {
        @SerializedName("phoneNumber")
        public String mPhoneNumber;
        @SerializedName("regCode")
        public String mRegCode;
    }

    public static class Password {
        @SerializedName("password")
        public String mPassword;
    }
}
