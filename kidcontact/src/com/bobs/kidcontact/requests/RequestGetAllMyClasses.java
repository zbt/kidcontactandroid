package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.Klasses;

/**
 * Created by hzheng3 on 14-6-27.
 */
public final class RequestGetAllMyClasses extends CookieGsonRequest<Klasses> {

    public RequestGetAllMyClasses(Response.Listener<Klasses> listener, Response.ErrorListener errorListener) {
        super(Method.GET, Constants.getFunctionURL(Constants.FUNC_GET_ALL_MY_CLASSES),
                Klasses.class, listener, errorListener);
    }
}