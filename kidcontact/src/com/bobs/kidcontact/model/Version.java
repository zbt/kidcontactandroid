package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * @author zhanglong
 */
public class Version {
    
    @SerializedName("id")
    public long mId;

    @SerializedName("versionId")
    public String mVersionId;

    @SerializedName("versionDescription")
    public String mDescription;

    @SerializedName("versionDate")
    public String mDate;
}
