package com.bobs.kidcontact.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-2-2.
 */
public class IdProxy {
    @SerializedName("id")
    public long mId;
}
