package com.bobs.kidcontact.requests;

import com.android.volley.Response;
import com.bobs.kidcontact.contants.Constants;
import com.bobs.kidcontact.model.SingleCloudMsg;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hzheng3 on 14-05-14.
 */
public final class RequestSendSingleCloudMsg extends CookieGsonJsonRequest<CookieGsonRequest.Succeed> {
    public RequestSendSingleCloudMsg(SingleCloudMsg input, Response.Listener<Succeed> listener, Response.ErrorListener errorListener) {
        super(Constants.getFunctionURL(Constants.FUNC_SEND_SINGLE_CLOUD_MSG),
                input, Succeed.class, listener, errorListener);
    }
}
