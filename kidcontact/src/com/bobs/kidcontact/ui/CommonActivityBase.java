package com.bobs.kidcontact.ui;

import com.bobs.kidcontact.chat.service.ChatService;
import com.bobs.kidcontact.chat.service.IChatService;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * This is the base class for all the common activities in this application.
 * @author zhanglong
 */
public class CommonActivityBase extends Activity {
    public static final String TAG = "CommonActivityBase";
    protected IChatService.Stub mChatServiceStub = null;
    protected ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mChatServiceStub = (IChatService.Stub) iBinder;
            if (!KidContactApp.getSharePrefUtil().getUserName().equals(KidContactApp.getSharePrefUtil().getLastUserName())) {
//                try {
//                    mChatServiceStub.onNotifyNewUser();
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
            }
            if (!clearNotification()) {
                Log.w(TAG, "failed to clear notification!");
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mChatServiceStub = null;
        }
    };
    
    @Override
    protected void onStart() {
        super.onStart();
        if (null == startService(new Intent(this, ChatService.class))) {
            Log.e(TAG, "failed to start chat service!");
        } else {
            if (!bindService(new Intent(this, ChatService.class), mConnection, Context.BIND_AUTO_CREATE)) {
                Log.e(TAG, "failed to bind to chat service!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mChatServiceStub) {
            unbindService(mConnection);
        }
    }

    /** Bind to the chat service if it is not bound */
    protected boolean bindToChatService() {
        if (null == mChatServiceStub) {
            return bindService(new Intent(getApplicationContext(), ChatService.class), mConnection, Context.BIND_AUTO_CREATE);
        }
        return true;
    }

    /** Cancel the existing notification and clear all notification cache */
    protected boolean clearNotification() {
        if (null == mChatServiceStub) {
            return false;
        }
        try {
            mChatServiceStub.cancelNotification();
            mChatServiceStub.clearNotificationCache();
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
